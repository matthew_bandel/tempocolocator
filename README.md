# tempocolocator

TempoColocator is meant for colocating satellite data such as MODIS onto a geolocation grid from TEMPO or GEMS, using scipy's nearest neighbors interpolator.


# Usage

TempoColocator is currently set to run from Sagemaker on AWS, using GEMS or TEMPO data in an S3 bucket called "gems-oceans".
Usage is as follows:

```buildoutcfg
pip install cartopy
pip install netCDF4

import colocators
colocator = colocators.Colocator(sink, year, month, day, configuration='modis.yaml', gems=False, smoothed=True, average=True)
colocator.locate(start=1100, finish=1300, blocks=1, verify=True)
```

sink: directory for dumping files.  A folder called "colocation" will be created in this directory, and the colocated data file will be stored here.
date: python datetime.datetime object, currently set to 2023-11-06, by default
configuration: the path to the modis.yaml configuration file
gems=True: boolean toggle that switches to TEMPO data if false.  Currently there is no TEMPO data available.
smoothed=True: boolean toggle that switches from using raw GEMS files to prepared Smoothed GEMS files
average=True: boolean toggle that switches compositing method to average of all satellites per day

The .locate command performs nearest neighbors interpolation using scipy.iterpolate.interpn.  All GEMS or TEMPO files on the given day will be processed.  However, the start and finish times can be set to only process a subset.  The blocks keyword allows the interpolation to be performed on smaller portions of GEMS data at time to avoid possible memory issues.  If the verify keyword is set to True, a plot will be constructed in a folder called "plots" that shows a comparison between the MODIS data and the MODIS data located to the TEMPO or GEMS grid ( + additional satellite data for fills ).

# General purpose outside Sageaker

The colocator can also work outside of Sagemaker with a general purpose colocate function, as below:

```buildoutcfg
pip install cartopy
pip install netCDF4

import colocators
colocator = colocators.Colocator(studio=False)
colocator.colocate(path, target, sink, field, coordinates=None, coordinatesii=None, verify=True)
```

path: file path of gridded data
target: file path of latitude and longitude coordinates to accept the gridded data
sink: file path of folder in which to store plots and colocation files
field: address to field of interest separated by slashes, included indices for subsetting the array ( for instance 'SCIDATA/GPP/11')
coordinates: the addresses of the latitude and longitude fields in the gridded data, defaults to ('lat', 'lon')
coordinatesii: the addresses of the latitude and longitude fields in the target data, defaults to ('latitude', 'longitude')
verify: if set to True, produces a plot


# Gap filling data

The Aqua Modis data on the same day as the GEMS data is used as the primary data source.  If there are gaps, they are filled by another satellite's data on the same day, using the following hierarchy:

JPSS2_VIIRS, TERRA_MODIS, JPSS1_VIIRS, S3A_OLCI_ERRNT

If there are still gaps, Aqua Modis data on the day before and the day after is checked.  If both days have data, the average is used.  Otherwise, whichever day has data is used.

If there are still gaps, the day before and after are checked for each satellite in the same hierarchy as above.