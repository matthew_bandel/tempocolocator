PRO fpar_nn,bands,gpp,name,fpar_trained, geven, godd, $
  par,par_inc_min, sif=sif, nband=nband, use_band=use_band, $
  cosvza=cosvza, cossza=cossza, cosphase=cosphase, $
  use_angs=use_angs, use_brdf=use_brdf, brdf=brdf, $
  use_sif=use_sif, bband=brdf_band, noband=noband, $
  temp=temp, bands_pred=bands_pred, do_gpp=do_gpp, vpd=vpd, type=type,$
  niter=niter, aqua=aqua, mult_par=mult_par, par_pred=par_pred, $
  use_temp=use_temp, use_type=use_type, use_parinc=use_parinc, $
  use_vpd=use_vpd, parinc=parinc, kop=kop, use_kop=use_kop, $
  model=model, norm1=normalizer1, norm2=normalizer2, dir=dir, nnode=nnode, $
  conv=conv, maxloss=maxloss, do_save=do_save, features_all=features_all

if (n_elements(do_save) eq 0) then do_save=1
if (n_elements(dir) eq 0) then dir=''
if (n_elements(bands) gt 0) then BEGIN 
  n_points=n_elements(bands[0,*]) 
endif else if (n_elements(sif) gt 0) then BEGIN
  n_points=n_elements(sif)
endif
if (n_elements(use_kop) eq 0 or n_elements(kop) eq 0) then use_kop=0
if (n_elements(use_parinc) eq 0 or n_elements(parinc) eq 0) then use_parinc=0
if (n_elements(use_temp) eq 0 or n_elements(temp) eq 0) then use_temp=0
if (n_elements(use_type) eq 0 or n_elements(type) eq 0) then use_type=0
if (n_elements(use_vpd) eq 0 or n_elements(vpd) eq 0) then use_vpd=0
if (n_elements(do_gpp) eq 0) then do_gpp=1
if (n_elements(niter) eq 0) then niter=1000
if (n_elements(par_pred) eq 0) then par_pred=0
if (n_elements(nband) eq 0) then nband=n_elements(bands[*,0])
if (n_elements(use_angs) eq 0) then use_angs=0
if (n_elements(use_brdf) eq 0) then use_brdf=0
if (n_elements(use_sif) eq 0) then use_sif=0
if (n_elements(mult_par) eq 0) then mult_par=1
if (n_elements(noband) eq 0) then noband=0
if (n_elements(brdf_band) eq 0) then brdf_band=indgen(7)

n_input=0
if (use_angs eq 1) then n_input=n_input+3
if (use_angs eq 2) then n_input=n_input+2
if (use_sif) then n_input=n_input+1
if (n_elements(aqua) gt 0) then n_input=n_input+1
;if (use_par) then n_input=n_input+1
if (use_temp) then n_input=n_input+1
if (use_type) then n_input=n_input+1
if (use_vpd) then n_input=n_input+1
if (use_kop) then n_input=n_input+1
if (use_parinc) then n_input=n_input+1
if (n_elements(use_band) gt 0) then BEGIN
  nband=n_elements(use_band)
endif else if nband gt 0 then BEGIN
  use_band=indgen(nband)
endif
if (use_brdf) then BEGIN
  ncoef=n_elements(brdf[0,*,0])
  nband_brdf=n_elements(brdf_band)
  n_input=n_input+nband_brdf*ncoef
endif
if (par_pred) then BEGIN
  n_input=n_input+1
  mult_par=0
endif
ninput=n_input
if (noband eq 0) then BEGIN
  ninput=nband+ninput
endif
;print, n_input, ninput

features_all=fltarr(ninput,n_points)
nind=0
if (noband eq 0) then BEGIN
 for iband=0, nband-1 do BEGIN
  if (mult_par) then BEGIN
    features_all[iband,*]=bands[use_band[iband],*] * par
  endif else BEGIN
    features_all[iband,*]=bands[use_band[iband],*] 
  endelse
 endfor
 nind=nind+nband
endif 
if (use_angs gt 0) then BEGIN
  features_all[nind,*]=cossza
  features_all[nind+1,*]=cosphase
  if (use_angs eq 1) then BEGIN
    features_all[nind+2,*]=cosvza
    nind=nind+3
  endif else BEGIN
    nind=nind+2
  endelse
endif
if (n_elements(aqua) gt 0) then BEGIN
  features_all[nind,*]=aqua
  nind=nind+1
endif
if (use_sif) then BEGIN
  features_all[nind,*]=sif ;/par
  nind=nind+1
endif
if (use_temp) then BEGIN
  features_all[nind,*]=temp
  nind=nind+1
endif
if (use_type) then BEGIN
  features_all[nind,*]=type
  nind=nind+1
endif
if (use_vpd) then BEGIN
  features_all[nind,*]=vpd
  nind=nind+1
endif
if (use_kop) then BEGIN
  features_all[nind,*]=kop
  nind=nind+1
endif
if (par_pred) then BEGIN
  features_all[nind,*]=par
  nind=nind+1
endif
if (use_parinc) then BEGIN
  features_all[nind,*]=parinc
  nind=nind+1
endif
if (use_brdf) then BEGIN
  for iband=0, nband_brdf-1 do BEGIN
   for icoef=0, ncoef-1 do BEGIN
    features_all[nind+icoef+iband*ncoef,*]=brdf[brdf_band[iband],icoef,*]
   endfor
  endfor
endif

;text='' & read,text

geven2=findgen(n_elements(geven)) & ngeven2=n_points

if (ngeven2 gt 0) then BEGIN
geven3=geven[geven2]

features=features_all[*,geven3]
if (n_elements(nnode) eq 0) then BEGIN
  nnode=nband*2+2*(n_input);-2
  if (nband*2+2*(n_input) gt 30) then nnode=nband*1+1*(n_input)
endif
data=features
noutput=0

if (do_gpp) then noutput=1
pred_refl=n_elements(bands_pred) gt 0
if (pred_refl) then BEGIN
 nband_out=n_elements(bands_pred[*,0])
 noutput=noutput+nband_out
endif
scores=fltarr(noutput,n_elements(geven3))
out_ind=0
if (do_gpp) then BEGIN
 scores[out_ind,*]=gpp[geven3] 
 out_ind=out_ind+1
endif
if (pred_refl) then BEGIN
 scores[out_ind:out_ind+nband_out-1,*]=bands_pred[0:nband_out-1,geven3]
endif
n_inputs=n_elements(features[*,0])
;text='' & read,text

;shuffle and partition the data into two groups: 
;one group will be used for training, the other for testing. 
;Make the groups the same size
;============================================================
;IDLmlShuffle, features, scores

;define a normalizer that will transform the data. 
;You can use any normalizer; this example uses IDLmlVarianceNormalizer:
Normalizer1 = IDLmlVarianceNormalizer(features)
;===============================================================
;Normalizer1 = IDLmlRangeNormalizer(features)
Normalizer1.Normalize, features

;The same applies to the model outputs. The output of a regression is continuous, 
;and it is important to keep it normalized.
;===============================================================
Normalizer2 = IDLmlVarianceNormalizer(scores)
;Normalizer2 = IDLmlRangeNormalizer(scores)
Normalizer2.Normalize, scores

;part = IDLmlPartition({train:50, test:50}, features, scores)

;Now define the neural network, and determine how many layers to use. In this case, use three layers. 
;Since there are three attributes, the size for the first layer must be 3. 
;For a regression problem, the size of the last layer will be 1, since you will not be classifying into different classes.
;===============================================================
  seed=123456789 ; need this for repeatability
 Model = IDLmlFeedForwardNeuralNetwork([n_inputs, nnode, nnode, noutput], $ 
  SEED = seed, $
  ACTIVATION_FUNCTIONS = [IDLmlafSoftSign(), $ 
    IDLmlafLogistic(), $
    IDLmlafBentIdentity()] ) ; BentIdentity a little better than SoftPlus
;   ;Softmax did not work!

;text='' & read,text

;Train the network
;==================
Optimizer = IDLmloptAdam(0.10)
;Optimizer = IDLmloptAdam(0.08)
;Optimizer = IDLmloptGradientDescent(0.1)
print,nnode,' nodes ',ninput,' inputs ',n_elements(geven),' points'
Loss = List()
;p = Plot(Fltarr(10), title='Loss')
if (n_elements(conv) eq 0) then conv=0.005
if (n_elements(maxloss) eq 0) then maxloss=0.7
i=long(0)
delt_loss=1e8
loss0=100.
while (i lt niter and (delt_loss gt conv or loss0 gt maxloss)) do BEGIN 
;for i=1, niter do begin
  Loss.Add, Model.Train(features, $
    SCORES=scores, OPTIMIZER=Optimizer)
  if (i ge 102) then BEGIN 
   if (loss[i-100] lt maxloss) then BEGIN
    delt_loss=abs(loss[i-1]-loss[i-100])/loss[i-1]
    loss0=loss[i-1]
   endif
  endif
  i=i+1
  ;p.SetData, Loss.ToArray()
  if (i mod 100 eq 0) then print, 'iter ', i, loss[i-1], delt_loss
endwhile
print, 'iter ',i, loss[i-2], delt_loss
;endfor

;Now pass the entire dataset through the model and see how well the output resembles the data it tried to learn:
;===============================================================================================================
data=features_all
Normalizer1.Normalize, data
fpar_all = Model.Evaluate(data)
Normalizer2.unnormalize, fpar_all
fpar_trained=fpar_all ; / par

;You can also use the Evaluate method to evaluate the accuracy of the model against the test data. 
;This is returned by the LOSS keyword that will produce the RMSE (root mean square error) of the calculated results as compared to the actual scores:
;===================================================================================================================================================
;lue_even = Model.Evaluate(features, SCORES=scores, $
;  LOSS=loss2)

;To use the model in a future IDL session without having to retrain it, you can save it to a file. 
;Also save the normalizer, since you will have to renormalize your data too:
;============================================================================
print,'saving NN model ',name
Model.Save, dir+'model_fpar_'+name+'.sav'
Normalizer1.Save, dir+'normalizer1_fpar_'+name+'.sav'
Normalizer2.Save, dir+'normalizer2_fpar_'+name+'.sav'

endif

;To restore these in a future IDL session:
;Model = IDLmlModel.Restore('c:\tmp\model.sav')
;Normalizer = IDLmlNormalizer.Restore('c:\tmp\normalizer.sav')

;print,'in fpar_nn'
;text='' & read,text

end
