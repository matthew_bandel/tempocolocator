hc=1
do_restore=0
use_vpd=0 ; T, VPD improve R^2 by < 0.02
use_temp=0
do_avg=0

tek_color
dir_out='/tis/acps/scratch/joiner/MCD43/eps/'
!y.style=1
IF not hc THEN BEGIN
       SET_PLOT,'x'
       !P.CHARSIZE=1.2
       !P.CHARTHICK=1.0
       !P.THICK=1.0
ENDIF ELSE BEGIN
        SET_PLOT,'PS'
        dir='/tis/acps/scratch/joiner/fluxnet15/hourly_plots/'
        filebase=dir_out+'NEE_hourly'
        device,/portrait,/encapsulated,/color
        !P.CHARSIZE=0.9
        !P.THICK=1.5
        !P.CHARTHICK=1.2
        !P.FONT=0
ENDELSE
!y.style=1

 dir='/tis/acps/scratch/joiner/onefluxnet/'
 fn1=dir+'mcd43d_hourly.sav'
 if (n_elements(rec_all) eq 0 or do_restore) then BEGIN
  print, 'restoring ',fn1
  restore,fn1
 endif
 nrec=n_elements(rec_all)
 minpts=30000

 year=long(rec_all.year)
 month=rec_all.month
 day=rec_all.day
 month_cum=[0,31,59,90,120,151,181,212,243,273,304,334]
 doy=month_cum[month-1]+day
 hour=rec_all.hour
 minute=long(rec_all.minute)
 time=hour+minute/60.
 ymd=year*10000+month*100+day
 ;hms=hour*10000+minute*100
 hms=(hour*60+minute)/(60*24.)
 nee_qc=rec_all.nee_vut_ref_qc
 meas=where(nee_qc eq 0, nm)
 print,nm,' qc=0 out of ',nrec
 meas=where(nee_qc eq 1, nm)
 print,nm,' qc=1 of ',nrec
 meas=where(nee_qc eq 2, nm)
 print,nm,' qc=2 of ',nrec
 nee_uc=rec_all.nee_vut_ref_randunc
 ;swp=rec_all.SW_IN_POT
 sw_in=rec_all.SW_IN_F
 sw_qc=rec_all.SW_IN_F_QC
 ta_f=rec_all.TA_F
 vpd_f=rec_all.VPD_F
 ws_f=rec_all.WS_F
;nee_vut_ref=rec_all.nee_vut_ref
 gpp=rec_all.gpp_dt_vut_ref
 gpp_nt=rec_all.gpp_nt_vut_ref
 diff_gpp=gpp-gpp_nt
 max_diff=5

 min_date=2003001
 sw_min=100
 gpp_min=0

 t=where( $
  nee_qc le 1 and $ ; measured or good quality gap filled, could try to include 2 as well.
  nee_uc lt 3 $
  and gpp gt gpp_min $ 
  and gpp_nt gt -5 $ 
  and abs(diff_gpp) lt max_diff $
  and year ge 2003 $
  ;and date1 gt min_date $ 
 ;and ta_f gt -999 $ 
 ;and vpd_f gt -999 $ 
  and sw_in gt sw_min $ 
  and sw_qc eq 0 $ 
  ;and (swp gt 0) $ 
  and recs_mcd43[0,*] gt -999 $
  and recs_mcd43[1,*] gt -999 $
  and recs_mcd43[2,*] gt -999 $
  and recs_mcd43[3,*] gt -999 $
  and recs_mcd43[4,*] gt -999 $
  and recs_mcd43[5,*] gt -999 $
  and recs_mcd43[6,*] gt -999 $
  and code ne 'AR-SLu' $
  and code ne 'AU-Lox' $
  and code ne 'AU-Rob' $
  and code ne 'AU-Tum' $
  and code ne 'AU-TTE' $
  and code ne 'BE-Lon' $
  and code ne 'CA-Ca2' $
  and code ne 'CA-SJ2' $
  and code ne 'CA-TP2' $
  and code ne 'CA-WP2' $
  and code ne 'CH-Cha' $
  and code ne 'CH-Oe1' $
  and code ne 'CH-Oe2' $
  and code ne 'CN-Sw2' $
  and code ne 'CZ-BK2' $
  and code ne 'DE-Geb' $
  and code ne 'DE-RuS' $
  and code ne 'DE-Seh' $
  and code ne 'DE-Zrk' $
  and code ne 'ES-Amo' $
  and code ne 'ES-Ln2' $
  and code ne 'FR-Fon' $
  and code ne 'FR-Gri' $
  and code ne 'GF-Guy' $
  and code ne 'IT-BCi' $
  and code ne 'IT-CA2' $
  and code ne 'IT-CA3' $
  and code ne 'IT-Ro2' $
  and code ne 'US-AR1' $
  and code ne 'US-ARb' $
  and code ne 'US-CRT' $
  and code ne 'US-Lin' $
  and code ne 'US-LWW' $
  and code ne 'US-Me4' $
  and code ne 'US-Me6' $
  and code ne 'US-Myb' $
  and code ne 'US-Ne3' $
  and code ne 'US-OWC' $
  and code ne 'US-Pnp' $
  and code ne 'US-Ro1' $
  and code ne 'US-Ro5' $
  and code ne 'US-Ro6' $
  and code ne 'US-Tw1' $
  and code ne 'US-Tw3' $
  and code ne 'US-Twt' $
  and code ne 'US-Wi1' $
  and code ne 'US-Wi2' $
  and code ne 'US-Wi3' $
  and code ne 'US-Wi6' $
  and code ne 'US-WPT' $
  and code ne 'US-Wgr' $
  and code ne 'US-Wi9' $
  and code ne 'ZM-Mon' $
  and code ne 'US-Bi1' $
  and code ne 'PA-SPs' $
  and code ne 'IT-CA1' $
  and code ne 'CG-Tch' $
  and code ne 'US-Dk1' $
  and code ne 'RU-Che' $
  and code ne 'AT-Neu' $
  and code ne 'AU-ASM' $
  and code ne 'AU-Cpr' $
  and code ne 'AU-RDF' $
  and code ne 'CA-NS7' $
  and code ne 'CA-TP1' $
  and code ne 'CN-Du2' $
  and code ne 'CN-HaM' $
  and code ne 'CZ-BK1' $
  and code ne 'DE-Akm' $
  and code ne 'DE-Gri' $
  and code ne 'DE-Obe' $
  and code ne 'DK-Eng' $
  and code ne 'DK-ZaH' $
  and code ne 'ES-LJu' $
  and code ne 'ES-LgS' $
  and code ne 'FR-LBr' $
  and code ne 'FR-Pue' $
  and code ne 'IT-Cpz' $
  and code ne 'NO-Blv' $
  and code ne 'RU-Cok' $
  and code ne 'SD-Dem' $
  and code ne 'US-AR2' $
  and code ne 'US-Atq' $
  and code ne 'US-Bi2' $
  and code ne 'US-Blo' $
  and code ne 'US-Cop' $
  and code ne 'US-FR2' $
  and code ne 'US-Fmf' $
  and code ne 'US-Fuf' $
  and code ne 'US-GBT' $
  and code ne 'US-Me1' $
  and code ne 'US-Men' $
  and code ne 'US-Mpj' $
  and code ne 'US-Ne1' $
  and code ne 'US-Seg' $
  and code ne 'US-Ses' $
  and code ne 'US-Ton' $
  and code ne 'US-Tw2' $
  and code ne 'US-Vcm' $
  and code ne 'US-Vcp' $
  and code ne 'US-Whs' $
  and code ne 'AU-GWW' $
  and code ne 'DK-Fou' $
  and code ne 'SN-Dhr' $
  and code ne 'US-Me2' $
  and code ne 'US-Me3' $
  and code ne 'US-Rws' $
  and code ne 'CN-Du3' $
  and code ne 'IT-PT1' $
  and code ne 'AU-Ade' $
  and code ne 'AU-Fog' $
  and code ne 'AU-How' $
  and code ne 'AU-Whr' $
  and code ne 'AU-Wom' $
  and code ne 'BE-Vie' $
  and code ne 'BR-Sa3' $
  and code ne 'CA-SF2' $
  and code ne 'DE-Tha' $
  and code ne 'NL-Loo' $
  and code ne 'US-Wjs' $
  and code ne 'US-Goo' $
  and code ne 'CZ-wet' $
  and code ne 'AU-Dry' $
  and code ne 'AU-Cum' $
  and code ne 'AU-DaS' $
  and code ne 'AU-Wac' $
  and code ne 'AU-Gin' $
  and code ne 'DE-Kli' $
  and code ne 'CN-Cha' $
  and code ne 'CN-Dan' $
  and code ne 'CN-Din' $
  and code ne 'US-Ho1' $
  and code ne 'US-PFa' $
  and code ne 'US-Rls' $
  and code ne 'US-Wi4' $
  and code ne 'AU-Emr' $
  and code ne 'JP-MBF' $
  and code ne 'US-KS2' $
  and code ne 'IT-Lav' $
  and code ne 'US-Dk3' $
  and code ne 'FI-Jok' $
  and code ne 'CH-Dav' $
  and code ne 'AR-Vir' $
  and code ne 'IT-Ren' $
  and code ne 'NO-Adv' $
  and code ne 'IT-Cp2' $
  and code ne 'IT-Noe' $
  and code ne 'IT-SRo' $
  and code ne 'CA-SF1' $
  and code ne 'CA-TP4' $
  and code ne 'MY-PSO' $
  and code ne 'US-ARM' $
  and code ne 'IT-Isp' $
  and code ne 'US-Sta' $
  and code ne 'CA-Ca3' $
  and code ne 'CN-Qia' $
  and code ne 'US-Wi7' $
  and code ne 'IT-SR2' $
  and code ne 'BE-Bra' $
  and code ne 'CA-Gro' $
  and code ne 'NL-Hor' $
  ;and code ne 'US-Bar' $
  , nt)

 print,' found ',nt,' good points'

 if (nt gt minpts) then BEGIN
   ;nee=nee_vut_ref[t]
   temp=ta_f[t]
   vpd=vpd_f[t]
   if (do_avg) then BEGIN
     gpp_avg = (gpp[t]+gpp_nt[t])/2.
   endif else BEGIN
     gpp_avg = gpp[t]
   endelse
   sw=SW_IN[t]
   mcd43d=recs_mcd43[*,t]
   ;date2=date[t]

   ;train up to predict hourly GPP
   ;==============================
   niter=500
   name='GPP_hourly_'
   nred=5
   g_fit=findgen(nt/nred)*nred
   par_pred=1

   ;without temp and VPD
   ;========================
   fpar_nn,mcd43d,gpp_avg,name,gpp_trained_notv, g_fit, g_fit, $
    sw,par_inc_min,temp=temp,use_temp=use_temp, conv=1e-6, niter=niter, $
    model=model, norm1=norm1, norm2=norm2, use_vpd=use_vpd, vpd=vpd, $
    dir=dir_out, par_pred=par_pred, $
    features_all=features_all

   u=where(gpp_trained_notv lt 0,nu)
   if (nu gt 0) then gpp_trained_notv[u]=0.
   diff2=gpp_avg-gpp_trained_notv

   ;oplot,sw,diff2,psym=3,color=2
   ;oplot,[0,max(sw)],[0,0],color=3
   ;oplot,date2, gpp_trained_notv, psym=3,color=3
   print_gpp_output,'GPP hourly (trn) ',gpp_avg[g_fit],gpp_trained_notv[g_fit]
   print_gpp_output,'GPP hourly (ind) ',gpp_avg,gpp_trained_notv
   ;text='' & read,text

   codes=code[t]
   ucode=uniq(codes)
   ncode=n_elements(ucode)
   print,ncode,' sites'
   month_plot=10 & month_str='October'
   month_plot=7 & month_str='July'
   do_plot=1
   tek_color
   for icode=0, ncode-1 do BEGIN
    code1=codes[ucode[icode]]
    k=where(codes eq code1,nk)
    print_gpp_output,code1,gpp_avg[k],gpp_trained_notv[k], str1=str1
    stats1=strsplit(str1,'&',/extract)
    if (do_plot) then BEGIN
     if (hc) then device,file=dir_out+code1+'_'+month_str+'_hourly.eps'
     !p.multi=[0,1,3,0,0]
     ytit='!5GPP (umol CO!d2!n m!u-2!n s!u-1!n)'
     years=year[t[k]]
     date=years+doy[t[k]]/365.
     plot,date,gpp_avg[k],psym=3,title=code1,ytitle=ytit,xtitle='!5Date'
     oplot,date,gpp_trained_notv[k],psym=3,color=2
     tit1='!5R!u2!n='+stats1[2]+' bias='+stats1[3]+' RMSE='+stats1[4]
     plot,gpp_avg[k],gpp_trained_notv[k],psym=3,title=tit1
     oplot,[0,50],[0,50],color=2
     year_ind=uniq(years)
     year_site=years[year_ind]
     nyear=n_elements(year_site)
     npt1=intarr(nyear)
     for j=0, nyear-1 do BEGIN
       ind=where(years eq year_site[j] and month[t[k]] eq month_plot,npts)
       npt1[j]=npts
     endfor
     ind_year=where(npt1 eq max(npt1))
     ind=where(years eq year_site[ind_year[0]] and month[t[k]] eq month_plot,npts)
     nhr=24
     gpp_day=fltarr(nhr)
     gpp_day_std=fltarr(nhr)
     gpp_fs=fltarr(nhr)
     gpp_fs_std=fltarr(nhr)
     npt=intarr(nhr)
     for j=0, nhr-1 do BEGIN
       ind_hr=where(floor(hour[t[k[ind]]]) eq j, nhrs)
       npt[j]=nhrs
       if (nhr gt 0) then BEGIN
	 gpp_day[j]=mean(gpp_avg[k[ind[ind_hr]]])
	 gpp_day_std[j]=stddev(gpp_avg[k[ind[ind_hr]]])
	 gpp_fs[j]=mean(gpp_trained_notv[k[ind[ind_hr]]])
	 gpp_fs_std[j]=stddev(gpp_trained_notv[k[ind[ind_hr]]])
       endif
     endfor
     ind_good=where(npt gt 0, ngood)
     min_pt=6
     hrs=indgen(nhr)
     text=''
     if (ngood gt min_pt) then BEGIN
	tit=code1+' '+string(year_site[ind_year[0]],form='(i4)')+' '+month_str
	yr=[min([gpp_day-gpp_day_std,gpp_fs-gpp_fs_std]),max([gpp_day+gpp_day_std,gpp_fs+gpp_fs_std])]
	plot,hrs[ind_good],gpp_day[ind_good],psym=5, title=tit, yrange=yr,ytitle=ytit,xtitle='!5Hour'
	oplot,hrs[ind_good],gpp_fs[ind_good],psym=6, color=2
	for j=0, ngood-1 do BEGIN
          oplot,[hrs[ind_good[j]],hrs[ind_good[j]]],[gpp_day[ind_good[j]]-gpp_day_std[ind_good[j]],gpp_day[ind_good[j]]+gpp_day_std[ind_good[j]]],thick=3
          oplot,[hrs[ind_good[j]],hrs[ind_good[j]]],[gpp_fs[ind_good[j]]-gpp_fs_std[ind_good[j]],gpp_fs[ind_good[j]]+gpp_fs_std[ind_good[j]]], color=2
        endfor
	if (not hc) then read,text
	if (hc) then device,/close
     endif
    endif ; do_plot
   endfor

endif ; npts gt min

recs_all=rec_all[t]
do_save=0
if (do_save) then BEGIN
  save,file=dir_out+'gpp_hourly.dat',gpp_avg,gpp_trained_notv,recs_all
endif

end
