PRO fpar_nn,bands,gpp,name,fpar_trained, geven, godd, $
  par,par_inc_min, sif, nband, use_band=use_band, $
  cosvza=cosvza, cossza=cossza, cosphase=cosphase, $
  use_angs=use_angs, use_brdf=use_brdf, brdf=brdf, $
  use_sif=use_sif, bband=brdf_band, noband=noband, $
  temp=temp, $
  niter=niter, aqua=aqua, mult_par=mult_par, par_pred=par_pred

n_points=n_elements(gpp)
use_temp= (n_elements(temp) gt 0)
if (n_elements(niter) eq 0) then niter=1000
if (n_elements(par_pred) eq 0) then par_pred=0
if (n_elements(nband) eq 0) then nband=n_elements(bands[*,0])
if (n_elements(use_angs) eq 0) then use_angs=0
if (n_elements(use_brdf) eq 0) then use_brdf=0
if (n_elements(use_sif) eq 0) then use_sif=0
if (n_elements(mult_par) eq 0) then mult_par=1
if (n_elements(noband) eq 0) then noband=0
if (n_elements(brdf_band) eq 0) then brdf_band=indgen(7)

n_input=0
if (use_angs) then n_input=n_input+3
if (use_sif) then n_input=n_input+1
if (n_elements(aqua) gt 0) then n_input=n_input+1
;if (use_par) then n_input=n_input+1
if (use_temp) then n_input=n_input+1
if (n_elements(use_band) gt 0) then BEGIN
  nband=n_elements(use_band)
endif else BEGIN
  use_band=indgen(nband)
endelse
if (use_brdf) then BEGIN
  ncoef=n_elements(brdf[0,*,0])
  nband_brdf=n_elements(brdf_band)
  n_input=n_input+nband_brdf*ncoef
endif
if (par_pred) then BEGIN
  n_input=n_input+1
  mult_par=0
endif
ninput=n_input
if (noband eq 0) then BEGIN
  ninput=nband+ninput
endif
;print, n_input, ninput

features_all=fltarr(ninput,n_points)
nind=0
if (noband eq 0) then BEGIN
 for iband=0, nband-1 do BEGIN
  if (mult_par) then BEGIN
    features_all[iband,*]=bands[use_band[iband],*] * par
  endif else BEGIN
    features_all[iband,*]=bands[use_band[iband],*] 
  endelse
 endfor
 nind=nind+nband
endif 
if (use_angs) then BEGIN
  features_all[nind,*]=cossza
  features_all[nind+1,*]=cosvza
  features_all[nind+2,*]=cosphase
  nind=nind+3
endif
if (n_elements(aqua) gt 0) then BEGIN
  features_all[nind,*]=aqua
  nind=nind+1
endif
if (use_sif) then BEGIN
  features_all[nind,*]=sif ;/par
  nind=nind+1
endif
if (use_temp) then BEGIN
  features_all[nind,*]=temp
  nind=nind+1
endif
if (par_pred) then BEGIN
  features_all[nind,*]=par
  nind=nind+1
endif
if (use_brdf) then BEGIN
  for iband=0, nband_brdf-1 do BEGIN
   for icoef=0, ncoef-1 do BEGIN
    features_all[nind+icoef+iband*ncoef,*]=brdf[brdf_band[iband],icoef,*]
   endfor
  endfor
endif

geven2=findgen(n_elements(geven)) & ngeven2=n_points

if (ngeven2 gt 0) then BEGIN
geven3=geven[geven2]

features=features_all[*,geven3]
nnode=nband*2+2*(n_input);-2
data=features
scores=gpp[geven3] 
n_inputs=n_elements(features[*,0])

;shuffle and partition the data into two groups: 
;one group will be used for training, the other for testing. 
;Make the groups the same size
;============================================================
;IDLmlShuffle, features, scores

;define a normalizer that will transform the data. 
;You can use any normalizer; this example uses IDLmlVarianceNormalizer:
Normalizer1 = IDLmlVarianceNormalizer(features)
;===============================================================
;Normalizer1 = IDLmlRangeNormalizer(features)
Normalizer1.Normalize, features

;The same applies to the model outputs. The output of a regression is continuous, 
;and it is important to keep it normalized.
;===============================================================
Normalizer2 = IDLmlVarianceNormalizer(scores)
;Normalizer2 = IDLmlRangeNormalizer(scores)
Normalizer2.Normalize, scores

;part = IDLmlPartition({train:50, test:50}, features, scores)

;Now define the neural network, and determine how many layers to use. In this case, use three layers. 
;Since there are three attributes, the size for the first layer must be 3. 
;For a regression problem, the size of the last layer will be 1, since you will not be classifying into different classes.
;===============================================================
  seed=123456789 ; need this for repeatability
 Model = IDLmlFeedForwardNeuralNetwork([n_inputs, nnode, nnode, 1], $ 
  SEED = seed, $
  ACTIVATION_FUNCTIONS = [IDLmlafSoftSign(), $ 
    IDLmlafLogistic(), $
    IDLmlafBentIdentity()] ) ; BentIdentity a little better than SoftPlus
;   ;Softmax did not work!

;Train the network
;==================
Optimizer = IDLmloptAdam(0.10)
;Optimizer = IDLmloptAdam(0.08)
;Optimizer = IDLmloptGradientDescent(0.1)
print,nnode,' nodes ',ninput,' inputs ',n_elements(geven),' points'
Loss = List()
;p = Plot(Fltarr(10), title='Loss')
for i=1, niter do begin
  Loss.Add, Model.Train(features, $
    SCORES=scores, OPTIMIZER=Optimizer)
  ;p.SetData, Loss.ToArray()
  if (i mod 100 eq 0) then print, 'iter ', i, loss[i-1]
endfor

;Now pass the entire dataset through the model and see how well the output resembles the data it tried to learn:
;===============================================================================================================
data=features_all
Normalizer1.Normalize, data
fpar_all = Model.Evaluate(data)
Normalizer2.unnormalize, fpar_all
fpar_trained=fpar_all ; / par

;You can also use the Evaluate method to evaluate the accuracy of the model against the test data. 
;This is returned by the LOSS keyword that will produce the RMSE (root mean square error) of the calculated results as compared to the actual scores:
;===================================================================================================================================================
;lue_even = Model.Evaluate(features, SCORES=scores, $
;  LOSS=loss2)

;To use the model in a future IDL session without having to retrain it, you can save it to a file. 
;Also save the normalizer, since you will have to renormalize your data too:
;============================================================================
Model.Save, 'model_fpar_'+name+'.sav'
Normalizer1.Save, 'normalizer1_fpar_'+name+'.sav'
Normalizer2.Save, 'normalizer2_fpar_'+name+'.sav'

endif

;To restore these in a future IDL session:
;Model = IDLmlModel.Restore('c:\tmp\model.sav')
;Normalizer = IDLmlNormalizer.Restore('c:\tmp\normalizer.sav')

;print,'in fpar_nn'
;text='' & read,text

end
