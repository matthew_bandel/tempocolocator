import earthaccess
from scipy import stats
import h5py
import numpy as np
import re
from netCDF4 import Dataset
import glob
import os 

def grab_files(inst_name,year,month,day):
    date_str = str(year)+'-'+str(month).zfill(2)+'-'+str(day).zfill(2)
    
    results = earthaccess.search_data(
    short_name =  inst_name,
    version = 'R2022.0',
    cloud_hosted=True,
    temporal=(date_str+" 00:00:00", date_str+" 23:59:00"),
    count=200,
    bounding_box = (-180,0,0,90)
)   
    return results

earthaccess.login(persist=True)

#Recommended bit flagging for NASA L2 Ocean Color Products
flagged_bits = [0,1,3,4,5,8,9,10,12,14,15,16,19,21,22,25]

#Lat Lon Bins used for L2->L3 grid 
lon_bins = np.arange(-175,-29.98,0.02)
lat_bins = np.arange(15,65.02,0.02)

oc_sums = np.zeros((len(lon_bins)-1,len(lat_bins)-1,24))
oc_counts = np.zeros((len(lon_bins)-1,len(lat_bins)-1,24))

year = 2024; month = 2; day = 27

#Looping through current day, previous, and next to fill OC grid cube
for delta in [0,-1,1]:

    #Looping through VIIRS SNPP/J1/J2 and MODIS Aqua to fill OC grid 
    for inst in ['VIIRSJ2_L2_OC','MODISA_L2_OC','VIIRSJ1_L2_OC','VIIRSN_L2_OC']:
        #Grabbing paths for L2 ocean color products
        files = grab_files(inst,year,month,day+delta)
        #If no files found, looking for nrt files 
        if len(files) == 0:
            print('NO FILES FOUND, TRYING NRT')
            files = grab_files(inst+'_NRT',year,month,day)
        #If no standard or nrt OC files found, continuing to next instrument 
        if len(files) == 0:
            print('NO FILES FOUND, NO NRT')
            continue

        #Downloading selected OC files 
        earthaccess.download(files, "TMP_OC_Files/")

        oc_files = sorted(glob.glob('TMP_OC_Files/*'))
        #Iterating through downloaded OC files 
        for filename in oc_files:
            print(filename)

            #Grabbing OC retrieval hour to put in OC data cube 
            hour = int(re.findall('[0-9]{8}T[0-9]{2}',filename)[0].split('T')[1])
        
            f = Dataset(filename,'r')    
            
            oc_chl = f['/geophysical_data/chlor_a'][:]
            oc_lat = f['/navigation_data/latitude'][:]
            oc_lon = f['/navigation_data/longitude'][:]
            oc_flags = f['/geophysical_data/l2_flags'][:]
        
            oc_chl=np.ma.filled(oc_chl,float('nan'))
            oc_lat=np.ma.filled(oc_lat,float('nan'))
            oc_lon=np.ma.filled(oc_lon,float('nan'))
            oc_flags=np.ma.filled(oc_flags,float('nan'))

            #Flagging L2 OC data based on NASA OB recommendations 
            for bit in flagged_bits:
                oc_chl[oc_flags & (2**bit) == (2**bit)] = np.nan
                
            nan_inds = ~np.isnan(oc_chl)
        
            if len(oc_chl[nan_inds]) == 0:
                os.remove(filename)
                continue

            #Binning OC data into 0.2 degree grid 
            ret_sum, x_edge, y_edge, _ = stats.binned_statistic_2d(oc_lon[nan_inds], oc_lat[nan_inds], np.log(oc_chl[nan_inds]), 'sum', bins=[lon_bins,lat_bins])
            ret_count, x_edge, y_edge, _  = stats.binned_statistic_2d(oc_lon[nan_inds], oc_lat[nan_inds], np.log(oc_chl[nan_inds]), 'count', bins=[lon_bins,lat_bins])    
        
            oc_sums[:,:,hour] += ret_sum
            oc_counts[:,:,hour] += ret_count
        
            f.close()
            os.remove(filename)


#Calculating OC data cube based on MODIS/VIIRS which has dimensions lon, lat, time 
oc_data_cube = oc_sums/oc_counts
