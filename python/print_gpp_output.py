import numpy as np
from scipy.stats import pearsonr

def print_gpp_output(title, gppf, gpp):
    r2 = np.corrcoef(gpp, gppf)[0, 1]**2
    mean1 = np.mean(gpp - gppf)
    rmse1 = np.sqrt(np.mean((gpp - gppf)**2))
    #mef1 = 1.0 - (np.sum((gpp - gppf)**2) / np.sum((gpp - np.mean(gpp))**2))
    #lam1 = np.polyfit(gpp, gppf, 1)[0]
    
    amp = ' & '
    str1 = f'{title:<26}{amp}{len(gpp):7d}{amp}{r2:6.3f}{amp}{mean1:7.4f}{amp}{rmse1:6.3f}\\'
      #{amp}{mef1:{.3f}}{amp}{lam1:{form}}\\'
    print(str1)
    #return, r2, mean1, rmse1

# Example usage
#gppf = np.array([1.2, 2.3, 3.4, 4.5, 5.6])
#gpp = np.array([1.1, 2.4, 3.5, 4.6, 5.8])
#title = 'Example Title'

#print_gpp_output(title, gppf, gpp)
