#!/usr/bin/env python3

# pushers.py to push and pull files between Sagemaker and S3

# import os, subprocess
import os
import subprocess

# import time
import time

# import boto
import boto3


# class Pusher to do OMOCNUV analysis
class Pusher(list):
    """Pusher class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, show=False):
        """Initialize instance.

        Arguments:
            show: boolean, show all paths
        """

        # set show variable
        self.show = show

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Pusher instance >'

        return representation

    def _file(self, path, folders=0):
        """Get the filename from a path.

        Arguments:
            path: str, pathname
            folders: int, number of subfolders

        Returns:
            str, file name
        """

        # split on slash
        fragments = path.split('/')

        # join together fragments according to number
        name = '/'.join(fragments[-(1 + folders):])

        return name

    def _fold(self, path):
        """Break apart a path name into directory and file.

        Arguments:
            path: str, pathname

        Returns:
            str, directory
        """

        # get fileame
        words = path.split('/')

        # get folder
        folder = '/'.join(words[:-1])

        return folder

    def _see(self, directory):
        """See all the paths in a directory.

        Arguments:
            directory: str, directory path

        Returns:
            list of str, the file paths.
        """

        # try to
        try:

            # make paths
            paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        # unless the directory does not exist
        except FileNotFoundError:

            # in which case, alert and return empty list
            self._print('{} does not exist'.format(directory))
            paths = []

        return paths

    def _tell(self, queue):
        """Enumerate the contents of a list.

        Arguments:
            queue: list

        Returns:
            None
        """

        # for each item
        for index, member in enumerate(queue):

            # print
            self._print('{}) {}'.format(index, member))

        # print spacer
        self._print(' ')

        return None

    def _print(self, *messages):
        """Print the message, localizes print statements.

        Arguments:
            *messagea: unpacked list of str, etc

        Returns:
            None
        """

        # construct  message
        message = ', '.join([str(message) for message in messages])

        # print
        print(message)

        # # go through each meassage
        # for message in messages:
        #
        #     # print
        #     print(message)

        return message

    def clean(self, folder='.', extensions=None, initial=True):
        """Clean up all folders recursively, deleting files with specified extensions.

        Arguments:
            folder: path to beginning folder
            extensions: tuple of str, extensions to delete
            initial: boolean, initial run?

        Returns:
            None
        """

        # set default extensions
        extensions = extensions or ('.nc', '.h5', '.sav', '.png', '.json', '.pb', '.index', '00001')

        # get the contents of the folder
        contents = self._see(folder)

        # for each path
        for path in contents:

            # ignore if beginning with a dot
            if not path.replace(folder + '/', '').startswith('.'):

                # try to
                try:

                    # pass on the folder to next iteration
                    self.clean(path, extensions=extensions, initial=False)

                # unless not a directory
                except NotADirectoryError:

                    # in which case, check extension
                    if any([path.endswith(extension) for extension in extensions]):

                        # if showing
                        if self.show:

                            # print status
                            print('deleting {}...'.format(path))

                        # delete path
                        os.remove(path)

        # if initial
        if initial:

            # print status
            print('all done with deletions')

            # sleep for 2 seconds
            time.sleep(2)

            # delete .local Trash
            print('deleting local trash...')

            # create call
            call = ['rm', '-rf', '.local/share/Trash/files']
            print(' '.join(call))
            subprocess.call(call)

            # create call
            call = ['rm', '-rf', '.local/share/Trash/info']
            print(' '.join(call))
            subprocess.call(call)

            # print status
            print('deleted local trash.')

            # verify
            call = ['ls', '.local/share/Trash']
            print(' '.join(call))
            subprocess.call(call)

            # # verify
            # call = ['ls', '.local/share/Trash/info']
            # print(' '.join(call))
            # subprocess.call(call)

        return None

    def pull(self, folder, source, criterion=''):
        """Pull files into a sagemaker folder from an s3 bucket.

        Arguments:
            folder: str, path to sagemaker folder
            source: s3 source, bucket and prefix
            criterion: str, search criterion

        Returns:
            None
        """

        # parse the source
        bucket = source.split('/')[0]
        prefix = '/'.join(source.split('/')[1:])

        # intialize client
        client = boto3.client('s3')

        # set up objects
        continuation = True
        contents = []

        # set initial parameters
        parameters = {'Bucket': bucket, 'Prefix': prefix}

        # while a continuation token is generated
        while continuation:

            # retrieve file paths and add to contents
            response = client.list_objects_v2(**parameters)
            contents += response.get('Contents', [])

            # if there are new entries
            if 'NextContinuationToken' in response.keys():

                # replace token and add to parameters
                token = response['NextContinuationToken']
                parameters.update({'ContinuationToken': token})

            # otherwise
            else:

                # don't continue
                continuation = False

        # get subset of paths according to target
        paths = [entry['Key'] for entry in contents]

        # only grab immediate files that meet the criterion
        paths = [path for path in paths if self._fold(path) == prefix and len(self._file(path)) > 0]
        paths = [path for path in paths if criterion in path]

        # for each path
        for index, path in enumerate(paths):

            # if showing
            if self.show:

                # print
                print('downloading {} ( {} of {} )...'.format(path, index, len(paths)))

            # construct destination
            destination = '{}/{}'.format(folder, path.split('/')[-1])

            # try to
            try:

                # download
                client.download_file(bucket, path, destination)

                # if showing
                if self.show:

                    # print status
                    print('done!')

            # unless an error
            except (NotADirectoryError, FileNotFoundError):

                # if showing
                if self.show:

                    # print status
                    print('skipping!')

        # print done with folder
        print('all done!')

        return None

    def push(self, folder, sink):
        """Push files from a Sagemaker folder to an S3 bucket.

        Arguments:
            folder: str, Sagemaker folder
            sink: s3 bucket and prefix

        Returns:
            None
        """

        # parse the source
        bucket = sink.split('/')[0]
        prefix = '/'.join(sink.split('/')[1:])

        # intialize client
        client = boto3.client('s3')

        # for each path
        for name in os.listdir(folder):

            # create path
            path = '{}/{}'.format(folder, name)

            # create destination
            destination = '{}/{}'.format(prefix, name)

            # if showing
            if self.show:

                # print
                print('copying {} to {}/{}...'.format(path, bucket, destination))

            # try to:
            try:

                # load
                client.upload_file(path, bucket, destination)

                # if showing
                if self.show:

                    # print status
                    print('done!')

            # unless it is a directory
            except IsADirectoryError:

                # skip
                print('{} is a directory, skipping!'.format(path))

        # print done with folder
        print('all done!')

        return None