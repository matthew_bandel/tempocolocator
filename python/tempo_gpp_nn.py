# from tempo_libraries.tempo_libraries import * 
from tempo_libraries import * 
import gc
import scipy.io as sio
import re
import glob 
import h5py 
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow_probability as tfp
from tensorflow.keras import backend as K

tfd = tfp.distributions
tfpl = tfp.layers

#Prior/Posterior for the Bayesian Approximation in model training 
def prior(kernel_size, bias_size=0, dtype=None):
  n = kernel_size + bias_size
  return tf.keras.Sequential([
      tfp.layers.VariableLayer(n, dtype=dtype),
      tfp.layers.DistributionLambda(lambda t: tfd.Independent(
          tfd.Normal(loc=t, scale=1),
          reinterpreted_batch_ndims=1)),
  ])

def posterior(kernel_size, bias_size=0, dtype=None):
  n = kernel_size + bias_size
  c = np.log(np.expm1(1.))
  return tf.keras.Sequential([
      tfp.layers.VariableLayer(2 * n, dtype=dtype),
      tfp.layers.DistributionLambda(lambda t: tfd.Independent(
          tfd.Normal(loc=t[..., :n],
                     scale=1e-5 + 0.02*tf.nn.tanh(c + t[..., n:])),
          reinterpreted_batch_ndims=1)),
  ])


#Probability loss function to consider uncertainty 
def custom_loss(targets, estimated_distribution):
  return -estimated_distribution.log_prob(targets)


# data_dir = '/user/1002/zfasnach/TEMPO/L1b_Reduced_Upd/*/*/*'

data_dir = 'TEMPO_PC_Reduced/*/*/*'


print('grabbing tempo files....')

tempo_files = glob.glob(data_dir+'/*2024*T*_LandOcean_V2_AllWaves.nc')
# tempo_files += glob.glob(data_dir+'/*20240410T*_LandOcean_V2_AllWaves.nc')


# print(tempo_files)

tempo_files = tempo_files[:]
tempo_files.sort()
# for entry in tempo_files:
#     print(entry)
# print(' ')


start = True

#Reading PAR data for scaling GPP 
par_data = sio.readsav('PAR/PAR_clrsky_daily.sav')
par_clr = par_data['par_clearsky']
par_lat= par_data['lat_grid_par']

for filename in tempo_files[:]:
    print(filename)
    tempo_data = tempo_l1b(filename)
    time_stamp = re.findall('[0-9]{8}T[0-9]{4}',filename)[0]

    year = int(time_stamp[0:4])
    month = int(time_stamp[4:6])
    day = int(time_stamp[6:8])

    #Reading TEMPO PC reduced L1b data 
    tempo_data.rd_tempo_l1b()

    
    
    #Reading co-located GPP files
    # col_files = glob.glob('GPP/TEMPO_*'+time_stamp+'*nc') 
    col_files = glob.glob('GPP_Colocations/2024/*/*/TEMPO_*'+time_stamp+'*.nc') 
    col_files.sort()

    print(time_stamp)
    for entry in col_files:
        print(entry)
    
# glob.glob('/user/1002/zfasnach/TEMPO_GPP_Colocated/colocations/TEMPO_*'+time_stamp+'*nc')

    if len(col_files) == 0:
        print('NO Co-located files')
        continue 
    f = h5py.File(col_files[0],'r')

    gpp_interp = f['GPP'][:].flatten()
    gpp_latitude = f['latitude'][:].flatten()
    gpp_interp[gpp_interp< 0] = np.nan
    gpp_interp = gpp_interp[gpp_latitude > 0]
    f.close()
    del f
    gc.collect()

    # Reading TEMPO cloud product. Earlier versions screened based on crf, but doesn't seem
    # to be needed. Works well under thick clouds as well 
    # cld_file = glob.glob('/user/1002/zfasnach/TEMPO_CLDO4_L2/*/*/*/*'+time_stamp+'*nc')
    
    cld_file = glob.glob('TEMPO_CLDO4_L2/2024/*/*/TEMPO_CLDO4_L2*'+time_stamp+'*nc')

    print(time_stamp)
    for entry in cld_file:
        print(entry)
    
    
    if len(cld_file) == 0:
        print('NO CLD FILE')
        print(time_stamp)
        continue
    
    f = h5py.File(cld_file[0],'r')
    crf = f['/product/cloud_fraction'][:]
    crf[crf ==-2**100] = np.nan

    #Screening out a few bad calibration xtrack positions 
    crf[:,1027] = np.nan
    crf[:,1026] = np.nan
    crf[:,1048] = np.nan
    
    crf_latitude = f['geolocation/latitude'][:]
    crf = crf[abs(crf_latitude) < 1e30]
    
    crf = crf.flatten()
    
    # get snow ice fraction
    snow = f['support_data/snow_ice_fraction'][:]
    snow = snow[abs(crf_latitude) < 1e30]
    snow = snow.flatten()

    
    f.close()
    del f 
    gc.collect()

    # print('sza: ', tempo_data.sza.shape)
    # print('gpp: ', gpp_interp.shape)
    # print('crf: ', crf.shape)
    # print('')


    #Interpolation par data to use as training input to account for solar variaibility 
    interp_par  = np.array(np.interp(tempo_data.lat, par_lat[::-1],par_clr[day-1,month-1,:][::-1]))

    #Screening NN for specific view/solar angles and land only conditions 
    water_inds = (tempo_data.water == 1)
    # inds = (tempo_data.rad[:,0] > (-2**100)) & water_inds  & (~np.isnan(crf)) & (~np.isnan(gpp_interp)) & (crf < 1) & (~np.isnan(tempo_data.sza)) &  (tempo_data.sza < 65) & (tempo_data.vza < 70)
    
    
    # print('rad: ', tempo_data.rad.shape)
    # print('crf: ', crf.shape)
    # print('gpp_interp: ', gpp_interp.shape)
    # print('water_inds: ', water_inds.shape)
    
    
    # print('tempo rad: ', (tempo_data.rad[:,0] > (-2**100)).sum())
    # print('water: ', water_inds.sum())
    # print('gpp: ', (~np.isnan(gpp_interp)).sum())
    # print('sza: ', (~np.isnan(tempo_data.sza)).sum())
    # print('sza angle: ', (tempo_data.sza < 75).sum())
    # print('vza angle: ', (tempo_data.vza < 70).sum())
    
    # inds = (tempo_data.rad[:,0] > (-2**100)) & water_inds  & (~np.isnan(crf)) & (~np.isnan(gpp_interp)) & (crf < 1) & (~np.isnan(tempo_data.sza)) &  (tempo_data.sza < 65) & (tempo_data.vza < 70)
    
    inds = (tempo_data.rad[:,0] > (-2**100)) & water_inds  & (~np.isnan(crf)) & (~np.isnan(gpp_interp)) & (crf <= 0.8) & (~np.isnan(tempo_data.sza)) & (tempo_data.sza <= 65) & (tempo_data.vza <= 70) & (snow <= 0)
    # inds = (tempo_data.rad[:,0] > (-2**100)) & water_inds  & (~np.isnan(gpp_interp)) & (~np.isnan(tempo_data.sza)) &  (tempo_data.sza < 75) & (tempo_data.vza < 70)
    
    print(inds)
    print(inds.sum())
    
    if len(tempo_data.sza[:][inds]) == 0:
      continue

    #Concatenating training data 
    inputs = np.array(tempo_data.rad[:][inds,0:30])
    inputs = np.hstack((inputs[:],interp_par[:][inds].reshape(-1,1)))
    inputs = np.hstack((inputs[:],np.cos(tempo_data.sza)[:][inds].reshape(-1,1)))
    inputs = np.hstack((inputs[:],np.cos(tempo_data.vza)[:][inds].reshape(-1,1)))
    inputs = np.hstack((inputs[:],np.cos(tempo_data.raa)[:][inds].reshape(-1,1)))

    if start:
        total_inputs = np.array(inputs)
        total_gpp = np.array(gpp_interp[:][inds])
        start = False
    else:
        total_inputs = np.vstack((total_inputs,inputs))
        total_gpp = np.hstack((total_gpp,gpp_interp[inds]))
    
    del tempo_data
    del inputs 
    gc.collect()


#Normalizing the inputs/outputs 
sc_in = MinMaxScaler(feature_range=(0,1))
inputs = sc_in.fit_transform(total_inputs)
del total_inputs
gc.collect()

sc_out = MinMaxScaler(feature_range=(0,1))
outputs = total_gpp.reshape(-1,1)
outputs = sc_out.fit_transform(outputs)

input_scale = sc_in.scale_
input_min = sc_in.data_min_

output_scale = sc_out.scale_
output_min = sc_out.data_min_

typ = 'Test' 

#Storing scaling variables for NN 
# f = h5py.File('GPP_NN/TEMPO_'+typ+'_ML_Scaling.h5','w')
f = h5py.File('GPP_NN/TEMPO_'+typ+'_ML_Scaling.h5','w')
f.create_dataset('Input_Scale',data=input_scale)
f.create_dataset('Input_Min',data=input_min)
f.create_dataset('Output_Scale',data=output_scale)
f.create_dataset('Output_Min',data=output_min)
f.close()

#Splitting training data for model training 
inputs_train,inputs_test,outputs_train,outputs_test = train_test_split(inputs,outputs,test_size=0.7)

del inputs_test, outputs_test
gc.collect()

n_samples, n_features = inputs_train.shape


print('training matrix: ')
print(inputs_train.shape)


BATCH_SIZE =128
EPOCHS = 250
INIT_LR = 1e-4
MAX_LR = 1e-2

#Setting up model architecture and training
nn_inp = tf.keras.Input(shape=(n_features))
out = tfp.layers.DenseVariational(64, posterior, prior, kl_weight=1/(n_samples/BATCH_SIZE), activation='relu')(nn_inp)
out = tfp.layers.DenseVariational(128, posterior, prior, kl_weight=1/(n_samples/BATCH_SIZE), activation='relu')(out)
out = tfp.layers.DenseVariational(64, posterior, prior, kl_weight=1/(n_samples/BATCH_SIZE), activation='relu')(out)
out  = tf.keras.layers.Dense(units=2)(out)
out = tfp.layers.IndependentNormal(1)(out)

callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=20)


steps_per_epoch = n_samples // BATCH_SIZE
clr = tfa.optimizers.CyclicalLearningRate(initial_learning_rate=INIT_LR,
    maximal_learning_rate=MAX_LR,scale_fn=lambda z: 1/(2.**(z-1)),step_size= 7)


model = tf.keras.Model(nn_inp, out, name="LSTM")                                                                                                                                                                            
model.compile(loss=custom_loss, optimizer=tf.optimizers.Adam(clr), metrics=['mae','mse','acc','mape'])                                                                                                                                                     
history = model.fit(inputs_train, outputs_train, epochs=EPOCHS, batch_size=BATCH_SIZE,validation_split=0.2)
model.save('GPP_NN/TEMPO_'+typ+'_ML_model')

tf.keras.backend.clear_session()
