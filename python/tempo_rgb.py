import earthaccess
import h5py
import numpy as np
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from pylab import *
import gc 
from pathlib import Path
import sys
earthaccess.login(persist=True)

year = int(sys.argv[1])
month = int(sys.argv[2])
day = int(sys.argv[3])

date_str = str(year)+'-'+str(month).zfill(2)+'-'+str(day).zfill(2)
results = earthaccess.search_data(
    short_name =  'TEMPO_RAD_L1',
    version = 'V03',
    cloud_hosted=True,
    temporal=(date_str+" 00:00:00", date_str+" 23:59:00"),
    count=140
)

files = earthaccess.open(results)


for file in files:
    print(file)
    with h5py.File(file, 'r') as f:

        nline, nrow = np.array(f['cloud_mask_group/red'][:]).shape
 
        
        red = f['cloud_mask_group/red'][:].flatten()
        blue = f['cloud_mask_group/blue'][:].flatten()
        green = f['cloud_mask_group/green'][:].flatten()

       
        
        rgb = np.dstack((red,green,blue))[0].reshape(nline,nrow,3)
  
        
        out_file = 'TEMPO_RGB/'+str(year)+'/'+str(month).zfill(2)+'/'+str(day).zfill(2)+'/'
  
        Path(out_file).mkdir(parents=True, exist_ok=True)
        
        d = h5py.File(out_file+file.full_name.split('/')[-1].replace('TEMPO_RAD_L1','TEMPO_RGB_L1'),'w')
        d.create_dataset('RGB',data=rgb,dtype=np.float32)
        d.close()
    del red, green, blue, rgb, d, f, out_file 
    gc.collect()  