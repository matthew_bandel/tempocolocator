from scipy import interpolate
import scipy 
import numpy as np 
import gc 

def smooth_l1b(smooth_waves_uv,smooth_waves_red,rad_uv,rad_red,waves_uv,waves_red,irr_uv,irr_red,irr_waves_uv,irr_waves_red,tempo_water_mask,land,fwhm=1,fine_spacing=0.2):
    

    nrow, nline, nwave = rad_uv.shape
    interp_rad_uv = np.array(np.zeros((nrow,nline,len(smooth_waves_uv))) + np.nan,dtype=np.float32)
    interp_rad_red = np.array(np.zeros((nrow,nline,len(smooth_waves_red))) + np.nan,dtype=np.float32)

    
    for i in range(nline):

        inds = ~np.isnan(irr_uv[i,:])
        if len(irr_uv[i,:][inds]) == 0:
            continue

        fn = interpolate.interp1d(irr_waves_uv[i,:][inds],irr_uv[i,:][inds],kind='linear',bounds_error=False,fill_value=np.nan)
        irr_interp_uv = fn(waves_uv[i,:])

        inds = ~np.isnan(irr_red[i,:])
        if len(irr_red[i,:][inds]) == 0:
            continue

        fn = interpolate.interp1d(irr_waves_red[i,:][inds],irr_red[i,:][inds],kind='linear',bounds_error=False,fill_value=np.nan)
        irr_interp_red = fn(waves_red[i,:])
                                          
        if land:
            water_inds = (tempo_water_mask[:,i] > -1)
        elif not land:
            water_inds = (tempo_water_mask[:,i] != 1)
        uv_fine = np.arange(314,486+fine_spacing,fine_spacing,dtype=np.float32).round(1)
        red_fine = np.arange(544,736+fine_spacing,fine_spacing,dtype=np.float32).round(1)

        uv_inds = np.array([np.where(uv_fine == smooth_waves_uv[i])[0] for i in range(len(smooth_waves_uv))]).flatten()
        red_inds = np.array([np.where(red_fine == smooth_waves_red[i])[0] for i in range(len(smooth_waves_red))]).flatten()

        nsamples_uv, nwaves_uv = rad_uv[:,i,:].shape
        nsamples_red, nwaves_red = rad_red[:,i,:].shape
 
    
        
        common_grid_uv = np.zeros((nsamples_uv,len(uv_fine))) + np.nan
        fn = interpolate.interp1d(waves_uv[i,:],np.array(rad_uv[:,i,:]/np.repeat(irr_interp_uv[np.newaxis,:],nrow,axis=0))[water_inds,:],axis=1,kind='linear')
        common_grid_uv[water_inds,:] = fn(uv_fine)


        common_grid_red = np.zeros((nsamples_red,len(red_fine))) + np.nan
        fn = interpolate.interp1d(waves_red[i,:],np.array(rad_red[:,i,:]/np.repeat(irr_interp_red[np.newaxis,:],nrow,axis=0))[water_inds,:],axis=1,kind='linear')
        common_grid_red[water_inds,:] = fn(red_fine)

        inds, = np.where(np.isnan(np.mean(common_grid_red,axis=1)))

        for irow in inds:
            if tempo_water_mask[irow,i]  == 1:
                continue
            tmp_rad = np.array(rad_red[:,i,:]/np.repeat(irr_interp_red[np.newaxis,:],nrow,axis=0))[irow,:]
            if len(tmp_rad[np.isnan(tmp_rad)]) > 0:
                inds = np.isnan(common_grid_red[irow,:])
                if len(tmp_rad[~np.isnan(tmp_rad)]) < 1000:
                    continue
                common_grid_red[irow,inds] = np.interp(red_fine[inds], waves_red[i,:][~np.isnan(tmp_rad)],tmp_rad[~np.isnan(tmp_rad)])

        inds, = np.where(np.isnan(np.mean(common_grid_uv,axis=1)))
        for irow in inds:
            if tempo_water_mask[irow,i]  == 1:
                continue
            tmp_rad = np.array(rad_uv[:,i,:]/np.repeat(irr_interp_uv[np.newaxis,:],nrow,axis=0))[irow,:]
            if len(tmp_rad[np.isnan(tmp_rad)]) > 0:
                inds = np.isnan(common_grid_uv[irow,:])
                if len(tmp_rad[~np.isnan(tmp_rad)]) < 1000:
                    continue
                common_grid_uv[irow,inds] = np.interp(uv_fine[inds], waves_uv[i,:][~np.isnan(tmp_rad)],tmp_rad[~np.isnan(tmp_rad)])

        boxcar_width = fwhm*2  # in nm                                                                                                                                                          

        nsamples = len(common_grid_uv[:,0])
        boxcar_uv = np.ones(int(boxcar_width / fine_spacing)) / int(boxcar_width / fine_spacing)
        boxcar_red = np.ones(int(boxcar_width /fine_spacing)) / int(boxcar_width / fine_spacing)

        for samp in range(nsamples):
            interp_rad_uv[samp,i,:] = scipy.signal.convolve(common_grid_uv[samp,:], boxcar_uv, mode='same')[uv_inds]
            interp_rad_red[samp,i,:] = scipy.signal.convolve(common_grid_red[samp,:], boxcar_red, mode='same')[red_inds]
            
    del common_grid_uv, common_grid_red,uv_fine, red_fine
    
    return interp_rad_uv, interp_rad_red


