#!/usr/bin/env python3

# tesseract.py to extract info from GPP plots

# import hydra
from hydras import Hydra

# import tesseract
from PIL import Image
import pytesseract
import cv2

# get site paths
hydra = Hydra()
paths = hydra._see('../diurnal/plots')
paths.sort()

# begin statistics
name = '../sites/statistics.json'
statistics = hydra._load(name)

# begin manual entry
nameii = '../sites/manual.json'
manual = hydra._load(nameii)

# for each path
for path in paths:

    # extract site
    site = hydra._file(path).split('_')[0]

    # if path missing
    if site not in statistics.keys():

        # Load and preprocess the image
        image = cv2.imread(path)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        _, binary = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY)
        image = cv2.medianBlur(binary, 3)

        # extract text
        text = pytesseract.image_to_string(image)

        # if bias and RMSE in text
        if ('bias' in text) and ('RMSE' in text):

            # extract line with bias
            lines = text.split('\n')
            line = [line for line in lines if 'bias' in line][0]

            # try to
            try:

                # remove spaces
                line = line.replace(' ', '')

                # begin extract
                extract = ''

                # for each character
                for character in line:

                    # if a digit
                    if (character.isdigit()) or (character in ('.', '-')):

                        # add to extact
                        extract += character

                    # otherwise
                    else:

                        # add X
                        extract += 'X'

                # extract numbers
                numbers = [fragment for fragment in extract.split('X') if len(fragment) > 0]

                # extract scores
                score = float(numbers[0])
                bias = float(numbers[1])
                error = float(numbers[2])

                # default samles to 0 and calculate ratio
                samples = 0
                ratio = error / abs(bias)

                # add entry
                details = {'score': score, 'error': error, 'bias': bias, 'ratio': ratio, 'samples': samples}
                statistics[site] = details
                hydra._dump(statistics, name)

                # print details
                print(site, details)

            # otherwise
            except (ValueError, IndexError):

                # abort
                print('aborted {}'.format(site))

                # if site was manaully entered
                if site in manual.keys():

                    # add manual entry
                    statistics[site] = manual[site]

                # otherwise
                else:

                    # print site
                    print('{}:'.format(site))

                    # input numbers
                    score = float(input('R^2 score?'))
                    bias = float(input('bias?'))
                    error = float(input('error?'))
                    samples = 0
                    ratio = error / abs(bias)

                    # create manual entry
                    details = {'score': score, 'error': error, 'bias': bias, 'ratio': ratio, 'samples': samples}
                    statistics[site] = details
                    manual[site] = details
                    hydra._dump(statistics, name)
                    hydra._dump(manual, nameii)

