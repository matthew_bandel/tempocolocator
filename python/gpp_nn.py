import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPRegressor
import warnings
import joblib
"""
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Dense
from tensorflow.keras import layers
from tensorflow.keras import activations
from tensorflow.keras.callbacks import EarlyStopping
"""

def gpp_nn(bands, gpp, name, ind_train, ind_test, par, niter=5, use_band=None,  
            dir='', nnode=None, bands_pred=None, par_pred=True, use_temp=False,  
            use_vpd=False, temp=None, vpd=None, do_save=True, mlpr=True, verbose=True):

    """
    Example usage:
    gpp_nn(bands, gpp, name, gpp_trained, ind_train, ind_test, par, 
         use_band=use_band, 
         niter=niter,
         par_pred=par_pred, 
         dir=dir, nnode=nnode, 
         do_save=do_save, mlpr=mlpr)
    """
    n_points = len(gpp)
    ninput = 0

    if use_band != None:
        nband = len(use_band)
    else:
        nband = len(bands[:, 0])
        use_band = list(range(nband))

    if par_pred:
         ninput += 1

    if use_temp:
         ninput += 1

    if use_vpd:
         ninput += 1

    ninput += nband 

    # Create an array to store features
    features_all = np.zeros((n_points, ninput), dtype=float)
    nind = 0

    for iband in range(nband):
        features_all[:, iband] = bands[use_band[iband], :]
         
    nind += nband

    if par_pred:
        features_all[:, nind] = par
        nind += 1

    if use_temp:
        features_all[:, nind] = temp
        nind += 1

    if use_vpd:
        features_all[:, nind] = vpd
        nind += 1

    features = features_all[ind_train, :]

    if nnode is None:
        nnode = ninput * 2
        if nnode > 30:
            nnode = ninput

    noutput = 1

    if bands_pred != None:
        noutput =+ len(bands_pred[0])

    scores = np.zeros((len(ind_train),noutput), dtype=float)

    if bands_pred != None:
        scores[:, 0:nband_out] = bands_pred[:, ind_train]
    else:
        scores[:, 0] = gpp[ind_train]

    # Normalize the input features using Variance Normalizer
    Normalizer1 = StandardScaler()
    features_normalized = Normalizer1.fit_transform(features)

    # Normalize the output scores using Variance Normalizer
    Normalizer2 = StandardScaler()
    scores_normalized = Normalizer2.fit_transform(scores).ravel()

    seed = 123456789
    print(' ')
    print(nnode, ' nodes ', ninput, ' inputs ', len(ind_train), ' points')

    if (mlpr):
        #Cannot do different activation functions at different layers with MLPRegressor
        # Define the neural network model
        """
        def SoftSign(x):
            return x / (1 + np.abs(x))
        def BentIdentity(x):
            return (np.sqrt(x**2 + 1) - 1) / 2 + x
        """
        hidden_layer_sizes = (nnode, nnode+1)
        #activation_functions = ('logistic')
        #activation_functions = ('tanh') # works pretty well
        activation_functions = ('relu')
        batch_size = 32

        model = MLPRegressor(hidden_layer_sizes = hidden_layer_sizes,
                     activation = activation_functions,
                     solver='adam',
                     alpha=0.0001,
                     learning_rate_init=0.01, max_iter=niter,
                     batch_size = batch_size,
                     early_stopping = True, verbose = verbose,
                     random_state=seed)

        # Train the neural network
        model.fit(features_normalized, scores_normalized)

    else:
        """
        EarlyStopping keeps track of what the NN is learning. If the model isn't learning
        anything new have so many iterations or epochs, it will stop running. Patience
        is the number of iterations it keeps track of to monitoring if learning is still occuring.
        If after 5 iterations the model isn't improving, it stops.
        I have set earlystopping to restore the best weights. The metric EarlyStopping is
        keeping track of is 'val_loss'
        es = EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)
        es = EarlyStopping(monitor='val_loss', patience=10, restore_best_weights=True)
        selecting the optimizer and the learning rate. I forgot to change the learning rate back to
        0.1 to be consistent with the paper.
        opt = keras.optimizers.Adam(learning_rate=0.1) # didn't work too well
        """
        es = EarlyStopping(monitor='loss', patience=5, restore_best_weights=True)
        opt = keras.optimizers.Adam(learning_rate=0.01)
        metrics = ['mae', 'mse']
        metrics = ['logcosh']

        #adding layers with twice as many neurons as predictor features
        #setting the activation functions similarly to the paper
        kerasmodel = Sequential()
        #kerasmodel.add(Dense(nnode, input_dim=n_input+nband, activation='softsign'))
        kerasmodel.add(Dense(nnode, input_dim=ninput+nband, activation='tanh'))
        #kerasmodel.add(Dense(nnode, input_dim=n_input+nband, activation='sigmoid'))
        kerasmodel.add(Dense(nnode+1, activation='sigmoid'))
        #kerasmodel.add(Dense(1, activation='sigmoid')) # didn't work well as well as edu
        kerasmodel.add(Dense(1, activation='elu'))
        #kerasmodel.add(Dense(1, activation='tanh')) # didn't work as well as elu
        #kerasmodel.add(Dense(1, activation='swish')) # didn't work as well as elu

        kerasmodel.compile(loss='mean_squared_error', optimizer=opt, metrics=metrics)
        results = kerasmodel.fit( #X_train, y_train,
                         features_normalized, scores_normalized,
                         #epochs=20,
                         #batch_size=5, # slowed down convergence
                         #batch_size=32,
                         #validation_data=(X_test, y_test),
                         callbacks=[es])

        """
        plt.plot(results.history['loss'], label='loss')
        plt.plot(results.history['val_loss'], label='val_loss')
        plt.legend()
        plt.savefig("GPP_loss_hist.png")
        """

    # Normalize the entire dataset
    data_normalized = Normalizer1.transform(features_all)

    # Pass the entire dataset through the model
    if (mlpr):
        gpp_trained = model.predict(data_normalized)
    else:
        gpp_trained = kerasmodel.predict(data_normalized)

    # transform back
    gpp_trained = Normalizer2.inverse_transform(gpp_trained.reshape(-1,1)).ravel()

    # Save the trained model and normalizers
    if (do_save):
       print('saving NN model', name)
       model_filename = dir + 'model_gpp_' + name + '.joblib'
       normalizer1_filename = dir + 'normalizer1_gpp_' + name + '.joblib'
       normalizer2_filename = dir + 'normalizer2_gpp_' + name + '.joblib'

       joblib.dump(model, model_filename)
       joblib.dump(Normalizer1, normalizer1_filename)
       joblib.dump(Normalizer2, normalizer2_filename)
       print('done')

    return gpp_trained
