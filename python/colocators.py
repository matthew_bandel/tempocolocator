#!/usr/bin/env python3

# colocators.py for the Colocator class to colocate modis variable tempo grid

# import garbage collection
import gc

# import general tools
import sys
import os

# import requests
import requests

# import HYdra
from hydras import Hydra

# import warning module
import warnings

# import regex
import re

# import time and datetime
import time
import datetime

# import numpy and math, scipy
import numpy
import math
import scipy

# import scipy nearest neighbors
from scipy.interpolate import NearestNDInterpolator
from scipy.interpolate import interpn

# import yaml
import yaml

# import netcdf
import netCDF4

# import matplotlib for plots
import matplotlib
from matplotlib import pyplot
from matplotlib import style as Style
from matplotlib import rcParams
matplotlib.use('Agg')
Style.use('fast')
rcParams['axes.formatter.useoffset'] = False
rcParams['figure.max_open_warning'] = False

# import cartopy
import cartopy

# try to
try:

    # import sagemaker for S3 bucket manipulation
    import boto3

    # import from sagemaker
    from sagemaker.s3 import S3Downloader

# unless not in Sagemaker
except ImportError:

    # print warning
    print('not connected to Sagemaker!')


# class Colocator to colocate MODIS data to TEMPO grid
class Colocator(list):
    """Colocator class to colocate MODIS data to TEMPO grid

    Inherits from:
        list
    """

    def __init__(self, sink='.', year=2023, month=8, day=18, variable='chlorophyll', configuration='modis.yaml',
                 averaging=True, grid=None, field='GPP/10', coordinates=('lat', 'lon')):
        """Initialize a Colocator instance.

        Arguments:
            sink: str, path to sink folder
            year: int, the year
            month: int, the month
            day: int, the day
            variable: str, the variable name
            configuration: str, file path to configuration file
            averaging: boolean, use averaging method instead of composite?
            grid: str, path for arbitrary gridded data file ( non modaps )
            field: str, address, name, and array index of variable in directory/name/index format
            coordinates: latitude and longitude coordinate names for

        Returns:
            None
        """

        # set current time
        self.now = time.time()

        # set fill value
        self.fill = -32767

        # set initialized value
        self.initialized = False

        # set sink
        self.sink = sink or '.'

        # create datetime object
        self.year = year
        self.month = month
        self.day = day
        self.date = None

        # set variable name and configuration file
        self.variable = variable
        self.configuration = configuration

        # designate yaml file attributes
        self.yam = {}
        self.names = {}
        self.folders = {}
        self.bases = {}
        self.templates = {}
        self.variables = {}
        self.units = {}
        self.abbreviations = {}

        # establish compositing method
        self.averaging = averaging
        self.method = 'average' * averaging + 'composite' * (not averaging)

        # allocate for gap filling hierarchy
        self.hierarchy = []

        # set grid file attributes
        self.grid = grid
        self.field = field
        self.coordinates = coordinates

        # allocate for modis, etc site addresses
        self.sites = {}
        self.before = {}
        self.after = {}

        # set stack of satellite data
        self.stack = None
        self.latitude = None
        self.longitude = None

        # measure memory size
        self._measure('beginning')

        # set default options
        self.gems = False
        self.smoothed = True
        self.target = ''

        # reserve for tempo / gems file paths and destination paths
        self.originals = []
        self.paths = []
        self.destinations = []
        self.dates = []
        self.buckets = {}
        self.designations = {}

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Colocator instance at: {}, {} >'.format(self.sink, self.variable)

        return representation

    def _acquire(self, path):
        """Load in a yaml file.

        Arguments:
            path: str, filepath

        Returns:
            dict

        """

        # default information to empty
        information = {}

        # try to
        try:

            # open yaml file
            with open(path, 'r') as pointer:

                # and read contents
                information = yaml.safe_load(pointer)

        # unless not found
        except FileNotFoundError:

            # in which case print error
            self._print('yaml {} not found!'.format(path))

        return information

    def _adjust(self, date):
        """Adjust satellite fill hierarchy based on closeness to original time.

        Arguments:
            date: datetime.datetime object

        Returns:
            None

        Sets:
            self.hierarchy
        """

        # get sites based on initial hierarchy
        sites = [(satellite, self.sites[satellite]) for satellite in self.hierarchy]

        # split into those with valid saites
        valids = [pair for pair in sites if pair[1]]
        invalids = [pair for pair in sites if not pair[1]]

        # for each valid site
        triplets = []
        for satellite, site in valids:

            # get the time start coverage from the file
            self._stamp('getting start time from {}...'.format(site), initial=True)
            net = netCDF4.Dataset(site)
            start = net.getncattr('time_coverage_start')
            net.close()
            self._stamp('time: {}'.format(start))

            # make datetime from coverage string
            dateii = self._distil(start)

            # create triplet entry
            triplet = (satellite, site, abs(date - dateii))
            triplets.append(triplet)

        # sort by shortest time delta
        triplets.sort(key=lambda triplet: triplet[-1])

        # recreate hierarchy
        hierarchy = [triplet[0] for triplet in triplets] + [pair[0] for pair in invalids]
        self.hierarchy = hierarchy

        return None

    def _clean(self, directory, force=False):
        """Delete all files in a directory and the directory itself.

        Arguments:
            directory: str, directory path
            force=True: boolean, avoid asking

        Returns:
            None
        """

        # try to
        try:

            # get all paths in the directory
            paths = self._see(directory)

        # unless not a directory
        except OSError:

            # set path to just file
            paths = [directory]

        # set default prompt
        prompt = 'X'

        # if there are paths
        if len(paths) > 0:

            # if not forcing delete
            if not force:

                # prompt user and continue on blank
                prompt = input('erase {} !?>>'.format(directory))

            # if blank reply or forcing
            if prompt in ('', ' ') or force:

                # remove all files
                [os.remove(path) for path in paths]

        return None

    def _construct(self, satellite, date):
        """Construct a new site address based on the modis address.

        Arguments:
            satellite: str, satellite abbriviation
            date: str, date in yyyy/mmdd format

        Returns:
            str
        """

        # get info from yaml
        base = self.bases[satellite]
        name = self.names[satellite]
        folder = self.folders[satellite]
        template = self.templates[satellite]

        # make replacements
        site = template.replace('BASE', base).replace('FOLDER', folder).replace('NAME', name)
        site = site.replace('YYYY', date[:4]).replace('MM', date[4:6]).replace('DD', date[6:8])
        site = site.replace('VVV', self.abbreviations[satellite][self.variable])
        site = site.replace('VARIABLE', self.variables[satellite][self.variable])

        # attempt to contact site
        valid = self._request(site)

        # if not valid
        if not valid:

            # try nrt site
            site = site.replace('.nc', '.NRT.nc')
            valid = self._request(site)

            # if not valid
            if not valid:

                # site is blank
                site = ''

        return site

    def _distil(self, string):
        """Extract a datetime object from a string.

        Arguments:
            string: str, date in format yyyymmdd_HHMM or yyyy-mm-ddTHH:MM

        Returns:
            datetime.datetime object
        """

        # try to
        try:

            # if underscore in string
            if '-' not in string:

                # extract datetime parts from string, assuming ( yyyymmdd_HHMM or yyyymmddTHHMM )
                year = int(string[0:4])
                month = int(string[4:6])
                day = int(string[6:8])
                hour = int(string[9:11])
                minute = int(string[11:13])

            # otherwise
            else:

                # assume yyyy-mm-ddTHH:MM
                year = int(string[0:4])
                month = int(string[5:7])
                day = int(string[8:10])
                hour = int(string[11:13])
                minute = int(string[14:16])

        # unless ValueError
        except(ValueError):

            # try to
            try:

                # in which case, try to extract date
                search = re.search('[0-9]{8}', string).group()
                year = int(search[:4])
                month = int(search[4:6])
                day = int(search[6:8])
                hour = 0
                minute = 0

                # and try to find hour
                search = re.search('T[0-9]{4}', string).group()
                hour = int(search[1:3])
                minute = int(search[3:5])

            # unless no results
            except AttributeError:

                # in which case, use dummy values
                year = 1
                month = 1
                day = 1
                hour = 0
                minute = 0

        # construct datetime
        date = datetime.datetime(year=year, month=month, day=day, hour=hour, minute=minute)

        return date

    def _establish(self):
        """Establish a gap filing hierarchy.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.hierarchy
        """

        # create gap filling hierarchy
        hierarchy = ['Aqua', 'J2', 'Terra', 'J1', 'S3']
        self.hierarchy = hierarchy

        return None

    def _file(self, path):
        """Get the filename from a path.

        Arguments:
            path: str, pathname

        Returns:
            str, fio name
        """

        # get fileame
        name = path.split('/')[-1]

        return name

    def _fold(self, path):
        """Break apart a path name into directory and file.

        Arguments:
            path: str, pathname

        Returns:
            str, directory
        """

        # get fileame
        words = path.split('/')

        # get folder
        folder = '/'.join(words[:-1])

        return folder

    def _grab(self, path, original):
        """Grab the path from S3 and download to local folder.

        Arguments:
            path: str, file path
            original: original S3 file path

        Returns:
            None
        """

        # if path and original are different
        if path != original:

            # get folder
            folder = self._fold(path)

            # set bucket
            bucket = self.buckets[self.target]

            # check for path in folder
            names = [self._file(member) for member in self._see(folder)]
            if self._file(original) not in names:

                # print download message
                self._stamp('downloading {} from S3...'.format(original), initial=True)

                # Use the S3Downloader to download locally
                address = 's3://{}/{}'.format(bucket, original)
                paths = S3Downloader.download(local_path=folder, s3_uri=address)
                path = paths[0]

                # print download message
                self._stamp('downloaded.')

        return None

    def _initialize(self):
        """Perform initialization.

        Arguments:
            None
        """

        # set initialized status
        self.initialized = True

        # if using modaps sites instead of given file
        if not self.grid:

            # create date
            date = datetime.datetime(self.year, self.month, self.day)
            self.date = date

            # establish initial gap filling hierarchy
            self._establish()

            # parse configuration yaml
            self._parse(self.configuration)

            # prepare site addresses
            self._prepare()

        return None

    def _interpolate(self, quantity, latitude, longitude, latitudeii, longitudeii, blocks=1):
        """Colocate a quantity given by a regular grid onto arbitrary coordinates by nearest neightbors.

        Arguments:
            quantity: 2-d numpy array, quantity of interest
            latitude: 1-D numpy array, latitude grid points
            longitude: 1-D numpy.array, longitude grid points
            latitudeii: numpy array, target latitude coordinates
            longitudeii: numpy array, target longitude coordinates
            blocks: int, number of separately interpolated blocks

        Returns:
            tuple of numpy array, gridded quantity and corrdinates with fill
        """

        # set the grid shapes
        shape = (latitude.shape[0], longitude.shape[0])
        shapeii = latitudeii.shape

        # construct latitude mask
        self._stamp('\nextracting valid geo coordinates...')
        mask = numpy.ones(latitudeii.shape).astype(bool)
        if latitudeii.mask.sum() > 0:

            # construct from inversion
            mask = numpy.logical_not(latitudeii.mask)

        # construct longitude mask
        maskii = numpy.ones(longitudeii.shape).astype(bool)
        if longitudeii.mask.sum() > 0:

            # construct from inversion
            maskii = numpy.logical_not(longitudeii.mask)

        # combine masks
        masque = mask & maskii

        # apply masks to tempo data to get valid data
        latitudeii = latitudeii[masque]
        longitudeii = longitudeii[masque]

        # set up interpolation blocks
        size = int(numpy.ceil(latitudeii.shape[0] / blocks))
        brackets = [(index * size, size + index * size) for index in range(blocks)]

        # for each block
        interpolations = []
        for index, (first, last) in enumerate(brackets):

            # use scipy nearest neighbors
            self._stamp('using nearest neighbors, block {} of {}...'.format(index, blocks))
            parameters = ((latitude, longitude), quantity, (latitudeii[first: last], longitudeii[first: last]))
            interpolation = interpn(*parameters, method='nearest', bounds_error=False, fill_value = self.fill)

            # add to collection
            interpolations.append(interpolation)

            # print status
            self._stamp('calculated.')

        # concatenate interpolations
        quantityii = numpy.hstack(interpolations)

        # set fill value
        quantityii.fill_value = self.fill

        # create array of fill values
        fills = numpy.ones(masque.shape) * self.fill
        fills[numpy.where(masque)] = quantityii
        quantityii = fills.reshape(shapeii)

        # create array of fill values for latitude
        fills = numpy.ones(masque.shape) * self.fill
        fills[numpy.where(masque)] = latitudeii.squeeze()
        latitudeii = fills.reshape(shapeii)

        # create array of fill values for longitude
        fills = numpy.ones(masque.shape) * self.fill
        fills[numpy.where(masque)] = longitudeii.squeeze()
        longitudeii = fills.reshape(shapeii)

        return quantityii, latitudeii, longitudeii

    def _make(self, folder):
        """Make a folder in the directory if not yet made.

        Arguments:
            folder: str, directory path

        Returns:
            None
        """

        # try to
        try:

            # create the directory
            os.mkdir(folder)
            self._print('made {}'.format(folder))

        # or if part of the directory is not found
        except FileNotFoundError:

            # in which case, print error
            self._print('error making {}, file not found error!'.format(folder))

        # or if there are permissions issues
        except PermissionError:

            # in which case, print error
            self._print('error making {}, permissions error!'.format(folder))

        # unless directory already exists
        except FileExistsError:

            # in which case, nevermind
            pass

        return None

    def _measure(self, message=''):
        """Measure the memory size of all objects.

        Arguments:
            message: str, message to add

        Returns:
            None
        """

        # initialize size
        size = 0

        # for each object
        for element in gc.get_objects():

            # add to size
            size += sys.getsizeof(element)

        # print
        self._print('{}: total size: {} MB'.format(message, size / 1e6))

        return None

    def _pad(self, number, length=2):
        """Pad a number by converting to string and zfilling.

        Arguments:
            number: int
            length: length of final number

        Returns:
            str
        """

        # convert to str
        pad = str(number)

        # zfill
        pad = pad.zfill(length)

        return pad

    def _parse(self, configuration):
        """Establish dictionary of variables names and abbreviations.

        Arguments:
            configuration: str, name of yaml file

        Returns:
            None

        Populates:
            self.variables, self.abbreviations
        """

        # get yaml contents
        yam = self._acquire(configuration)
        self.yam = yam

        # set satellite names
        names = {satellite: yam[satellite]['name'] for satellite in self.hierarchy}
        self.names = names

        # set satellite folders
        folders = {satellite: yam[satellite]['folder'] for satellite in self.hierarchy}
        self.folders = folders

        # set base urls
        bases = {satellite: yam[satellite]['base'] for satellite in self.hierarchy}
        self.bases = bases

        # set file templates
        templates = {satellite: yam[satellite]['template'] for satellite in self.hierarchy}
        self.templates = templates

        # set up variables
        variables = {}
        units = {}
        for satellite in self.hierarchy:

            # construct varialbes
            information = yam[satellite]['variables']
            collection = {variable: details['name'] for variable, details in information.items()}
            variables[satellite] = collection
            collection = {variable: details['units'] for variable, details in information.items()}
            units[satellite] = collection

        # set up variable abbreviations
        abbreviations = {}
        for satellite in self.hierarchy:

            # construct varialbes
            information = yam[satellite]['variables']
            collection = {variable: details['abbreviation'] for variable, details in information.items()}
            abbreviations[satellite] = collection

        # set attributes
        self.variables = variables
        self.abbreviations = abbreviations
        self.units = units

        return None

    def _populate(self):
        """Populate file paths.

        Arguments:
            path: str, path to grid source

        Returns:
            None

        Populates:
            self.path, self.destination, self.buckets, self.folders, self.designations
        """

        # populate gems / tempo information
        self.buckets = {'GEMS': 'gems-oceans', 'TEMPO': 'gems-oceans'}
        self.designations = {'GEMS': 'Ancillary.nc', 'TEMPO': 'Ancillary.nc'}
        folders = {'GEMS': 'gems', 'TEMPO': 'tempo'}

        # set date search strings
        searches = {'GEMS': '[0-9]{8}_[0-9]{4}', 'TEMPO': '[0-9]{8}T[0-9]{4}'}

        # get all names in the bucket
        bucket = self.buckets[self.target]
        client = boto3.client('s3')

        # set up objects
        continuation = True
        contents = []

        # set initial parameters
        parameters = {'Bucket': bucket}

        # while a continuation token is generated
        while continuation:

            # retrieve file paths and add to contents
            response = client.list_objects_v2(**parameters)
            contents += response.get('Contents', [])

            # if there are new entries
            if 'NextContinuationToken' in response.keys():

                # replace token and add to parameters
                token = response['NextContinuationToken']
                parameters.update({'ContinuationToken': token})

            # otherwise
            else:

                # don't continue
                continuation = False

        # get subset of paths according to target
        paths = [entry['Key'] for entry in contents]
        paths = [path for path in paths if path.startswith(self.target)]

        # remove possible duplicates and sort
        paths = self._skim(paths)

        # if using smoothed files
        if self.smoothed:

            # only keep paths with smoothed designation
            paths = [path for path in paths if self.designations[self.target] in self._file(path)]
            paths.sort()

        # otherwise
        else:

            # disregard files with smoothed designation
            paths = [path for path in paths if self.designations[self.target] not in self._file(path)]
            paths.sort()

        # extract dates and times from gems files, in yyyymmdd_HHMM format
        dates = [re.search(searches[self.target], path) for path in paths]

        # for each file and date
        for name, date in zip(paths, dates):

            # convert date to datetime
            date = self._distil(date.group())

            # if date is equal
            if (date.year, date.month, date.day) == (self.date.year, self.date.month, self.date.day):

                # create the folder
                folder = '{}/{}'.format(self.sink, folders[self.target])
                self._make(folder)

                # create path
                path = '{}/{}'.format(folder, name)

                # set path and date
                self.paths.append('{}/{}'.format(folder, self._file(path)))
                self.dates.append(date)
                self.originals.append(name)

                # set destination file
                self._make('{}/colocation'.format(self.sink))
                formats = [self.sink, self.target, date.year, self._pad(date.month), self._pad(date.day)]
                formats += [self._pad(date.hour), self._pad(date.minute), self.method]
                self.destinations.append('{}/colocation/MODIS_Chl_Colocated_{}_{}m{}{}t{}{}_{}.nc'.format(*formats))

        return None

    def _prepare(self):
        """Prepare netCDF4 file paths.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.modis
        """

        # timestamp
        self._stamp('checking for valid sites...', initial=True)

        # grab datetime object
        date = self.date

        # construct current date
        formats = (date.year, self._pad(date.month), self._pad(date.day))
        current = '{}{}{}'.format(*formats)

        # create previous date
        previous = date - datetime.timedelta(days=1)
        before = '{}{}{}'.format(previous.year, self._pad(previous.month), self._pad(previous.day))

        # create posterior date
        posterior = date + datetime.timedelta(days=1)
        after = '{}{}{}'.format(posterior.year, self._pad(posterior.month), self._pad(posterior.day))

        # construct other satellite site addresses
        sites = {satellite: self._construct(satellite, current) for satellite in self.hierarchy}
        self.sites = sites

        # construct previous day sites
        sites = {satellite: self._construct(satellite, before) for satellite in self.hierarchy}
        self.before = sites

        # construct posterior day sites
        sites = {satellite: self._construct(satellite, after) for satellite in self.hierarchy}
        self.after = sites

        # timestamp
        self._stamp('sites validated.')

        return None

    def _print(self, *messages):
        """Print the message, localizes print statements.

        Arguments:
            *messagea: unpacked list of str, etc

        Returns:
            None
        """

        # construct  message
        message = ', '.join([str(message) for message in messages])

        # print
        print(message)

        return message

    def _request(self, site):
        """Check the validity of a website.

        Arguments:
            site: str, website

        Returns:
            boolean, good site?
        """

        # assume invalid
        validity = False

        # check response
        response = requests.get(site)

        # if status code checks out
        if response.status_code == 403:

            # set validity to True
            validity = True

        return validity

    def _round(self, quantity, digits=2, up=False):
        """Round a value based on digits.

        Arguments:
            quantity: float
            digits: int, number of post decimal digits to keep
            up: boolean, round up?

        Returns:
            None
        """

        # multiple by power of digtsp
        power = 10 ** digits
        approximation = quantity * 10 ** digits

        # if rounding up
        if up:

            # round up
            approximation = math.ceil(approximation)

        # otherwise
        else:

            # round down
            approximation = math.floor(approximation)

        # divide by power
        approximation = approximation / power

        return approximation

    def _see(self, directory):
        """See all the paths in a directory.

        Arguments:
            directory: str, directory path

        Returns:
            list of str, the file paths.
        """

        # try to
        try:

            # make paths
            paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        # unless the directory does not exist
        except FileNotFoundError:

            # in which case, alert and return empty list
            paths = []

        return paths

    def _skim(self, members, maintain=False):
        """Skim off the unique members from a list.

        Arguments:
            members: list
            maintain: boolean: maintain order?

        Returns:
            list
        """

        # if maintaining order
        if maintain:

            # for each member
            uniques = []
            for member in members:

                # if not already in uniques
                if member not in uniques:

                    # add it
                    uniques.append(member)

        # otherwise
        else:

            # trim duplicates and sort
            uniques = list(set(members))
            uniques.sort()

        return uniques

    def _stamp(self, message, initial=False, clock=True):
        """Start timing a block of code, and print results with a message.

        Arguments:
            message: str
            initial: boolean, initial message of block?
            clock: boolean, include timing in output?

        Returns:
            float
        """

        # get final time
        final = time.time()

        # calculate duration and reset time
        duration = round(final - self.now, 7)

        # if rounding to magnitudes:
        if not clock:

            # set duration to approximation
            duration = 0.0

        # reset tme
        self.now = final

        # if initial message
        if initial:

            # add newline
            message = '\n' + message

        # if not an initial message
        if not initial:

            # print duration
            self._print('took {} seconds.'.format(duration))

        # begin new block
        self._print(message)

        return duration

    def _target(self, gems):
        """Establish the target satellite.

        Arguments:
            gems: boolean, use GEMS?

        Returns:
            None

        Populates:
            self.target
        """

        # default target to tempo
        target = 'TEMPO'

        # if gems:
        if gems:

            # target gems
            target = 'GEMS'

        # populates
        self.target = target

        return None

    def _tell(self, queue):
        """Enumerate the contents of a list.

        Arguments:
            queue: list

        Returns:
            None
        """

        # for each item
        for index, member in enumerate(queue):

            # print
            self._print('{}) {}'.format(index, member))

        # print spacer
        self._print(' ')

        return None

    def _view(self, data, pixel=None, fields=None):
        """Print list of data shapes from a dataset.

        Arguments:
            data: dict of numpy arrays.
            fields: list of str, the fields to see
            pixel: tuple of ints, particular pixel

        Returns:
            None
        """

        # print spacer
        self._print('')

        # set fields
        fields = fields or list(data.keys())

        # for each item
        for name, array in data.items():

            # check for field membership
            if name in fields:

                # try to
                try:

                    # if pariruclar pixel
                    if pixel:

                        # print each one
                        self._print(name, array.shape, '{}: {}'.format(pixel, array[pixel]))

                    else:

                        # print each one
                        self._print(name, array.shape, '{} to {}'.format(array.min(), array.max()))

                # unless error
                except (ValueError, IndexError):

                    # print alert
                    self._print('{}: pass'.format(name))
                    pass

        # print spacer
        self._print('')

        return None

    def average(self):
        """Stack all satellite data into one grid, sequentially filling gaps

        Arguments:
            None

        Returns:
            None
        """

        # assume stack has already been produced
        stack = self.stack
        latitude = self.latitude
        longitude = self.longitude

        # if stack is empty
        if stack is None:

            # set site collections
            collections = [(self.sites,), (self.before,), (self.after,)]

            # for each collection
            for collection in collections:

                # begin arrays
                arrays = []

                # for each satellite
                for satellite in self.hierarchy:

                    # default net to None for later deletion
                    net = None

                    # for each member of the collection
                    for member in collection:

                        # get site address
                        site = member[satellite]

                        # if site is available
                        if site:

                            # get the modis latitude, longitude, and variable
                            self._stamp('\ningesting {} data at site {}...'.format(satellite, site))
                            net = netCDF4.Dataset(site)
                            variable = net[self.variables[satellite][self.variable]][:]

                            # assign fill value
                            variable.fill_value = self.fill

                            # check for defaults
                            if stack is None:

                                # get latitude and longitude
                                latitude = net['lat'][:]
                                longitude = net['lon'][:]

                                # set stack to variable
                                stack = variable

                            # close netcdf4 file
                            net.close()

                            # create dimensions
                            shape = variable.shape

                            # add to arrays
                            arrays.append(variable)

                # if arrays were collecgted
                if len(arrays) > 0:

                    # if chlorophyll
                    if self.variable == 'chlorophyll':

                        # convert to logarithms
                        arrays = [numpy.log(array) for array in arrays]

                        # create new array as average of old
                        average = numpy.ma.array(arrays).mean(axis=0)

                        # take the exoonential
                        average = numpy.exp(average)

                        # create substitution mask
                        substitution = (stack.mask) & (numpy.invert(average.mask))

                        # make substitution
                        stack[substitution] = average[substitution]

                    # otherwise
                    else:

                        # create new array as average of old
                        average = numpy.ma.array(arrays).mean(axis=0)

                        # create substitution mask
                        substitution = (stack.mask) & (numpy.invert(average.mask))

                        # make substitution
                        stack[substitution] = average[substitution]

                # delete unneeded references and collect garbage
                self._measure('before deletion')
                del substitution, average, net, variable, arrays
                gc.collect()
                self._measure('after deletion')

            # populate variable and self
            self.stack = stack
            self.latitude = latitude
            self.longitude = longitude

        return stack, latitude, longitude

    def colocate(self, path, targets, sink, field=None, coordinates=None, coordinatesii=None, verify=True):
        """Perform basic colocation from input file onto a target file.


        Arguments:
            path: str, input gridded data source path name
            targets: str, name of directory of target files
            sink: folder for colocations and plots
            field: str, netcdf address of field + indices
            coordinates: tuple of str, the latitude and longitude field names
            coordinatesii: tuple of str, the latitude and longitude field names for target
            verify: boolean, verify with plot?

        Returns:
            None
        """

        # create folders
        self._make('{}'.format(sink))
        self._make('{}/colocations'.format(sink))
        self._make('{}/plots'.format(sink))

        # set defaults
        field = field or "GPP/0"
        coordinates = coordinates or ("lat", "lon")
        coordinatesii = coordinatesii or ("latitude", "longitude")

        # try to
        try:

            # get the paths in the target directory
            targets = self._see(targets)

        # unless not a directory
        except TypeError:

            # assume targets is a list of paths
            pass

        # print status
        self._stamp('ingesting {}...'.format(path), initial=True)

        # parse field into address and indices
        fragments = field.split('/')
        address = '/'.join([fragment for fragment in fragments if not fragment.isdigit()])
        indices = [int(fragment) for fragment in fragments if fragment.isdigit()]

        # reset field
        field = address.split('/')[-1]

        # get the grid latitude and longitude, and ground pixel quality
        net = netCDF4.Dataset(path)
        latitude = net[coordinates[0]][:]
        longitude = net[coordinates[1]][:]

        # get the data
        quantity = net[address][:]

        # for each index
        for index in indices:

            # subset data
            quantity = quantity[index]

        # close file
        net.close()

        # make sure grid is ordered
        order = numpy.argsort(latitude.flatten())
        orderii = numpy.argsort(longitude.flatten())

        print(order.shape)
        print(orderii.shape)
        print(quantity.shape)

        # put into order
        quantity = quantity[order]
        quantity = quantity[:, orderii]
        latitude = latitude[order]
        longitude = longitude[orderii]

        # for each target in the targets directory
        for target in targets:

            # print status
            self._stamp('colocating {} to {}...'.format(path, target), initial=True)

            # get the target latitude and longitude, and ground pixel quality
            netii = netCDF4.Dataset(target)
            latitudeii = netii[coordinatesii[0]][:]
            longitudeii = netii[coordinatesii[1]][:]
            netii.close()

            # set the grid shapes
            shape = (latitude.shape[0], longitude.shape[0])
            shapeii = latitudeii.shape

            # colocate
            parameters = (quantity, latitude, longitude, latitudeii, longitudeii)
            quantityii, latitudeii, longitudeii = self._interpolate(*parameters, blocks=1)

            # recreate latitude, longitude global grid
            shape = quantity.shape
            grid = numpy.array([latitude] * shape[1]).transpose(1, 0)
            gridii = numpy.array([longitude] * shape[0])

            # subset modis arrays based on extent of target grid
            self._stamp('subsetting...')
            parameters = (quantity, latitude, longitude, latitudeii, longitudeii)
            quantityiii, latitudeiii, longitudeiii, bounds = self.subset(*parameters)

            # recreate subsetted latitude, longitude grid
            shapeiii = (latitudeiii.shape[0], longitudeiii.shape[0])
            zoom = numpy.array([latitudeiii] * shapeiii[1]).transpose(1, 0)
            zoomii = numpy.array([longitudeiii] * shapeiii[0])

            # create dimensions
            dimensions = {'lat': shapeii[0], 'lon': shapeii[1], 'lat_global': shape[0], 'lon_global': shape[1]}
            dimensions.update({'lat_zoom': shapeiii[0], 'lon_zoom': shapeiii[1]})

            # set up file data
            data = {field: quantityii}
            data.update({'latitude': latitudeii, 'longitude': longitudeii})
            data.update({'{}_global'.format(field): quantity})
            data.update({'latitude_global': grid, 'longitude_global': gridii})
            data.update({'{}_zoom'.format(field): quantityiii})
            data.update({'latitude_zoom': zoom, 'longitude_zoom': zoomii})

            self._view(data)

            # stash data
            destination = '{}/colocations/{}'.format(sink, self._file(target).replace('.nc', '_colocated.nc'))
            self.stash(data, dimensions, destination)

            # if seeking verification by plot
            date = re.search('[0-9]{8}T[0-9]{4}', target).group()
            date = datetime.datetime(int(date[:4]), int(date[4:6]), int(date[6:8]))
            if verify:

                # verify with plot
                self.validate(quantity, latitude, longitude, destination, field, date)

        return None

    def composite(self):
        """Stack all satellite data into one grid, sequentially filling gaps

        Arguments:
            None

        Returns:
            None
        """

        # assume stack has already been produced
        stack = self.stack
        latitude = self.latitude
        longitude = self.longitude

        # if stack is empty
        if stack is None:

            # set site collections
            collections = [(self.sites,), (self.before, self.after)]

            # for each collection
            for collection in collections:

                # for each satellite
                for satellite in self.hierarchy:

                    # default net to None for later deletion
                    net = None

                    # begin arrays
                    arrays = []

                    # for each member of the collection
                    for member in collection:

                        # get site address
                        site = member[satellite]

                        # if site is available
                        if site:

                            # get the modis latitude, longitude, and variable
                            self._stamp('\ningesting {} data at site {}...'.format(satellite, site))
                            net = netCDF4.Dataset(site)
                            variable = net[self.variables[satellite][self.variable]][:]

                            # assign fill value
                            variable.fill_value = self.fill

                            # check for defaults
                            if stack is None:

                                # get latitude and longitude
                                latitude = net['lat'][:]
                                longitude = net['lon'][:]

                                # set stack to variable
                                stack = variable

                            # close netcdf4 file
                            net.close()

                            # create dimensions
                            shape = variable.shape

                            # add to arrays
                            arrays.append(variable)

                    # if arrays were collecgted
                    if len(arrays) > 0:

                        # add interpolated average to arrays
                        average = numpy.ma.array(arrays).mean(axis=0)
                        arrays = [average] + arrays

                        # for each array
                        for array in arrays:

                            # create substitution mask
                            substitution = (stack.mask) & (numpy.invert(array.mask))

                            # make substitution
                            stack[substitution] = array[substitution]

                    # delete unneeded references and collect garbage
                    self._measure('before deletion')
                    del substitution, average, net, variable, arrays, array
                    gc.collect()
                    self._measure('after deletion')

            # populate variable and self
            self.stack = stack
            self.latitude = latitude
            self.longitude = longitude

        return stack, latitude, longitude

    def contrast(self, tag):
        """PLot the difference between two compositing methods.

        Arguments:
            tag: str, unique file identifier

        Returns:
            None
        """

        # get colocation files
        colocations = self._see('colocation')

        print(colocations)

        # get the set with the tag
        paths = [path for path in colocations if tag in path]

        print(paths)

        # create plots folder
        sink = '{}/plots'.format(self.sink)
        self._make(sink)

        # create destination file name
        destination = '{}/{}'.format(sink, self._file(paths[0]).replace('.nc', '_contrast.png'))

        # begin status
        self._stamp('plotting {}...'.format(destination), initial=True)

        # erase preevious copy
        self._clean(destination, force=True)

        # get modis data
        net = netCDF4.Dataset(paths[0])
        latitude = net['latitude'][:]
        longitude = net['longitude'][:]
        variable = net[self.variables['Aqua'][self.variable]][:]
        net.close()

        # grab the colocation date
        netii = netCDF4.Dataset(paths[1])
        latitudeii = netii['latitude'][:]
        longitudeii = netii['longitude'][:]
        variableii = netii[self.variables['Aqua'][self.variable]][:]
        netii.close()

        # take difference
        difference = variableii - variable

        # # apply mask to colocation data
        # mask = (latitudeii > self.fill) & (longitudeii > self.fill) & (variableii > self.fill)
        # latitudeii = latitudeii[mask]
        # longitudeii = longitudeii[mask]
        # variableii = variableii[mask]

        # # expand modis latitudes
        # latitude = numpy.array([latitude] * variable.shape[1]).transpose(1, 0)
        # longitude = numpy.array([longitude] * variable.shape[0])

        # # create masks for TEMPO regiom
        # mask = (latitude >= latitudeii.min() - 5) & (latitude <= latitudeii.max() + 5)
        # maskii = (longitude >= longitudeii.min() - 5) & (longitude <= longitudeii.max() + 5)
        # maskiii = (variable <= variableii.max()) & (variable > 0)
        # masque = mask & maskii & maskiii

        # # create date
        # formats = [date.year, self._pad(date.month), self._pad(date.day)]
        # formats += [self._pad(date.hour), self._pad(date.minute)]
        #
        # # create titles
        # titles = ['MODIS {}\n{}'.format(self.variable.capitalize(), date)]
        # titles += ['MODIS+ {} colocated to {} Grid\n{}'.format(self.variable.capitalize(), self.target, date)]

        # set title
        date = destination.split('_')[-3]
        titles = ['Averaged - Composite, {}'.format(date)]

        # # collect data
        # abscissas = [longitude[masque].flatten(), longitudeii.flatten()]
        # ordinates = [latitude[masque].flatten(), latitudeii.flatten()]
        # tracers = [variable[masque].flatten(), variableii.flatten()]
        #
        # # get minimum and maximum tracer values
        # minimum = min([tracers[0].min(), tracers[1].min()])
        # maximum = max([tracers[0].max(), tracers[1].max()])

        # set abscissa
        abscissas = [longitude.flatten()]
        ordinates = [latitude.flatten()]
        tracers = [difference.flatten()]

        # get percentile
        percentile = numpy.percentile(tracers[0], 2)
        percentileii = numpy.percentile(tracers[0], 98)
        absolute = max([abs(percentile), abs(percentileii)])

        # get mi and max
        minimum = -absolute
        maximum = absolute

        # set up figure
        pyplot.clf()
        pyplot.margins(1.0, 0.2)
        figure, axes = pyplot.subplots(1, 1, subplot_kw={'projection': cartopy.crs.PlateCarree()}, figsize=(4, 5))
        pyplot.subplots_adjust(wspace=0.4)

        # for each set
        for abscissa, ordinate, tracer, axis, title in zip(abscissas, ordinates, tracers, [axes], titles):

            # set cartopy axis with coastlines
            axis.coastlines()
            _ = axis.gridlines(draw_labels=True)

            # begin plot with title
            axis.grid(True)
            axis.set_global()
            axis.set_aspect('auto')
            axis.set_title(title, fontsize=10)
            axis.tick_params(axis='both', labelsize=7)

            # set up colors
            colors = matplotlib.cm.seismic
            normal = matplotlib.colors.Normalize(minimum, maximum)

            # plot modis / tempo data
            axis.scatter(abscissa, ordinate, marker='.', s=0.1, c=tracer, cmap=colors, norm=normal)

            # set axis limits
            axis.set_xlim(longitudeii.min() - 5, longitudeii.max() + 5)
            axis.set_ylim(latitudeii.min() - 5, latitudeii.max() + 5)

            # add colorbar
            units = self.units['Aqua'][self.variable]
            position = axis.get_position()
            bar = pyplot.gcf().add_axes([position.x0, position.y0 - 0.06, position.width, 0.02])
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', label=units)
            formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
            colorbar.ax.yaxis.set_major_formatter(formatter)

        # save to destination
        pyplot.savefig(destination)
        pyplot.clf()

        # delete references
        self._measure('before plot deletion')
        del net, netii
        del abscissas, ordinates, tracers
        del abscissa, ordinate, tracer
        del latitude, longitude, variable
        del latitudeii, longitudeii, variableii
        # del mask, maskii, maskiii, masque
        del figure, axes, axis
        gc.collect()
        self._measure('after plot deletion')

        # end status
        self._stamp('plotted.')

        return None

    def land(self, year=2024, month=3, day=23, folder='../land'):
        """Create a regular grid land cover file to colocate onto TEMPO grid.

        Arguments:
            year: int, year
            month: int, month
            day: int, the day
            folder: str, folder for depositing gridded file

        Returns:
            None
        """

        # make hydra
        formats = (year, self._pad(month), self._pad(day))
        hydra = Hydra('/tis/acps/OMI/10004/OMULNDTYP/{}/{}/{}'.format(*formats))

        # begin data
        land = []
        latitude = []
        longitude = []

        # for each path
        for path in hydra.paths:

            # ingest and add data
            hydra.ingest(path)
            land.append(hydra.grab('land_cover'))
            latitude.append(hydra.grab('Latitude'))
            longitude.append(hydra.grab('Longitude'))

        # construct arrays
        latitude = numpy.vstack(latitude)
        longitude = numpy.vstack(longitude)
        land = numpy.vstack(land)

        # reduce land to biggest percent
        order = numpy.argsort(land, axis=2)
        land = order[:, :, -1]

        # remove fill values
        mask = (abs(latitude) < 1e20) & (abs(longitude) < 1e20)
        latitude = latitude[mask]
        longitude = longitude[mask]
        land = land[mask]

        # construct interpolation grid
        latitudes = 3600
        longitudes = 7200
        nodes = [-90 + (90 / latitudes) + index * (180 / latitudes) for index in range(latitudes)]
        nodesii = [-180 + (180 / longitudes) + index * (360 / longitudes) for index in range(longitudes)]

        # crate mesh
        # mesh, meshii = numpy.meshgrid(indexes, indexesii, indexing='ij')
        mesh, meshii = numpy.meshgrid(nodes, nodesii, indexing='ij')

        # interpolate
        self._stamp('interpolating...', initial=True)
        interpolator = scipy.interpolate.NearestNDInterpolator((latitude, longitude), land)
        # interpolator = scipy.interpolate.RBFInterpolator((parallels, meridians), quantities, kernel='gaussian')
        # interpolator = scipy.interpolate.Rbf(parallels, meridians, quantities, function='gaussian')
        grid = interpolator(mesh, meshii)
        self._stamp('interpolated.')

        self._print(grid.min(), grid.max())
        self._print(grid.shape, mesh.shape, meshii.shape)

        # make file
        data = {'land': grid, 'latitude': mesh[:, 0], 'longitude': meshii[0]}
        destination = '{}/Land_cover_OMULNDTYP_{}m{}{}.h5'.format(folder, *formats)
        hydra.spawn(destination, data)

        return None

    def locate(self, start=1420, finish=1520, gems=False, smoothed=True, blocks=1, verify=False,
               swaths=None, coordinates=('latitude', 'longitude')):
        """Colocate the modis variable data onto the tempo grid using scipy nearest neighbors.

        Arguments:
            start: int, starting time of granule list, HHMM format
            finish: int, finishing time of granule list, HHMM format
            gems: boolean, using gems data?
            smoothed: booean, using smoothed versions?
            blocks: int, number of blocks to split the target data, to avoid memory issues
            verify: boolean, verify with plot?
            swaths: list of str, the target files
            coordinates: the latitude and longitude coordinate names

        Returns:
            None
        """

        # if not initialized
        if not self.initialized:

            # perform initialization
            self._initialize()

            # if no target files given
            if not swaths:

                # set satellite target
                self.gems = gems
                self.smoothed = smoothed
                self._target(self.gems)

                # gather input paths from S3
                self._populate()

            # if using grid file
            if self.grid:

                # retreive from grid file
                stack, latitude, longitude = self.retrieve()

            # otherwise
            else:

                # if using the averaging method
                if self.method == 'average':

                    # get composite of satellite data
                    stack, latitude, longitude = self.average()

                # otherwise
                else:

                    # use standard original composite scheme
                    stack, latitude, longitude = self.composite()

            # make sure grid is ordered
            order = numpy.argsort(latitude.flatten())
            orderii = numpy.argsort(longitude.flatten())

            # put into order
            stack = stack[order]
            stack = stack[:, orderii]
            latitude = latitude[order]
            longitude = longitude[orderii]

            # add to sel
            self.stack = stack
            self.latitude = latitude
            self.longitude = longitude

        # if swath files are used
        if swaths:

            # set up blocks
            paths = []
            originals = []
            destinations = []
            dates = []

            # for each swath file
            for swath in swaths:

                # append swath
                paths.append(swath)
                originals.append(swath)

                # append destination
                destination = '{}/colocation/{}'.format(self.sink, self._file(swath).replace('.nc', '_colocated.nc'))
                destination = destination.replace('.h5', '_colocated.h5')
                destinations.append(destination)

                # try to extract date
                date = self._distil(swath)
                dates.append(date)

            # create zipper
            zipper = list(zip(paths, destinations, dates, originals))

            # parse field into address and indices
            fragments = self.field.split('/')
            address = '/'.join([fragment for fragment in fragments if not fragment.isdigit()])
            name = address.split('/')[-1]

            # set variable to name
            self.variable = name
            self.target = self._file(swaths[0]).split('_')[0]

        # otherwise
        else:

            # create datetime object for starting hour
            hour = int(('00' + str(start))[-4:-2])
            minute = int(str(start)[-2:])
            first = datetime.datetime(self.date.year, self.date.month, self.date.day, hour, minute)

            # create datetime object for ending hour
            hour = int(('00' + str(finish))[-4:-2])
            minute = int(str(finish)[-2:])
            last = datetime.datetime(self.date.year, self.date.month, self.date.day, hour, minute)

            # zip all paths together
            zipper = list(zip(self.paths, self.destinations, self.dates, self.originals))

            # get subset between first and last times
            zipper = [quartet for quartet in zipper if (quartet[2] >= first) and (quartet[2] <= last)]

            # set fields dictionary for gems vs tempo
            # fields = {'TEMPO': {'latitude': 'band_540_740_nm/latitude', 'longitude': 'band_540_740_nm/longitude'}}
            fields = {'TEMPO': {'latitude': 'latitude', 'longitude': 'longitude'}}
            fields.update({'GEMS': {'latitude': 'pixel_latitude', 'longitude': 'pixel_longitude'}})
            fields['GEMS'].update({'ground': 'ground_pixel_quality_flag'})
            fields['TEMPO'].update({'ground': 'ground_pixel_quality_flag'})

            # set coordinates
            coordinates = (fields[self.target]['latitude'], fields[self.target]['longitude'])

            # set variable name
            name = self.variables['Aqua'][self.variable]

        # for each path and destination
        for number, quartet in enumerate(zipper):

            # unpack quartet
            path, destination, date, original = quartet

            # grab file
            self._grab(path, original)

            # print status
            self._stamp('colocating {}, {} ( {} of {} )...'.format(path, date, number, len(zipper)), initial=True)

            # grab variable from composite stack
            variable = self.stack
            latitude = self.latitude
            longitude = self.longitude

            # get the tempo latitude and longitude, and ground pixel quality
            self._stamp('ingesting {} data at {}...'.format(self.target.lower(), path))
            netii = netCDF4.Dataset(path)

            # get latitude
            latitudeii = netii
            for entry in coordinates[0].split('/'):

                # advance address
                latitudeii = latitudeii[entry]

            # get final variable
            latitudeii = latitudeii[:]

            # get longitude
            longitudeii = netii
            for entry in coordinates[1].split('/'):

                # advance address
                longitudeii = longitudeii[entry]

            # get final variable
            longitudeii = longitudeii[:]

            # close file
            netii.close()

            # set the grid shapes
            shape = (latitude.shape[0], longitude.shape[0])
            shapeii = latitudeii.shape

            # colocate
            parameters = (variable, latitude, longitude, latitudeii, longitudeii)
            variableii, latitudeii, longitudeii = self._interpolate(*parameters, blocks=blocks)

            # recreate latitude, longitude global grid
            shape = variable.shape
            grid = numpy.array([latitude] * shape[1]).transpose(1, 0)
            gridii = numpy.array([longitude] * shape[0])

            # subset modis arrays based on extent of target grid
            self._stamp('subsetting...')
            parameters = (variable, latitude, longitude, latitudeii, longitudeii)
            variableiii, latitudeiii, longitudeiii, bounds = self.subset(*parameters)

            # recreate subsetted latitude, longitude grid
            shapeiii = (latitudeiii.shape[0], longitudeiii.shape[0])
            zoom = numpy.array([latitudeiii] * shapeiii[1]).transpose(1, 0)
            zoomii = numpy.array([longitudeiii] * shapeiii[0])

            # create dimensions
            dimensions = {'lat': shapeii[0], 'lon': shapeii[1], 'lat_global': shape[0], 'lon_global': shape[1]}
            dimensions.update({'lat_zoom': shapeiii[0], 'lon_zoom': shapeiii[1]})

            # set up file data
            data = {name: variableii}
            data.update({'latitude': latitudeii, 'longitude': longitudeii})
            data.update({'{}_global'.format(name): variable})
            # data.update({'latitude_global': grid, 'longitude_global': gridii})
            data.update({'{}_zoom'.format(name): variableiii})
            data.update({'latitude_zoom': zoom, 'longitude_zoom': zoomii})

            # stash data
            self.stash(data, dimensions, destination)

            # if seeking verification by plot
            if verify:

                # verify with plot
                self.verify(destination, date, name)

            # clean up and collect garbage
            self._measure('before data deletion')
            del data, netii
            del variable
            del latitudeii, longitudeii, variableii
            del latitudeiii, longitudeiii, zoom, zoomii
            # del masque, grid, gridii, fills
            # del interpolations, interpolation, parameters
            gc.collect()
            self._measure('after data deletion')

        return

    def retrieve(self):
        """Retrieve the gridded data from arbitrary grid file.

        Arguments:
            None

        Returns:
            tuple of numpy arrays, the stack, latitude and longitude grids
        """

        # parse field into address and indices
        fragments = self.field.split('/')
        address = '/'.join([fragment for fragment in fragments if not fragment.isdigit()])
        indices = [int(fragment) for fragment in fragments if fragment.isdigit()]

        # reset field
        field = address.split('/')[-1]

        # get the grid latitude and longitude, and ground pixel quality
        net = netCDF4.Dataset(self.grid)
        latitude = net[self.coordinates[0]][:]
        longitude = net[self.coordinates[1]][:]

        # get the data
        quantity = net[address][:]

        # for each index
        for index in indices:

            # subset data
            quantity = quantity[index]

        # close file
        net.close()

        # set stack
        stack = quantity

        # set attributes
        self.stack = stack
        self.latitude = latitude
        self.longitude = longitude

        return stack, latitude, longitude

    def stash(self, data, dimensions, destination):
        """Stash data as a netcdf4 file at the given destination.

        Arguments:
            data: dict of address, arrays
            dimensions: dict of dimension dimensions
            destination: str, filepath for destinatio

        Returns:
            None
        """

        # begin netcdf4 file
        self._stamp('writing to {}...'.format(destination), initial=True)
        net = netCDF4.Dataset(destination, mode='w', format='NETCDF4')

        # for each dimension:
        sizes = {}
        for label, size in dimensions.items():

            # create dimension
            dimension = net.createDimension(label, size)

            # add to sizes
            sizes[size] = label

        # for each variable
        for address, array in data.items():

            # get labels
            labels = [sizes[size] for size in array.shape]

            # add variable
            variable = net.createVariable(address, numpy.float32, labels)
            variable[:] = array

        # close file
        net.close()

        # delete and force garbage collection
        del net, data, variable, array
        gc.collect()

        # print status
        self._stamp('{} written.'.format(destination))

        return None

    def subset(self, variable, latitude, longitude, latitudeii, longitudeii):
        """Subset the modis data according to latitude and longitude bounds.

        Arguments:
            variable: numpy array of modis variable
            latitude: numpy array of modis latitude
            longitude: numpy array of modis longitude
            latitudeii: numpy array of tempo latitude
            longitudeii: numpy array of tempo longitude

        Returns:
            None
        """

        # contruct masks
        mask = (latitudeii > self.fill) & (numpy.isfinite(latitudeii))
        maskii = (longitudeii > self.fill) & (numpy.isfinite(longitudeii))

        # subset modis data
        minimum = latitudeii[mask].min() - 5
        maximum = latitudeii[mask].max() + 5
        minimumii = longitudeii[maskii].min() - 5
        maximumii = longitudeii[maskii].max() + 5

        # try to
        try:

            # extract northern index as first less than max
            north = (latitude < maximum).tolist().index(True)

        # unless not found
        except ValueError:

            # in which case, assume first
            north = 0

        # try to
        try:

            # extract southern index as first less than minimum
            south = (minimum > latitude).tolist().index(True)

        # unless not found
        except ValueError:

            # in which case, assume final
            south = latitude.shape[0]

        # take latitude subset
        latitude = latitude[north: south]
        variable = variable[north: south, :]

        # try to
        try:

            # extract northern index as first less than max
            west = (longitude > minimumii).tolist().index(True)

        # unless not found
        except ValueError:

            # in which case, assume first
            west = 0

        # try to
        try:

            # extract southern index as first less than minimum
            east = (maximumii < longitude).tolist().index(True)

        # unless not found
        except ValueError:

            # in which case, assume final
            east = longitude.shape[0]

        # take subset
        longitude = longitude[west: east]
        variable = variable[:, west: east]

        # set bounds
        bounds = (north, south, west, east)

        return variable, latitude, longitude, bounds

    def validate(self, variable, latitude, longitude, destination, field, date):
        """PLot the grid and tempo colocated data.

        Arguments:
            variable: numpy array, the gridded data
            latitude: numpy array, the latitude of the grid
            longitude: numpy array, the longitude of the grid
            destination: str, destination filepath
            field: str, variable name
            dates: str, date

        Returns:
            None
        """

        print(variable.shape)
        print(latitude.shape)
        print(longitude.shape)

        # create plot destination
        destinationii = destination.replace('/colocations', '/plots').replace('.nc', '.png')

        # begin status
        self._stamp('plotting {}...'.format(destination), initial=True)

        # # erase preevious copy
        # self._clean(destinationii, force=True)

        # grab the colocation date
        netii = netCDF4.Dataset(destination)
        latitudeii = netii['latitude'][:]
        longitudeii = netii['longitude'][:]
        variableii = netii[field][:]
        netii.close()

        # apply mask to colocation data
        mask = (latitudeii > self.fill) & (longitudeii > self.fill) & (variableii > self.fill)
        latitudeii = latitudeii[mask]
        longitudeii = longitudeii[mask]
        variableii = variableii[mask]

        # expand modis latitudes
        latitude = numpy.array([latitude] * variable.shape[1]).transpose(1, 0)
        longitude = numpy.array([longitude] * variable.shape[0])

        # create masks for TEMPO regiom
        mask = (latitude >= latitudeii.min() - 5) & (latitude <= latitudeii.max() + 5)
        maskii = (longitude >= longitudeii.min() - 5) & (longitude <= longitudeii.max() + 5)
        maskiii = (variable <= variableii.max()) & (variable > 0)
        masque = mask & maskii & maskiii

        # create date
        formats = [date.year, self._pad(date.month), self._pad(date.day)]
        formats += [self._pad(date.hour), self._pad(date.minute)]

        # create titles
        titles = ['{} Grid\n{}'.format(field, date)]
        titles += ['{} colocated\n{}'.format(field, date)]

        # collect data
        abscissas = [longitude[masque].flatten(), longitudeii.flatten()]
        ordinates = [latitude[masque].flatten(), latitudeii.flatten()]
        tracers = [variable[masque].flatten(), variableii.flatten()]

        # get minimum and maximum tracer values
        minimum = min([tracers[0][tracers[0] > 0].min(), tracers[1][tracers[1] > 0].min()])
        maximum = max([tracers[0][tracers[0] > 0].max(), tracers[1][tracers[1] > 0].max()])

        # set up figure
        pyplot.clf()
        pyplot.margins(1.0, 0.2)
        figure, axes = pyplot.subplots(1, 2, subplot_kw={'projection': cartopy.crs.PlateCarree()}, figsize=(8, 5))
        pyplot.subplots_adjust(wspace=0.4)

        # for each set
        for abscissa, ordinate, tracer, axis, title in zip(abscissas, ordinates, tracers, axes, titles):

            # set cartopy axis with coastlines
            axis.coastlines()
            _ = axis.gridlines(draw_labels=True)

            # begin plot with title
            axis.grid(True)
            axis.set_global()
            axis.set_aspect('auto')
            axis.set_title(title, fontsize=10)
            axis.tick_params(axis='both', labelsize=7)

            # # set up colors and logarithmic scale
            # colors = matplotlib.cm.gist_rainbow_r
            # normal = matplotlib.colors.LogNorm(minimum, maximum)

            # set up colors and linear scale
            colors = matplotlib.cm.gist_rainbow_r
            normal = matplotlib.colors.Normalize(minimum, maximum)

            mask = (tracer > 0)

            # plot modis / tempo data
            axis.scatter(abscissa[mask], ordinate[mask], marker='.', s=0.1, c=tracer[mask], cmap=colors, norm=normal)

            # set axis limits
            axis.set_xlim(longitudeii.min() - 5, longitudeii.max() + 5)
            axis.set_ylim(latitudeii.min() - 5, latitudeii.max() + 5)

            # add colorbar
            units = ''
            position = axis.get_position()
            bar = pyplot.gcf().add_axes([position.x0, position.y0 - 0.06, position.width, 0.02])
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', label=units)
            formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
            colorbar.ax.yaxis.set_major_formatter(formatter)

        # save to destination
        pyplot.savefig(destinationii)
        pyplot.clf()

        # delete references
        self._measure('before plot deletion')
        del netii
        del abscissas, ordinates, tracers
        del abscissa, ordinate, tracer
        del latitude, longitude, variable
        del latitudeii, longitudeii, variableii
        del mask, maskii, maskiii, masque
        del figure, axes, axis
        gc.collect()
        self._measure('after plot deletion')

        # end status
        self._stamp('plotted.')

        return None

    def verify(self, destination, date, name):
        """PLot the modis and tempo colocated data.

        Arguments:
            destination: str, destination filepath
            date: datetime object, associated datetime
            name: str, variable name

        Returns:
            None
        """

        # begin status
        self._stamp('plotting {}, {}...'.format(destination, date), initial=True)

        # create plots folder
        sink = '{}/plots'.format(self.sink)
        self._make(sink)

        # create destination file name
        destinationii = '{}/{}'.format(sink, self._file(destination).replace('.nc', '.png'))

        # erase preevious copy
        self._clean(destinationii, force=True)

        # # construct site addresses, including possible near real time
        # site = self.sites['Aqua']
        #
        # # check for validity
        # valid = self._request(site)
        #
        # # if not valid
        # if not valid:
        #
        #     # try nrt site
        #     site = site.replace('.nc', '.NRT.nc')
        #
        # # get modis data
        # net = netCDF4.Dataset(site)
        # latitude = net['lat'][:]
        # longitude = net['lon'][:]
        # variable = net[self.variables['Aqua'][self.variable]][:]
        # net.close()

        # get data
        latitude = self.latitude
        longitude = self.longitude
        variable = self.stack

        # grab the colocation date
        netii = netCDF4.Dataset(destination)
        latitudeii = netii['latitude'][:]
        longitudeii = netii['longitude'][:]
        variableii = netii[name][:]
        netii.close()

        print(latitude.shape)
        print(longitude.shape)
        print(variable.shape)
        print(latitudeii.shape)
        print(longitudeii.shape)
        print(variableii.shape)

        # apply mask to colocation data
        mask = (latitudeii > self.fill) & (longitudeii > self.fill) & (variableii > self.fill) & (variableii > 0)
        latitudeii = latitudeii[mask]
        longitudeii = longitudeii[mask]
        variableii = variableii[mask]

        # expand modis latitudes
        latitude = numpy.array([latitude] * variable.shape[1]).transpose(1, 0)
        longitude = numpy.array([longitude] * variable.shape[0])

        # create masks for TEMPO regiom
        mask = (latitude >= latitudeii.min() - 5) & (latitude <= latitudeii.max() + 5)
        maskii = (longitude >= longitudeii.min() - 5) & (longitude <= longitudeii.max() + 5)
        maskiii = (variable <= variableii.max()) & (variable > 0)
        masque = mask & maskii & maskiii

        # create date
        formats = [date.year, self._pad(date.month), self._pad(date.day)]
        formats += [self._pad(date.hour), self._pad(date.minute)]

        # create titles
        titles = ['Gridded Composite {}\n{}'.format(self.variable.capitalize(), date)]
        titles += ['{} Colocated to {} Coordinates\n{}'.format(self.variable.capitalize(), self.target, date)]

        # collect data
        abscissas = [longitude[masque].flatten(), longitudeii.flatten()]
        ordinates = [latitude[masque].flatten(), latitudeii.flatten()]
        tracers = [variable[masque].flatten(), variableii.flatten()]

        # get minimum and maximum tracer values
        minimum = min([tracers[0].min(), tracers[1].min()])
        maximum = max([tracers[0].max(), tracers[1].max()])

        # set up figure
        pyplot.clf()
        pyplot.margins(1.0, 0.2)
        figure, axes = pyplot.subplots(1, 2, subplot_kw={'projection': cartopy.crs.PlateCarree()}, figsize=(8, 5))
        pyplot.subplots_adjust(wspace=0.4)

        # for each set
        for abscissa, ordinate, tracer, axis, title in zip(abscissas, ordinates, tracers, axes, titles):

            # set cartopy axis with coastlines
            axis.coastlines()
            _ = axis.gridlines(draw_labels=True)

            # begin plot with title
            axis.grid(True)
            axis.set_global()
            axis.set_aspect('auto')
            axis.set_title(title, fontsize=10)
            axis.tick_params(axis='both', labelsize=7)

            # set up colors and logarithmic scale
            colors = matplotlib.cm.gist_rainbow_r
            normal = matplotlib.colors.Normalize(minimum, maximum)

            # if chlorophyll
            if self.variable == 'chlorophyll':

                # use log scale
                normal = matplotlib.colors.LogNorm(minimum, maximum)

            # plot modis / tempo data
            axis.scatter(abscissa, ordinate, marker='.', s=0.1, c=tracer, cmap=colors, norm=normal)

            # set axis limits
            axis.set_xlim(longitudeii.min() - 5, longitudeii.max() + 5)
            axis.set_ylim(latitudeii.min() - 5, latitudeii.max() + 5)

            # add colorbar
            # units = self.units['Aqua'][self.variable]
            units = ''
            position = axis.get_position()
            bar = pyplot.gcf().add_axes([position.x0, position.y0 - 0.06, position.width, 0.02])
            scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
            colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', label=units)
            formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
            colorbar.ax.yaxis.set_major_formatter(formatter)

        # save to destination
        pyplot.savefig(destinationii)
        pyplot.clf()

        # delete references
        self._measure('before plot deletion')
        del netii
        del abscissas, ordinates, tracers
        del abscissa, ordinate, tracer
        del latitude, longitude, variable
        del latitudeii, longitudeii, variableii
        del mask, maskii, maskiii, masque
        del figure, axes, axis
        gc.collect()
        self._measure('after plot deletion')

        # end status
        self._stamp('plotted.')

        return None


# run from command line
arguments = sys.argv[1:]
if len(arguments) > 0:

    # separate out options
    options = [argument for argument in arguments if argument.startswith('-')]
    arguments = [argument for argument in arguments if not argument.startswith('-')]