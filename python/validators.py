#!/usr/bin/env python3

# validators.py to validate NN predictions

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula

# import system
import sys

# import re
import re

# import datetime
import datetime

# import math
import math

# import numpy functions
import numpy
import scipy

# import netcdf4
import netCDF4

# import random forest
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.metrics import r2_score

# # import netcdf4
# import netCDF4


# import tensorflow
import tensorflow


# try to
try:

    # import matplotlib for plots
    import matplotlib
    from matplotlib import pyplot
    from matplotlib import style as Style
    from matplotlib import rcParams
    Style.use('fast')
    rcParams['axes.formatter.useoffset'] = False
    rcParams['figure.max_open_warning'] = False

# unless import error
except ImportError:

    # ignore matplotlib
    print('matplotlib not available!')

# try to
try:

    # import cartopy
    import cartopy
    from cartopy.mpl.geoaxes import GeoAxes

# unless import error
except ImportError:

    # ignore matplotlib
    print('cartopy not available!')


# class Validator to do OMOCNUV analysis
class Validator(Hydra):
    """Validator class to concatenate single orbit files.

    Inherits from:
        Hydra
    """

    def __init__(self, sink='.', year=2024, month=3, day=23, limits=None):
        """Initialize instance.

        Arguments:
            sink: str, file path for sink folder
            year: int, the year
            month: int, the month
            day: int, the day
        """

        # initialize the base Core instance
        Hydra.__init__(self, show=False)

        # set fill value
        self.fill = numpy.nan

        # set sink
        self.sink = sink

        # set date attribute
        self.year = year
        self.month = month
        self.day = day
        self.date = ''
        self.stub = ''
        self._calendar()

        # set grid size
        self.mirror = 1200
        self.track = 2048

        # set limits
        self.limits = limits or {}
        self._limit()

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Validator instance at {} >'.format(self.sink)

        return representation

    def _calendar(self):
        """Format the date strings.

        Arguments:
            None

        Returns:
            None
        """

        # create formatw
        formats = (self.year, self._pad(self.month), self._pad(self.day))
        self.date = '{}m{}{}'.format(*formats)
        self.stub = '{}/{}/{}'.format(*formats)

        return None

    def _interpolate(self, quantity, latitude, longitude, latitudeii, longitudeii, blocks=1):
        """Colocate a quantity given by a regular grid onto arbitrary coordinates by nearest neightbors.

        Arguments:
            quantity: 2-d numpy array, quantity of interest
            latitude: 1-D numpy array, latitude grid points
            longitude: 1-D numpy.array, longitude grid points
            latitudeii: numpy array, target latitude coordinates
            longitudeii: numpy array, target longitude coordinates
            blocks: int, number of separately interpolated blocks

        Returns:
            tuple of numpy array, gridded quantity and corrdinates with fill
        """

        # set the grid shapes
        shape = (latitude.shape[0], longitude.shape[0])
        shapeii = latitudeii.shape

        # construct latitude mask
        self._stamp('\nextracting valid geo coordinates...')
        mask = numpy.ones(latitudeii.shape).astype(bool)
        if latitudeii.mask.sum() > 0:

            # construct from inversion
            mask = numpy.logical_not(latitudeii.mask)

        # construct longitude mask
        maskii = numpy.ones(longitudeii.shape).astype(bool)
        if longitudeii.mask.sum() > 0:

            # construct from inversion
            maskii = numpy.logical_not(longitudeii.mask)

        # combine masks
        masque = mask & maskii

        # apply masks to tempo data to get valid data
        latitudeii = latitudeii[masque]
        longitudeii = longitudeii[masque]

        # set up interpolation blocks
        size = int(numpy.ceil(latitudeii.shape[0] / blocks))
        brackets = [(index * size, size + index * size) for index in range(blocks)]

        # for each block
        interpolations = []
        for index, (first, last) in enumerate(brackets):

            # use scipy nearest neighbors
            self._stamp('using nearest neighbors, block {} of {}...'.format(index, blocks))
            parameters = ((latitude, longitude), quantity, (latitudeii[first: last], longitudeii[first: last]))
            options = {'method': 'nearest', 'bounds_error': False, 'fill_value': self.fill}
            interpolation = scipy.interpolate.interpn(*parameters, **options)

            # add to collection
            interpolations.append(interpolation)

            # print status
            self._stamp('calculated.')

        # concatenate interpolations
        quantityii = numpy.hstack(interpolations)

        # set fill value
        quantityii.fill_value = self.fill

        # create array of fill values
        fills = numpy.ones(masque.shape) * self.fill
        fills[numpy.where(masque)] = quantityii
        quantityii = fills.reshape(shapeii)

        # create array of fill values for latitude
        fills = numpy.ones(masque.shape) * self.fill
        fills[numpy.where(masque)] = latitudeii.squeeze()
        latitudeii = fills.reshape(shapeii)

        # create array of fill values for longitude
        fills = numpy.ones(masque.shape) * self.fill
        fills[numpy.where(masque)] = longitudeii.squeeze()
        longitudeii = fills.reshape(shapeii)

        return quantityii, latitudeii, longitudeii

    def _graph(self, abscissa, ordinate, intensity, bounds, scale, gradient, title, unit, labels, **options):
        """Generate graph object for plotting.

        Arguments:
            abscissa: numpy array, horizontal coordinates
            ordinate: numpy array, vertical coordinates
            intensity: numpy array, value for color
            bounds: list of ( float, float ) tuples, the x-axis and y-axis bounds
            scale: tuple of floats, the color scale boundaries
            gradient: str, the name of the matplotlib color gradient
            title: str, plot title
            units: str, plot units
            labels: list of str, the x and y axis labels
            **options: unpacked dictionary of options:
                globe: boolean, draw coastlines?
                logarithm: boolean, use logarithmic scale?
                colorbar: boolean, add colorbar?
                lines: list of (array, array, str) tuples, additional lines ( x, y, format str )
                pixel: float, the pixel size
                polygons: boolean, graph as polygons?
                corners: list of numpy arrays, the latitude and longitude corners
                selection: indices of color gradient
                categories: list of ( str, str ) tuples, the colors and categories
                reflectivity: boolean, plotting reflectivity?
                alpha: float, transparency level
                font: int, title font size
                fontii: int, marker font size

        Returns:
            dictionary, graph specifics
        """

        # set default options
        globe = options.get('globe', True)
        logarithm = options.get('logarithm', False)
        colorbar = options.get('colorbar', True)
        lines = options.get('lines', [])
        pixel = options.get('pixel', abscissa.shape[0] / ( 72 * 16))
        polygons = options.get('polygons', False)
        corners = options.get('corners', [None, None])
        ticks = options.get('ticks', None)
        ticksii = options.get('ticksii', None)
        marks = options.get('marks', None)
        marksii = options.get('marksii', None)
        marker = options.get('marker', 2)
        selection = options.get('selection', (0, 256))
        categories = options.get('categories', None)
        reflectivity = options.get('reflectivity', None)
        alpha = options.get('alpha', 0.8)
        font = options.get('font', 20)
        fontii = options.get('fontii', 20)

        # create graph object
        graph = {'abscissa': abscissa, 'ordinate': ordinate, 'intensity': intensity}
        graph.update({'x_bounds': bounds[0], 'y_bounds': bounds[1], 'scale': scale, 'gradient': gradient})
        graph.update({'title': title, 'units': unit, 'x_label': labels[0], 'y_label': labels[1]})
        graph.update({'globe': globe, 'logarithm': logarithm, 'colorbar': colorbar, 'lines': lines})
        graph.update({'pixel': pixel, 'polygons': polygons, 'corners': corners})
        graph.update({'x_ticks': ticks, 'y_ticks': ticksii, 'x_marks': marks, 'y_marks': marksii})
        graph.update({'marker': marker, 'selection': selection, 'categories': categories})
        graph.update({'reflectivity': reflectivity, 'alpha': alpha, 'font': font, 'fontii': fontii})

        return graph

    def _label(self, scan, granule=None):
        """Create a scan label.

        Arguments:
            scan: int, the scan number
            granule: int, the granule number

        returns:
            str, the scan label
        """

        # make label
        label = '_S{}G'.format(self._pad(scan, 3))

        # if a granule number given
        if granule:

            # add granule number
            label += self._pad(granule)

        return label

    def _limit(self):
        """Define valid data limits.

        Arguments:
            None

        Returns:
            None
        """

        # define limits
        limits = {}
        limits['zenith'] = 65
        limits['view'] = 60
        limits['cloud'] = 0.6
        limits['snow'] = 0.0
        limits['truth'] = 15

        # update with given limits
        limits.update(self.limits)
        self.limits = limits

        return None

    def _paint(self, destination, graphs, size=(16, 16), space=None, gap=0.065):
        """Create a plot with subplots.

        Arguments:
            destination: str, the file path
            graphs: list of dicts, the graph objects
            size: tuple of floats, the total plot size
            space: dict, subplot spacing paramaeters
            gap: gap between grqph and colorbars

        Returns:
            None
        """

        # print status
        self._stamp('plotting {}...'.format(destination), initial=True)

        # erase previous copy
        self._clean(destination, force=True)

        # define default spacing
        space = space or {}

        # get number of subplots
        number = len(graphs)

        # set up dictionary of subplot values based on number
        layout = {'rows': {1: 1, 2: 1, 3: 1, 4: 2}, 'columns': {1: 1, 2: 2, 3: 3, 4: 2}}

        # calculate total size
        rows = layout['rows'][number]
        columns = layout['columns'][number]

        # set up figure
        pyplot.clf()
        figure = pyplot.figure(figsize=size)
        figure.patch.set_facecolor('white')
        # words = {'subplot_kw': {'projection': cartopy.crs.PlateCarree()}, 'figsize': size}
        # figure, axes = pyplot.subplots(rows, columns, figsize=size)

        # begin axes
        axes = []

        # for each graph
        for index, graph in enumerate(graphs):

            # if drawing globe:
            if graph['globe']:

                # activate cartopy
                code = int('{}{}{}'.format(rows, columns, index + 1))
                axis = pyplot.subplot(code, projection=cartopy.crs.PlateCarree())
                axis.set_facecolor('white')
                axes.append(axis)

            # otherwise:
            else:

                # activate cartopy
                code = int('{}{}{}'.format(rows, columns, index + 1))
                axis = pyplot.subplot(code)
                axis.set_facecolor('white')
                axes.append(axis)

        # adjust spacing
        margins = {'bottom': 0.12, 'top': 0.94, 'left': 0.07, 'right': 0.93, 'wspace': 0.2, 'hspace': 0.5}
        margins.update(space)
        pyplot.subplots_adjust(**margins)

        # for each graph
        for axis, graph in zip(axes, graphs):

            # unpack arrays
            abscissa = graph['abscissa']
            ordinate = graph['ordinate']
            intensity = graph['intensity']

            # unpack bounds
            bounds = graph['x_bounds']
            boundsii = graph['y_bounds']

            # unpack color scale
            scale = graph['scale']
            gradient = graph['gradient']
            selection = graph['selection']
            categories = graph['categories']

            # unpack texts
            title = graph['title']
            units = graph['units']
            label = graph['x_label']
            labelii = graph['y_label']
            font = graph['font']
            fontii = graph['fontii']

            # if not reflectivity
            if not graph['reflectivity']:

                # create mask for finite data
                mask = numpy.isfinite(intensity)

                # apply mask
                intensity = intensity[mask]
                abscissa = abscissa[mask]
                ordinate = ordinate[mask]

                # get scale bounds
                minimum = scale[0]
                maximum = scale[1]

                # clip intensity
                intensity = numpy.where(intensity < minimum, minimum, intensity)
                intensity = numpy.where(intensity > maximum, maximum, intensity)

            # if drawing globe:
            if graph['globe']:

                # set cartopy axis with coastlines
                axis.coastlines()
                # axis.add_feature(cartopy.feature.OCEAN)
                # axis.add_feature(cartopy.feature.COASTLINE, edgecolor='black', linewidth=1)
                axis.add_feature(cartopy.feature.STATES)
                axis.add_feature(cartopy.feature.LAKES, facecolor='white')
                # axis.add_feature(cartopy.feature.OCEAN, facecolor='blue')
                options = {'edgecolor': 'face', 'facecolor': 'white'}
                axis.add_feature(cartopy.feature.NaturalEarthFeature('physical', 'ocean', '50m', **options))

                # set title
                axis.set_aspect('auto')
                axis.set_title(title, fontsize=font)

                # if xticks
                if graph['x_ticks'] and graph['y_ticks']:

                    # set x ticks
                    axis.set_xticks(graph['x_ticks'])
                    axis.set_xticklabels(graph['x_marks'])
                    # axis.tick_params(axis='x', rotation=45)

                    # set yticks
                    axis.set_yticks(graph['y_ticks'])
                    axis.set_yticklabels(graph['y_marks'])
                    # axis.tick_params(axis='y', rotation=45)

                    # set font size
                    axis.tick_params(axis='both', which='major', labelsize=fontii)  # Major ticks
                    axis.tick_params(axis='both', which='minor', labelsize=fontii)

                # otherwies
                else:

                    # set grid marks
                    axis.grid(True)
                    axis.set_global()
                    _ = axis.gridlines(draw_labels=True)

                    # set font size
                    axis.tick_params(axis='both', which='major', labelsize=fontii)
                    axis.tick_params(axis='both', which='minor', labelsize=fontii)

            # otherwise:
            else:

                # set title
                axis.set_title(title, fontsize=font)

                # add labels and ticks
                axis.set_title(title, fontsize=font)
                axis.set_xlabel(label, fontsize=fontii)
                axis.set_ylabel(labelii, fontsize=fontii)

                # set font size
                axis.tick_params(axis='both', which='major', labelsize=fontii)
                axis.tick_params(axis='both', which='minor', labelsize=fontii)

                # if xticks
                if graph['x_ticks']:

                    # set x ticks
                    axis.set_xticks(graph['x_ticks'])
                    axis.set_xticklabels(graph['x_marks'])
                    axis.tick_params(axis='x', rotation=45)

                # if yticks
                if graph['y_ticks']:

                    # set yticks
                    axis.set_yticks(graph['y_ticks'])
                    axis.set_yticklabels(graph['y_marks'])
                    axis.tick_params(axis='y', rotation=45)

            # set up colors and logarithmic scale
            colors = matplotlib.cm.get_cmap(gradient)
            selection = numpy.array(list(range(*selection)))
            colors = matplotlib.colors.ListedColormap(colors(selection))
            normal = matplotlib.colors.Normalize(minimum, maximum)

            # if colorbar categores
            if categories:

                # split names and hues
                hues = [category[0] for category in categories]
                texts = [category[1] for category in categories]
                colors = matplotlib.colors.ListedColormap(hues)
                normal = matplotlib.colors.BoundaryNorm(boundaries=numpy.arange(len(hues) + 1) - 0.5, ncolors=len(hues))

            # if logarithmic
            if graph['logarithm']:

                # make normal logarithmic
                normal = matplotlib.colors.LogNorm(minimum, maximum)

            # if grpahing polygons
            if graph['polygons']:

                # begin patches
                patches = []

                # for each polygon
                for vertices, verticesii in zip(graph['corners'][1], graph['corners'][0]):

                    # set up patch
                    polygon = numpy.array([[first, second] for first, second in zip(vertices, verticesii)])
                    patch = matplotlib.patches.Polygon(xy=polygon, linewidth=0, edgecolor=None, alpha=graph['alpha'])
                    patches.append(patch)

                # if reflectivity plot
                if graph['reflectivity']:

                    # plot polygons with colormap
                    options = {'facecolor': intensity, 'alpha': graph['alpha']}
                    collection = matplotlib.collections.PatchCollection(patches, **options)
                    collection.set_edgecolor("face")
                    # collection.set_clim(scale)
                    # collection.set_array(intensity)
                    polygons = axis.add_collection(collection)
                    # axis.add_feature(cartopy.feature.OCEANS, facecolor='white')

                # otherwise
                else:

                    # plot polygons with colormap
                    collection = matplotlib.collections.PatchCollection(patches, cmap=colors, alpha=graph['alpha'])
                    collection.set_edgecolor("face")
                    collection.set_clim(scale)
                    collection.set_array(intensity)
                    polygons = axis.add_collection(collection)

            # otherwise
            else:

                # if reflectivity plot
                if graph['reflectivity']:

                    # no need for colors
                    axis.scatter(abscissa, ordinate, c=intensity, s=0.75, lw=0)

                # otheriwse
                else:

                    # plot data
                    words = {'c': intensity, 'cmap': colors, 'norm': normal}
                    axis.scatter(abscissa, ordinate, marker='.', s=graph['pixel'], **words)

            # plot additional lines
            for line in graph['lines']:

                # plot line
                options = {'markersize': graph['marker']}
                axis.plot(*line, **options)

            # set axis limits
            axis.set_xlim(*bounds)
            axis.set_ylim(*boundsii)

            # if using colorbar
            if graph['colorbar']:

                # add colorbar
                position = axis.get_position()
                bar = pyplot.gcf().add_axes([position.x0, position.y0 - gap, position.width, 0.02])
                scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
                # colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', label=units)

                # if cateogories
                if categories:

                    # add categorical colorbar
                    ticksii = numpy.array(range(len(hues)))
                    colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', ticks=ticksii)
                    colorbar.ax.set_xticklabels(texts)  # Set category names
                    colorbar.ax.tick_params(axis='x', rotation=45, labelsize=fontii)
                    # colorbar.set_label(units, labelpad=1)
                    # formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
                    # colorbar.ax.yaxis.set_major_formatter(formatter)
                    # # Create the colorbar with category labels
                    # # barii = bar.imshow(numpy.array(range(len(hues))), cmap=colors, norm=normal)
                    # colorbar = pyplot.gcf().colorbar(scalar, ticks=numpy.arange(len(hues)))
                    # colorbar.ax.set_yticklabels(texts)  # Set category names

                # otherwise
                else:

                    # add regular colorbar
                    colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal')
                    colorbar.set_label(units, labelpad=1, fontsize=fontii)
                    formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
                    colorbar.ax.yaxis.set_major_formatter(formatter)
                    colorbar.ax.tick_params(labelsize=fontii)  # For tick labels

        # save to destination
        pyplot.savefig(destination)
        pyplot.clf()

        # end status
        self._stamp('plotted.')

        return None

    def _patch(self, reflectance, latitude, longitude, cloud):
        """Patch bad reflectance data with nearest neighbors good data.

        Arguments:
            reflectance: numpy array, reflectqnce pcas
            latitude: numpy array, latitude
            longitude: numpy array, longitude
            cloud: numpy array, cloud fraction

        Returns:
            numpy array: patched data
        """

        # squeeze latitude and longitude
        latitude = latitude.squeeze()
        longitude = longitude.squeeze()
        cloud = cloud.squeeze()

        # create mask for good and bad data
        summation = reflectance.sum(axis=1)
        good = (numpy.isfinite(summation) & (abs(summation) < 1e20))
        bad = numpy.logical_not(good)

        print('good: {}'.format(good.sum()))
        print('bad: {}'.format(bad.sum()))

        # create a KDTree from the good lat/lon
        tree = scipy.spatial.cKDTree(numpy.column_stack((latitude[good], longitude[good])))

        # query the tree for bad data points
        distances, indices = tree.query(numpy.column_stack((latitude[bad], longitude[bad])), k=1)

        # replace bad data with the nearest good data
        reflectance[bad] = reflectance[good][indices.flatten()]

        return reflectance

    def chronicle(self, history):
        """Make plots of loss functions.

        Arguments:
            history: model history

        Returns:
            None
        """

        # get history contents
        contents = history.history

        # for each entry
        for loss, trace in contents.items():

            # plot history
            pyplot.clf()
            pyplot.plot(trace)
            pyplot.title(loss)
            pyplot.savefig('GPP_NN_History/{}.png'.format(loss))
            pyplot.clf()

        return None

    def cloud(self):
        """Plot the difference in GPP series versus cloud radiative fraction.

        Arguments:
            None

        Returns:
            None
        """

        # construct date
        date = '{}m{}{}'.format(self.year, self._pad(self.month), self._pad(self.day))

        # get the series files
        hydra = Hydra('GPP_Series')
        hydra.ingest(date)

        # get data
        prediction = hydra.grab('gpp_prediction')
        cloud = hydra.grab('cloud_fraction')
        valid = hydra.grab('valid_mask').astype(bool)

        # apply valid mask
        predictionii = numpy.where(valid, prediction, 0)
        cloudii = numpy.where(valid, cloud, 0)
        number = numpy.where(valid, 1, 0).sum(axis=0)

        # create averages
        average = predictionii.sum(axis=0) / number
        averageii = cloudii.sum(axis=0) / number

        # calculate the differences
        difference = prediction - average
        differenceii = cloud - averageii

        # create valid mask
        mask = numpy.isfinite(difference) & numpy.isfinite(differenceii)
        mask = mask & (abs(difference) < 1e10) & (abs(differenceii) < 1e10)

        # make plot
        pyplot.clf()
        pyplot.title('Difference in GPP vs Cloud Fraction')
        pyplot.plot(differenceii[mask], difference[mask], 'b.')
        pyplot.xlabel('cloud fraction')
        pyplot.ylabel('gpp')

        # save figure
        destination = 'Cloud_Plots/Scatter_plot_clouds_{}.png'.format(date)
        pyplot.savefig(destination)

        # adjust for lag plots
        difference = difference[1:, :, :]
        differenceii = differenceii[:-1, :, :]

        # create valid mask for lag plot
        mask = numpy.isfinite(difference) & numpy.isfinite(differenceii)
        mask = mask & (abs(difference) < 1e10) & (abs(differenceii) < 1e10)

        # make plot
        pyplot.clf()
        pyplot.title('Difference in GPP vs Cloud Fraction, 1 scan lag')
        pyplot.plot(differenceii[mask], difference[mask], 'b.')
        pyplot.xlabel('cloud fraction')
        pyplot.ylabel('gpp')

        # save figure
        destination = 'Cloud_Plots/Scatter_plot_clouds_lag_{}.png'.format(date)
        pyplot.savefig(destination)

        return None

    def compare(self, scan=7, method='pixel', instances=100):
        """Compare predicted GPPs to noon GPP.

        Arguments:
            scan: int, the scan number for comparison
            method: str, 'nearest', 'pixel'
            instances: int, number of instances

        Returns:
            None
        """

        # distil data
        reservoir, times, noon = self.distil(scan=scan)

        # begin graphs
        graphs = {}

        # for each label
        for label in reservoir.keys():

            # try to
            try:

                # create percent difference
                prediction = reservoir[noon]['gpp_prediction']
                predictionii = reservoir[label]['gpp_prediction']
                percent = self._relate(prediction, predictionii)

                # create valid data mask
                valid = (reservoir[label]['water_mask'] == 1) & (numpy.isfinite(reservoir[label]['cloud_fraction']))
                valid = valid & (numpy.isfinite(predictionii))
                valid = valid & (numpy.isfinite(reservoir[label]['solar_zenith_angle']))
                valid = valid & (reservoir[label]['cloud_fraction'] <= self.limits['cloud'])
                valid = valid & (reservoir[label]['solar_zenith_angle'] <= self.limits['zenith'])
                valid = valid & (reservoir[label]['viewing_zenith_angle'] <= self.limits['view'])
                valid = valid & (reservoir[label]['snow_ice_fraction'] <= self.limits['snow'])
                valid = valid & (predictionii <= self.limits['truth']) & (prediction <= self.limits['truth'])

                # set scale and gradient
                scale = (-2, 2)
                gradient = 'coolwarm'

                # clip percent
                tracer = percent
                tracer = numpy.where((tracer < scale[0]) & numpy.isfinite(tracer), scale[0], tracer)
                tracer = numpy.where((tracer > scale[1]) & numpy.isfinite(tracer), scale[1], tracer)

                # create mask and apply
                mask = numpy.isfinite(tracer[valid])
                data = tracer[valid][mask]
                abscissa = reservoir[label]['longitude'][valid][mask]
                ordinate = reservoir[label]['latitude'][valid][mask]
                corners = reservoir[label]['latitude_bounds'][valid][mask]
                cornersii = reservoir[label]['longitude_bounds'][valid][mask]

                # order data, biggest values last
                order = numpy.argsort(abs(data))

                # order data
                data = data[order]
                abscissa = abscissa[order]
                ordinate = ordinate[order]
                corners = corners[order]
                cornersii = cornersii[order]

                # set zoom bounds
                bounds = [(-100, -80), (25, 40)]

                # create graph
                formats = (times[label][9:13], times[noon][9:13], instances, self.date)
                title = 'UTC{} - UTC{} ( avg of {} ) {}\n'.format(*formats)
                unit = '$g C/m^2 d$'
                labels = ['', '']
                options = {'polygons': True, 'corners': [corners, cornersii]}
                graph = self._graph(abscissa, ordinate, data, bounds, scale, gradient, title, unit, labels, **options)
                graphs[label] = graph

            # unless shapes don't match
            except ValueError as error:

                # print
                print(error)
                print('no comparison for {}, shapes mismatch'.format(label))

        # get labels
        labels = list(graphs.keys())
        labels.sort()

        # get position of noon
        position = labels.index(noon)

        # create data
        date = '{}m{}{}'.format(self.year, self._pad(self.month), self._pad(self.day))

        # make plot of three scans prior to noon
        destination = 'GPP_Anomaly_Plots/Anomaly_Plot_before_{}_{}.png'.format(noon, date)
        self._paint(destination, [graphs[label] for label in labels[position - 3: position + 1]])

        # make plot of three scans after to noon
        destination = 'GPP_Anomaly_Plots/Anomaly_Plot_after_{}_{}.png'.format(noon, date)
        self._paint(destination, [graphs[label] for label in labels[position: position + 4]])

        return None

    def cross(self, scan=6, method='pixel', parallels=None):
        """Plot cross sections.

        Arguments:
            scan: int, the scan number for comparison
            method: str, 'nearest', 'pixel'
            parallels: list of floats, the latitude coordiantes

        Returns:
            None
        """

        # set default parallels
        parallels = parallels or (20, 25, 30, 35, 40, 45)

        # distil data
        reservoir, times, noon = self.distil(scan=scan)

        # get latitude and track positions
        indices = [self._pin(parallel, reservoir[noon]['latitude'], 1)[0][1] for parallel in parallels]
        assignments = indices

        # for each label
        for label in reservoir.keys():

            # for each parallel
            for parallel, assignment in zip(parallels, assignments):

                # try to
                try:

                    # create destination
                    formats = (self._orient(parallel), label, noon)
                    destination = 'GPP_Cross_Plots/Cross_Plot_{}_{}_vs_{}.png'.format(*formats)

                    # begin status
                    self._stamp('plotting {}...'.format(destination), initial=True)

                    # erase preevious copy
                    self._clean(destination, force=True)

                    # create titles
                    time = times[label]
                    degrees = self._orient(parallel)
                    titles = ['GPP Prediction {}, {}\n'.format(time, degrees)]
                    # titles += ['Noon % Difference {}, {}\n'.format(time, degrees)]
                    titles += ['Noon Difference {}, {}\n'.format(time, degrees)]

                    # create percent difference
                    prediction = reservoir[noon]['gpp_prediction']
                    predictionii = reservoir[label]['gpp_prediction']
                    # percent = self._relate(prediction, predictionii, percent=True)
                    percent = self._relate(prediction, predictionii)

                    print('percent calculated.')

                    # create valid data mask
                    valid = (reservoir[label]['water_mask'] == 1) & (numpy.isfinite(reservoir[label]['cloud_fraction']))
                    valid = valid & (numpy.isfinite(predictionii))
                    valid = valid & (numpy.isfinite(reservoir[label]['solar_zenith_angle']))
                    valid = valid & (reservoir[label]['cloud_fraction'] <= self.limits['cloud'])
                    valid = valid & (reservoir[label]['solar_zenith_angle'] <= self.limits['zenith'])
                    valid = valid & (reservoir[label]['viewing_zenith_angle'] <= self.limits['view'])
                    valid = valid & (reservoir[label]['snow_ice_fraction'] <= self.limits['snow'])
                    valid = valid & (predictionii <= self.limits['truth']) & (prediction <= self.limits['truth'])
                    valid = valid[:, assignment]

                    # set scales
                    scale = (0, self.limits['truth'])
                    scaleii = (-10, 10)
                    scales = (scale, scaleii)

                    # clip data
                    tracer = predictionii[:, assignment]
                    tracer = numpy.where((tracer < scale[0]) & numpy.isfinite(tracer), scale[0], tracer)
                    tracer = numpy.where((tracer > scale[1]) & numpy.isfinite(tracer), scale[1], tracer)

                    # clip percent
                    tracerii = percent[:, assignment]
                    tracerii = numpy.where((tracerii < scaleii[0]) & numpy.isfinite(tracer), scaleii[0], tracerii)
                    tracerii = numpy.where((tracerii > scaleii[1]) & numpy.isfinite(tracer), scaleii[1], tracerii)

                    # set tracers
                    tracers = [tracer, tracerii]

                    # set gradiants
                    gradients = [matplotlib.cm.plasma, matplotlib.cm.coolwarm]
                    brackets = ['prediction', 'percent']

                    # set units
                    units = ['$g C/m^2 d$', '%']

                    # set up figure
                    pyplot.clf()
                    pyplot.margins(1.0, 0.1)
                    figure, axes = pyplot.subplots(1, 2, figsize=(8, 5))
                    pyplot.subplots_adjust(wspace=0.4, hspace=0.6)

                    # for each set
                    zipper = zip(tracers, axes, titles, gradients, scales, units)
                    for tracer, axis, title, gradient, scale, unit in zipper:

                        print('tracer: {}'.format(tracer.shape))
                        print('valid: {}'.format(valid.shape))

                        # create mask and apply
                        # mask = numpy.isfinite(tracer[valid])
                        # data = tracer[valid][mask]
                        mask = numpy.isfinite(tracer)
                        data = numpy.where(valid & mask, tracer, numpy.nan)
                        # abscissa = meridians[mask]
                        # ordinate = parallels[mask]
                        abscissa = reservoir[label]['longitude'][:, assignment]
                        ordinate = reservoir[label]['latitude'][:, assignment]

                        print("here!")
                        print('abscissa: {}'.format(abscissa.shape))
                        print('ordinate: {}'.format(ordinate.shape))

                        print(mask.sum())
                        # print('')
                        # print(bracket)
                        print(data.min())
                        print(data.max())
                        print(numpy.percentile(data, 10), numpy.percentile(data, 90))

                        # set scale
                        minimum = scale[0]
                        maximum = scale[1]

                        # # determine min and max
                        # minimum = data.min()
                        # maximum = data.max()

                        # # if bracket is percent
                        # if bracket == 'percent':
                        #
                        #     # determine percent min and max
                        #     absolute = max([abs(minimum), abs(maximum)])
                        #     minimum = -absolute - 1
                        #     maximum = absolute + 1
                        #
                        #     print(absolute)

                        # # set cartopy axis with coastlines
                        # axis.coastlines()
                        # _ = axis.gridlines(draw_labels=True)

                        # take subset within parallel
                        # subset = (ordinate > parallel - 0.01) & (ordinate < parallel + 0.01)
                        print('subsetting...')
                        # print('track: {}'.format(track.shape))
                        # print('position: {}'.format(assignment))
                        # subset = (track == assignment)
                        #
                        # print('subset, {}: {}'.format(assignment, subset.sum()))

                        # dataii = data[:, assignment]
                        # abscissaii = abscissa[:, assignment]
                        ordinate = data

                        # begin plot with title
                        axis.grid(True)
                        # axis.set_global()
                        axis.set_aspect('auto')
                        axis.set_title(title, fontsize=10)
                        axis.tick_params(axis='both', labelsize=7)

                        # set up colors and logarithmic scale
                        colors = gradient
                        normal = matplotlib.colors.Normalize(minimum, maximum)

                        # plot modis / tempo data
                        # axis.scatter(abscissa, ordinate, marker='.', s=0.5, c=data, cmap=colors, norm=normal)
                        # axis.scatter(abscissaii, dataii, marker='x', s=0.5, color='black', norm=normal)
                        axis.plot(abscissa, ordinate, 'b-')

                        # # set axis limits
                        # axis.set_xlim(abscissa.min() - 5, abscissa.max() + 5)
                        # axis.set_ylim(ordinate.min() - 5, ordinate.max() + 5)
                        axis.set_xlim(-150, -50)
                        axis.set_ylim(*scale)

                        # add colorbar
                        position = axis.get_position()
                        bar = pyplot.gcf().add_axes([position.x0, position.y0 - 0.065, position.width, 0.02])
                        scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
                        colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', label=unit)
                        formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
                        colorbar.ax.yaxis.set_major_formatter(formatter)

                    # save to destination
                    pyplot.savefig(destination)
                    pyplot.clf()

                    # end status
                    self._stamp('plotted.')

                # unless shapes don't match
                except ValueError as error:

                    # print
                    print(error)
                    print('no comparison for {}, shapes mismatch'.format(label))

        return None

    def diagnose(self, diagnosis, time):
        """Create plots of percent error vs parameter.

        Arguments:
            diagnosis: dict of parameters
            time: str, the timestamp

        Returns:
            None
        """

        # calculate percents
        percent = 100 * ((diagnosis['prediction'] / diagnosis['truth']) - 1)
        percent = numpy.where(numpy.isfinite(percent), percent, 0)

        # for each variable
        for name, array in diagnosis.items():

            # crete plot destination
            formats = (self.sink, self.year, self._pad(self.month), self._pad(self.day), time, name)
            destination = '{}/GPP_Diagnosis/Diagnosis_{}m{}{}_{}_{}.png'.format(*formats)

            # begin plot
            pyplot.clf()

            # create title
            title = 'GPP {}m{}{} {}\nGPP % error vs {}'.format(*formats[1:])
            pyplot.title(title)

            # add labels
            pyplot.xlabel('% error')
            pyplot.ylabel(name)

            # plot data
            pyplot.plot(percent, array, 'gx')

            # save plot
            pyplot.savefig(destination)
            pyplot.clf()

        return None

    def distil(self, scan=7, start=2, finish=11):
        """Distil the predictions.

        Arguments:
            scan: int, the scan to use for noon comparisons
            start: int, the scan with which to start
            finish: int, the scan with which to finish

        Returns:
            tuple of (dict, dict, str), the reservoir, times, and noon label
        """

        # make hydra for panels
        formats = (self.year, self._pad(self.month), self._pad(self.day))
        hydra = Hydra('GPP_Panels/{}/{}/{}'.format(*formats))

        # begin reservoirs
        reservoir = {}
        times = {}

        # for each path
        for path in hydra.paths:

            # extract label
            label = re.search('S[0-9]{3}G', path).group()

            # check for inclusiveness
            if (int(label[1:-1]) >= start) and (int(label[1:-1]) <= finish):

                # ingest
                self._print('ingesting {}...'.format(path))
                hydra.ingest(path)

                # extract time
                time = re.search('[0-9]{8}T[0-9]{6}Z', path).group()
                times[label] = time

                # set fields
                fields = ['gpp_prediction', 'water_mask', 'cloud_fraction', 'snow_ice_fraction']
                fields += ['solar_zenith_angle', 'viewing_zenith_angle', 'latitude', 'longitude']
                fields += ['gpp_deviation', 'latitude_bounds', 'longitude_bounds', 'land_cover']
                fields += ['solar_azimuth_angle', 'viewing_azimuth_angle', 'relative_azimuth_angle']

                # add data to reservoir
                reservoir[label] = {field: hydra.grab(field) for field in fields}

        # set noon label
        noon = 'S{}G'.format(self._pad(scan, 3))

        return reservoir, times, noon

    def fake(self, position=0, bracket=(-5, 5), increment=0.1):
        """Fake an input matrix.

        Arguments:
            position: int, the matrix position
            bracket: tuple of floats, the input range
            increment: float, the increment between nodes

        Returns:
            (numpy array, numpy array) tuple
        """

        # construct mean pca values and par value
        means = [2, 0.1] + [0] * 28
        means.append(0.25)
        means = numpy.array(means)

        # get the scaler
        scaler = Hydra('{}/GPP_NN/TEMPO_Test_ML_Scaling.h5'.format(self.sink), show=False)
        scaler.ingest()
        scale = scaler.grab('Input_Scale')
        minimum = scaler.grab('Input_Min')

        # construct input proxy
        chunks = int(((bracket[1]) - (bracket[0])) / increment) + 1
        proxy = [bracket[0] + index * increment for index in range(chunks)]
        proxy = numpy.array(proxy)

        # construct matrix
        matrix = numpy.array([means] * chunks)
        matrix[:, position] = proxy

        # apply scale to inputs
        matrix = matrix.transpose(1, 0)
        matrix = (matrix - minimum.reshape(-1, 1)) * scale.reshape(-1, 1)
        matrix = matrix.transpose(1, 0)
        matrix = matrix.astype(numpy.float32)

        return matrix, proxy

    def gather(self, scan=6, method='pixel'):
        """Gather the predictions.

        Arguments:
            scan: int, the scan number for comparison
            method: str, 'nearest', 'pixel'

        Returns:
            None
        """

        # get all predictions
        formats = (self.year, self._pad(self.month), self._pad(self.day))

        # get hydra for predictions
        hydra = Hydra('GPP_Predictions/{}/{}/{}'.format(*formats), show=False)

        # group all hydra paths by scan number
        groups = self._group(hydra.paths, lambda path: re.search('S[0-9]{3}G', path).group())

        # set shape
        shape = (self.mirror, self.track)

        # default noon label, to be set in loop
        noon = ''

        # for each group
        for label, members in groups.items():

            # begin data with blank nan arraya
            latitude = numpy.ones(shape) * numpy.nan
            longitude = numpy.ones(shape) * numpy.nan
            prediction = numpy.ones(shape) * numpy.nan
            deviation = numpy.ones(shape) * numpy.nan
            cloud = numpy.ones(shape) * numpy.nan
            water = numpy.ones(shape) * numpy.nan
            snow = numpy.ones(shape) * numpy.nan
            zenith = numpy.ones(shape) * numpy.nan
            view = numpy.ones(shape) * numpy.nan
            quality = numpy.ones(shape) * numpy.nan
            truth = numpy.ones(shape) * numpy.nan
            radiation = numpy.ones(shape) * numpy.nan
            reflectance = numpy.ones((*shape, 50)) * numpy.nan
            matrix = numpy.ones((*shape, 34)) * numpy.nan
            corners = numpy.ones((*shape, 4)) * numpy.nan
            cornersii = numpy.ones((*shape, 4)) * numpy.nan
            land = numpy.ones(shape) * numpy.nan
            azimuth = numpy.ones(shape) * numpy.nan
            azimuthii = numpy.ones(shape) * numpy.nan
            relative = numpy.ones(shape) * numpy.nan

            # for each member
            for member in members:

                # ingest the file
                hydra.ingest(member)

                # grab the mirror steps and track positions
                mirror = hydra.grab('mirror_step').reshape(-1)
                track = hydra.grab('xtrack').reshape(-1)

                # grab the data and insert into array
                latitude[mirror, track] = hydra.grab('latitude').reshape(-1)
                longitude[mirror, track] = hydra.grab('longitude').reshape(-1)
                prediction[mirror, track] = hydra.grab('gpp_prediction').reshape(-1)
                deviation[mirror, track] = hydra.grab('gpp_deviation').reshape(-1)
                zenith[mirror, track] = hydra.grab('solar_zenith_angle').reshape(-1)
                view[mirror, track] = hydra.grab('viewing_zenith_angle').reshape(-1)
                water[mirror, track] = hydra.grab('water_mask').reshape(-1)
                cloud[mirror, track] = hydra.grab('cloud_fraction').reshape(-1)
                snow[mirror, track] = hydra.grab('snow_ice_fraction').reshape(-1)
                quality[mirror, track] = hydra.grab('pixel_quality_flag').reshape(-1)
                truth[mirror, track] = hydra.grab('gpp_truth').reshape(-1)
                radiation[mirror, track] = hydra.grab('par_interpolation').reshape(-1)
                reflectance[mirror, track, :] = hydra.grab('reflectance_pcas').reshape(-1, 50)
                matrix[mirror, track, :] = hydra.grab('training_matrix').reshape(-1, 34)
                corners[mirror, track, :] = hydra.grab('latitude_bounds').reshape(-1, 4)
                cornersii[mirror, track, :] = hydra.grab('longitude_bounds').reshape(-1, 4)
                land[mirror, track] = hydra.grab('land_cover').reshape(-1)
                azimuth[mirror, track] = hydra.grab('solar_azimuth_angle').reshape(-1)
                azimuthii[mirror, track] = hydra.grab('viewing_azimuth_angle').reshape(-1)
                relative[mirror, track] = hydra.grab('relative_azimuth_angle').reshape(-1)

            # construct data
            data = {'latitude': latitude, 'longitude': longitude, 'gpp_prediction': prediction}
            data.update({'solar_zenith_angle': zenith, 'viewing_zenith_angle': view})
            data.update({'water_mask': water, 'cloud_fraction': cloud, 'snow_ice_fraction': snow})
            data.update({'pixel_quality_flag': quality, 'gpp_truth': truth})
            data.update({'par_interpolation': radiation, 'reflectance_pcas': reflectance})
            data.update({'training_matrix': matrix, 'gpp_deviation': deviation})
            data.update({'latitude_bounds': corners, 'longitude_bounds': cornersii})
            data.update({'land_cover': land, 'solar_azimuth_angle': azimuth})
            data.update({'viewing_azimuth_angle': azimuthii, 'relative_azimuth_angle': relative})

            # construct destination
            formats = (self.year, self._pad(self.month), self._pad(self.day))
            date = re.search('[0-9]{8}T[0-9]{6}Z', members[0]).group()
            name = 'GPP_Panel_{}_{}.h5'.format(label, date)
            destination = 'GPP_Panels/{}/{}/{}/{}'.format(*formats, name)

            # stash output
            self._print('stashing {}...'.format(destination))
            hydra.spawn(destination, data)

        return None

    def historize(self, scan=7, granule=3, north=35, east=-85, bins=30, instances=1, secondary=False):
        """Make histograms of variational predictions.

        Arguments:
            scan: int, scan number to predict
            position: int, the position of the pixel
            north: float, closest latitude
            east: float, closest longitude
            bins: int, number of histogram bins
            instances: int, number of instances to average
            secondary: boolean, use secondary

        Returns:
            None
        """

        # load model
        model = self.retrieve(secondary=secondary)

        # set network folder
        network = 'GPP_NN'

        # if using secondary model
        if secondary:

            # use secondary model
            network = 'GPP_NN_Secondary'

        # load in par data
        radiation = scipy.io.readsav('PAR/PAR_clrsky_daily.sav')
        grid = radiation['lat_grid_par']
        sky = radiation['par_clearsky']

        # get the scaler
        scaler = Hydra('{}/{}/TEMPO_Test_ML_Scaling.h5'.format(self.sink, network))
        scaler.ingest()
        scale = scaler.grab('Input_Scale')
        minimum = scaler.grab('Input_Min')
        scaleii = scaler.grab('Output_Scale')
        minimumii = scaler.grab('Output_Min')

        # get paths for date
        formats = (self.year, self._pad(self.month), self._pad(self.day))

        # create hydra
        hydra = Hydra('{}/TEMPO_PC_Reduced/{}/{}/{}'.format(self.sink, *formats), show=False)
        paths = hydra.paths

        # if a scan number is given
        if scan:

            # only use paths from that scan
            paths = [path for path in paths if self._label(scan, granule) in path]

        # begin reservoir
        reservoir = {'model': model, 'scale': scale, 'scaleii': scaleii, 'minimum': minimum, 'minimumii': minimumii}
        reservoir.update({'data': {}})

        # for each path
        for path in paths:

            # ingest the first path
            print('ingesting {}...'.format(path))
            hydra.ingest(path)

            # extract the date time
            date = re.search('[0-9]{8}T[0-9]{6}Z', hydra.current).group()

            # get inputs
            reflectance = hydra.grab('smoothed_radiance').transpose(1, 0)
            water = hydra.grab('water_mask')
            mirror = hydra.grab('steps')
            track = hydra.grab('lines')
            mirrors = hydra.grab('mirror_step')
            tracks = hydra.grab('xtrack')
            components = reflectance.shape[1]

            # set shape
            shape = (mirrors, tracks)

            # create nan grid for reflectance
            blank = numpy.ones((mirrors, tracks, components)) * numpy.nan
            blank[mirror, track, :] = reflectance
            reflectance = blank

            # create nan grid for water mask
            blank = numpy.ones(shape) * numpy.nan
            blank[mirror, track] = water
            water = blank

            # read in cloud data
            hydraii = Hydra('{}/TEMPO_CLDO4_L2/{}/{}/{}'.format(self.sink, *formats), show=False)
            hydraii.ingest(date)

            # get crf data
            cloud = hydraii.grab('cloud_fraction')
            snow = hydraii.grab('snow_ice_fraction')
            latitude = hydraii.grab('latitude')
            longitude = hydraii.grab('longitude')
            zenith = hydraii.grab('solar_zenith_angle')
            view = hydraii.grab('viewing_zenith_angle')
            quality = hydraii.grab('pixel_quality_flag')
            corners = hydraii.grab('latitude_bounds')
            cornersii = hydraii.grab('longitude_bounds')
            azimuth = hydraii.grab('solar_azimuth_angle')
            azimuthii = hydraii.grab('viewing_azimuth_angle')
            relative = azimuthii - azimuth

            # create xtract and mirror grids
            track = hydraii.grab('xtrack')
            mirror = hydraii.grab('mirror_step')
            track, mirror = numpy.meshgrid(track, mirror)

            # filter out fills and bad track positions
            cloud[cloud == -2**100] = numpy.nan
            cloud[:, 1027] = numpy.nan
            cloud[:, 1026] = numpy.nan
            cloud[:, 1048] = numpy.nan
            cloud[abs(latitude) > 1e30] = numpy.nan

            # read in colocation data
            hydraiii = Hydra('{}/GPP_Colocations/{}/{}/{}'.format(self.sink, *formats), show=False)
            hydraiii.ingest(date)
            truth = hydraiii.grab('GPP')

            # interpolate clear sky par
            latitudeii = latitude.reshape(-1)
            longitudeii = longitude.reshape(-1)
            interpolation = numpy.interp(latitudeii, grid[::-1], sky[self.day-1, self.month-1, :][::-1])
            interpolation = numpy.array([interpolation]).transpose(1, 0)
            interpolation = interpolation.reshape(shape)

            # assemble matrix
            reflectanceii = reflectance.reshape(-1, components)
            interpolationii = interpolation.reshape(-1, 1)
            zenithii = numpy.cos(zenith).reshape(-1, 1)
            viewii = numpy.cos(view).reshape(-1, 1)
            relativeii = numpy.cos(relative).reshape(-1, 1)
            matrix = numpy.hstack([reflectanceii[:, :30], interpolationii, zenithii, viewii, relativeii])
            matrix = matrix.astype(numpy.float32)

            # apply scale to inputs
            matrix = matrix.transpose(1, 0)
            matrix = (matrix - minimum.reshape(-1, 1)) * scale.reshape(-1, 1)
            matrix = matrix.transpose(1, 0)
            matrix = matrix.astype(numpy.float32)

            # if getting an instance
            predictions = []
            for instance in range(instances):

                # get predictions
                prediction = model(matrix)
                prediction = prediction.numpy().squeeze()
                predictions.append(prediction)

            # apply scale to predictions
            predictions = (predictions / scaleii) + minimumii

            # get the closets position
            position = self._pin([north, east], [latitudeii, longitudeii])[0][0]

            print('latitude, longitude:')
            print(latitudeii[position], longitudeii[position])

            # make histogram of predictions at position
            data = predictions[:, position]
            self._print('data: {}'.format(data.shape))

            # begin plot
            pyplot.clf()
            pyplot.hist(data, bins=bins)

            # set title and destination
            formats = (self._label(scan, granule), self._orient(north), self._orient(east, east=True), instances)
            pyplot.title('Histogram for {}, {}, {}, {} instances'.format(*formats))
            destination = 'GPP_Histogram/Histogram_{}_{}_{}'.format(*formats)

            # savefigure
            pyplot.savefig(destination)
            pyplot.clf()

        return None

    def land(self, chart=True, series=True):
        """Plot the land cover map.

        Arguments:
            chart: boolean, make map?

        Returns:
            None
        """

        # get Regression file
        hydra = Hydra('GPP_Regression')
        hydra.ingest(self.date)

        # get panel paths
        hydraii = Hydra('GPP_Panels/{}'.format(self.stub))
        numbers = [int(re.search('_S[0-9]{3}G', path).group()[2:5]) for path in hydraii.paths]
        times = [int(re.search('[0-9]{8}T[0-9]{6}Z', path).group()[9:13]) for path in hydraii.paths]
        hours = {index: ' ' for index in range(20)}
        hours.update({number: 'UTC {}'.format(time) for number, time in zip(numbers, times)})

        # construct categories
        categories = [('lightblue', 'water'), ('darkgreen', 'egn ndle'), ('green', 'egn brdlf')]
        categories += [('orange', 'dec ndle'), ('salmon', 'dec brdlf'), ('lightgreen', 'mxd frst')]
        categories += [('brown', 'c shrbld'), ('tan', 'o shrbld'), ('khaki', 'wdy svna')]
        categories += [('beige', 'svna'), ('lime', 'grsslnd'), ('aquamarine', 'wetlnd')]
        categories += [('yellow', 'croplnd'), ('gray', 'urban'), ('orchid', 'mosaic')]
        categories += [('ivory', 'snow'), ('lavender', 'barren')]

        # load in lands
        lands = self._acquire('Land_Types/land.yaml')

        # if makeing map
        if chart:

            # get land data
            latitude = hydra.grab('latitude')
            longitude = hydra.grab('longitude')
            land = hydra.grab('land')

            # make mask
            mask = numpy.isfinite(latitude) & numpy.isfinite(longitude) & numpy.isfinite(land)
            mask = mask & (land < 255)

            # apply mask
            latitude = latitude[mask]
            longitude = longitude[mask]
            land = land[mask]

            print(latitude.shape, latitude.min(), latitude.max())
            print(longitude.shape, longitude.min(), longitude.max())
            print(land.shape, land.min(), land.max())

            # set bounds
            bounds = [(-125, -60), (15, 55)]
            scale = (0, 17)

            # construct samples graph
            unit = 'land cover'
            title = 'Land Cover, {}\nn'.format(self.date)
            gradient = 'tab20'
            selection = (0, 216)
            labels = ['', '']
            options = {'globe': True, 'pixel': 1, 'polygons': False, 'selection': selection, 'categories': categories}
            graph = self._graph(longitude, latitude, land, bounds, scale, gradient, title, unit, labels, **options)

            # plot
            destination = 'GPP_Land_Plots/Land_plot_{}.png'.format(self.date)
            self._paint(destination, [graph])

        # # begin graphs
        # graphs = []
        #
        # # extract data
        # data = hydra.extract()
        #
        # # for each position
        # for position in range(2, 10):
        #
        #     # get gpp and land
        #     prediction = data['prediction'][:, position]
        #     valid = data['valid'][:, position]
        #     land = data['land']
        #     time = data['time'][:, position]
        #
        #     # make mask and apply
        #     mask = (valid == 1) & (numpy.isfinite(land)) & (land < 255)
        #     prediction = prediction[mask]
        #     land = land[mask]
        #     time = time[mask]
        #
        #     # add random spread to abscissa
        #     random = numpy.random.rand(*land.shape) - 0.5
        #     abscissa = land + random
        #     ordinate = prediction
        #
        #     # set title
        #     title = 'GPP versus landtype, scan: {}'.format(self._label(time[0]))
        #
        #     # set line
        #     lines = [(abscissa, ordinate, 'b.')]
        #
        #     # set blank
        #     blank = numpy.array([0])
        #
        #     # set limit and scale
        #     limit = [(0, 17), (0, 17)]
        #     scale = (0, 17)
        #     unit = '_'
        #     label = ['', 'GPP']
        #
        #     # set ticks
        #     ticks = [index for index in range(17)]
        #     texts = [category[1] for category in categories]
        #
        #     # construct graphs
        #     options = {'polygons': False, 'corners': None, 'globe': False, 'lines': lines}
        #     options.update({'colorbar': False, 'marker': 0.01, 'ticks': ticks, 'marks': texts})
        #     graph = self._graph(blank, blank, blank, limit, scale, 'plasma', title, unit, label, **options)
        #     graphs.append(graph)
        #
        # # print status
        # self._print('done plotting.')
        #
        # # create plots
        # destination = '{}/GPP_Regression_Land_Cover/Regression_Land_{}_01.png'.format(self.sink, self.date)
        # self._clean(destination, force=True)
        # self._paint(destination, graphs[0:4])
        # destination = '{}/GPP_Regression_Land_Cover/Regression_Land_{}_02.png'.format(self.sink, self.date)
        # self._clean(destination, force=True)
        # self._paint(destination, graphs[4:8])

        # begin graphs
        graphs = []

        # extract data
        data = hydra.extract()

        # for each position
        for form in range(1, 17):

            # mask form
            land = data['land']
            mask = (land == form)

            # get gpp and land
            prediction = data['prediction'][mask]
            valid = data['valid'][mask]
            # land = data['land']
            time = data['time'][mask]

            # make mask and apply
            maskii = (valid == 1)
            prediction = prediction[maskii]
            time = time[maskii]

            # add random spread to abscissa
            random = numpy.random.rand(*time.shape) - 0.5
            abscissa = time + random
            ordinate = prediction

            # set title
            title = 'GPP vs time: {}, {}'.format(self.date, lands[form])

            # set line
            lines = [(abscissa, ordinate, 'b.')]

            # set blank
            blank = numpy.array([0])

            # set limit and scale
            limit = [(1, 13), (0, 15)]
            scale = (0, 13)
            unit = '_'
            label = ['scan number', 'GPP']

            # set ticks
            ticks = [index - 1 for index in range(2, 13)]
            texts = [hours[index] for index in range(2, 12)] + ['']

            # construct graphs
            options = {'polygons': False, 'corners': None, 'globe': False, 'lines': lines}
            options.update({'colorbar': False, 'marker': 0.05, 'ticks': ticks, 'marks': texts})
            graph = self._graph(blank, blank, blank, limit, scale, 'plasma', title, unit, label, **options)
            graphs.append(graph)

        # print status
        self._print('done plotting.')

        # for each quartet
        for quartet in range(4):

            # create plots
            formats = (self.sink, self.date, quartet)
            destination = '{}/GPP_Regression_Land_Cover/Regression_Land_Series_{}_{}.png'.format(*formats)
            self._clean(destination, force=True)
            self._paint(destination, graphs[4 * quartet:4 + 4 * quartet])

        return None

        # # begin status
        # self._stamp('plotting {}...'.format(destination), initial=True)
        #
        # # erase preevious copy
        # self._clean(destination, force=True)

        # # extract d
        # data = hydra.extract()
        #
        # # get scores and latitude, longitude
        # score = data['score']
        # scoreii = data['scoreii']
        # curvature = data['coefficient'][:, 0]
        # slope = data['coefficientii'][:, 0]
        # constant = data['coefficient'][:, 2]

        # # calculate center and height
        # center = -slope / (2 * curvature)
        # height = constant - (slope ** 2) / (4 * curvature)

        # get other attributes
        # latitude = data['latitude']
        # longitude = data['longitude']
        # corners = data['corners']
        # cornersii = data['cornersii']
        # land = data['land']
        # prediction = data['prediction']
        # # mirror = data['mirror']
        # track = data['track']

        # # create titles
        # modes = ['R^2', 'curvature']
        # titles = ['GPP quadratic fit {}, {}\n'.format(mode, self.date) for mode in modes]
        #
        # # create titles
        # modes = ['R^2', 'slope']
        # titles += ['GPP linear fit {}, {}\n'.format(mode, self.date) for mode in modes]
        #
        # # set scale
        # scales = [(0, 1), (-0.1, 0.1), (0, 1), (-0.3, 0.3)]
        #
        # # set ticks
        # ticks = [(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0)]
        # ticks += [(-0.1, -0.08, -0.06, -0.04, -0.02, -0.0, 0.02, 0.04, 0.06, 0.08, 0.1)]
        # ticks += [(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0)]
        # ticks += [(-0.3, -0.2, -0.1, -0.0, 0.1, 0.2, 0.3)]

        # # set gradiants
        # gradients = ['plasma'] + ['coolwarm'] * 3
        #
        # # set blank
        # blank = numpy.array([0])
        #
        # # set arrays
        # arrays = [[blank, land, array] for array in (score, curvature, scoreii, slope)]
        #
        # # set bounds
        # limits = [[(-1, 17), scale] for scale in scales]
        #
        # # set labels
        # labels = [['land type', field] for field in ('score', 'curvature', 'score', 'slope')]
        #
        # # set units
        # units = ['R^2', '-', 'R^2', '-']
        #
        # # begin graphs
        # graphs = []
        # zipper = zip(arrays, titles, labels, limits, scales, gradients, units, ticks)
        # for triplet, title, label, limit, scale, gradient, unit, tick in zipper:
        #
        #     # unpack triplet
        #     tracer, abscissa, ordinate = triplet
        #
        #     # add random spread to abscissa
        #     random = numpy.random.rand(*abscissa.shape) - 0.5
        #     abscissa = abscissa + random
        #
        #     # set line
        #     lines = [(abscissa, ordinate, 'b.')]
        #
        #     # construct graphs
        #     options = {'polygons': False, 'corners': [corners, cornersii], 'globe': False, 'lines': lines}
        #     options.update({'colorbar': False, 'marker': 0.01, 'ticksii': tick, 'marksii': tick})
        #     graph = self._graph(blank, blank, blank, limit, scale, gradient, title, unit, label, **options)
        #     graphs.append(graph)
        #
        # # plot
        # self._paint(destination, graphs)

        # print status
        self._print('done plotting.')

        return None

    def persist(self, *pixel):
        """Check timeseries for a single pixel across multiple days.

        Arguments:
            *pixel: unpacked tuple of ints, the pixel coordinates

        Returns:
            None
        """

        # begin graphs
        graphs = []

        # grab regressions
        hydra = Hydra('GPP_Regression')

        # get land types
        lands = hydra._acquire('Land_Types/land.yaml')
        code = 0

        # for each path
        for path in hydra.paths:

            # ingest data
            hydra.ingest(path)

            # extract date
            date = path.split('.')[0].split('_')[-1]

            # grab mirror and track
            mirror = hydra.grab('mirror')
            track = hydra.grab('track')

            # get closest index
            index = hydra._pin(pixel, [mirror, track])[0][0]

            # extract other data data
            latitude = hydra.grab('latitude')[index]
            longitude = hydra.grab('longitude')[index]
            prediction = hydra.grab('prediction')[index]
            deviation = hydra.grab('deviation')[index]
            time = hydra.grab('time')[index]
            valid = hydra.grab('valid')[index].astype(bool)
            code = int(hydra.grab('land')[index])
            land = lands[code]

            # create title
            formats = (date, self._orient(latitude), self._orient(longitude, east=True))
            title = 'Timewise GPP +/- 1 std {}, {}, {}\npixel: ( {}, {} ), {}'.format(*formats, *pixel, land)

            # # create valid mask
            # valid = numpy.isfinite(prediction)

            # set scales
            scale = (0, self.limits['truth'])

            # set gradiants
            gradient = 'coolwarm'

            # set units
            unit = '%'

            # set abscissa and ordinate
            abscissa = time[valid]
            ordinate = prediction[valid]
            offset = deviation[valid]
            ordinateii = ordinate - offset
            ordinateiii = ordinate + offset

            # make blank array
            blank = numpy.array([0])

            # create graph
            labels = ['scan', 'GPP']
            lines = [(abscissa, ordinate, 'bo-'), (abscissa, ordinateii, 'g--'), (abscissa, ordinateiii, 'g--')]
            options = {'globe': False, 'colorbar': False, 'pixel': 0.01, 'lines': lines, 'marker': 3}
            limits = [(1, 12), (0, self.limits['truth'] + 1)]
            parameters = (blank, blank, blank, limits, scale, gradient, title, unit, labels)
            graph = self._graph(*parameters, **options)
            graphs.append(graph)

            print('prediction: {}'.format(prediction))
            print('time: {}'.format(time))
            print('abscissa: {}'.format(abscissa))
            print('ordinate: {}'.format(ordinate))

        # set destination
        formats = (self._pad(code), self._pad(pixel[0], 4), self._pad(pixel[1], 4))
        destination = '{}/GPP_Persistence_Plots/Persistence_Plot_{}_{}_{}.png'.format(self.sink, *formats)
        self._paint(destination, graphs)

        return None

    def point(self, scan=6, method='pixel', parallels=None, meridians=None):
        """Plot timewise slices.

        Arguments:
            scan: int, the scan number for comparison
            method: str, 'nearest', 'pixel'
            parallels: list of floats, the latitude coordiantes
            meridians: list of loats, the longitude coordinates

        Returns:
            None
        """

        # set default parallels
        parallels = parallels or (20, 22, 25, 28, 30, 32, 35, 38, 40, 42, 45)
        meridians = meridians or (-110, -108, -105, -102, -100, -98, -95, -92)

        # distil data
        reservoir, times, noon = self.distil(scan=scan)

        # get latitude and track positions
        indices = [self._pin(parallel, reservoir[noon]['latitude'], 1)[0][1] for parallel in parallels]
        assignments = indices

        # get latitude and track positions
        indices = [self._pin(meridian, reservoir[noon]['longitude'], 1)[0][0] for meridian in meridians]
        assignmentsii = indices

        # for each parallel
        for parallel, assignment in zip(parallels, assignments):

            # for each meridian
            for meridian, assignmentii in zip(meridians, assignmentsii):

                # create destination
                formats = (self._orient(parallel), self._orient(meridian, east=True), noon)
                destination = 'GPP_Point_Plots/Point_Plot_{}_{}_vs_{}.png'.format(*formats)

                # begin status
                self._stamp('plotting {}...'.format(destination), initial=True)

                # erase preevious copy
                self._clean(destination, force=True)

                # create titles
                labels = list(reservoir.keys())
                labels.sort()
                time = numpy.array([int(label[1:-1]) for label in labels])
                titles = ['GPP Prediction {}, {}\n'.format(*formats[:2])]
                # titles += ['Noon % Difference {}, {}\n'.format(time, degrees)]
                titles += ['Noon Difference {}, {}\n'.format(*formats[:2])]

                # create arrays
                prediction = numpy.array([reservoir[noon]['gpp_prediction'] for label in labels])
                predictionii = numpy.array([reservoir[label]['gpp_prediction'] for label in labels])
                water = numpy.array([reservoir[label]['water_mask'] for label in labels])
                cloud = numpy.array([reservoir[label]['cloud_fraction'] for label in labels])
                snow = numpy.array([reservoir[label]['snow_ice_fraction'] for label in labels])
                zenith = numpy.array([reservoir[label]['solar_zenith_angle'] for label in labels])
                view = numpy.array([reservoir[label]['viewing_zenith_angle'] for label in labels])
                latitude = numpy.array([reservoir[label]['latitude'] for label in labels])
                longitude = numpy.array([reservoir[label]['longitude'] for label in labels])

                # create percent difference
                percent = self._relate(prediction, predictionii)


                print(latitude.shape)
                print(longitude.shape)
                print(percent.shape)


                print('percent calculated.')

                # create valid data mask
                valid = (water == 1) & (numpy.isfinite(cloud))
                valid = valid & (numpy.isfinite(predictionii))
                valid = valid & (numpy.isfinite(zenith))
                valid = valid & (cloud <= self.limits['cloud'])
                valid = valid & (zenith <= self.limits['zenith'])
                valid = valid & (view <= self.limits['view'])
                valid = valid & (snow <= self.limits['snow'])
                valid = valid & (predictionii <= self.limits['truth']) & (prediction <= self.limits['truth'])
                valid = valid[:, assignmentii, assignment]

                # set scales
                scale = (0, self.limits['truth'])
                scaleii = (-10, 10)
                scales = (scale, scaleii)

                # clip data
                tracer = predictionii[:, assignmentii, assignment]
                tracer = numpy.where((tracer < scale[0]) & numpy.isfinite(tracer), scale[0], tracer)
                tracer = numpy.where((tracer > scale[1]) & numpy.isfinite(tracer), scale[1], tracer)

                # clip percent
                tracerii = percent[:, assignmentii, assignment]
                tracerii = numpy.where((tracerii < scaleii[0]) & numpy.isfinite(tracer), scaleii[0], tracerii)
                tracerii = numpy.where((tracerii > scaleii[1]) & numpy.isfinite(tracer), scaleii[1], tracerii)

                # set tracers
                tracers = [tracer, tracerii]

                # set gradiants
                gradients = [matplotlib.cm.plasma, matplotlib.cm.coolwarm]
                brackets = ['prediction', 'percent']

                # set units
                units = ['$g C/m^2 d$', '%']

                # set up figure
                pyplot.clf()
                pyplot.margins(1.0, 0.1)
                figure, axes = pyplot.subplots(1, 2, figsize=(8, 5))
                pyplot.subplots_adjust(wspace=0.4, hspace=0.6)

                # for each set
                zipper = zip(tracers, axes, titles, gradients, scales, units)
                for tracer, axis, title, gradient, scale, unit in zipper:

                    print('tracer: {}'.format(tracer.shape))
                    print('valid: {}'.format(valid.shape))

                    # create mask and apply
                    # mask = numpy.isfinite(tracer[valid])
                    # data = tracer[valid][mask]
                    mask = numpy.isfinite(tracer)
                    data = numpy.where(valid & mask, tracer, numpy.nan)
                    # abscissa = meridians[mask]
                    # ordinate = parallels[mask]
                    abscissa = longitude[:, assignmentii, assignment]
                    ordinate = latitude[:, assignmentii, assignment]

                    print("here!")
                    print('abscissa: {}'.format(abscissa.shape))
                    print('ordinate: {}'.format(ordinate.shape))

                    print(mask.sum())
                    # print('')
                    # print(bracket)
                    print(data.min())
                    print(data.max())
                    print(numpy.percentile(data, 10), numpy.percentile(data, 90))

                    # set scale
                    minimum = scale[0]
                    maximum = scale[1]

                    # # determine min and max
                    # minimum = data.min()
                    # maximum = data.max()

                    # # if bracket is percent
                    # if bracket == 'percent':
                    #
                    #     # determine percent min and max
                    #     absolute = max([abs(minimum), abs(maximum)])
                    #     minimum = -absolute - 1
                    #     maximum = absolute + 1
                    #
                    #     print(absolute)

                    # # set cartopy axis with coastlines
                    # axis.coastlines()
                    # _ = axis.gridlines(draw_labels=True)

                    # take subset within parallel
                    # subset = (ordinate > parallel - 0.01) & (ordinate < parallel + 0.01)
                    print('subsetting...')
                    # print('track: {}'.format(track.shape))
                    # print('position: {}'.format(assignment))
                    # subset = (track == assignment)
                    #
                    # print('subset, {}: {}'.format(assignment, subset.sum()))

                    # dataii = data[:, assignment]
                    # abscissaii = abscissa[:, assignment]
                    ordinate = data

                    # begin plot with title
                    axis.grid(True)
                    # axis.set_global()
                    axis.set_aspect('auto')
                    axis.set_title(title, fontsize=10)
                    axis.tick_params(axis='both', labelsize=7)

                    # set up colors and logarithmic scale
                    colors = gradient
                    normal = matplotlib.colors.Normalize(minimum, maximum)

                    # plot modis / tempo data
                    # axis.scatter(abscissa, ordinate, marker='.', s=0.5, c=data, cmap=colors, norm=normal)
                    # axis.scatter(abscissaii, dataii, marker='x', s=0.5, color='black', norm=normal)
                    axis.plot(time, ordinate, 'b-')

                    # # set axis limits
                    # axis.set_xlim(abscissa.min() - 5, abscissa.max() + 5)
                    # axis.set_ylim(ordinate.min() - 5, ordinate.max() + 5)
                    axis.set_xlim(-150, -50)
                    axis.set_xlim(0, 12)
                    axis.set_ylim(*scale)

                    # add colorbar
                    position = axis.get_position()
                    bar = pyplot.gcf().add_axes([position.x0, position.y0 - 0.065, position.width, 0.02])
                    scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
                    colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', label=unit)
                    formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
                    colorbar.ax.yaxis.set_major_formatter(formatter)

                # save to destination
                pyplot.savefig(destination)
                pyplot.clf()

                # end status
                self._stamp('plotted.')

        return None

    def predict(self, scan=None, give=False, verify=True, instances=100, secondary=False):
        """Make predictions from the NN model.

        Arguments:
            scan: int, scan number to predict
            give: boolean, return outputs?
            verify: boolean, make verification plot?
            instances: int, number of instances to average
            secondary: boolean, use secondary

        Returns:
            None
        """

        # load model
        model = self.retrieve(secondary=secondary)

        # set network folder
        network = 'GPP_NN'

        # if using secondary model
        if secondary:

            # use secondary model
            network = 'GPP_NN_Secondary'

        # load in par data
        radiation = scipy.io.readsav('PAR/PAR_clrsky_daily.sav')
        grid = radiation['lat_grid_par']
        sky = radiation['par_clearsky']

        # get the scaler
        scaler = Hydra('{}/{}/TEMPO_Test_ML_Scaling.h5'.format(self.sink, network))
        scaler.ingest()
        scale = scaler.grab('Input_Scale')
        minimum = scaler.grab('Input_Min')
        scaleii = scaler.grab('Output_Scale')
        minimumii = scaler.grab('Output_Min')

        # get paths for date
        formats = (self.year, self._pad(self.month), self._pad(self.day))

        # create hydra
        hydra = Hydra('{}/TEMPO_PC_Reduced/{}/{}/{}'.format(self.sink, *formats), show=False)
        paths = hydra.paths

        # if a scan number is given
        if scan:

            # only use paths from that scan
            paths = [path for path in paths if '_S{}G'.format(self._pad(scan, 3)) in path]

        # begin reservoir
        reservoir = {'model': model, 'scale': scale, 'scaleii': scaleii, 'minimum': minimum, 'minimumii': minimumii}
        reservoir.update({'data': {}})

        # for each path
        for path in paths:

            # ingest the first path
            print('ingesting {}...'.format(path))
            hydra.ingest(path)

            # extract the date time
            date = re.search('[0-9]{8}T[0-9]{6}Z', hydra.current).group()

            # get inputs
            reflectance = hydra.grab('smoothed_radiance').transpose(1, 0)
            water = hydra.grab('water_mask')
            mirror = hydra.grab('steps')
            track = hydra.grab('lines')
            mirrors = hydra.grab('mirror_step')
            tracks = hydra.grab('xtrack')
            components = reflectance.shape[1]

            # set shape
            shape = (mirrors, tracks)

            # create nan grid for reflectance
            blank = numpy.ones((mirrors, tracks, components)) * numpy.nan
            blank[mirror, track, :] = reflectance
            reflectance = blank

            # create nan grid for water mask
            blank = numpy.ones(shape) * numpy.nan
            blank[mirror, track] = water
            water = blank

            # read in cloud data
            hydraii = Hydra('{}/TEMPO_CLDO4_L2/{}/{}/{}'.format(self.sink, *formats), show=False)
            hydraii.ingest(date)

            # get crf data
            cloud = hydraii.grab('cloud_fraction')
            snow = hydraii.grab('snow_ice_fraction')
            latitude = hydraii.grab('latitude')
            longitude = hydraii.grab('longitude')
            zenith = hydraii.grab('solar_zenith_angle')
            view = hydraii.grab('viewing_zenith_angle')
            quality = hydraii.grab('pixel_quality_flag')
            corners = hydraii.grab('latitude_bounds')
            cornersii = hydraii.grab('longitude_bounds')
            azimuth = hydraii.grab('solar_azimuth_angle')
            azimuthii = hydraii.grab('viewing_azimuth_angle')
            relative = azimuthii - azimuth

            # get ground pixel quality
            ground = hydraii.grab('ground_pixel_quality')

            # extract bits for land type
            mask = (1 << (23 - 16 + 1)) - 1
            land = (ground >> 16) & mask

            print('land: {}, {}'.format(land.min(), land.max()))

            # create xtract and mirror grids
            track = hydraii.grab('xtrack')
            mirror = hydraii.grab('mirror_step')
            track, mirror = numpy.meshgrid(track, mirror)

            # filter out fills and bad track positions
            cloud[cloud == -2**100] = numpy.nan
            cloud[:, 1027] = numpy.nan
            cloud[:, 1026] = numpy.nan
            cloud[:, 1048] = numpy.nan
            cloud[abs(latitude) > 1e30] = numpy.nan

            # read in colocation data
            hydraiii = Hydra('{}/GPP_Colocations/{}/{}/{}'.format(self.sink, *formats), show=False)
            hydraiii.ingest(date)
            truth = hydraiii.grab('GPP')

            # interpolate clear sky par
            latitudeii = latitude.reshape(-1)
            interpolation = numpy.interp(latitudeii, grid[::-1], sky[self.day-1, self.month-1, :][::-1])
            interpolation = numpy.array([interpolation]).transpose(1, 0)
            interpolation = interpolation.reshape(shape)

            # assemble matrix
            reflectanceii = reflectance.reshape(-1, components)
            interpolationii = interpolation.reshape(-1, 1)
            zenithii = numpy.cos(zenith).reshape(-1, 1)
            viewii = numpy.cos(view).reshape(-1, 1)
            relativeii = numpy.cos(relative).reshape(-1, 1)
            matrix = numpy.hstack([reflectanceii[:, :30], interpolationii, zenithii, viewii, relativeii])
            matrix = matrix.astype(numpy.float32)

            # apply scale to inputs
            matrix = matrix.transpose(1, 0)
            matrix = (matrix - minimum.reshape(-1, 1)) * scale.reshape(-1, 1)
            matrix = matrix.transpose(1, 0)
            matrix = matrix.astype(numpy.float32)

            # if getting an instance
            predictions = []
            for instance in range(instances):

                # get predictions
                prediction = model(matrix)
                prediction = prediction.numpy().squeeze()
                predictions.append(prediction)

            # apply scale to predictions
            predictions = (predictions / scaleii) + minimumii

            # take mean and standard deviation of predictions
            prediction = numpy.array(predictions).mean(axis=0)
            deviation = numpy.array(predictions).std(axis=0)

            # reshape prediction and matrix
            matrix = matrix.reshape((shape[0], shape[1], -1))
            prediction = prediction.reshape(shape)
            deviation = deviation.reshape(shape)

            # create valid indices based on water mask and clouds, and eliminate
            valid = (water.squeeze() == 1) & (numpy.isfinite(cloud.squeeze()))
            prediction = numpy.where(valid, prediction, numpy.nan)

            # # open up land forms
            # hydraiii = Hydra('Land')
            # hydraiii.ingest()
            # landiii = hydraiii.grab('land')
            # latitudeiii = hydraiii.grab('latitude')
            # longitudeiii = hydraiii.grab('longitude')
            #
            # # use scipy nearest neighbors
            # parameters = ((latitudeiii, longitudeiii), landiii, (latitude, longitude))
            # options = {'method': 'nearest', 'bounds_error': False, 'fill_value': self.fill}
            # land = scipy.interpolate.interpn(*parameters, **options)

            # store predictions
            data = {'latitude': latitude, 'longitude': longitude, 'gpp_prediction': prediction}
            data.update({'solar_zenith_angle': zenith, 'viewing_zenith_angle': view, 'water_mask':  water})
            data.update({'cloud_fraction': cloud, 'snow_ice_fraction': snow})
            data.update({'xtrack': track, 'mirror_step': mirror, 'par_interpolation': interpolation})
            data.update({'training_matrix': matrix, 'gpp_truth': truth, 'pixel_quality_flag': quality})
            data.update({'reflectance_pcas': reflectance, 'gpp_deviation': deviation})
            data.update({'latitude_bounds': corners, 'longitude_bounds': cornersii})
            data.update({'land_cover': land, 'solar_azimuth_angle': azimuth})
            data.update({'viewing_azimuth_angle': azimuthii, 'relative_azimuth_angle': relative})

            # view data
            self._view(data)

            # stash
            name = hydra._file(hydra.current).replace('.nc', '_predicted.h5')
            destination = 'GPP_Predictions/{}/{}/{}/{}'.format(*formats, name)
            hydra.spawn(destination, data)

            # add to reservoir
            reservoir['data'][date] = data

        # default package to None
        package = None

        # if give
        if give:

            # set package to last data
            package = reservoir

        return package

    def present(self, destination, arrays, corners, cornersii, instances, additions, chart=True):
        """Make presetation plots.

        Arguments:
            destination: str, destination filepath
            arrays: tuple of numpy arrays, the truth, prediction, latitue, longitude, and crf
            corners: latitude polygon corners
            cornersii: longitude polygon corners
            instances: int, number of prediction instances
            additions: tuple of numpy arrays, additional data
            chart: boolean, plot maps?

        Returns:
            None
        """

        # begin status
        self._stamp('plotting {}...'.format(destination), initial=True)

        # erase preevious copy
        self._clean(destination, force=True)

        # extract date and time from destination
        date, time = re.search('[0-9]{4}m[0-9]{4}_T[0-9]{6}Z', destination).group().split('_')
        time = time[1:-1]
        time = str(int(time) - 60000)
        time = '{}:{}'.format(time[:2], time[2:4])

        # unpack arrays
        truth, prediction, latitude, longitude, cloud, uncertainty, deviation = arrays

        # caluclate ratio
        difference = prediction - truth

        print(truth.shape)
        print(cloud.shape)
        print('prediction: {}'.format(prediction.max()))
        print('truth: {}'.format(truth.max()))
        print('difference: {}, {}'.format(difference.min(), difference.max()))

        # calculate r2 score
        score = r2_score(truth, prediction)

        # calculate regression line
        slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(truth, prediction)
        # create titles
        titles = ['GPP Truth {}\n'.format(re.search('[0-9]{4}m[0-9]{4}_T[0-9]{6}Z', destination).group())]
        titles += ['GPP Prediction {}\n'.format(re.search('[0-9]{4}m[0-9]{4}_T[0-9]{6}Z', destination).group())]
        titles += ['Prediction vs Truth\n( R^2 = {} )'.format(self._round(score, 2))]
        titles += ['Percent Difference\n']

        # calculate differences
        difference = prediction - truth
        difference = numpy.where(numpy.isfinite(difference), difference, 0)

        # calculate sample bins
        bins = 1000
        options = {'statistic': 'count', 'range': ((0, self.limits['truth']), (0, self.limits['truth'])), 'bins': bins}
        counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(truth, prediction, None, **options)
        truths = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
        predictions = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
        mesh, meshii = numpy.meshgrid(truths, predictions)

        # apply mask
        mask = counts > 0
        counts = counts[mask]
        mesh = mesh[mask]
        meshii = meshii[mask]

        # clip counters to 100
        samples = 100
        counts = numpy.where(counts > samples, samples, counts)

        # order counters
        order = numpy.argsort(counts)
        counts = counts[order]
        mesh = mesh[order]
        meshii = meshii[order]

        # get minimum and maximum tracer values
        minimum = min([truth.min(), prediction.min()])
        maximum = max([truth.max(), prediction.max()])

        # set percent bracket
        absolute = max([abs(numpy.percentile(difference, 5)), abs(numpy.percentile(difference, 95))])

        # if plotting maps:
        if chart:

            # set boundaries
            boundaries = [[(-125, -70), (25, 50)]]
            boundaries += [[(-100, -80), (28, 40)]]
            boundaries += [[(-95, -85), (29, 36)]]

            # set alphas
            alphas = [1.0, 1.0, 1.0]

            # set longitude ticks
            ticks = [(-120, -110, -100, -90, -80, -70)]
            ticks += [(-100, -95, -90, -85, -80)]
            ticks += [(-95, -90, -85)]

            # set longitude tickmarks
            marks = [[self._orient(entry, east=True).strip('0') for entry in tick] for tick in ticks]

            # set latitude ticks
            ticksii = [(30, 35, 40, 45, 50)]
            ticksii += [(30, 35, 40)]
            ticksii += [(30, 32, 34)]

            # set latitude tickmarks
            marksii = [[self._orient(entry).strip('0') for entry in tick] for tick in ticksii]

            # set boundaries
            zipper = zip(boundaries, alphas, ticks, marks, ticksii, marksii)
            for bounds, alpha, tick, mark, tickii, markii in zipper:

                # construct geo tag
                formats = [self._orient(bounds[0][0], east=True), self._orient(bounds[0][1], east=True)]
                formats += [self._orient(bounds[1][0]), self._orient(bounds[1][1])]
                tag = '{}{}{}{}'.format(*formats)

                # replace destination
                destinationii = destination.replace('.png', '_{}.png'.format(tag))
                destinationiii = destination.replace('.png', '_uncertainty_{}.png'.format(tag))

                # set bounds
                scale = (minimum, maximum)
                scaleii = (-absolute, absolute)

                # begin graphs
                graphs = []

                # construct GPP truth plot
                title = 'GPP FluxSat {} {} CST'.format(date, time)
                unit = '$g C/m^2 d$'
                labels = ['', '']
                options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
                options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
                graph = self._graph(longitude, latitude, truth, bounds, scale, 'plasma', title, unit, labels, **options)
                graphs.append(graph)

                # construct GPP prediction plot
                title = 'GPP Prediction {} {} CST'.format(date, time)
                unit = '$g C/m^2 d$'
                labels = ['', '']
                options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
                options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
                parameters = (longitude, latitude, prediction, bounds, scale, 'plasma', title, unit, labels)
                graph = self._graph(*parameters, **options)
                graphs.append(graph)

                # unpack reflectivity
                latitudeii, longitudeii, norths, easts, colors, land = additions

                # # get brightest and dimmest intensities after filtering out large numbers
                # brightest = numpy.where(colors > 10, 0, colors).max(axis=0)
                # dimmest = numpy.where(colors < -10, 0, colors).min(axis=0)
                #
                # # normalizing colors
                # colors = (colors - dimmest) / (brightest - dimmest)

                # clip boundaries
                colors = numpy.where(colors > 1, 1, colors)
                colors = numpy.where(colors < 0, 0, colors)
                mask = (numpy.isfinite(latitudeii)) & (numpy.isfinite(longitudeii)) & (land != 0)
                mask = mask & (latitudeii > (-longitudeii - 95))
                colors = colors[mask]
                latitudeii = latitudeii[mask]
                longitudeii = longitudeii[mask]
                norths = norths[mask]
                easts = easts[mask]

                pixel = self._pin([25, -90], [latitudeii, longitudeii])[0]
                print('pixel: {}'.format(pixel))
                print('color: {}'.format(colors[pixel]))
                print('laitutde: {}'.format(latitudeii[pixel]))
                print('longitude: {}'.format(longitudeii[pixel]))
                print('land: {}'.format(land[pixel]))

                # construct rgb plot
                title = 'RGB Map {} {} CST'.format(date, time)
                unit = '$g C/m^2 d$'
                labels = ['', '']
                options = {'polygons': True, 'corners': [norths, easts], 'globe': True, 'colorbar': False}
                options.update({'reflectivity': True, 'ticks': tick, 'alpha': alpha})
                options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
                parameters = (longitudeii, latitudeii, colors, bounds, scaleii, 'plasma', title, unit, labels)
                graph = self._graph(*parameters, **options)
                graphs.append(graph)

                # construct percent difference plot
                title = 'Difference ( Prediction - FluxSat )'
                unit = '$g C/m^2 d$'
                labels = ['', '']
                options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
                options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
                parameters = (longitude, latitude, difference, bounds, scaleii, 'coolwarm', title, unit, labels)
                graph = self._graph(*parameters, **options)
                graphs.append(graph)

                # construct percent difference plot
                title = 'FluxSat Uncertainty {}'.format(date)
                unit = '$g C/m^2 d$'
                labels = ['', '']
                # scaleiii = (uncertainty.min(), uncertainty.max())
                scaleiii = (0, 3)
                options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
                options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
                parameters = (longitude, latitude, uncertainty, bounds, scaleiii, 'plasma', title, unit, labels)
                graph = self._graph(*parameters, **options)
                graphs.append(graph)

                # construct percent difference plot
                title = 'Prediction Stdev {} {} CST'.format(date, time)
                unit = '$g C/m^2 d$'
                labels = ['', '']
                # scaleiii = (deviation.min(), numpy.percentile(deviation, 95))
                scaleiii = (0, 3)
                options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
                options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
                parameters = (longitude, latitude, deviation, bounds, scaleiii, 'plasma', title, unit, labels)
                graph = self._graph(*parameters, **options)
                graphs.append(graph)

                # create plots
                self._paint(destinationii, graphs[:4])
                space = {'top': 0.88, 'bottom': 0.28, 'wspace': 0.35, 'right': 0.95}
                self._paint(destinationiii, graphs[4:6], size=(16, 8), space=space, gap=0.11)

        # begin scatter plots
        scatters = []

        # set cloud sceens
        fractions = [(0, 1.0), (0, 0.25), (0.25, 0.5), (0.5, 0.75), (0.75, 1.0)]
        screens = [(one * self.limits['cloud'], two * self.limits['cloud']) for one, two in fractions]

        # for each screen
        for screen in screens:

            # create less than or equal symbol
            symbol = '\u2264'

            # unpack screen
            low, high = screen

            # apply screen
            mask = (cloud >= low) & (cloud <= high)
            truthii = truth[mask]
            predictionii = prediction[mask]
            cloudii = cloud[mask]
            differenceii = difference[mask]

            # calculate r2 score
            score = r2_score(truthii, predictionii)

            # calculate regression line
            slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(truthii, predictionii)

            # calculate sample bins
            bins = 1000
            options = {'statistic': 'count', 'range': ((0, self.limits['truth']), (0, self.limits['truth']))}
            options.update({'bins': bins})
            counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(truthii, predictionii, None, **options)
            truths = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
            predictions = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
            mesh, meshii = numpy.meshgrid(truths, predictions, indexing='ij')

            # apply mask
            mask = counts > 0
            counts = counts[mask]
            mesh = mesh[mask]
            meshii = meshii[mask]

            # clip counters to 100
            samples = 100
            counts = numpy.where(counts > samples, samples, counts)

            # order counters
            order = numpy.argsort(counts)
            counts = counts[order]
            mesh = mesh[order]
            meshii = meshii[order]

            # construct samples title depending on instances
            formats = (low, symbol, symbol, high, date, time, self._round(score, 2))
            title = 'Prediction vs FluxSat ( {:.2f} {} CRF {} {:.2f} )\n{} {} CST ( $R^2$ = {:.2f} )'.format(*formats)

            # construct samples graph
            unit = 'samples'
            labels = ['GPP FluxSat', 'GPP prediction']
            boundsii = [(0, self.limits['truth']), (0, self.limits['truth'])]
            scaleiii = (1, 100)
            points = [0, truthii.max()]
            regression = [intercept + point * slope for point in points]
            line = (points, points, 'k-')
            lineii = (points, regression, 'k--')
            lines = [line, lineii]
            options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.5, 'polygons': False}
            graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
            scatters.append(graph)

            # if full cloud screen
            if screen == screens[0]:

                zipper = zip(truthii, predictionii, differenceii, cloudii)
                for i, j, k, l in list(zipper)[:100]:
                    print(i, j, k, l)

                # calculate r2 score
                score = r2_score(cloudii, differenceii)

                # calculate regression line
                slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(cloudii, differenceii)

                # remove outliers
                lower = numpy.percentile(differenceii, 0)
                upper = numpy.percentile(differenceii, 100)

                # calculate sample bins
                bins = 1000
                options = {'statistic': 'count', 'range': ((0, self.limits['cloud']), (lower, upper))}
                options.update({'bins': bins})
                counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(cloudii, differenceii, None, **options)
                clouds = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
                differences = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
                mesh, meshii = numpy.meshgrid(clouds, differences, indexing='ij')

                # apply mask
                mask = counts > 0
                counts = counts[mask]
                mesh = mesh[mask]
                meshii = meshii[mask]

                # clip counters to 100
                samples = 100
                counts = numpy.where(counts > samples, samples, counts)

                # order counters
                order = numpy.argsort(counts)
                counts = counts[order]
                mesh = mesh[order]
                meshii = meshii[order]

                # construct samples title depending on instances
                # formats = (date, time, self._round(score, 2))
                # title = 'Prediction / FluxSat Ratio vs CRF\n{} {} CST ( $R^2$ = {:.2f} )'.format(*formats)
                title = 'Prediction - FluxSat vs CRF\n{} {} CST'.format(date, time)

                # set limits for scatter
                minimum = lower
                maximum = upper

                # construct samples graph
                unit = 'samples'
                labels = ['CRF', 'Prediction - FluxSat']
                boundsii = [(0, self.limits['cloud']), (minimum, maximum)]
                scaleiii = (1, 100)
                points = [0, self.limits['cloud']]
                pointsii = [0, 0]
                regression = [intercept + point * slope for point in points]
                line = (points, pointsii, 'k-')
                lineii = (points, regression, 'k--')
                lines = [line, lineii]
                options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.5, 'polygons': False}
                options.update({'marker': 0.1})
                graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
                scatters.append(graph)

        # plot scatters
        space = {'top': 0.88, 'bottom': 0.24, 'wspace': 0.36, 'right': 0.96}
        destinationii = destination.replace('.png', '_crf_{}.png'.format(int(self.limits['cloud'] * 10)))
        self._paint(destinationii, scatters[0:2], size=(16, 8), space=space, gap=0.12)

        # plot crf scatters
        space = {'hspace': 0.6}
        destinationiii = destination.replace('.png', '_scatters_{}.png'.format(int(self.limits['cloud'] * 10)))
        self._paint(destinationiii, scatters[2:6], space=space)

        return None

    def probe(self, bracket=(-5, 5), increment=0.1, instances=1):
        """Probe the model with proxy data.

        Arguments:
            bracket: tuple of floats, the input range
            increment: float, the step size
            instances: int, number of instances from which to take mean

        Returns:
            None
        """

        # get the output scalings
        scaler = Hydra('{}/GPP_NN/TEMPO_Test_ML_Scaling.h5'.format(self.sink), show=False)
        scaler.ingest()
        scaleii = scaler.grab('Output_Scale')
        minimumii = scaler.grab('Output_Min')

        # for each position
        for position in range(31):

            # fake matrix
            matrix, proxy = self.fake(position, bracket, increment)

            # load model
            model = self.retrieve()

            # if getting an instance
            predictions = []
            for instance in range(instances):

                # get predictions
                prediction = model(matrix)
                prediction = prediction.numpy().squeeze()
                predictions.append(prediction)

            # take mean of predictions
            prediction = numpy.array(predictions).mean(axis=0)

            # apply scale to predictions
            prediction = (prediction / scaleii) + minimumii

            # plot
            pyplot.clf()

            # plot data
            pyplot.plot(proxy, prediction)
            pyplot.xlabel('input pca {}'.format(position))
            pyplot.ylabel('gpp_prediction')
            pyplot.title('Model predictions matrix positition {}'.format(position))

            # set destination
            destination = '{}/GPP_Feed_Plots/Model_predictions_position_{}.png'.format(self.sink, self._pad(position))
            pyplot.savefig(destination)
            pyplot.clf()

        return None

    def reconstruct(self, position=6, granule=6):
        """Reconstruct the reflectances from raw radiance and irradiance.

        Arguments:
            position: int, scan position number
            granule: int, granule number

        Returns:
            None
        """

        # construct scan label
        label = 'S{}G{}'.format(self._pad(position, 3), self._pad(granule))

        # grab radiance data
        formats = (self.year, self._pad(self.month), self._pad(self.day))
        hydra = Hydra('{}/TEMPO_L1b/{}/{}/{}'.format(self.sink, *formats), show=False)
        hydra.ingest(label)
        blue = hydra.grab('band_290_490/radiance')
        red = hydra.grab('band_540_740/radiance')
        waves = hydra.grab('band_290_490/nominal_wavelength')
        wavesii = hydra.grab('band_540_740/nominal_wavelength')
        quality = hydra.grab('band_290_490/pixel_quality_flag')
        qualityii = hydra.grab('band_540_740/pixel_quality_flag')
        mirror = hydra.grab('mirror_step')
        track = hydra.grab('xtrack')
        zenith = hydra.grab('solar_zenith_angle')

        # apply qualities
        blue = numpy.where(quality == 0, blue, numpy.nan)
        red = numpy.where(qualityii == 0, red, numpy.nan)

        # grab irradiance data
        date = '{}{}{}T'.format(*formats)
        hydraii = Hydra('{}/TEMPO_IRR/{}/{}/{}'.format(self.sink, *formats), show=False)
        hydraii.ingest(0)
        blueii = hydraii.grab('band_290_490/radiance').squeeze()
        redii = hydraii.grab('band_540_740/radiance').squeeze()
        wavelengths = hydraii.grab('band_290_490/nominal_wavelength').squeeze()
        wavelengthsii = hydraii.grab('band_540_740/nominal_wavelength').squeeze()

        # grab pc coefficients
        hydraiii = Hydra('{}/TEMPO_PCs'.format(self.sink), show=False)
        hydraiii.ingest('PCA')
        coefficients = hydraiii.grab('PCA_Coeff')

        # set gridding attributes
        start = 315
        finish = 480
        startii = 545
        finishii = 735
        nodes = 5
        width = 2
        widthii = 1 / nodes

        # construct wavelength gtid
        grid = numpy.array(range(start, finish + 1))
        gridii = numpy.array(range(startii, finishii + 1))

        # construct finely spaced grids ( 0.2 nm )
        fine = numpy.array(range(start * nodes, finish * nodes + 1)) / nodes
        fineii = numpy.array(range(startii * nodes, finishii * nodes + 1)) / nodes

        # construct boxcar
        boxcar = numpy.ones(int(width / widthii)) / int(width / widthii)

        print(red.shape, blue.shape)
        print(waves.shape, wavesii.shape)

        # for each mirror step in reflectance data
        reflectance = []
        reflectanceii = []
        for step in range(mirror.shape[0]):

            # if even number
            if step % 10 == 0:

                # print status
                print('interpolating {}...'.format(step))

            # for each scan
            panel = []
            panelii = []
            for scan in range(track.shape[0]):

                # interpolate irradiance onto radiance wavelengths
                options = {'kind': 'linear', 'fill_value': 'extrapolate', 'bounds_error': False}
                interpolator = scipy.interpolate.interp1d(wavelengths[scan], blueii[scan], **options)
                irradiance = interpolator(waves[scan])
                interpolatorii = scipy.interpolate.interp1d(wavelengthsii[scan], redii[scan], **options)
                irradianceii = interpolatorii(wavesii[scan])

                # divide by irradiance
                ratio = blue[step, scan] / irradiance
                ratioii = red[step, scan] / irradianceii

                # interpolate to fine grid
                interpolator = scipy.interpolate.interp1d(waves[scan], ratio, **options)
                interpolation = interpolator(fine)
                interpolatorii = scipy.interpolate.interp1d(wavesii[scan], ratioii, **options)
                interpolationii = interpolatorii(fineii)

                # convolve onto course grid
                convolution = scipy.signal.convolve(interpolation, boxcar, mode='same')
                convolutionii = scipy.signal.convolve(interpolationii, boxcar, mode='same')

                # get every tenth wavelength
                tens = numpy.array([indexii for indexii in range(fine.shape[0]) if indexii % nodes == 0])
                tensii = numpy.array([indexii for indexii in range(fineii.shape[0]) if indexii % nodes == 0])

                # add to reflectances
                panel.append(numpy.array(convolution)[tens])
                panelii.append(numpy.array(convolutionii)[tensii])

            # append panels
            reflectance.append(panel)
            reflectanceii.append(panelii)

        # construct arrays
        reflectance = numpy.array(reflectance)
        reflectanceii = numpy.array(reflectanceii)

        # divide by cosine of solar zenith anggle
        cosine = numpy.cos(numpy.radians(zenith)).reshape(*zenith.shape, 1)
        reflectance = reflectance / cosine
        reflectanceii = reflectanceii / cosine

        print(reflectance.shape, reflectanceii.shape)
        print(grid.shape, gridii.shape)

        # concatenate grids
        grid = numpy.hstack([grid, gridii])
        reflectance = numpy.dstack([reflectance, reflectanceii])

        print(reflectance.shape)
        print(grid.shape)

        # excise wavelengths
        indices = (grid > 350) & ((grid < 371) | (grid > 375))
        grid = grid[indices]
        reflectance = reflectance[:, :, indices]

        print(reflectance.shape)
        print(grid.shape)

        # reduce to pcas
        self._print('reducing...')
        reduction = numpy.dot(reflectance, coefficients)

        print(reduction.shape)

        # create destination
        destination = '{}/GPP_Reconstruction/PCA_Reconstruction_{}.h5'.format(self.sink, label)
        data = {'pca_reduction': reduction, 'reflectance': reflectance, 'wavelength': grid}

        # stash
        self.spawn(destination, data)

        return None

    def regress(self, scan=7, number=30, length=4, bounds=(-100, -80), boundsii=(25, 40), skip=True, chart=True):
        """Use quadratic regression to find the most trending points.

        Arguments:
            scan: int, the scan number for comparison
            number: int, number of most interesting points
            length: int, minimum number of points in regression
            skip: boolean, skip regeneration of regressions?
            chart: boolean, plot regression map?

        Returns:
            None
        """

        # create hydra to access Regression file
        hydra = Hydra('{}/GPP_Regression'.format(self.sink))

        # determine date
        date = '{}m{}{}'.format(self.year, self._pad(self.month), self._pad(self.day))

        # get panel paths
        hydraii = Hydra('GPP_Panels/{}'.format(self.stub))
        numbers = [int(re.search('_S[0-9]{3}G', path).group()[2:5]) for path in hydraii.paths]
        times = [int(re.search('[0-9]{8}T[0-9]{6}Z', path).group()[9:13]) for path in hydraii.paths]
        hours = {index: ' ' for index in range(20)}
        hours.update({number: 'UTC {}'.format(time) for number, time in zip(numbers, times)})

        # load in lands
        lands = self._acquire('Land_Types/land.yaml')

        # if empty or not skipping
        paths = [path for path in hydra.paths if date in path]
        if len(paths) < 1 or not skip:

            # distil data
            reservoir, times, noon = self.distil(scan=scan, start=2, finish=11)

            # get shape
            shape = reservoir[noon]['gpp_prediction'].shape

            # get latitude and longitude
            latitudes = reservoir[noon]['latitude']
            longitudes = reservoir[noon]['longitude']

            # set land covers and corners
            lands = reservoir[noon]['land_cover']
            corners = reservoir[noon]['latitude_bounds']
            cornersii = reservoir[noon]['longitude_bounds']

            # begin data reservoir
            data = {'score': [], 'latitude': [], 'longitude': [], 'track': [], 'mirror': []}
            data.update({'coefficient': [], 'prediction': [], 'time': [], 'valid': [], 'deviation': []})
            data.update({'snow': [], 'cloud': [], 'land': [], 'corners': [], 'cornersii': []})
            data.update({'scoreii': [], 'coefficientii': [], 'length': []})

            # set status
            self._stamp('fitting curves with at least {} points...'.format(length), initial=True)

            # define quadratic function
            def quadratic(x, a, b, c): return a * x ** 2 + b * x + c

            # define linear function
            def linear(x, a, b): return a * x + b

            # start count
            count = 0
            countii = 0

            # for each parallel
            for mirror in range(shape[0]):

                # for each meridian
                for track in range(shape[1]):

                    # get latitude and longitude
                    latitude = latitudes[mirror, track]
                    longitude = longitudes[mirror, track]
                    corner = corners[mirror, track]
                    cornerii = cornersii[mirror, track]
                    land = lands[mirror, track]

                    # get labels
                    labels = list(reservoir.keys())
                    labels.sort()

                    # extract times ( scan numbers )
                    time = numpy.array([int(label[1:-1]) for label in labels])

                    # create arrays
                    prediction = numpy.array([reservoir[label]['gpp_prediction'][mirror, track] for label in labels])
                    deviation = numpy.array([reservoir[label]['gpp_deviation'][mirror, track] for label in labels])
                    water = numpy.array([reservoir[label]['water_mask'][mirror, track] for label in labels])
                    cloud = numpy.array([reservoir[label]['cloud_fraction'][mirror, track] for label in labels])
                    snow = numpy.array([reservoir[label]['snow_ice_fraction'][mirror, track] for label in labels])
                    zenith = numpy.array([reservoir[label]['solar_zenith_angle'][mirror, track] for label in labels])
                    view = numpy.array([reservoir[label]['viewing_zenith_angle'][mirror, track] for label in labels])

                    # create valid data mask
                    valid = (water == 1) & (numpy.isfinite(cloud))
                    valid = valid & (numpy.isfinite(zenith))
                    valid = valid & (cloud <= self.limits['cloud'])
                    valid = valid & (zenith <= self.limits['zenith'])
                    valid = valid & (view <= self.limits['view'])
                    valid = valid & (snow <= self.limits['snow'])
                    valid = valid & (prediction <= self.limits['truth']) & numpy.isfinite(prediction)

                    # create abscissas and ordinates
                    ordinate = prediction[valid]
                    abscissa = time[valid]

                    # default scores and coefficients
                    score = 0
                    coefficient = numpy.array([0, 0, 0])
                    scoreii = 0
                    coefficientii = numpy.array([0, 0])

                    # if finite
                    if (numpy.isfinite(latitude)) & (numpy.isfinite(longitude)):

                        # if len is at least four
                        if len(ordinate) >= length:

                            # add to valid count
                            countii += 1

                            # fit curve
                            coefficient, covariance = scipy.optimize.curve_fit(quadratic, abscissa, ordinate)

                            # compute the r2 value
                            score = r2_score(ordinate, quadratic(abscissa, *coefficient))

                            # fit linear currve
                            coefficientii, covarianceii = scipy.optimize.curve_fit(linear, abscissa, ordinate)

                            # compute the r2 value
                            scoreii = r2_score(ordinate, quadratic(abscissa, *coefficient))

                        # otherwise
                        else:

                            # add invalid count
                            count += 1

                    # otherwise
                    else:

                        # add to invalid count
                        count += 1

                    # add to data
                    data['score'].append(score)
                    data['scoreii'].append(scoreii)
                    data['time'].append(time)
                    data['prediction'].append(prediction)
                    data['deviation'].append(deviation)
                    data['valid'].append(valid)
                    data['coefficient'].append(coefficient)
                    data['coefficientii'].append(coefficientii)
                    data['latitude'].append(latitude)
                    data['longitude'].append(longitude)
                    data['mirror'].append(mirror)
                    data['track'].append(track)
                    data['cloud'].append(cloud)
                    data['snow'].append(snow)
                    data['land'].append(land)
                    data['corners'].append(corner)
                    data['cornersii'].append(cornerii)
                    data['length'].append(len(ordinate))

            # create arrays
            data.update({name: numpy.array(array) for name, array in data.items()})

            # combine scores and lengths for sorting
            combination = numpy.array([data['score'], data['length']])

            # argsort by scores and lengths
            # order = numpy.argsort(data['score'])[::-1]
            order = numpy.lexsort(combination)[::-1]
            data.update({name: array[order] for name, array in data.items()})

            # create destination and stash
            destination = '{}/GPP_Regression/GPP_Regressions_{}.h5'.format(self.sink, date)
            self.spawn(destination, data)

            print('valid count: {}'.format(countii))
            print('invalid count: {}'.format(count))

        # renew hydra and ingest data
        hydra.renew()
        hydra.ingest(date)
        data = hydra.extract()

        # get scores and latitude, longitude
        score = data['score']
        scoreii = data['scoreii']
        curvature = data['coefficient'][:, 0]
        slope = data['coefficientii'][:, 0]
        # constant = data['coefficient'][:, 2]

        # # calculate center and height
        # center = -slope / (2 * curvature)
        # height = constant - (slope ** 2) / (4 * curvature)

        # get other attributes
        latitude = data['latitude']
        longitude = data['longitude']
        corners = data['corners']
        cornersii = data['cornersii']
        land = data['land']
        mirror = data['mirror']
        track = data['track']

        # if plotting map
        if chart:

            # create destination
            destination = '{}/GPP_Regression_Map/Regression_Map_{}.png'.format(self.sink, date)

            # begin status
            self._stamp('plotting {}...'.format(destination), initial=True)

            # erase preevious copy
            self._clean(destination, force=True)

            # create titles
            modes = ['R^2', 'curvature']
            titles = ['GPP quadratic fit {}, {}\n'.format(mode, date) for mode in modes]

            # create titles
            modes = ['R^2', 'slope']
            titles += ['GPP linear fit {}, {}\n'.format(mode, date) for mode in modes]

            # set scale
            scales = [(0, 1), (-0.1, 0.1), (0, 1), (-0.3, 0.3)]

            # set gradiants
            gradients = ['plasma', 'coolwarm', 'plasma', 'coolwarm']

            # mask out polygons with big deviations
            mask = (numpy.std(corners, axis=1) < 1) & (numpy.std(cornersii, axis=1) < 1)

            # set arrays, inverting to plot highest scores last
            arrays = (score, curvature, scoreii, slope)
            arrays = [[array[mask][::-1], longitude[mask][::-1], latitude[mask][::-1]] for array in arrays]

            # set bounds
            limits = [[(-100, -80), (25, 40)]] * 4

            # set labels
            labels = [['longitude', 'latitude']] * 4

            # set units
            units = ['R^2', '-', '-', '-']

            # begin graphs
            graphs = []
            zipper = zip(arrays, titles, labels, limits, scales, gradients, units)
            for triplet, title, label, limit, scale, gradient, unit in zipper:

                # unpack triplet
                tracer, abscissa, ordinate = triplet

                # construct graphs
                options = {'polygons': True, 'corners': [corners[mask][::-1], cornersii[mask][::-1]]}
                graph = self._graph(abscissa, ordinate, tracer, limit, scale, gradient, title, unit, label, **options)
                graphs.append(graph)

            # plot
            self._paint(destination, graphs)

        # apply mask for bounds
        mask = (data['longitude'] > bounds[0]) & (data['longitude'] < bounds[1])
        mask = mask & (data['latitude'] > boundsii[0]) & (data['latitude'] < boundsii[1])
        data = {name: array[mask] for name, array in data.items()}

        # construct graphs
        graphs = []
        graphsii = []

        # for each index
        for index in range(number):

            # create title
            formats = (date, self._orient(data['latitude'][index]), self._orient(data['longitude'][index], east=True))
            pixel = (data['mirror'][index], data['track'][index])
            formatsii = (*pixel, self._round(score[index]), lands[int(data['land'][index])])
            title = 'Timewise GPP +/- 1 std {}, {}, {}\npixel: ( {}, {} ), R^2 {}, {}'.format(*formats, *formatsii)

            # grab data
            valid = data['valid'][index].astype(bool)
            prediction = data['prediction'][index][valid]
            deviation = data['deviation'][index][valid]
            time = data['time'][index][valid]
            cloud = data['cloud'][index][valid]

            print('index: {}'.format(index))
            print('mirror: {}'.format(data['mirror'][index]))
            print('track: {}'.format(data['track'][index]))
            print('latitude: {}'.format(data['latitude'][index]))
            print('longitude: {}'.format(data['longitude'][index]))
            print('prediction: {}'.format(prediction))

            # check for existence of scan
            if scan in time.tolist():

                # set scales
                scale = (0, self.limits['truth'])

                # set tracers
                tracer = prediction

                # set gradiants
                gradient = 'coolwarm'

                # set units
                unit = '%'

                # set abscissa and ordinate
                abscissa = time
                ordinate = tracer
                ordinateii = tracer - deviation
                ordinateiii = tracer + deviation

                # make blank array
                blank = numpy.array([0])

                # set ticks
                ticks = [index - 1 for index in range(2, 13)]
                marks = [hours[index] for index in range(2, 12)] + ['']

                # create graph
                labels = ['scan', 'GPP']
                lines = [(abscissa, ordinate, 'bo-'), (abscissa, ordinateii, 'g--'), (abscissa, ordinateiii, 'g--')]
                options = {'globe': False, 'colorbar': False, 'pixel': 0.01, 'lines': lines, 'marker': 3}
                options.update({'ticks': ticks, 'marks': marks})
                limits = [(1, 12), (0, self.limits['truth'] + 1)]
                parameters = (blank, blank, blank, limits, scale, gradient, title, unit, labels)
                graph = self._graph(*parameters, **options)
                graphs.append(graph)

                # make cloud graph
                ordinate = cloud

                # create graph
                labels = ['scan', 'CRF']
                lines = [(abscissa, ordinate, 'bo-')]
                options = {'globe': False, 'colorbar': False, 'pixel': 0.01, 'lines': lines, 'marker': 3}
                options.update({'ticksii': (0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8)})
                options.update({'marksii': (0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8)})
                options.update({'ticks': ticks, 'marks': marks})
                limits = [(1, 12), (0, self.limits['cloud'] + 0.1)]
                parameters = (blank, blank, blank, limits, scale, gradient, title, unit, labels)
                graph = self._graph(*parameters, **options)
                graphsii.append(graph)

        # create plots
        for plot in range(12):

            # set destination
            destination = '{}/GPP_Regression_Plots/Point_Plots_{}_{}.png'.format(self.sink, plot, date)
            self._paint(destination, graphs[plot * 2: 2 + plot * 2] + graphsii[plot * 2: 2 + plot * 2])

        # # create destination
        # destination = '{}/GPP_Regression_Land_Cover/Regression_Land_{}.png'.format(self.sink, date)
        #
        # # begin status
        # self._stamp('plotting {}...'.format(destination), initial=True)
        #
        # # erase preevious copy
        # self._clean(destination, force=True)
        #
        # # create titles
        # modes = ['R^2', 'curvature']
        # titles = ['GPP quadratic fit {}, {}\n'.format(mode, date) for mode in modes]
        #
        # # create titles
        # modes = ['R^2', 'slope']
        # titles += ['GPP linear fit {}, {}\n'.format(mode, date) for mode in modes]
        #
        # # set scale
        # scales = [(0, 1), (-0.1, 0.1), (0, 1), (-0.3, 0.3)]
        #
        # # set ticks
        # ticks = [(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0)]
        # ticks += [(-0.1, -0.08, -0.06, -0.04, -0.02, -0.0, 0.02, 0.04, 0.06, 0.08, 0.1)]
        # ticks += [(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0)]
        # ticks += [(-0.3, -0.2, -0.1, -0.0, 0.1, 0.2, 0.3)]
        #
        # # set gradiants
        # gradients = ['plasma'] + ['coolwarm'] * 3
        #
        # # set blank
        # blank = numpy.array([0])
        #
        # # set arrays
        # arrays = [[blank, land, array] for array in (score, curvature, scoreii, slope)]
        #
        # # set bounds
        # limits = [[(-1, 17), scale] for scale in scales]
        #
        # # set labels
        # labels = [['land type', field] for field in ('score', 'curvature', 'score', 'slope')]
        #
        # # set units
        # units = ['R^2', '-', 'R^2', '-']
        #
        # # begin graphs
        # graphs = []
        # zipper = zip(arrays, titles, labels, limits, scales, gradients, units, ticks)
        # for triplet, title, label, limit, scale, gradient, unit, tick in zipper:
        #
        #     # unpack triplet
        #     tracer, abscissa, ordinate = triplet
        #
        #     # add random spread to abscissa
        #     random = numpy.random.rand(*abscissa.shape) - 0.5
        #     abscissa = abscissa + random
        #
        #     # set line
        #     lines = [(abscissa, ordinate, 'b.')]
        #
        #     # construct graphs
        #     options = {'polygons': False, 'corners': [corners, cornersii], 'globe': False, 'lines': lines}
        #     options.update({'colorbar': False, 'marker': 0.01, 'ticksii': tick, 'marksii': tick})
        #     graph = self._graph(blank, blank, blank, limit, scale, gradient, title, unit, label, **options)
        #     graphs.append(graph)
        #
        # # plot
        # self._paint(destination, graphs)

        # print status
        self._print('done plotting.')

        return None

    def retrieve(self, keras=False, secondary=False):
        """Retrieve the current model.

        Arguments:
            keras: boolean, use Keras style?
            secondary: boolean, use secondary model?

        Returns:
            tensor flow model
        """

        # set network folder
        network = 'GPP_NN'

        # if using secondary model
        if secondary:

            # use secondary model
            network = 'GPP_NN_Secondary'

        # if keras
        if keras:

            # load model
            model = tensorflow.keras.models.load_model('{}/{}/TEMPO_Test_ML_model'.format(self.sink, network), compile=False)

        # otherwise
        else:

            # load model
            model = tensorflow.saved_model.load('{}/{}/TEMPO_Test_ML_model'.format(self.sink, network))

        return model

    def serialize(self, scan=7):
        """Create time series files.

        Arguments:
            scan: numpy of scan for "noon"

        Returns:
            None
        """

        # determine date
        date = '{}m{}{}'.format(self.year, self._pad(self.month), self._pad(self.day))

        # distil data
        reservoir, times, noon = self.distil(scan=scan, start=2, finish=11)

        # get labels
        labels = list(reservoir.keys())
        labels.sort()

        # extract times ( scan numbers )
        time = numpy.array([int(label[1:-1]) for label in labels])

        # construct data
        data = {'scan_number': time}

        # get all fields
        fields = list(reservoir[noon].keys())

        # update data
        data.update({field: numpy.array([reservoir[label][field] for label in labels]) for field in fields})

        # create valid data mask
        valid = numpy.isfinite(data['latitude'])
        valid = valid & numpy.isfinite(data['longitude'])
        valid = valid & (numpy.std(data['latitude_bounds'], axis=3) < 1)
        valid = valid & (numpy.std(data['longitude_bounds'], axis=3) < 1)
        valid = valid & (data['water_mask'] == 1)
        valid = valid & (data['cloud_fraction'] <= self.limits['cloud'])
        valid = valid & (data['snow_ice_fraction'] <= self.limits['snow'])
        valid = valid & (data['solar_zenith_angle'] <= self.limits['zenith'])
        valid = valid & (data['viewing_zenith_angle'] <= self.limits['view'])
        valid = valid & (data['gpp_prediction'] <= self.limits['truth'])
        valid = valid & (data['gpp_prediction'] >= 0) & numpy.isfinite(data['gpp_prediction'])

        # add valid data mask
        data['valid_mask'] = valid.astype(int)

        # spawn series
        destination = 'GPP_Series/GPP_Series_{}.h5'.format(date)
        self.spawn(destination, data)

        return None

    def streak(self, scan=9, granule=5, east=-91, north=33):
        """Examine data streaks.

        Arguments:
            scan: int, scan number
            granule: int, granule number
            east: float, closest longitude
            north: float, closest latitude

        Returns:
            None
        """

        # get L1B data
        label = self._label(scan, granule)
        hydra = Hydra('TEMPO_L1b/{}'.format(self.stub), show=False)
        hydra.ingest(label)

        # get Smoothed PC
        label = self._label(scan, granule)
        hydraii = Hydra('TEMPO_PC_Reduced/{}'.format(self.stub), show=False)
        hydraii.ingest(label)
        hydraii.survey()

        # get cloud data
        label = self._label(scan, granule)
        hydraiii = Hydra('TEMPO_CLDO4_L2/{}'.format(self.stub), show=False)
        hydraiii.ingest(label)

        # retrieve radiances and wavelengths
        radiance = hydra.grab('band_290_490/radiance')
        latitude = hydra.grab('latitude')
        longitude = hydra.grab('longitude')
        wavelength = hydra.grab('band_290_490/nominal_wavelength')
        quality = hydra.grab('pixel_quality')

        # retrieve smoothed radiances
        mirrors = hydraii.grab('mirror_step')
        tracks = hydraii.grab('xtrack')
        smooth = hydraii.grab('smoothed_radiance').transpose(1, 0)
        smooth = smooth.reshape(mirrors, tracks, 50)
        smooth = numpy.where((abs(smooth) < 1e20) & numpy.isfinite(smooth), smooth, 10)

        # retrieve cloud fraction
        cloud = hydraiii.grab('cloud_fraction')

        # print shapes
        self._print('radiance: {}'.format(radiance.shape))
        self._print('latitude: {}'.format(latitude.shape))
        self._print('longitude: {}'.format(cloud.shape))
        self._print('wavelength: {}'.format(wavelength.shape))
        self._print('cloud: {}'.format(cloud.shape))
        self._print('smooth: {}'.format(smooth.shape))
        self._print('mirrors: {}'.format(mirrors))
        self._print('tracks: {}'.format(tracks))
        self._print('quality: {}'.format(quality.shape))

        print(smooth[:, :, 0].min(), smooth[:, :, 0].max())

        # get closest pixel
        mirror, track = hydra._pin([north, east], [latitude, longitude])[0]
        self._print(latitude[mirror, track], longitude[mirror, track])

        # print cloud and first pc
        formats = (self.date, label, self._orient(east, east=True))
        pyplot.clf()
        pyplot.title('TEMPO radiance PC {}, {}, {}'.format(*formats))
        pyplot.xlabel('latitude')
        pyplot.ylabel('radiance PC 0')

        # limit x to certain range
        pyplot.xlim(29, 36)
        pyplot.ylim(-2, 12)

        # plot radiance vs latitude
        pyplot.plot(latitude[mirror, :], smooth[mirror, :, 0], 'b-')

        # plot clouds on second axis
        axisii = pyplot.gca().secondary_yaxis('right')
        axisii.set_ylabel('CRF, log(pqf)')
        pyplot.plot(latitude[mirror, :], cloud[mirror, :] * 10, 'g-')
        pyplot.plot(latitude[mirror, :], numpy.log2(quality[mirror, :] + 1), 'r.')

        # set destination
        destination = 'TEMPO_Streaks/PCA_CRF_{}_{}_{}.png'.format(*formats)
        pyplot.savefig(destination)
        pyplot.clf()

        # for each test wave
        waves = [10 * wave for wave in range(29, 50)]
        for wave in waves:

            # plot versus latitude
            formats = (self.date, label, self._orient(east, east=True), wave)
            pyplot.clf()
            pyplot.title('TEMPO radiance {}, {}, {}, {}nm'.format(*formats))
            pyplot.xlabel('latitude')
            pyplot.ylabel('radiance')

            # get wavelength position
            position = self._pin(wave, wavelength[track])[0][0]

            # limit x to certain range
            pyplot.xlim(29, 36)

            # plot radiance vs latitude
            pyplot.plot(latitude[mirror, :], radiance[mirror, :, position])

            # set destination
            destination = 'TEMPO_Streaks/{}_{}_{}_{}.png'.format(*formats)
            pyplot.savefig(destination)
            pyplot.clf()

        return None

    def survey(self, method='pixel'):
        """PLot the zenith angles and ice fraction for cloud files.

        Arguments:
            method: str, 'nearest' or 'pixel'

        Returns:
            None
        """

        # get all predictions
        formats = (self.year, self._pad(self.month), self._pad(self.day))

        # get cloud for reconstructing original pixels
        hydra = Hydra('TEMPO_CLDO4_L2/{}/{}/{}'.format(*formats), show=False)

        # group all hydra paths by scan number
        groups = self._group(hydra.paths, lambda path: re.search('S[0-9]{3}G', path).group())

        # begin data
        zeniths = {}
        views = {}
        snows = {}
        clouds = {}
        abscissas = {}
        ordinates = {}

        # # define grid as quarter degree bins
        # latitudes = (10, 70)
        # longitudes = (-150, -50)
        # width = 0.1
        #
        # # determine bins
        # bins = (int((latitudes[1] - latitudes[0]) / width), int((longitudes[1] - longitudes[0]) / width))
        #
        # # define the grid latitude and longitude centerpoints
        # parallels = [(width / 2) + latitudes[0] + index * width for index in range(bins[0])]
        # meridians = [(width / 2) + longitudes[0] + index * width for index in range(bins[1])]

        # set frame shape to encompass all data
        mirrors = 132
        scans = 2048
        shape = (mirrors, scans)

        # # expand into grids
        # parallels = numpy.array([parallels] * bins[1]).transpose(1, 0)
        # meridians = numpy.array([meridians] * bins[0])

        # for each group
        for label, members in groups.items():

            # begin data
            latitude = []
            longitude = []
            zenith = []
            view = []
            snow = []
            cloud = []

            # for each member
            for member in members:

                # ingest the file
                hydra.ingest(member)

                # grab the data
                latitude.append(hydra.grab('latitude').reshape(-1, 1))
                longitude.append(hydra.grab('longitude').reshape(-1, 1))
                zenith.append(hydra.grab('solar_zenith_angle').reshape(-1, 1))
                view.append(hydra.grab('viewing_zenith_angle').reshape(-1, 1))
                snow.append(hydra.grab('snow_ice_fraction').reshape(-1, 1))
                cloud.append(hydra.grab('cloud_fraction').reshape(-1, 1))

                # # if method is pixelwise
                # if method == 'pixel':
                #
                #     # get timestamp
                #     stamp = re.search('[0-9]{8}T[0-9]{6}', member).group()
                #
                #     # ingest cloud file
                #     hydraii.ingest(str(stamp))
                #
                #     # grab cloud fraction and coordinates
                #     cloud = hydraii.grab('cloud_fraction')
                #     latitudeii = hydraii.grab('latitude')
                #     longitudeii = hydraii.grab('longitude')
                #
                #     # for each variable
                #     for variable in (latitude, longitude, prediction):
                #
                #         # set up frame with nan
                #         frame = numpy.ones(shape) * numpy.nan
                #
                #         # set up grid shape for specific granule
                #         shapeii = latitudeii.shape
                #         grid = numpy.ones(shapeii) * numpy.nan
                #
                #         # set up valid cloud data mask
                #         mask = abs(latitudeii) < 1e30
                #
                #         # insert predictions data into grid
                #         grid[mask] = variable[-1].squeeze()
                #         frame[:shapeii[0], :shapeii[1]] = grid
                #         variable[-1] = frame.reshape(-1, 1)

            # create arrays
            latitude = numpy.vstack(latitude).squeeze()
            longitude = numpy.vstack(longitude).squeeze()
            zenith = numpy.vstack(zenith).squeeze()
            view = numpy.vstack(view).squeeze()
            snow = numpy.vstack(snow).squeeze()
            cloud = numpy.vstack(cloud).squeeze()

            # bin
            # bins = ((latitudes[1] - latitudes[0]) / width, (longitudes[1] - longitudes[0]) / width)
            # options = {'statistic': 'mean', 'range': (latitudes, longitudes), 'bins': bins}
            # result = scipy.stats.binned_statistic_2d(latitude, longitude, prediction, **options)[0]

            # parallels = latitudeii[mask]
            # meridians = longitudeii[mask]
            # quantities = ozoneii[mask]
            #
            # # if method is nearest neighbors:
            # if method == 'nearest':
            #
            #     # crate mesh
            #     # mesh, meshii = numpy.meshgrid(indexes, indexesii, indexing='ij')
            #     mesh, meshii = numpy.meshgrid(parallels, meridians, indexing='ij')
            #     #
            #     # create mask
            #     fill = numpy.nan
            #     mask = (abs(prediction) < 1e10) & numpy.isfinite(prediction)
            #     prediction = numpy.where(mask, prediction, fill)
            #
            #     # # interpolate
            #     # self._stamp('interpolating...', initial=True)
            #     interpolator = scipy.interpolate.NearestNDInterpolator((latitude, longitude), prediction)
            #     grid = interpolator(mesh, meshii)
            #
            #     # add to predictions
            #     predictions[label] = grid
            #     ordinates[label] = mesh
            #     abscissas[label] = meshii

            # if method is equivalent pixel
            if method == 'pixel':

                # add to predictions
                zeniths[label] = zenith
                views[label] = view
                clouds[label] = cloud
                snows[label] = snow
                ordinates[label] = latitude
                abscissas[label] = longitude

        # for each label
        for label, members in groups.items():

            # set tracers and names
            tracers = {'zenith': zeniths[label], 'view': views[label]}
            tracers.update({'cloud': clouds[label], 'snow': snows[label]})

            # set scales and units
            scales = {'zenith': (0, 120), 'view': (0, 120), 'cloud': (0, 1), 'snow': (0, 1)}
            units = {'zenith': 'degress', 'view': 'degrees', 'cloud': 'fraction', 'snow': 'fraction'}

            # for each tracer
            for name, tracer in tracers.items():

                # create destination
                destination = 'GPP_Zenith_Plots/Zenith_Plot_{}_{}.png'.format(label, name)

                # begin status
                self._stamp('plotting {}...'.format(destination), initial=True)

                # erase preevious copy
                self._clean(destination, force=True)

                # create titles
                title = '{} {}\n'.format(name, re.search('[0-9]{8}T[0-9]{6}Z', members[0]).group())

                # set scale and unit
                scale = scales[name]
                unit = units[name]

                # clip data
                tracer = numpy.where((tracer < scale[0]) & numpy.isfinite(tracer), scale[0], tracer)
                tracer = numpy.where((tracer > scale[1]) & numpy.isfinite(tracer), scale[1], tracer)

                # set gradiants
                gradient = matplotlib.cm.plasma

                # set up figure
                pyplot.clf()
                pyplot.margins(1.0, 0.1)
                projection = {'projection': cartopy.crs.PlateCarree()}
                figure, axes = pyplot.subplots(1, 1, subplot_kw=projection, figsize=(8, 5))
                axis = axes
                # pyplot.subplots_adjust(wspace=0.4, hspace=0.6)

                # create mask and apply
                mask = numpy.isfinite(tracer)
                data = tracer[mask]
                # abscissa = meridians[mask]
                # ordinate = parallels[mask]
                abscissa = abscissas[label][mask]
                ordinate = ordinates[label][mask]

                # set scale
                minimum = scale[0]
                maximum = scale[1]

                # set cartopy axis with coastlines
                axis.coastlines()
                _ = axis.gridlines(draw_labels=True)

                # begin plot with title
                axis.grid(True)
                axis.set_global()
                axis.set_aspect('auto')
                axis.set_title(title, fontsize=10)
                axis.tick_params(axis='both', labelsize=7)

                # set up colors and logarithmic scale
                colors = gradient
                normal = matplotlib.colors.Normalize(minimum, maximum)

                # plot modis / tempo data
                axis.scatter(abscissa, ordinate, marker='.', s=0.1, c=data, cmap=colors, norm=normal)

                # # set axis limits
                # axis.set_xlim(abscissa.min() - 5, abscissa.max() + 5)
                # axis.set_ylim(ordinate.min() - 5, ordinate.max() + 5)
                axis.set_xlim(-150, -50)
                axis.set_ylim(10, 70)

                # add colorbar
                position = axis.get_position()
                bar = pyplot.gcf().add_axes([position.x0, position.y0 - 0.065, position.width, 0.02])
                scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
                colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', label=unit)
                formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
                colorbar.ax.yaxis.set_major_formatter(formatter)

                # save to destination
                pyplot.savefig(destination)
                pyplot.clf()

                # end status
                self._stamp('plotted.')

        return None

    def validate(self, scan=1, verify=True, diagnose=True, instances=1, secondary=False, present=False, **options):
        """Produce plots to validate the NN model.

        Arguments:
            scan: int, scan number
            give: boolean, return outputs?
            verify: boolean, make verification plot?
            diagnose: boolean, make diagnosis plots?
            instances: int, number of predictions to average
            secondary: boolean, use secondary model?
            present: boolean, make presentation plots?
            **options:
                chart: boolean, plot chart in presentation plots?
                give: boolean, give output
                patch: boolean, patch missing lines?

        Returns:
            None
        """

        # set default options
        give = options.get('give', False)
        chart = options.get('chart', True)
        patch = options.get('patch', False)

        # read in colocated data
        formats = (self.year, self._pad(self.month), self._pad(self.day))
        hydra = Hydra('{}/GPP_Colocations/{}/{}/{}'.format(self.sink, *formats), show=False)

        # get paths for date
        formats = (self.year, self._pad(self.month), self._pad(self.day))
        # paths = [path for path in hydra.paths if '{}{}{}'.format(*formats)]
        paths = [path for path in hydra.paths if 'S{}G'.format(self._pad(scan, 3)) in path]
        paths.sort()

        # sat up data
        fields = ['truth', 'latitude', 'longitude', 'reflectance', 'water', 'latitudeii', 'longitudeii']
        fields += ['zenith', 'view', 'cloud', 'snow', 'corners', 'cornersii', 'relative']
        fields += ['color', 'latitudeiii', 'longitudeiii', 'land', 'norths', 'easts', 'northsii', 'eastsii']
        fields += ['uncertainty']
        data = {field: [] for field in fields}

        # for each path
        for path in paths:

            # begin datum
            datum = {}

            # ingest the first path
            print('ingesting {}...'.format(path))
            hydra.ingest(path)

            # read in wavelength data
            hydraii = Hydra('{}/TEMPO_PC_Reduced/{}/{}/{}'.format(self.sink, *formats), show=False)

            # find the file with matching time
            date = re.search('[0-9]{8}T[0-9]{6}Z', hydra.current).group()
            hydraii.ingest(date)

            # get gpp truths
            datum['truth'] = hydra.grab('GPP').flatten()
            datum['latitude'] = hydra.grab('latitude').flatten()
            datum['longitude'] = hydra.grab('longitude').flatten()

            # apply invalid data mask
            mask = abs(datum['latitude']) < 1e30
            mask = numpy.isfinite(datum['latitude'])
            mask = datum['latitude'] > -32767
            datum['truth'] = datum['truth'][mask].reshape(-1, 1)
            datum['latitude'] = datum['latitude'][mask].reshape(-1, 1)
            datum['longitude'] = datum['longitude'][mask].reshape(-1, 1)

            # get inputs
            datum['latitudeii'] = hydraii.grab('latitude').reshape(-1, 1)
            datum['longitudeii'] = hydraii.grab('longitude').reshape(-1, 1)
            datum['reflectance'] = hydraii.grab('smoothed_radiance').transpose(1, 0)
            datum['water'] = hydraii.grab('water_mask').reshape(-1, 1)
            datum['zenith'] = hydraii.grab('solar_zenith_angle').reshape(-1, 1)
            datum['view'] = hydraii.grab('viewing_zenith_angle').reshape(-1, 1)
            datum['azimuth'] = hydraii.grab('solar_azimuth_angle').reshape(-1, 1)
            datum['azimuthii'] = hydraii.grab('viewing_azimuth_angle').reshape(-1, 1)

            # calculate relative azimuth
            datum['relative'] = datum['azimuthii'] - datum['azimuth']

            # read in cloud data
            hydraiii = Hydra('{}/TEMPO_CLDO4_L2/{}/{}/{}'.format(self.sink, *formats), show=False)
            hydraiii.ingest(date)

            # get crf data
            datum['cloud'] = hydraiii.grab('cloud_fraction')
            datum['snow'] = hydraiii.grab('snow_ice_fraction')
            datum['corners'] = hydraiii.grab('latitude_bounds')
            datum['cornersii'] = hydraiii.grab('longitude_bounds')

            # get ground pixel quality
            ground = hydraiii.grab('ground_pixel_quality')

            # extract bits for land type
            forms = (1 << (23 - 16 + 1)) - 1
            land = (ground >> 16) & forms
            datum['land'] = land.reshape(-1, 1)

            # # filter out fills and bad track positions
            # datum['cloud'][datum['cloud'] == -2**100] = numpy.nan
            # datum['cloud'][:, 1027] = numpy.nan
            # datum['cloud'][:, 1026] = numpy.nan
            # datum['cloud'][:, 1048] = numpy.nan

            # apply mask
            datum['cloud'] = datum['cloud'].flatten()[mask].reshape(-1, 1)
            datum['snow'] = datum['snow'].flatten()[mask].reshape(-1, 1)

            # apply to corners
            datum['norths'] = datum['corners'].reshape(-1, 4)[mask]
            datum['easts'] = datum['cornersii'].reshape(-1, 4)[mask]

            # get full latitude and longitude
            datum['northsii'] = datum['corners'].reshape(-1, 4)
            datum['eastsii'] = datum['cornersii'].reshape(-1, 4)
            datum['latitudeiii'] = hydraiii.grab('latitude').reshape(-1, 1)
            datum['longitudeiii'] = hydraiii.grab('longitude').reshape(-1, 1)

            # read in rgb data
            hydraiv = Hydra('{}/TEMPO_RGB/{}/{}/{}'.format(self.sink, *formats), show=False)
            hydraiv.ingest(date)
            datum['color'] = hydraiv.grab('RGB').reshape(-1, 3)

            # read in uncertainty data
            hydrav = Hydra('{}/FluxSat_Uncertainty/{}/{}/{}'.format(self.sink, *formats), show=False)
            hydrav.ingest(date)
            datum['uncertainty'] = hydrav.grab('uncertainty').reshape(-1, 1)[mask]

            # if patching
            if patch:

                # replace bad smoothed radiance data with nearest good data
                self._print('patching streaks...')
                parameters = [datum['reflectance'], datum['latitudeii'], datum['longitudeii']]
                parameters += [datum['cloud']]
                datum['reflectance'] = self._patch(*parameters)

            # append
            [data[field].append(numpy.array(datum[field])) for field in fields]

        # create arrays
        truth = numpy.vstack(data['truth']).squeeze()
        latitude = numpy.vstack(data['latitude']).squeeze()
        longitude = numpy.vstack(data['longitude']).squeeze()
        water = numpy.vstack(data['water']).squeeze()
        reflectance = numpy.vstack(data['reflectance'])
        latitudeii = numpy.vstack(data['latitudeii']).squeeze()
        longitudeii = numpy.vstack(data['longitudeii']).squeeze()
        zenith = numpy.vstack(data['zenith']).squeeze()
        view = numpy.vstack(data['view']).squeeze()
        cloud = numpy.vstack(data['cloud']).squeeze()
        snow = numpy.vstack(data['snow']).squeeze()
        norths = numpy.vstack(data['norths']).squeeze()
        easts = numpy.vstack(data['easts']).squeeze()
        northsii = numpy.vstack(data['northsii']).squeeze()
        eastsii = numpy.vstack(data['eastsii']).squeeze()
        relative = numpy.vstack(data['relative']).squeeze()
        color = numpy.vstack(data['color']).squeeze()
        latitudeiii = numpy.vstack(data['latitudeiii']).squeeze()
        longitudeiii = numpy.vstack(data['longitudeiii']).squeeze()
        land = numpy.vstack(data['land']).squeeze()
        uncertainty = numpy.vstack(data['uncertainty']).squeeze()

        # load in par data
        radiation = scipy.io.readsav('PAR/PAR_clrsky_daily.sav')
        grid = radiation['lat_grid_par']
        sky = radiation['par_clearsky']

        print('interpolating...')
        print(latitudeii.shape)
        print(grid.shape)
        print(sky.shape)

        # interpolate clear sky par
        interpolation = numpy.interp(latitudeii, grid[::-1], sky[self.day-1, self.month-1,:][::-1])
        interpolation = numpy.array([interpolation]).transpose(1, 0)

        print(interpolation.shape)
        print('interpolated.')

        # stack into inputs matrix, changing type to float32
        arrays = [reflectance[:, :30], interpolation]
        arrays += [numpy.cos(zenith.reshape(-1, 1)), numpy.cos(view.reshape(-1, 1))]
        arrays += [numpy.cos(relative.reshape(-1, 1))]
        matrix = numpy.hstack(arrays).astype(numpy.float32)
        print('matrix: {}'.format(matrix.shape))

        # print('cloud: {}'.format(cloud.shape))
        # print('water: {}'.format(water.shape))
        # print('zenith: {}'.format(zenith.shape))
        # print('view: {}'.format(view.shape))
        # print('latitude: {}'.format(latitude.shape))
        # print('longitude: {}'.format(longitude.shape))
        # print('latitudeii: {}'.format(latitudeii.shape))
        # print('longitudeii: {}'.format(longitudeii.shape))
        # print('truth: {}'.format(truth.shape))
        # print('corners: {}'.format(corners.shape))

        # create valid indices
        # valid = (reflectance[:, 0] > (-2 ** 100)) & (water == 1) & (numpy.isfinite(cloud))
        valid = (water == 1) & (numpy.isfinite(cloud))
        valid = valid & (numpy.isfinite(truth)) & (cloud <= self.limits['cloud']) & (numpy.isfinite(zenith))
        valid = valid & (zenith <= self.limits['zenith'])
        valid = valid & (view <= self.limits['view'])
        valid = valid & (snow <= self.limits['snow'])

        # print('latitude: {}'.format(latitude.shape))
        # print('longitude: {}'.format(longitude.shape))
        # print('latitude corners: {}'.format(corners.shape))
        # print('longitud corners: {}'.format(cornersii.shape))

        # apply to matrix and truths
        matrix = matrix[valid]
        truth = truth[valid]
        latitude = latitude[valid]
        longitude = longitude[valid]
        norths = norths[valid]
        easts = easts[valid]
        print('valid matrix: {}'.format(matrix.shape))

        # set network folder
        network = 'GPP_NN'

        # if using secondary model
        if secondary:

            # use secondary model
            network = 'GPP_NN_Secondary'

        # get the scaler
        scaler = Hydra('{}/{}/TEMPO_Test_ML_Scaling.h5'.format(self.sink, network))
        scaler.ingest()
        scale = scaler.grab('Input_Scale')
        minimum = scaler.grab('Input_Min')
        scaleii = scaler.grab('Output_Scale')
        minimumii = scaler.grab('Output_Min')

        # apply scale to inputs
        matrix = matrix.transpose(1, 0)
        matrix = (matrix - minimum.reshape(-1, 1)) * scale.reshape(-1, 1)
        matrix = matrix.transpose(1, 0)
        matrix = matrix.astype(numpy.float32)

        # load model
        model = self.retrieve(secondary=secondary)

        # if getting an instance
        predictions = []
        for instance in range(instances):

            # get predictions
            prediction = model(matrix)
            prediction = prediction.numpy().squeeze()
            predictions.append(prediction)

        # apply scaling to predictions
        predictions = (predictions / scaleii) + minimumii

        # take mean and stdev of predictions
        prediction = numpy.array(predictions).mean(axis=0)
        deviation = numpy.array(predictions).std(axis=0)

        # make mask for land entries and valid entries
        mask = (truth > 0) & (truth < self.limits['truth'])
        mask = mask & (prediction > 0) & (prediction < self.limits['truth'])

        # print r^2 score
        self._print('score: ')
        self._print(r2_score(prediction[mask], truth[mask]))

        # set diagnosis
        diagnosis = {'truth': truth[mask], 'prediction': prediction[mask]}
        diagnosis.update({'latitude': latitude[mask], 'longitude': longitude[mask]})
        diagnosis.update({'zenith': zenith[valid][mask]})
        diagnosis.update({'view': view[valid][mask]})
        diagnosis.update({'cloud': cloud[valid][mask]})
        diagnosis.update({'snow': snow[valid][mask]})

        # get time field
        time = re.search('T[0-9]{6}Z', paths[0]).group()

        # if diagnosing
        if diagnose:

            # diagnosie
            self.diagnose(diagnosis, time)

        # if verifying
        if verify:

            # create plot destination
            destination = '{}/GPP_Plots/GPP_{}m{}{}_{}_{}.png'.format(self.sink, *formats, time, self._pad(instances))

            # verify with plot
            parameters = [destination, truth[mask], prediction[mask], latitude[mask], longitude[mask]]
            parameters += [norths[mask], easts[mask], instances]
            self.verify(*parameters)

        # if presenting
        if present:

            # create plot destination
            formats = (self.sink, self.date, time, self._pad(instances))
            destination = '{}/GPP_Presentation/GPP_{}_{}_{}.png'.format(*formats)

            # set arrays
            arrays = [truth[mask], prediction[mask], latitude[mask], longitude[mask], cloud[valid][mask]]
            arrays += [uncertainty[valid][mask], deviation[mask]]

            # verify with plot
            parameters = [destination, arrays]
            parameters += [norths[mask], easts[mask], instances]
            parameters += [(latitudeiii, longitudeiii, northsii, eastsii, color, land)]
            self.present(*parameters, chart=chart)

        # default package to None
        package = None

        # if give
        if give:

            # construct package
            package = {'model': model, 'hydra': hydra, 'hydraii': hydraii, 'radiation': radiation}
            package.update({'matrix': matrix, 'truth': truth, 'prediction': prediction})
            package.update({'latitude': latitude, 'longitude': longitude})
            package.update({'mask': mask, 'data': data})

        return package

    def verify(self, destination, truth, prediction, latitude, longitude, corners, cornersii, instances):
        """Plot a validation sample.

        Arguments:
            destination: str, destination filepath
            truth: ground truth observations
            prediction: NN predicetions
            latitude: latitude coordinates
            longitude: longitude coordinates
            corners: latitude polygon corners
            cornersii: longitude polygon corners
            instances: int, number of prediction instances

        Returns:
            None
        """

        # begin status
        self._stamp('plotting {}...'.format(destination), initial=True)

        # erase preevious copy
        self._clean(destination, force=True)

        # calculate r2 score
        score = r2_score(truth, prediction)

        # calculate regression line
        slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(truth, prediction)

        print('slope, intercept: {}, {}'.format(slope, intercept))
        print('prediction min, max: {}, {}'.format(prediction.min(), prediction.max()))
        print('predictions > truth, < 5: {}'.format(((prediction > truth) & (truth < 5)).sum()))
        print('predictions < truth, < 5: {}'.format(((prediction < truth) & (truth < 5)).sum()))
        print('predictions > truth, > 5: {}'.format(((prediction > truth) & (truth > 5)).sum()))
        print('predictions < truth, > 5: {}'.format(((prediction < truth) & (truth > 5)).sum()))

        # create titles
        titles = ['GPP Truth {}\n'.format(re.search('[0-9]{4}m[0-9]{4}_T[0-9]{6}Z', destination).group())]
        titles += ['GPP Prediction {}\n'.format(re.search('[0-9]{4}m[0-9]{4}_T[0-9]{6}Z', destination).group())]
        titles += ['Prediction vs Truth\n( R^2 = {} )'.format(self._round(score, 2))]
        titles += ['Percent Difference\n']

        # calculate differences
        difference = prediction - truth
        difference = numpy.where(numpy.isfinite(difference), difference, 0)

        # calculate sample bins
        bins = 1000
        options = {'statistic': 'count', 'range': ((0, self.limits['truth']), (0, self.limits['truth'])), 'bins': bins}
        counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(truth, prediction, None, **options)
        truths = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
        predictions = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
        mesh, meshii = numpy.meshgrid(truths, predictions, indexing='ij')

        # apply mask
        mask = counts > 0
        counts = counts[mask]
        mesh = mesh[mask]
        meshii = meshii[mask]

        # clip counters to 100
        samples = 100
        counts = numpy.where(counts > samples, samples, counts)

        # order counters
        order = numpy.argsort(counts)
        counts = counts[order]
        mesh = mesh[order]
        meshii = meshii[order]

        # get minimum and maximum tracer values
        minimum = min([truth.min(), prediction.min()])
        maximum = max([truth.max(), prediction.max()])

        # set percent bracket
        absolute = max([abs(numpy.percentile(difference, 5)), abs(numpy.percentile(difference, 95))])

        # set bounds
        bounds = [(-125, -60), (15, 55)]
        scale = (minimum, maximum)
        scaleii = (-absolute, absolute)

        # begin graphs
        graphs = []

        # construct GPP truth plot
        title = 'GPP Truth {}\n'.format(re.search('[0-9]{4}m[0-9]{4}_T[0-9]{6}Z', destination).group())
        unit = '$g C/m^2 d$'
        labels = ['', '']
        options = {'polygons': True, 'corners': [corners, cornersii]}
        graph = self._graph(longitude, latitude, truth, bounds, scale, 'plasma', title, unit, labels, **options)
        graphs.append(graph)

        # construct GPP prediction plot
        title = 'GPP Prediction {}\n'.format(re.search('[0-9]{4}m[0-9]{4}_T[0-9]{6}Z', destination).group())
        unit = '$g C/m^2 d$'
        labels = ['', '']
        options = {'polygons': True, 'corners': [corners, cornersii]}
        graph = self._graph(longitude, latitude, prediction, bounds, scale, 'plasma', title, unit, labels, **options)
        graphs.append(graph)

        # construct samples title depending on instances
        title = 'Prediction vs Truth ( 1 instance )\n( R^2 = {} )'.format(self._round(score, 2))
        if instances > 1:

            # add instances
            formats = (instances, self._round(score, 2))
            title = 'Prediction vs Truth ( avg of {} instances )\n( R^2 = {} )'.format(*formats)

        # construct samples graph
        unit = 'samples'
        labels = ['GPP truth', 'GPP prediction']
        boundsii = [(0, self.limits['truth']), (0, self.limits['truth'])]
        scaleiii = (1, 100)
        points = [0, truth.max()]
        regression = [intercept + point * slope for point in points]
        line = (points, points, 'k-')
        lineii = (points, regression, 'k--')
        # lines = [line, lineii]
        lines = [line]
        options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.01, 'polygons': False}
        graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
        graphs.append(graph)

        # construct percent difference plot
        title = 'Difference\n'
        unit = '$g C/m^2 d$'
        labels = ['', '']
        options = {'polygons': True, 'corners': [corners, cornersii]}
        graph = self._graph(longitude, latitude, difference, bounds, scaleii, 'coolwarm', title, unit, labels, **options)
        graphs.append(graph)

        # create plot
        self._paint(destination, graphs)

        return None
