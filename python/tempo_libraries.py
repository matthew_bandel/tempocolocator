import numpy as np
import xarray as xr
import sagemaker 
import cartopy.feature as cfeature
import cartopy.io.shapereader as shpreader
import cartopy.crs as ccrs
from pylab import * 


print(plt.style.available)
plt.style.use('ggplot')

class tempo_l1b:
    def __init__(self, fname):
        self.fname =  fname

    def rd_tempo_l1b(self):
        
        data = xr.open_dataset(self.fname)
        
        self.rad = np.array(data.smoothed_radiance.values,dtype=np.float32)
        self.lat = np.array(data.latitude.values).flatten()
        self.lon = np.array(data.longitude.values).flatten()
        self.vaa = np.array(data.viewing_azimuth_angle.values).flatten()
        self.vza = np.array(data.viewing_zenith_angle.values).flatten()
        self.saa = np.array(data.solar_azimuth_angle.values).flatten()
        self.sza = np.array(data.solar_zenith_angle.values).flatten()

        if np.array(data.solar_zenith_angle.values).ndim == 2:
            self.nrow, self.nline  = np.array(data.solar_zenith_angle.values).shape
        else:
            self.row = data.mirror_step.values
            self.nline = data.xtrack.values
            
        self.raa = np.abs(180.-(self.saa - self.vaa))
        self.raa[self.raa > 180.0] = 360.0 - self.raa[self.raa > 180.0]

        try:
            self.row = np.array(data.row,dtype=int)
            self.line = np.array(data.line,dtype=int)
        except:
            self.row = np.array(data.steps,dtype=int)
            self.line = np.array(data.lines,dtype=int)
        self.sa = (np.cos(np.radians(self.sza))*np.cos(np.radians(self.vza))) + (np.sin(np.radians(self.sza))*np.sin(np.radians(self.vza))*np.cos(np.radians(self.raa)))
        self.water = np.array(data.water_mask.values).flatten()
        self.nwave, self.nsamples = self.rad.shape
        #self.rad = np.swapaxes(self.rad.reshape(self.nwave,self.nline*self.nrow),0,1)
        self.rad = np.swapaxes(self.rad,0,1)
        self.rad[self.rad ==-999] = np.nan
        
    def lim_waves(self,wave0,wave1):
        inds, = np.where((self.waves > wave0) & (self.waves < wave1))
        self.waves = self.waves[inds]
        self.rad = self.rad[:,inds]
        
    def qc_data(self,inds):
        self.rad[~inds] = np.nan
        
 
def copy_bucket(bucket,local_dir):

    url = f's3://{bucket}'
    paths = sagemaker.s3.S3Downloader.download(local_path=local_dir, s3_uri=url)
    


def make_map(map_extent=[-180,180,-90,90],zoom=False):
    fig, axes = plt.subplots(3,figsize=(5,13),subplot_kw=dict(projection=ccrs.PlateCarree()))
    for ax in axes.flatten():
        ax.set_xticks(np.arange(-180,200,2))
        ax.set_yticks(np.arange(-90,110,2))
        ax.grid()
        ax.set_extent(map_extent, crs=ccrs.PlateCarree())
        ax.coastlines(resolution='10m')
    return fig, axes
