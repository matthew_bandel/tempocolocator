#!/usr/bin/env python3

# shade.py to add background to plots

# import hydra
from hydras import Hydra

# import tesseract
from PIL import Image

# import numpy
import numpy

# get site paths
hydra = Hydra()
paths = hydra._see('../diurnal/plots')
paths.sort()

# for each path
for path in paths:

    # get image
    image = Image.open(path)

    # convert to array
    array = numpy.array(image)

    # raise alpha to 255
    array[:, :, 3] =255

    # resave image
    Image.fromarray(array).save(path.replace('plots', 'shade'))

