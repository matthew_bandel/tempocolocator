#!/usr/bin/env python3

# tempests.py to run GPP TEMPO models

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula

# import system
import sys

# import re
import re

# import datetime
import datetime

# import math
import math

# import numpy functions
import numpy
import scipy

# import shap for shapely analysis
import shap

# import netcdf4
import netCDF4

# import sklearn functions
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.metrics import r2_score, root_mean_squared_error
from sklearn.metrics.pairwise import cosine_similarity, pairwise_distances
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsRegressor
from sklearn.cluster import KMeans
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures


# # import netcdf4
# import netCDF4

# import tensorflow
import tensorflow
import tensorflow_addons
import tensorflow_probability

# import earthaccess
import earthaccess

# import boto3 for aws access
import boto3
import botocore

# import gc for garbage collection
import gc

# import pipeline tools
import tempo_pipeline_tools

# try to
try:

	# import matplotlib for plots
	import matplotlib
	from matplotlib import pyplot
	from matplotlib import style as Style
	from matplotlib import rcParams
	Style.use('fast')
	rcParams['axes.formatter.useoffset'] = False
	rcParams['figure.max_open_warning'] = False

# unless import error
except ImportError:

	# ignore matplotlib
	print('matplotlib not available!')

# try to
try:

	# import cartopy
	import cartopy
	from cartopy.mpl.geoaxes import GeoAxes

# unless import error
except ImportError:

	# ignore matplotlib
	print('cartopy not available!')


# class Tempest to do OMOCNUV analysis
class Tempest(Hydra):
	"""Tempest class to prepare tempo training data.

	Inherits from:
		Hydra
	"""

	def __init__(self, year=2024, month=7, day=1, tag=None, session=None, sink='.', limits=None):
		"""Initialize instance.

		Arguments:
			year: int, the year
			month: int, the month
			day: int, the day
			tag: tag for model training
			session: date of model training
			sink: str, file path for sink folder
			limits: dict, limit alterations
		"""

		# initialize the base Core instance
		Hydra.__init__(self, sink)

		# set fill value
		self.fill = numpy.nan

		# set sink
		self.sink = sink

		# set date attribute
		self.year = year
		self.month = month
		self.day = day
		self.date = ''
		self.stub = ''
		self._calendar()

		# set version
		self.version = 'V03'

		# set grid size
		self.mirror = 1200
		self.track = 2048

		# set model information
		self.session = session
		self.tag = tag

		# retrieve model
		self.model = None
		self.details = None
		self.folder = None
		self.features = None
		self._retrieve()

		# parse yaml file
		self.yam = {}
		self.architecture = {}
		self.training = {}
		self.validation = {}
		self._parse()

		# set limits
		self.limits = limits or {}
		self._limit()

		# set default instances
		self.instances = 50
		self._instantiate()

		# create site catalog
		self.catalog = {}
		self._catalog()

		# set units
		self.target = '$GPP$'
		self.unit = '$umolCO_2/m^2 s$'

		return

	def __repr__(self):
		"""Create string for on screen representation.

		Arguments:
			None

		Returns:
			str
		"""

		# create representation
		formats = (self.sink, self.date, self.tag, self.session)
		representation = ' < Tempest instance at {}, {}, {} ( {} ) >'.format(*formats)

		return representation

	def _access(self, name, labels=None, count=200, window=None):
		"""Collect tempo files.

		Arguments:
			name: str, short name
			labels: list of str, the scan labels
			count: number of results to obtain
			window: number of days to expand search

		Returns:
			None
		"""

		# set formats
		formats = (self.year, self._pad(self.month), self._pad(self.day))

		# create L1B folders
		self._make('{}/{}'.format(self.sink, name))
		self._make('{}/{}/{}'.format(self.sink, name, *formats[:1]))
		self._make('{}/{}/{}/{}'.format(self.sink, name, *formats[:2]))
		self._make('{}/{}/{}'.format(self.sink, name, self.stub))

		# login to earthaccess
		earthaccess.login(strategy="netrc", persist=True)

		# construct date
		dates = [datetime.datetime(self.year, self.month, self.day)]
		dates += [dates[0] + datetime.timedelta(days=1)]

		# if given a window:
		if window:

			# split in half
			half = int(window / 2)

			# construct date
			date = datetime.datetime(self.year, self.month, self.day) - datetime.timedelta(days=half)
			dateii = datetime.datetime(self.year, self.month, self.day) + datetime.timedelta(days=half)
			dates = [date, dateii]

		# convert to strings
		dates = [str(date)[:10] for date in dates]

		# get results
		results = earthaccess.search_data(short_name=name, version=self.version, temporal=tuple(dates), count=count)

		# if given scan labels:
		if labels:

			# group according to scan
			groups = self._group(labels, lambda label: re.findall('S[0-9]{3}', label)[0])

			# for each label
			for scan, group in groups.items():

				# print label
				self._print('scan {}...'.format(scan))

				# filter results by date and scan labels
				date = '_{}{}{}T'.format(*formats)
				subset = [result for result in results if any([label in result['umm']['GranuleUR'] for label in group])]
				subset = [result for result in subset if date in result['umm']['GranuleUR']]

				# if there are results
				if len(subset) > 0:

					# download
					earthaccess.download(subset, '{}/{}/{}'.format(self.sink, name, self.stub))

		# otherwise
		else:

			# download
			earthaccess.download(results, '{}/{}/{}'.format(self.sink, name, self.stub))

		return None

	def _anchor(self, target, reference):
		"""Choose the corner in which to position the plot legend.

		Arguments:
			target: tuple of floats, the point for comparison
			reference: tuple of floats, the reference point

		Returns:
			str, the legend location
		"""

		# begin location
		location = ''

		# if truth is greater than prediction
		if (target[1] > reference[1]):

			# put legend at bottom
			location += 'lower '

		# otherwise
		else:

			# put legend on bottom
			location += 'upper '

		# if truth is greater than median
		if (target[0] > reference[0]):

			# put legent at left
			location += 'left'

		# otherwise
		else:

			# put legend at right
			location += 'right'

		return location

	def _base(self, hours):
		"""Download ABI Basliner data.

		Arguments:
			hours: tuple of ints, the hour bracket

		Returns:
			None
		"""

		# create folders
		folder = 'ABI_CONUS'
		formats = (self.year, self._pad(self.month), self._pad(self.day))
		self._make('{}/{}'.format(self.sink, folder))
		self._make('{}/{}/{}'.format(self.sink, folder, *formats[:1]))
		self._make('{}/{}/{}/{}'.format(self.sink, folder, *formats[:2]))
		self._make('{}/{}/{}'.format(self.sink, folder, self.stub))
		[self._make('{}/{}/{}/{}'.format(self.sink, folder, self.stub, self._pad(wave + 1))) for wave in range(16)]

		# determine julian day
		julian = datetime.datetime(self.year, self.month, self.day).timetuple().tm_yday

		# intialize aws session
		configuration = botocore.client.Config(signature_version=botocore.UNSIGNED)
		client = boto3.client('s3', region_name='us-east-1', config=configuration)

		# Define the bucket and directory
		bucket = 'noaa-goes16'

		# for each hour
		for hour in range(*hours):

			# get all objects
			prefix = 'ABI-L1b-RadC/{}/{}/{}'.format(self.year, self._pad(julian, 3), self._pad(hour))
			response = client.list_objects_v2(Bucket=bucket, Prefix=prefix)
			objects = response.get('Contents', [])

			# for each wavelength
			for wave in range(16):

				# for each file
				for object in objects:

					# get file name
					name = object['Key'].split('/')[-1]

					# if wave in name
					if 'M6C{}_'.format(self._pad(wave + 1)) in name:

						# construct filename and download
						formats = (self.sink, folder, self.stub, self._pad(wave + 1), name)
						destination = '{}/{}/{}/{}/{}'.format(*formats)
						client.download_file(bucket, object['Key'], destination)
						self._print('downloaded {}.'.format(destination))

		return None

	def _bend(self, tensor):
		"""Define bend identity activation function.

		Arguments:
			tensor: numpy array, incoming tensor

		Returns:
			numpy array, bent identity
		"""

		# construct identity
		identity = ((tensorflow.sqrt(tensor ** 2 + 1) - 1) / 2) + tensor

		return identity

	def _bin(self, truth, prediction, samples, bracket, bins=1000):
		"""Perform binning for regression sample plots.

		Arguments:
			truth: numpy array, the truths
			prediction: numpy array, the predictions
			samples: int, number of samples for sample cutoff
			bracket: tuple of floats, the prediction limits
			bins: int, number of 2-D bins

		Returns:
			tuple of numpy arrays, the grids and counts
		"""

		# calculate sample bins
		options = {'statistic': 'count', 'range': (bracket, bracket), 'bins': bins}
		counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(truth, prediction, None, **options)
		truths = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
		predictions = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
		mesh, meshii = numpy.meshgrid(truths, predictions, indexing='ij')

		# apply mask
		mask = counts > 0
		counts = counts[mask]
		mesh = mesh[mask]
		meshii = meshii[mask]

		# clip counters to samples limit
		counts = numpy.where(counts > samples, samples, counts)

		# order counters
		order = numpy.argsort(counts)
		counts = counts[order]
		mesh = mesh[order]
		meshii = meshii[order]

		return mesh, meshii, counts

	def _calendar(self):
		"""Format the date strings.

		Arguments:
			None

		Returns:
			None
		"""

		# create formatw
		formats = (self.year, self._pad(self.month), self._pad(self.day))
		self.date = '{}m{}{}'.format(*formats)
		self.stub = '{}/{}/{}'.format(*formats)

		return None

	def _catalog(self):
		"""Get a list of site names.

		Arguments:
			None

		Returns:
			list of str, the site names
		"""

		# open site list
		paths = self._see('{}/Onefluxnet/sites'.format(self.sink))
		path = [path for path in paths if '.sav' in path][0]
		sites = scipy.io.readsav(path)

		# get all names
		names = [code.decode('utf-8') for code in sites['recs']['name']]
		latitudes = numpy.array(sites['recs']['lat'])
		longitudes = numpy.array(sites['recs']['lon'])

		# create catalog
		zipper = zip(names, latitudes, longitudes)
		catalog = {name: {'latitude': latitude, 'longitude': longitude} for name, latitude, longitude in zipper}

		# set catalog
		self.catalog = catalog

	def _clear(self, folder):
		"""Clear a folder.

		Arugments:
			folder: str, folder name

		Returns:
			None
		"""

		# clean folder
		self._clean('{}/{}'.format(self.folder, folder))

		return None

	def _cluster(self, matrix, number, exclusions):
		"""Perform clustering on a matrix.

		Arguments:
			matrix: numpy array
			number: int, number of clusters
			exclusions: list of ints, the columns to exclude

		Returns:
			numpy array, the cluster centroids
		"""

		# create a copy
		xerox = matrix.copy()
		mean = matrix.mean(axis=0)
		deviation = matrix.std(axis=0)

		# normalize matrix
		xerox = (xerox - mean) / deviation

		# for each excluded column ( including final truth column )
		for column in exclusions + [xerox.shape[-1] - 1]:

			# replace with median
			xerox[:, column] = numpy.percentile(xerox[:, column], 50)

		# set up clusterer
		clusterer = KMeans(n_clusters=number, random_state=42)
		clusterer.fit(xerox)

		# extract labels and centroids
		centroids = clusterer.cluster_centers_

		# unnormalize
		centroids = (centroids * deviation) + mean

		return centroids

	def _constrain(self, data, constraints):
		"""Apply constraints to a data set.

		Arguments:
			data: dict of numpy arrays
			constraints: numpy array, boolean mask

		Returns:
			dict of numpy arrays
		"""

		# begin valid dataset
		valids = {}

		# for each feature
		for name, array in data.items():

			# add to data
			valids[name] = array

			# if shapes match
			if array.shape[0] == constraints.shape[0]:

				# apply constraints
				valids[name] = array[constraints]

		return valids

	def _daze(self, validation=True):
		"""Get the list of days involved.

		Arguments:
			validation: boolean, get validation days first?

		Returns:
			list of (int, int) tuples, the months and days
		"""

		# get days
		days = self.training['days'] + self.validation['days']

		# if putting validation first
		if validation:

			# get days
			days = self.validation['days'] + self.training['days']

		return days

	def _decode(self, details):
		"""Decode byte strings.

		Arguments:
			details: dict of str, arrays

		Returns:
			dict
		"""

		# for each field
		for field, array in details.items():

			# if a string type
			if 'S' in str(array.dtype):

				# decode
				details[field] = numpy.char.decode(array, 'utf-8')

		return details

	def _feature(self):
		"""Set feature list for model training.

		Arguments:
			None

		Returns:
			None

		Populates:
			self.features
		"""

		# begin features
		features = []

		# for each feature
		for feature, limit in self.yam['features'].items():

			# if a limit is given:
			if limit:

				# add all indices
				features += ['{}|{}'.format(feature, self._pad(number)) for number in range(self.limits[limit])]

			# otherwise
			else:

				# add singlet feature
				features += [feature]

		# set features
		self.features = features

		return None

	def _fill(self, array):
		"""Replace various fill values with self.fill.

		Arguments:
			array: numpy array

		Returns:
			numpy array
		"""

		# create mask for valid data
		mask = self._mask(array)

		# fill with fill value
		array = numpy.where(mask, array, self.fill)

		return array

	def _forge(self, data, features):
		"""Forge a matrix from a dataset and list of features.

		Arguments:
			data: dict of fields and numpy arrays
			features: list of str, the features

		Returns:
			numpy array
		"""

		# begin vectors
		vectors = []

		# set transforms
		transforms = {'cos': lambda vector: numpy.cos(numpy.radians(vector))}

		# for each feature
		for feature in features:

			# set defaults
			name = feature
			position = None
			transform = None

			# if colon
			if '|' in name:

				# strip paranthesis
				name, position = name.split('|')
				position = int(position)

			# if parenthesis
			if '(' in name:

				# strip paranthesis
				transform, name = name.split('(')

			# get vector
			vector = data[name]

			# if position
			if position is not None:

				# extract position
				vector = vector[:, position].reshape(-1, 1)

			# if transform
			if transform:

				# apply transform
				vector = transforms[transform](vector)

			# add vector
			vectors.append(vector)

		# stack
		block = numpy.hstack(vectors)

		return block

	def _graph(self, abscissa, ordinate, intensity, bounds, scale, gradient, title, unit, labels, **options):
		"""Generate graph object for plotting.

		Arguments:
			abscissa: numpy array, horizontal coordinates
			ordinate: numpy array, vertical coordinates
			intensity: numpy array, value for color
			bounds: list of ( float, float ) tuples, the x-axis and y-axis bounds
			scale: tuple of floats, the color scale boundaries
			gradient: str, the name of the matplotlib color gradient
			title: str, plot title
			units: str, plot units
			labels: list of str, the x and y axis labels
			**options: unpacked dictionary of options:
				globe: boolean, draw coastlines?
				logarithm: boolean, use logarithmic scale?
				colorbar: boolean, add colorbar?
				lines: list of (array, array, str) tuples, additional lines ( x, y, format str )
				pixel: float, the pixel size
				polygons: boolean, graph as polygons?
				corners: list of numpy arrays, the latitude and longitude corners
				selection: indices of color gradient
				categories: list of ( str, str ) tuples, the colors and categories
				reflectivity: boolean, plotting reflectivity?
				alpha: float, transparency level
				font: int, title font size
				fontii: int, marker font size
				clip: boolean, clip endpoints to scale limits?

		Returns:
			dictionary, graph specifics
		"""

		# set default options
		globe = options.get('globe', True)
		logarithm = options.get('logarithm', False)
		colorbar = options.get('colorbar', True)
		lines = options.get('lines', [])
		pixel = options.get('pixel', abscissa.shape[0] / ( 72 * 16))
		polygons = options.get('polygons', False)
		corners = options.get('corners', [None, None])
		ticks = options.get('ticks', None)
		ticksii = options.get('ticksii', None)
		marks = options.get('marks', None)
		marksii = options.get('marksii', None)
		marker = options.get('marker', 2)
		selection = options.get('selection', (0, 256))
		categories = options.get('categories', None)
		reflectivity = options.get('reflectivity', None)
		alpha = options.get('alpha', 0.8)
		font = options.get('font', 20)
		fontii = options.get('fontii', 15)
		clip = options.get('clip', True)

		# create graph object
		graph = {'abscissa': abscissa, 'ordinate': ordinate, 'intensity': intensity}
		graph.update({'x_bounds': bounds[0], 'y_bounds': bounds[1], 'scale': scale, 'gradient': gradient})
		graph.update({'title': title, 'units': unit, 'x_label': labels[0], 'y_label': labels[1]})
		graph.update({'globe': globe, 'logarithm': logarithm, 'colorbar': colorbar, 'lines': lines})
		graph.update({'pixel': pixel, 'polygons': polygons, 'corners': corners})
		graph.update({'x_ticks': ticks, 'y_ticks': ticksii, 'x_marks': marks, 'y_marks': marksii})
		graph.update({'marker': marker, 'selection': selection, 'categories': categories})
		graph.update({'reflectivity': reflectivity, 'alpha': alpha, 'font': font, 'fontii': fontii})
		graph.update({'clip': clip})

		return graph

	def _instantiate(self):
		"""Set number of instances for bayesian means and deviations from yaml file.

		Arguments:
			None

		Returns:
			None

		Populates:
			self.instances
		"""

		# set instances
		self.instances = self.validation['instances']

		return None

	def _interpolate(self, quantity, nodes, nodesii, target, targetii, method='nearest'):
		"""Perform two dimensional interpolation of a quantity from one grid to another.

		Arguments:
			quantity: 2-D numpy.array of gridded data
			nodes: 1-D numpy array of first nodes
			nodesii: 1-D numpy array of second nodes
			target: array of first grid coordinates
			targetii: array of second grid coordinates
			method: interpolation method ('nearest', 'linear')

		Returns:
			numpy.array
		"""

		# get final shape from target
		shape = target.shape

		# interpolate gpp onto tempo coordinates
		options = {'method': method, 'fill_value': self.fill, 'bounds_error': False}
		parameters = ((nodes, nodesii), quantity, (target.reshape(-1), targetii.reshape(-1)))
		interpolation = scipy.interpolate.interpn(*parameters, **options).reshape(shape)

		return interpolation

	def _label(self, scan, granule=None):
		"""Create a scan label.

		Arguments:
			scan: int, the scan number
			granule: int, the granule number

		returns:
			str, the scan label
		"""

		# make label
		label = '_S{}G'.format(self._pad(scan, 3))

		# if a granule number given
		if granule:

			# add granule number
			label += self._pad(granule)

		return label

	def _like(self, points, distribution):
		"""Define the bayesian likelihood loss function with a penalty for higher weights.

		Arguments:
			points: numpy array of ponts
			distribution: tensorflow distribution fucntion

		Returns:
			numpy array, the loos
		"""

		# get custom loss penalty for high wieghted samples
		penalty = self.training['penalty']

		# create weights
		weights = 1 + penalty * tensorflow.math.abs(points)

		# define loss
		loss = -tensorflow.reduce_mean(weights * distribution.log_prob(points))

		return loss

	def _limit(self):
		"""Define valid data limits.

		Arguments:
			None

		Returns:
			None
		"""

		# get limits from yaml
		limits = self.yam['limits']

		# update with given limits
		limits.update(self.limits)
		self.limits = limits

		return None

	def _localize(self, seconds, longitude, epoch='1980-01-06'):
		"""Create local time form seconds past the epoch and longitude.

		Arguments:
			seconds: numpy array, seconds past epoch per mirror step
			longitude: numpy array, longitude
			epoch: str, the reference for the epoch

		Returns:
			numpy array
		"""

		# convert epoch to datetime
		year = int(epoch[:4])
		month = int(epoch[5:7])
		day = int(epoch[8:10])
		epoch = datetime.datetime(year, month, day)

		# convert seconds to datetimes
		dates = [epoch + datetime.timedelta(seconds=second) for second in seconds]

		# extract hours
		hours = numpy.array([date.hour + (date.minute / 60) + (date.second / 60 ** 2) for date in dates])

		# expand by track length
		hours = numpy.array([hours] * longitude.shape[1]).transpose(1, 0)

		# get offsets from longitudes
		offsets = longitude / 15.0

		# add offsets to get local time
		time = hours + offsets

		return time

	def _lose(self, truth, prediction):
		"""Define the custom loss function with a penalty for higher weights.

		Arguments:
			points: numpy array of ponts
			distribution: tensorflow distribution fucntion
			truth: numpy array of truth values
			prediction: numpy array of prediction values

		Returns:
			numpy array, the loos
		"""

		# get custom loss penalty for high wieghted samples
		penalty = self.training['penalty']

		# create weights
		weights = 1 + penalty * (tensorflow.math.abs(truth) ** 2)

		# # define loss
		# loss = -tensorflow.reduce_mean(weights * distribution.log_prob(points))
		loss = tensorflow.reduce_mean(weights * tensorflow.square(truth - prediction))

		return loss

	def _mask(self, array):
		"""Determine valid data in an array.

		Arguments:
			array: numpy array

		Returns:
			numpy boolean array
		"""

		# create mask
		mask = (numpy.isfinite(array)) & (abs(array) < 1e20) & (array > -9998)

		return mask

	def _paint(self, destination, graphs, size=(16, 16), space=None, gap=0.065):
		"""Create a plot with subplots.

		Arguments:
			destination: str, the file path
			graphs: list of dicts, the graph objects
			size: tuple of floats, the total plot size
			space: dict, subplot spacing paramaeters
			gap: gap between grqph and colorbars

		Returns:
			None
		"""

		# print status
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase previous copy
		self._clean(destination, force=True)

		# define default spacing
		space = space or {}

		# get number of subplots
		number = len(graphs)

		# set up dictionary of subplot values based on number
		layout = {'rows': {1: 1, 2: 1, 3: 1, 4: 2}, 'columns': {1: 1, 2: 2, 3: 3, 4: 2}}

		# calculate total size
		rows = layout['rows'][number]
		columns = layout['columns'][number]

		# set up figure
		pyplot.clf()
		figure = pyplot.figure(figsize=size)
		figure.patch.set_facecolor('white')
		# words = {'subplot_kw': {'projection': cartopy.crs.PlateCarree()}, 'figsize': size}
		# figure, axes = pyplot.subplots(rows, columns, figsize=size)

		# begin axes
		axes = []

		# for each graph
		for index, graph in enumerate(graphs):

			# if drawing globe:
			if graph['globe']:

				# activate cartopy
				code = int('{}{}{}'.format(rows, columns, index + 1))
				axis = pyplot.subplot(code, projection=cartopy.crs.PlateCarree())
				axis.set_facecolor('white')
				axes.append(axis)

			# otherwise:
			else:

				# activate cartopy
				code = int('{}{}{}'.format(rows, columns, index + 1))
				axis = pyplot.subplot(code)
				axis.set_facecolor('white')
				axes.append(axis)

		# adjust spacing
		margins = {'bottom': 0.12, 'top': 0.94, 'left': 0.07, 'right': 0.93, 'wspace': 0.2, 'hspace': 0.5}
		margins.update(space)
		pyplot.subplots_adjust(**margins)

		# for each graph
		for axis, graph in zip(axes, graphs):

			# unpack arrays
			abscissa = graph['abscissa']
			ordinate = graph['ordinate']
			intensity = graph['intensity']

			# unpack bounds
			bounds = graph['x_bounds']
			boundsii = graph['y_bounds']

			# unpack color scale
			scale = graph['scale']
			gradient = graph['gradient']
			selection = graph['selection']
			categories = graph['categories']

			# if clipping:
			if graph['clip']:

				# clip intensity to scale boundaries
				epsilon = 1e-10
				intensity = numpy.where(intensity > scale[0] + epsilon, intensity, scale[0] + epsilon)
				intensity = numpy.where(intensity < scale[1] - epsilon, intensity, scale[1] - epsilon)

			# unpack texts
			title = graph['title']
			units = graph['units']
			label = graph['x_label']
			labelii = graph['y_label']
			font = graph['font']
			fontii = graph['fontii']

			# if not reflectivity
			if not graph['reflectivity']:

				# create mask for finite data
				mask = numpy.isfinite(intensity)

				# apply mask
				intensity = intensity[mask]
				abscissa = abscissa[mask]
				ordinate = ordinate[mask]

				# get scale bounds
				minimum = scale[0]
				maximum = scale[1]

				# clip intensity
				intensity = numpy.where(intensity < minimum, minimum, intensity)
				intensity = numpy.where(intensity > maximum, maximum, intensity)

			# if drawing globe:
			if graph['globe']:

				# set cartopy axis with coastlines
				axis.coastlines()
				# axis.add_feature(cartopy.feature.OCEAN)
				# axis.add_feature(cartopy.feature.COASTLINE, edgecolor='black', linewidth=1)
				axis.add_feature(cartopy.feature.STATES)
				axis.add_feature(cartopy.feature.LAKES, facecolor='white')
				# axis.add_feature(cartopy.feature.OCEAN, facecolor='blue')
				options = {'edgecolor': 'face', 'facecolor': 'white'}
				axis.add_feature(cartopy.feature.NaturalEarthFeature('physical', 'ocean', '50m', **options))

				# set title
				axis.set_aspect('auto')
				axis.set_title(title, fontsize=font)

				# if xticks
				if graph['x_ticks'] and graph['y_ticks']:

					# set x ticks
					axis.set_xticks(graph['x_ticks'])
					axis.set_xticklabels(graph['x_marks'])
					# axis.tick_params(axis='x', rotation=45)

					# set yticks
					axis.set_yticks(graph['y_ticks'])
					axis.set_yticklabels(graph['y_marks'])
					# axis.tick_params(axis='y', rotation=45)

					# set font size
					axis.tick_params(axis='both', which='major', labelsize=fontii)	# Major ticks
					axis.tick_params(axis='both', which='minor', labelsize=fontii)

				# otherwies
				else:

					# set grid marks
					axis.grid(True)
					axis.set_global()
					_ = axis.gridlines(draw_labels=True)

					# set font size
					axis.tick_params(axis='both', which='major', labelsize=fontii)
					axis.tick_params(axis='both', which='minor', labelsize=fontii)

			# otherwise:
			else:

				# set title
				axis.set_title(title, fontsize=font)

				# add labels and ticks
				axis.set_title(title, fontsize=font)
				axis.set_xlabel(label, fontsize=fontii)
				axis.set_ylabel(labelii, fontsize=fontii)

				# set font size
				axis.tick_params(axis='both', which='major', labelsize=fontii)
				axis.tick_params(axis='both', which='minor', labelsize=fontii)

				# if xticks
				if graph['x_ticks']:

					# set x ticks
					axis.set_xticks(graph['x_ticks'])
					axis.set_xticklabels(graph['x_marks'])
					axis.tick_params(axis='x', rotation=45)

				# if yticks
				if graph['y_ticks']:

					# set yticks
					axis.set_yticks(graph['y_ticks'])
					axis.set_yticklabels(graph['y_marks'])
					axis.tick_params(axis='y', rotation=45)

			# set up colors and logarithmic scale
			colors = matplotlib.cm.get_cmap(gradient)
			selection = numpy.array(list(range(*selection)))
			colors = matplotlib.colors.ListedColormap(colors(selection))
			normal = matplotlib.colors.Normalize(minimum, maximum)

			# if colorbar categores
			if categories:

				# split names and hues
				hues = [category[0] for category in categories]
				texts = [category[1] for category in categories]
				colors = matplotlib.colors.ListedColormap(hues)
				normal = matplotlib.colors.BoundaryNorm(boundaries=numpy.arange(len(hues) + 1) - 0.5, ncolors=len(hues))

			# if logarithmic
			if graph['logarithm']:

				# make normal logarithmic
				normal = matplotlib.colors.LogNorm(minimum, maximum)

			# if grpahing polygons
			if graph['polygons']:

				# begin patches
				patches = []

				# for each polygon
				for vertices, verticesii in zip(graph['corners'][1].reshape(-1, 4), graph['corners'][0].reshape(-1, 4)):

					# set up patch
					polygon = numpy.array([[first, second] for first, second in zip(vertices, verticesii)])
					patch = matplotlib.patches.Polygon(xy=polygon, linewidth=0, edgecolor=None, alpha=graph['alpha'])
					patches.append(patch)

				# if reflectivity plot
				if graph['reflectivity']:

					# plot polygons with colormap
					options = {'facecolor': intensity, 'alpha': graph['alpha']}
					collection = matplotlib.collections.PatchCollection(patches, **options)
					collection.set_edgecolor("face")
					# collection.set_clim(scale)
					# collection.set_array(intensity)
					polygons = axis.add_collection(collection)
					# axis.add_feature(cartopy.feature.OCEANS, facecolor='white')

				# otherwise
				else:

					# plot polygons with colormap
					collection = matplotlib.collections.PatchCollection(patches, cmap=colors, alpha=graph['alpha'])
					collection.set_edgecolor("face")
					collection.set_clim(scale)
					collection.set_array(intensity)
					polygons = axis.add_collection(collection)

			# otherwise
			else:

				# if reflectivity plot
				if graph['reflectivity']:

					# no need for colors
					axis.scatter(abscissa, ordinate, c=intensity, s=0.75, lw=0)

				# otheriwse
				else:

					# plot data
					words = {'c': intensity, 'cmap': colors, 'norm': normal}
					axis.scatter(abscissa, ordinate, marker='.', s=graph['pixel'], **words)

			# plot additional lines
			for line in graph['lines']:

				# plot line
				options = {'markersize': graph['marker']}
				axis.plot(*line, **options)

			# set axis limits
			axis.set_xlim(*bounds)
			axis.set_ylim(*boundsii)

			# if using colorbar
			if graph['colorbar']:

				# add colorbar
				position = axis.get_position()
				bar = pyplot.gcf().add_axes([position.x0, position.y0 - gap, position.width, 0.02])
				scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
				# colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', label=units)

				# if cateogories
				if categories:

					# add categorical colorbar
					ticksii = numpy.array(range(len(hues)))
					colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', ticks=ticksii)
					colorbar.ax.set_xticklabels(texts)	# Set category names
					colorbar.ax.tick_params(axis='x', rotation=45, labelsize=fontii)
					# colorbar.set_label(units, labelpad=1)
					# formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
					# colorbar.ax.yaxis.set_major_formatter(formatter)
					# # Create the colorbar with category labels
					# # barii = bar.imshow(numpy.array(range(len(hues))), cmap=colors, norm=normal)
					# colorbar = pyplot.gcf().colorbar(scalar, ticks=numpy.arange(len(hues)))
					# colorbar.ax.set_yticklabels(texts)  # Set category names

				# otherwise
				else:

					# add regular colorbar
					colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal')
					colorbar.set_label(units, labelpad=1, fontsize=fontii)
					formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
					colorbar.ax.yaxis.set_major_formatter(formatter)
					colorbar.ax.tick_params(labelsize=fontii)  # For tick labels

		# save to destination
		pyplot.savefig(destination)
		pyplot.clf()

		# end status
		self._stamp('plotted.')

		return None

	def _parse(self):
		"""Parse the yaml file.

		Arguments:
			None

		Returns:
			None

		Populates:
			self.yam
		"""

		# get the yaml file
		yam = self._acquire('{}/GPP_Yams/{}.yaml'.format(self.sink, self.tag))

		# set yam
		self.yam = yam

		# populate subsections
		self.training = yam['training']
		self.architecture = yam['architecture']
		self.validation = yam['validation']

		return None

	def _patch(self, reflectance, latitude, longitude, cloud):
		"""Patch bad reflectance data with nearest neighbors good data.

		Arguments:
			reflectance: numpy array, reflectqnce pcas
			latitude: numpy array, latitude
			longitude: numpy array, longitude
			cloud: numpy array, cloud fraction

		Returns:
			numpy array: patched data
		"""

		# squeeze latitude and longitude
		latitude = latitude.squeeze()
		longitude = longitude.squeeze()
		cloud = cloud.squeeze()

		# create mask for good and bad data
		summation = reflectance.sum(axis=1)
		good = (numpy.isfinite(summation) & (abs(summation) < 1e20))
		bad = numpy.logical_not(good)

		# create a KDTree from the good lat/lon
		tree = scipy.spatial.cKDTree(numpy.column_stack((latitude[good], longitude[good])))

		# query the tree for bad data points
		distances, indices = tree.query(numpy.column_stack((latitude[bad], longitude[bad])), k=1)

		# replace bad data with the nearest good data
		reflectance[bad] = reflectance[good][indices.flatten()]

		return reflectance

	def _post(self, kernel, bias=0, datatype=None):
		"""Define the posterior distribution function.

		Arguments:
			kernal: int, the kernal size
			bias: int, the bias size
			datatype: datatype object, the datatype

		Returns:
			tensor flow model layer
		"""

		# define abbreviations
		probability = tensorflow_probability.layers
		independent = tensorflow_probability.distributions.Independent
		normal = tensorflow_probability.distributions.Normal

		# get total size
		size = kernel + bias

		# set scaling functions
		epsilon = 1e-5
		scale = 0.02
		constant = numpy.log(numpy.expm1(1.0))
		def locating(tensor): return tensor[..., :size]
		def scaling(tensor): return epsilon + scale * tensorflow.nn.tanh(constant + tensor[..., size:])

		# define independent normal function
		options = {'reinterpreted_batch_ndims': 1}
		def normalizing(tensor): return independent(normal(loc=locating(tensor), scale=scaling(tensor)), **options)

		# create layer sequence
		layers = [probability.VariableLayer(2 * size, dtype=datatype)]
		layers += [probability.DistributionLambda(normalizing)]
		sequence = tensorflow.keras.Sequential(layers)

		return sequence

	def _prioritize(self, kernel, bias=0, datatype=None):
		"""Define the prior distribution function.

		Arguments:
			kernal: int, the kernal size
			bias: int, the bias size
			datatype: datatype object, the datatype

		Returns:
			tensor flow model layer
		"""

		# define abbreviations
		probability = tensorflow_probability.layers
		independent = tensorflow_probability.distributions.Independent
		normal = tensorflow_probability.distributions.Normal

		# get total size
		size = kernel + bias

		# define independent normal function
		def normalizing(tensor): return independent(normal(loc=tensor, scale=1), reinterpreted_batch_ndims=1)

		# create layer sequence
		layers = [probability.VariableLayer(size, dtype=datatype)]
		layers += [probability.DistributionLambda(normalizing)]
		sequence = tensorflow.keras.Sequential(layers)

		return sequence

	def _project(self, horizontal, vertical, projection):
		"""Project the ABI angular grid onto a latitude longitude grid.

		Arguments:
			horizontal: numpy array, x coordinate values
			vertical: numpy array, y coordinate values
			projection: dict, projection information

		Returns:
			(numpy array, numpy array) tuple, the geocoordinates
		"""

		# get projection information
		origin = projection['longitude_of_projection_origin']
		equatorial = projection['semi_major_axis']
		polar = projection['semi_minor_axis']
		altitude = projection['perspective_point_height']
		height = altitude + equatorial

		# create mesh grid
		mesh, meshii = numpy.meshgrid(horizontal, vertical)

		# set up equations
		zero = (origin * numpy.pi) / 180.0
		sine = numpy.sin(mesh)
		sineii = numpy.sin(meshii)
		cosine = numpy.cos(mesh)
		cosineii = numpy.cos(meshii)
		variable = sine ** 2 + (cosine ** 2 * (cosineii ** 2 + (equatorial ** 2 / polar ** 2) * sineii ** 2))
		variableii = -2 * height * cosine * cosineii
		variableiii = height ** 2 - equatorial ** 2
		radius = (-variableii - numpy.sqrt(variableii ** 2 - (4 * variable * variableiii))) / (2 * variable)
		abscissa = radius * cosine * cosineii
		ordinate = -radius * sine
		applicate = radius * cosine * sineii

		# ignore errors
		errors = numpy.seterr(all='ignore')

		# calculate latitude and longitude
		degrees = (180.0 / numpy.pi)
		ratio = equatorial ** 2 / polar ** 2
		root = numpy.sqrt(((height-abscissa) * (height-abscissa)) + (ordinate ** 2))
		latitude = degrees * (numpy.arctan(ratio * ((applicate / root))))
		longitude = (zero - numpy.arctan(ordinate / (height-abscissa))) * degrees

		# reset numpy errors
		numpy.seterr(**errors)

		return latitude, longitude

	def _recite(self):
		"""List all models.

		Arguments:
			None

		Returns:
			None
		"""

		# show all paths of model directory
		self._show('{}/GPP_Models'.format(self.sink))

		return None

	def _represent(self, coefficients, features, number=3):
		"""Determine the representation of the pca coefficients.

		Arugments:
			coefficients: numpy array of transposed pca components
			features: list of features
			number: int, number of top entries

		Returns:
			list of str, the pca componeetn labesl
		"""

		# begin labels
		labels = []

		# transpose to get components on rows
		components = coefficients.transpose(1, 0)

		# for each component
		for component in components:

			# zip values with features
			pairs = list(zip(features, component))

			# sort by highest and keep top
			pairs.sort(key=lambda pair: abs(pair[1]), reverse=True)
			top = pairs[:number]

			# create label
			label = ['{}: {:.2f}'.format(*pair) for pair in top]
			label =' '.join(label)
			labels.append(label)

		return labels

	def _retrieve(self, keras=False):
		"""Retrieve a tensor flow model and scaling details.

		Arguments:
			keras: boolean, use Keras style?

		Returns:
			None

		Populates
			self.model
			self.details
			self.folder
		"""

		# set models folder
		models = 'GPP_Models'

		# get all model folders
		folders = self._see('{}/{}'.format(self.sink, models))

		# if given a date:
		if self.session:

			# restrict to date
			folders = [folder for folder in folders if self.session in folder]

		# if given a tag:
		if self.tag:

			# restrict to tag
			folders = [folder for folder in folders if self.tag == folder.split('_')[-1]]

		# try to
		try:

			# sort and get latest date
			folders.sort()
			folder = folders[-1]

			# construct model path
			path = '{}/TEMPO_GPP_Model'.format(folder)

			# if keras
			if keras:

				# load model
				model = tensorflow.keras.models.load_model(path, compile=False)

			# otherwise
			else:

				# load model
				model = tensorflow.saved_model.load(path)

			# also get model scaling details
			hydra = Hydra(folder)
			hydra.ingest('Model_Scaling.h5')
			details = hydra.extract(addresses=True)

			# decode strings in details
			details = self._decode(details)

			# set attributes
			self.model = model
			self.details = details
			self.folder = folder
			self.features = self.details['features'].tolist()

			# set session and tag
			self.session = folder.split('/')[-1].split('_')[2]
			self.tag = folder.split('/')[-1].split('_')[3]

		# unless no model found
		except (IndexError, OSError):

			# print status
			self._print('no model found, need to train.')

		return None

	def _search(self, pattern, string):
		"""Use regex to find a pattern in a string.

		Arguments:
			pattern: str, regex pattern
			string: str, the source string

		Returns:
			str, the found pattern
		"""

		# default element to ''
		element = ''

		# try to
		try:

			# find element
			element = re.findall(pattern, string)[0]

		# unless not found
		except IndexError:

			# in which case, pass
			pass

		return element

	def _transform(self, matrix):
		"""Transform the model inputs in accordance with preprocessing steps.

		Arguments:
			matrix: numpy array, the raw model inputs and truth value

		Returns:
			tuple of numpy arrays, the inputs and truth
		"""

		# get scaling information
		minimum = self.details['scaling/input_min']
		maximum = self.details['scaling/input_max']
		mean = self.details['scaling/input_mean']
		deviation = self.details['scaling/input_std']

		# remove truth and apply scaling
		truth = matrix[:, -1]

		# if normalizing to mean
		if self.training['normalize']:

			# normalize inputs
			inputs = (matrix[:, :-1] - mean) / deviation

		# otherwise
		else:

			# scale inputs
			scale = maximum - minimum
			inputs = (matrix[:, :-1] - minimum) / scale

		# get pca coefficients
		coefficients = self.details['scaling/pca_coefficients']

		# if using pca decomposition
		if self.training['decompose']:

			# apply pca
			inputs = numpy.dot(inputs, coefficients).astype('float32')

		return inputs, truth

	def _translate(self, outputs):
		"""translate raw model outputs into model predictions.

		Arguments:
			outputs: numpy array, raw model outputs

		Returns:
			numpy array, model predictions
		"""

		# get output scaling
		minimum = self.details['scaling/output_min']
		maximum = self.details['scaling/output_max']
		mean = self.details['scaling/output_mean']
		deviation = self.details['scaling/output_std']

		# if normalizing
		if self.training['normalize']:

			# normalize to mean, stdev
			predictions = (outputs * deviation) + mean

		# otherwise
		else:

			# apply scaling
			scale = maximum - minimum
			predictions = (outputs * scale) + minimum

		return predictions

	def activate(self, scan=10, quantity=5, sample=0):
		"""Inspect the activations triggered by a sample.

		Arguments:
			scan: int, scan number


		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/activation'.format(self.folder)
		self._make(folder)

		# get specific day matrix
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, _, _ = self.materialize(*parameters)

		# make predictions
		prediction, _, truth, _ = self.predict(matrix)

		# find the closest prediction, and order matrix
		distance = abs(prediction - quantity)
		order = numpy.argsort(distance)
		matrix = matrix[order]

		# pick the single sample
		singlet = matrix[sample: sample + 1]

		# establish the keras version of the model
		self._retrieve(keras=True)

		# Define the layers you want to inspect (e.g., by name or index)
		layers = [layer.name for layer in self.model.layers if 'dense' in layer.name]

		# create a submodel to output activations for the selected layers
		options = {'inputs': self.model.input}
		options.update({'outputs': [self.model.get_layer(name).output for name in layers]})
		model = tensorflow.keras.Model(**options)

		# transform the inputs
		inputs, truth = self._transform(singlet)
		activations = model.predict(inputs)

		# restore tensorflow model
		self._retrieve()

		# for each layer
		for layer, activation in zip(layers, activations):

			# Create a histogram
			pyplot.clf()
			pyplot.figure(figsize=(8, 8))
			pyplot.hist(activation.flatten(), bins=50, edgecolor='black')

			# Add titles and labels
			title = 'histogram of {} ( {} hiddens )'.format(layer, activation.shape[1])
			pyplot.title(title)
			pyplot.xlabel('value')
			pyplot.ylabel('counts')

			# save the plot
			formats = (folder, self.date, layer, activation.shape[1])
			destination = '{}/activations_{}_{}_{}.png'.format(*formats)
			pyplot.savefig(destination)
			pyplot.clf()

			# print status
			self._print('plotted {}'.format(destination))

		return None

	def baseline(self, path, latitude, longitude):
		"""Colocate ABI baseliner data.

		Arguments:
			path: radiance path
			latitude: numpy array, tempo latitude
			longitude: numpy arraym, tempo longitude

		Returns:
			numpy array, the baseliner data
		"""

		# cast tempo coordinates as target points
		shape = latitude.shape
		targets = numpy.vstack([latitude.reshape(-1), longitude.reshape(-1)]).transpose(1, 0)

		# create valid coordinates mask
		valid = self._mask(targets.sum(axis=1))

		# determine geographic extend
		south = targets[:, 0][valid].min()
		north = targets[:, 0][valid].max()
		west = targets[:, 1][valid].min()
		east = targets[:, 1][valid].max()

		# substitute minimums for invalids
		targets[:, 0] = numpy.where(valid, targets[:, 0], south)
		targets[:, 1] = numpy.where(valid, targets[:, 1], west)

# 		# get the ABI high resolution grid
# 		grid = Hydra('{}/ABI_High_Grid'.format(self.sink), show=False)
# 		grid.ingest(0)
#
# 		# set grid shapes
# 		shapes = {(1500, 2500): 'low', (3000, 5000): 'mid', (6000, 10000): 'high'}
#
# 		# set trees, indicess
# 		indices = {}
# 		masks = {}
#
# 		# for each mode:
# 		for mode in shapes.values():
#
# 			latitudeii = grid.grab('latitude_{}'.format(mode))
# 			longitudeii = grid.grab('longitude_{}'.format(mode))
#
# 			# cast as points
# 			points = numpy.vstack([latitudeii.reshape(-1), longitudeii.reshape(-1)]).transpose(1, 0)
#
# 			# create mask for valid points and restrict to boundaries
# 			mask = self._mask(points.sum(axis=1))
# 			mask = mask & (points[:, 0] > south - 1) & (points[:, 0] < north + 1)
# 			mask = mask & (points[:, 1] > west - 1) & (points[:, 1] < east + 1)
#
# 			# build a kd search tree
# 			tree = scipy.spatial.cKDTree(points[mask])
#
# 			# query the tree
# 			_, index = tree.query(targets, k=1)
# 			index = index.flatten()
#
# 			# add to dictionaries
# 			indices[mode] = index
# 			masks[mode] = mask

		# extract datetime from path
		search = '[0-9]{8}T[0-9]{6}Z'
		date = re.findall(search, path)[0]
		brackets = [(0, 4), (4, 6), (6, 8), (9, 11), (11, 13), (13, 15)]
		components = [int(date[bracket[0]: bracket[1]]) for bracket in brackets]
		time = datetime.datetime(*components)

		# begin reservoirs
		radiances = []
		wavelengths = []

		# for each wavelength
		for wave in range(16):

			# connect to abi data
			hydra = Hydra('{}/ABI_CONUS/{}/{}'.format(self.sink, self.stub, self._pad(wave + 1)), show=False)

			# for each path
			times = []
			for pathii in hydra.paths:

				# extract date
				search = 's[0-9]{14}'
				dateii = re.findall(search, pathii)[0]
				year = int(dateii[1:5])
				julian = int(dateii[5:8])
				brackets = [(8, 10), (10, 12), (12, 14)]
				components = [int(dateii[bracket[0]: bracket[1]]) for bracket in brackets]
				timeii = datetime.datetime(year, 1, 1, *components) + datetime.timedelta(days=julian - 1)

				# append to list with path
				times.append((timeii, pathii))

			# sort by closest time
			times.sort(key=lambda pair: abs(pair[0] - time))
			pathii = times[0][1]

			# ingest the path
			hydra.ingest(pathii)

			# get data
			radiance = hydra.grab('Rad')
			quality = hydra.grab('DQF')
			wavelength = hydra.grab('band_wavelength')

			# get geolocation data
			horizontal = hydra.grab('x')
			vertical = hydra.grab('y')

			# get projection information
			projection = hydra.dig('goes_imager_projection')[0].attributes

			# convert into latitude and longitude
			latitudeii, longitudeii = self._project(horizontal, vertical, projection)

			# cast as points
			points = numpy.vstack([latitudeii.reshape(-1), longitudeii.reshape(-1)]).transpose(1, 0)

# 			# remove infinities
# 			finite = numpy.isfinite(points.sum(axis=1))
# 			points = points[finite]

			# create mask for valid points and restrict to boundaries
			mask = self._mask(points.sum(axis=1))
			mask = mask & (points[:, 0] > south - 1) & (points[:, 0] < north + 1)
			mask = mask & (points[:, 1] > west - 1) & (points[:, 1] < east + 1)

			# build a kd search tree
			tree = scipy.spatial.cKDTree(points[mask])

			# query the tree
			_, indices = tree.query(targets, k=1)
			indices = indices.flatten()

# 			# add to dictionaries
# 			indices[mode] = index
# 			masks[mode] = mask

# 			# determine mode from radiance shape
# 			mode = shapes[radiance.shape]

			# flatten and apply geolocation boundary mask
			radiance = radiance.reshape(-1)[mask]
			quality = quality.reshape(-1)[mask]

			# add wavelenth
			wavelengths.append(wavelength[0])

			# apply indices from tree query
			radianceii = radiance[indices]
			qualityii = quality[indices]

			# reapply valid coordinate mask with fill data
			radianceii = numpy.where(valid, radianceii, self.fill)
			qualityii = numpy.where(valid, qualityii, self.fill)

			# reshape interpolations
			radianceii = radianceii.reshape(shape)
			qualityii = qualityii.reshape(shape)

			# remove low quality data and add to array
			radianceii = numpy.where(qualityii == 0, radianceii, self.fill)
			radiances.append(radianceii)

		# craete arrays, transposing radiances
		radiances = numpy.array(radiances).transpose(1, 2, 0)
		wavelengths = numpy.array(wavelengths)

		return radiances, wavelengths

	def chart(self, scan=10, margin=2, variables=None, constrain=False):
		"""Make maps of various variables.

		Arguments:
			scan: int, scan number
			margin: float, percentile margin
			variables: list of str, the variables to plot
			constrain: boolean, apply constraints?

		Returns:
			None
		"""

		# if variables not given
		if not variables:

			# set variables
			variables = ['reflectance_pcas|1', 'reflectance_pcas|2', 'reflectance_pcas|3']
			variables += ['reflectance_pcas|4', 'reflectance_pcas|6', 'reflectance_pcas|7']
			variables += ['clearsky_par', 'solar_zenith_angle', 'cloud_fraction', 'gpp_fluxsat']
			variables += ['land_type', 'reflectance_pcas|0', 'reflectance_pcas|5']
			variables += ['viewing_zenith_angle', 'relative_azimuth_angle']

		# make troubleshooting folder
		folder = '{}/chart'.format(self.folder)
		self._make(folder)

		# get data set
		_, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1))

		# if constraining:
		if constrain:

			# apply constraints
			data = self._constrain(data, constraints)

		# get geo coordinates
		latitude = data['latitude']
		longitude = data['longitude']
		corners = data['latitude_bounds']
		cornersii = data['longitude_bounds']

		# set scales
		scales = {'gpp_onefluxnet': (0, 30), 'gpp_fluxsat': (0, 30), 'short_wave': (0, 1000)}
		scales.update({'onefluxnet_deviation': (0, 10), 'gpp_mosaic': (0, 30)})

		# for each variable
		for variable in variables:

			# default name to variable
			name = variable
			position = None

			# if a position is given
			if '|' in variable:

				# unpack
				name, position = variable.split('|')
				position = int(position)

			# get array
			array = data[name]

			# if a position
			if position is not None:

				# get the subset
				array = array[:, position]

			# begin graphs
			graphs = []

			# create destination
			destination = '{}/{}_{}_{}.png'.format(folder, self.date, self._pad(scan), variable.replace('|', '_'))

			# construct percent difference plot
			title = '{} {} scan {}\n'.format(variable.replace('|', '_'), self.date, self._pad(scan))
			unit = ''
			bounds = [(-125, -60), (15, 55)]

			# create valid mask
			mask = self._mask(array).squeeze()

			# set gradient
			gradient = 'plasma'

			# construct scale
			minimum = numpy.percentile(array[mask], margin)
			maximum = numpy.percentile(array[mask], 100 - margin)
			scale = (minimum, maximum)

			# set categories to None
			categories = None

			# if land type
			if variable == 'land_type':

				# adjust scale and gradient
				scale = (0, 16)
				gradient = 'gist_ncar'

				# construct categories
				categories = [('lightblue', 'water'), ('darkgreen', 'egn ndle'), ('green', 'egn brdlf')]
				categories += [('orange', 'dec ndle'), ('salmon', 'dec brdlf'), ('lightgreen', 'mxd frst')]
				categories += [('brown', 'c shrbld'), ('tan', 'o shrbld'), ('khaki', 'wdy svna')]
				categories += [('beige', 'svna'), ('lime', 'grsslnd'), ('aquamarine', 'wetlnd')]
				categories += [('yellow', 'croplnd'), ('gray', 'urban'), ('orchid', 'mosaic')]
				categories += [('ivory', 'snow'), ('lavender', 'barren')]

			# get scale
			scale = scales.get(variable, scale)

			# make plot
			labels = ['', '']
			options = {'polygons': True, 'corners': [corners[mask], cornersii[mask]], 'bar': True, 'categories': categories}
			parameters = (longitude[mask], latitude[mask], array[mask], bounds, scale, gradient, title, unit, labels)
			graph = self._graph(*parameters, **options)
			graphs.append(graph)

			# create plot
			self._paint(destination, graphs)

		return None

	def chronicle(self, scales=None):
		"""Make plots of loss functions.

		Arguments:
			scales: dict, yscale for loss functions

		Returns:
			None
		"""

		# default scales to empty dictionary
		scales = scales or {}

		# make folder
		folder = '{}/chronicle'.format(self.folder)
		self._make(folder)

		# get history contents
		contents = self._load('{}/history.json'.format(self.folder))

		# get non validation losses
		losses = [loss for loss in contents.keys() if 'val' not in loss]

		# for each entry
		for loss in losses:

			# make validation key
			validation = 'val_' + loss

			# plot history
			pyplot.clf()
			pyplot.figure(figsize=(8,8))
			pyplot.plot(contents[loss], 'b-', label=loss)
			pyplot.plot(contents[validation], 'g-', label=validation)
			pyplot.title('{} and {}'.format(loss, validation))
			pyplot.xlabel('epoch')
			pyplot.ylabel(loss)
			pyplot.legend(loc='upper right')

			# if scale given
			if scales.get(loss, None):

				# set ylim
				pyplot.ylim(*scales[loss])

			# save figures
			pyplot.savefig('{}/{}.png'.format(folder, loss))
			pyplot.clf()

		# load learning rate
		learn = self._load('{}/rates.json'.format(self.folder))
		rates = numpy.array(learn['rates'])
		samples = learn['samples']

		# translate timepoints to epoches
		epochs = numpy.array(range(len(rates))) / (math.ceil(samples / self.training['batch']))

		# retain only changing rates for plot
		# first = rates[:-1]
		# second = rates[1:]
		# mask = (first != second)
		# maskii = numpy.append(mask, True)
		# rates = rates[maskii]
		# epochs = epochs[maskii]

		# also plot learning rate
		pyplot.clf()
		pyplot.figure(figsize=(8,8))
		pyplot.plot(epochs, rates)
		pyplot.title('learning rates')
		pyplot.xlabel('epoch')
		pyplot.ylabel('rate')
		pyplot.savefig('{}/rates.png'.format(folder))
		pyplot.clf()

		return None

	def cloud(self, sites=['CA-Cbo'], scan=10, radius=5):
		"""Make maps of short wave radiation compared to cloud fraction.

		Arguments:
			sites: list of str, the site names
			scan: int, the scan number
			radius: float, the radius around site to plot

		Returns:
			None
		"""

		# make folder
		folder = '{}/cloud'.format(self.folder)
		self._make(folder)

		# get data
		matrix, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1))

		# extend radius to ensure coverage
		radiusii = radius * 1.5

		# # get merra clouds
		# hydra = Hydra('{}/Merra/{}'.format(self.sink, self.stub))
		# hydra.ingest(0)
		# parallels = hydra.grab('lat')
		# meridians = hydra.grab('lon')
		# fractions = hydra.grab('CLDTOT')

		# get wavelengths
		hydra = Hydra('{}/TEMPO_RAD_Smooth/{}'.format(self.sink, self.stub))
		hydra.ingest()
		wavelengths = hydra.grab('reflectance_wavelengths')

		# get PCA coefficients
		hydraii = Hydra('{}/TEMPO_PCs'.format(self.sink))
		hydraii.ingest()
		coefficients = hydraii.grab('PCA_Coeff')

		# reconstruct spectra from pcas
		reflectance = data['reflectance_pcas'][:, :self.limits['pca']]
		transpose = coefficients.transpose(1, 0)[:self.limits['pca']]
		spectra = numpy.matmul(reflectance, transpose)
		position = numpy.where(wavelengths == 480)[0][0]
		blues = spectra[:, position]

		# for each site
		for site in sites:

			# get the latitude and longitude
			latitude = self.catalog[site]['latitude']
			longitude = self.catalog[site]['longitude']

			# construct mask for limits
			mask = (data['latitude'] > latitude - radiusii) & (data['latitude'] < latitude + radiusii)
			mask = mask & (data['longitude'] > longitude - radiusii) & (data['longitude'] < longitude + radiusii)
			mask = mask.squeeze()

			# retrieve block of longitudes and latitudes
			latitudes = data['latitude'][mask]
			corners = data['latitude_bounds'][mask]
			longitudes = data['longitude'][mask]
			cornersii = data['longitude_bounds'][mask]

			# get clouds and shortwave
			wave = data['short_wave'][mask]
			cloud = data['cloud_fraction'][mask]
			fraction = data['cloud_fraction_merra'][mask]
			blue = blues[mask]

			# for each variable
			names = ('wave', 'cloud', 'fraction', 'blue')
			arrays = (wave, cloud, fraction, blue)
			labels = ('merra short wave', 'tempo cloud fraction', 'merra cloud fraction', 'tempo 480 nm')
			limits = ([300, 1000], [0, 1], [0, 1], [0, 0.3])
			for name, label, array, limit in zip(names, labels, arrays, limits):

				# begin plot
				pyplot.clf()
				pyplot.figure(figsize=(8, 8))

				# set gradient
				gradient = 'plasma'

				# set labels
				formats = (site, label, self.date, scan)
				pyplot.title('{} {}, {}, scan {}'.format(*formats))
				pyplot.xlabel('longitude')
				pyplot.ylabel('latitude')

				# set limits
				pyplot.xlim(longitude - radius, longitude + radius)
				pyplot.ylim(latitude - radius, latitude + radius)

				# plot scatter points to activate colorbar
				options = {'cmap': gradient, 's': 1, 'alpha': 1, 'marker': '.'}
				abscissa = longitudes
				ordinate = latitudes
				colors = array
				pyplot.scatter(abscissa, ordinate, c=colors, **options)

				# begin patches
				patches = []

				# for each polygon
				for vertices, verticesii in zip(cornersii.reshape(-1, 4), corners.reshape(-1, 4)):

					# set up patch
					polygon = numpy.array([[first, second] for first, second in zip(vertices, verticesii)])
					patch = matplotlib.patches.Polygon(xy=polygon, linewidth=0, edgecolor=None, alpha=0.9)
					patches.append(patch)

				# plot polygons with colormap
				collection = matplotlib.collections.PatchCollection(patches, cmap=gradient, alpha=0.9)
				collection.set_edgecolor("face")
				collection.set_clim(*limit)
				collection.set_array(array.flatten())
				polygons = pyplot.gca().add_collection(collection)

				# add colorbar
				pyplot.colorbar(label='{}'.format(label))

				# save figure
				formats = (folder, site, name, self._pad(scan), self.date, self.session, self.tag, int(radius))
				destination = '{}/cloud_{}_{}_{}_{}_{}_{}_{}.png'.format(*formats)
				pyplot.savefig(destination)
				pyplot.clf()

				# print status
				self._print('plotted {}'.format(destination))

		return None

	def collect(self, scans=(8, 15), granules=(1, 10), hours=(9, 24), base=False):
		"""Collect data from earth data and aws.

		Arguments:
			scans: tuple of ints, the scan position bracket
			granules: tuple of ints, the granule position bracket
			hours: tuple of ints, the hour position bracket
			base: boolea, collect abi baseliner data?

		Returns:
			None
		"""

		# construct scan labels
		labels = [self._label(scan, granule) for scan in range(*scans) for granule in range(*granules)]

		# if collection abi baseliner
		if base:

			# get day's worth of abi baseliner data
			self._base(hours)

		# get nearest irr files within 15 days
		self._access('TEMPO_IRR_L1', window=15, count=10)

		# get tempo rad data and cloud date
		self._access('TEMPO_CLDO4_L2', labels)
		self._access('TEMPO_RAD_L1', labels)

		return None

	def confirm(self, sites=None, scans=(1, 19), constrain=True, radius=0.1):
		"""Compare the time series predicted by the model to onefluxnet site data.

		Arguments:
			sites: list of strings, the site ids
			scans: tuple of int, the bracket of scans
			constrain: boolean, constrain to valid pixels?
			radius: maximum radius in degrees for plotting a point

		Returns:
			None
		"""

		# make folder
		folder = '{}/confirm'.format(self.folder)
		self._make(folder)

		# make data file folders
		folderii = '{}/GPP_Confirmation'.format(self.sink)
		self._make(folderii)
		self._make('{}/{}'.format(folderii, self.year))
		self._make('{}/{}/{}'.format(folderii, self.year, self._pad(self.month)))
		self._make('{}/{}'.format(folderii, self.stub))

		# set matrices and data reservoirs
		matrices = []
		data = []

		# for each scan
		for scan in range(*scans):

			# get the matrix and data
			parameters = ([(self.month, self.day)], (scan, scan + 1))
			matrix, datum, constraints = self.materialize(*parameters, constrain=constrain)

			# if constraining
			if constrain:

				# apply constraints
				datum = self._constrain(datum, constraints)

			# append
			matrices.append(matrix)
			data.append(datum)

		# set default sites to US and CA
		sites = sites or list(self.catalog.keys())
		sites = [site for site in sites if any([country in site for country in ('CA', 'US')])]

		# create hydra
		hydra = Hydra('{}/Onefluxnet_Confirmation/{}'.format(self.sink, self.stub))

		# determine high or low resolution mode
		mode = self.features[-1].split('_')[-1]

		# for each site
		for site in sites:

			# try to
			try:

				# ingest site data
				hydra.ingest(site)
				dataii = hydra.extract()

				# unpack data
				year = dataii['fluxnet_year'][0]
				month = dataii['fluxnet_month'][0]
				hours = dataii['fluxnet_hours']
				predictions = dataii['model_prediction']
				deviations = dataii['model_deviation']
				truths = dataii['fluxnet_truth']
				deviationsii = dataii['fluxnet_deviation']
				local = dataii['synthetic_local_time']
				synthetic = dataii['synthetic_prediction_{}'.format(mode)]
				bands = dataii['synthetic_matrix_{}'.format(mode)][:, :7].mean(axis=0)

				# get longitude and latitude
				latitude = self.catalog[site]['latitude']
				longitude = self.catalog[site]['longitude']

				# begin tempo data collection
				localii = []
				timesii = []
				truthsii = []
				predictionsii = []
				matricesii = []
				scansii = []
				cosines = []
				clouds = []
				zeniths = []
				views = []
				azimuths = []
				angles = []

				# for each scan
				for matrix, datum, scan in zip(matrices, data, list(range(*scans))):

					# find the point closest to latitude and longitude
					index = self._pin([latitude, longitude], [datum['latitude'].squeeze(), datum['longitude'].squeeze()])
					index = index[0][0]

					# calculate squared distance
					distance = (latitude - datum['latitude'].squeeze()[index]) ** 2
					distance += (longitude - datum['longitude'].squeeze()[index]) ** 2

					# det local time
					time = datum['local_time'][index]

					# if within radius
					if distance <= radius ** 2:

						# append local time
						timesii.append(time[0])
						localii.append(time[0])

						# append matrix and scan
						matricesii.append(matrix[index: index + 1])
						scansii.append(scan)

						# make model predictions
						prediction, _, truth, _ = self.predict(matrix[index: index + 1])
						predictionsii.append(prediction[0])
						truthsii.append(truth[0])

						# append cloud fraction and angles
						cloud = datum['cloud_fraction'].squeeze()[index]
						zenith = datum['solar_zenith_angle'].squeeze()[index]
						view = datum['viewing_zenith_angle'].squeeze()[index]
						azimuth = datum['relative_azimuth_angle'].squeeze()[index]
						angle = 180 - (zenith + view - azimuth)

						# correct angle to range 0 to 180
						angle = numpy.where(angle < 0, angle + 360, angle)
						angle = numpy.where(angle > 180, 360 - angle, angle)

						# add to arrays
						clouds.append(cloud)
						zeniths.append(zenith)
						views.append(view)
						azimuths.append(azimuth)
						angles.append(angle)

						# # get bands information and compute cosinte similarity
						# bandsii = datum['mcd43_bands'][index]
						# cosine = cosine_similarity(bands.reshape(1, -1), bandsii.reshape(1, -1))
						# cosines.append(cosine[0][0])

					# otherwise
					else:

						# append 0 for time
						localii.append(0)

				# create arrays
				timesii = numpy.array(timesii)
				localii = numpy.array(localii)
				predictionsii = numpy.array(predictionsii)
				truthsii = numpy.array(truthsii)
				scansii = numpy.array(scansii)
				matricesii = numpy.vstack(matricesii)
				# cosines = numpy.array(cosines)

				# add 24 to negative times
				timesii = numpy.where(timesii < 0, timesii + 24, timesii)
				localii = numpy.where(localii < 0, localii + 24, localii)

				# order times and predictions
				order = numpy.argsort(timesii)
				timesii = timesii[order]
				predictionsii = predictionsii[order]
				truthsii = truthsii[order]
				scansii = scansii[order]
				matricesii = matricesii[order]
				# cosines = cosines[order]

				# order cloud fractionss and angles
				clouds = numpy.array(clouds)[order]
				zeniths = numpy.array(zeniths)[order]
				views = numpy.array(views)[order]
				azimuths = numpy.array(azimuths)[order]
				angles = numpy.array(angles)[order]

				# # construct sizes
				# sizes = (cosines - 0.99) * 100
				# sizes = numpy.where(sizes < 0, 0, sizes)

				# for each scan
				for index, scan in enumerate(range(*scans)):

					# begin plot
					pyplot.clf()
					formats = (site, self._orient(latitude), self._orient(longitude, east=True), self.date)
					pyplot.title('{} ( {}, {} ) diurnal GPP, {}'.format(*formats))
					pyplot.xlabel('local hour')
					pyplot.ylabel('{} ( {} )'.format(self.target, self.unit))

					# set limits
					pyplot.xlim(3, 21)
					minimums = [(truths - deviationsii).min(), (predictions - deviations).min(), synthetic.min()]
					minimums += [predictionsii.min(), truthsii.min()]
					maximums = [(truths + deviationsii).max(), (predictions + deviations).max(), synthetic.max()]
					maximums += [predictionsii.max(), truthsii.max()]
					pyplot.ylim(min(minimums) - 1, max(maximums) + 6)

					# plot the truth with markers
					label = 'onefluxnet ( {}m{} )'.format(year, self._pad(month))
					options = {'fmt': 'k^', 'linewidth': 2, 'capsize': 2, 'ms': 5, 'label': label}
					pyplot.errorbar(hours, truths, deviationsii, **options)

					# plot the prediction with markers
					label = 'prediction  ( {}m{} )'.format(year, self._pad(month))
					options = {'fmt': 'rs', 'linewidth': 2, 'capsize': 2, 'ms': 5, 'label': label}
					pyplot.errorbar(hours, predictions, deviations, **options)

					# plot the synthetic trend
					label = 'site synthetic  ( {} )'.format(self.date)
					pyplot.plot(local, synthetic, 'gx-', linewidth=4, label=label)

					# plot the synthetic truth
					label = 'tempo synthetic ( {} )'.format(self.date)
					pyplot.plot(timesii, truthsii, 'bx-', linewidth=4, label=label)

					# # add markers based on cosine similarity score
					# pyplot.scatter(timesii, truthsii, s=sizes * 150, c='b', alpha=0.5, marker='o')

					# plot the synthetic prediction
					label = 'tempo prediction ( {} )'.format(self.date)
					pyplot.plot(timesii, predictionsii, 'cx-', linewidth=4, label=label)

					# # add markers based on cosine similarity score
					# pyplot.scatter(timesii, predictionsii, s=sizes * 150, c='c', alpha=0.5, marker='o')

					# add scan marker
					pyplot.scatter(localii[index], min(minimums) - 1, s=150, c='c', alpha=0.5, marker='o')

					# add legend
					pyplot.legend(loc='upper left')

					# save figure
					formats = (folder, site, self.date, self.session, self.tag, self._pad(scan))
					destination = '{}/Confirmation_{}_{}_{}_{}_{}.png'.format(*formats)
					pyplot.savefig(destination)
					self._print('plotted {}.'.format(destination))
					pyplot.clf()

				# add gpp results to folder
				dataii.update({'scans': scansii})
				dataii.update({'tempo_predictions': predictionsii})
				dataii.update({'tempo_truths': truthsii})
				dataii.update({'tempo_times': timesii})
				dataii.update({'tempo_matrix': matricesii})

				# add clouds and angles
				dataii.update({'tempo_clouds': clouds})
				dataii.update({'tempo_zeniths': zeniths})
				dataii.update({'tempo_views': views})
				dataii.update({'tempo_azimuths': azimuths})
				dataii.update({'tempo_angles': angles})

				# store results in file
				destination = '{}/{}/Confirmation_{}_{}.h5'.format(folderii, self.stub, site, self.date)
				self.spawn(destination, dataii)

			# unless no file
			except FileNotFoundError:

				# pass
				pass

		return None

	def correlate(self, scan=10):
		"""Compute the correlation amongst all feature pairs.

		Arguments:
			scan: int, scan number

		Returns:
			None
		"""

		# make reports folder
		folder = '{}/reports'.format(self.folder)
		self._make(folder)

		# get training matrix for particular scan
		matrix, _, _ = self.materialize([(self.month, self.day)], (scan, scan + 1))

		# get scaling information
		minimum = self.details['input_min']
		maximum = self.details['input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		features = self.features[:-1]
		inputs = (matrix[:, :-1] - minimum) / scale

		# begin report and scores collection
		report = ['correlation report for {}\n'.format(folder)]
		scores = []

		# compute correlation
		correlation = numpy.corrcoef(inputs, rowvar=False)

		# for each row
		for row in range(correlation.shape[0]):

			# and each column
			for column in range(row + 1, correlation.shape[1]):

				# add to scores
				triplet = (correlation[row, column], features[row], features[column])
				scores.append(triplet)

		# sort scores
		scores.sort(key=lambda triplet: triplet[0], reverse=True)

		# compute feature string lengths
		lengths = [len(feature) for feature in features]
		lengths.sort(reverse=True)
		longest = lengths[0]

		# add to report
		for score, feature, featureii in scores:

			# compute spacing
			space = ' ' * (longest - len(feature))

			# add entry
			report.append('{:.3f}: {}{}x   {}'.format(score, feature, space, featureii))

		# jot report
		self._jot(report, '{}/correlation.txt'.format(folder))

		return None

	def cosine(self, scan=10, fraction=0.5):
		"""compute the cosine similarity score amonst all matrices.

		Arguments:
			scan: int, scan number
			fraction: float, fraction of data to use

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/cosine'.format(self.folder)
		self._make(folder)

		# collect training days
		days = self._daze(validation=False)
		days.sort()

		# begin matrices and scores
		matrices = []

		# for each day
		for month, day in days:

			# get training matrix for particular scan
			parameters = [(month, day)], (scan, scan + 1)
			matrix, data, constraints = self.materialize(*parameters)

			# get scaling information
			minimum = numpy.hstack([self.details['input_min'], numpy.array([self.details['output_min']])])
			maximum = numpy.hstack([self.details['input_max'], numpy.array([self.details['output_max']])])
			scale = maximum - minimum

			# scale matrix
			matrix = (matrix - minimum) / scale

			# add to matrices
			matrices.append(matrix)

		# determine sample size from smallest matrix
		lengths = [len(matrix) for matrix in matrices]
		rows = int(min(lengths) * fraction)

		# begin samples
		samples = []

		# for each matrix
		for matrix in matrices:

			# Randomly select 50 unique row indices from 0 to 99 without replacement
			indices = numpy.random.choice(matrix.shape[0], rows, replace=False)

			# Get the random sampling of 50 unique rows
			sample = matrix[indices]
			samples.append(sample)

		# begin cosine similarity scores
		scores = numpy.ones((len(days), len(days)))

		# for each matrix
		for index in range(len(days)):

			# and each second matrix
			for indexii in range(len(days)):

				# compute cosine similarity
				score = cosine_similarity(samples[index].reshape(1, -1), samples[indexii].reshape(1, -1))

				# add to matrix
				scores[index][indexii] = score

		# clip to highest non diagonal score
		xerox = scores.copy()
		numpy.fill_diagonal(xerox, 0)
		highest = xerox.max()
		scores = numpy.where(scores >= highest, highest, scores)

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# plot the matrix
		pyplot.imshow(scores, cmap='viridis')

		# add color bar
		pyplot.colorbar(label='Cosine Similarity')

		# add labels
		labels = [str(day) for day in days]
		pyplot.xticks(ticks=numpy.arange(len(labels)), labels=labels)
		pyplot.yticks(ticks=numpy.arange(len(labels)), labels=labels)

		# add title
		pyplot.title('Cosine similarity')

		# save the plot
		pyplot.savefig('{}/cosine_similarity.png'.format(folder))
		pyplot.clf()

		return None

	def cross(self, scan=99, latitude=31, longitude=-105, secondary=None, position=0, bounds=None):
		"""Make a longitudinal cross section plot of model results.

		Arguments:
			scan: int, scan number
			latitude: float, the closest latitude
			longitude: float, the closest longitude
			bounds: longitude bounds

		Returns:
			None
		"""

		# set default bounds
		bounds = bounds or (-120, -80)

		# make folder
		folder = '{}/cross'.format(self.folder)
		self._make(folder)

		# get training matrix for particular scan
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, data, constraints = self.materialize(*parameters)

		# apply constraints to data
		data = self._constrain(data, constraints)

		# get scanline closest to latitude
		index = self._pin([latitude, longitude], [data['latitude'].squeeze(), data['longitude'].squeeze()])[0][0]
		track = int(data['track'][index][0])

		# get all data at scanline
		line = (data['track'].squeeze() == track)
		matrix = matrix[line]

		# organize by longitude
		longitude = data['longitude'].squeeze()[line]
		order = numpy.argsort(longitude)
		longitude = longitude[order]
		matrix = matrix[order]

		# make predictions
		prediction, deviation, truth, _ = self.predict(matrix)

		# if secondary variable
		if secondary:
			# get the data
			tracer = data[secondary][:, position][line][order]

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# add text
		formats = (self.features[-1], self._orient(latitude), track, self.date)
		pyplot.title('Model vs {} for latitude {}, track {}, {}'.format(*formats))
		pyplot.xlabel('longitude')
		pyplot.ylabel('{} ({})'.format(self.target, self.unit))

		# set bounds
		pyplot.xlim(*bounds)
		pyplot.ylim(*self.limits['scale'])

		# plot truth
		pyplot.plot(longitude, truth, 'b-', linewidth=1, label='truth')

		# plot prediction
		pyplot.plot(longitude, prediction, 'g-', linewidth=1, label='prediction')

		# add legend
		pyplot.legend(loc='upper left')

		# if secondary axis
		label = '_'
		if secondary:
			# create secondary axis
			axisii = pyplot.gca().twinx()

			# plot row anomaly on secondary
			label = '{}_{}'.format(secondary, self._pad(position))
			axisii.plot(longitude, tracer, 'r-', label=label)
			axisii.set_ylabel(label)
			axisii.set_xlim(*bounds)

			# create legend
			axisii.legend(loc='upper right')

		# plot deviations
		# 		pyplot.plot(longitude, mean + deviation, 'g--')
		# 		pyplot.plot(longitude, mean - deviation, 'g--')

		# save
		formats = (folder, *formats, label)
		pyplot.savefig('{}/cross/CrossSection_{}_{}_{}_{}_{}.png'.format(*formats))
		pyplot.clf()

		return None

	def diagnose(self, truth, prediction, data):
		"""Create plots of percent error vs parameter.

		Arguments:
			truth: numpy array, gpp truth
			prediction: numpy array, model prediction
			data: dict, associated data

		Returns:
			None
		"""

		# make folder
		folder = '{}/diagnosis'.format(self.folder)
		self._make(folder)

		# calculate gpp difference
		difference = prediction - truth

		# set fields
		fields = ['latitude', 'longitude', 'snow_fraction', 'cloud_fraction', 'gpp_fluxsat']
		fields += ['solar_zenith_angle', 'viewing_zenith_angle', 'relative_azimuth_angle']
		fields += ['land_type', 'mirror', 'track', 'clearsky_par']
		fields += ['max_irr_quality_high', 'max_irr_quality_low']
		fields += ['max_rad_quality_high', 'max_rad_quality_low']

		# for each variable
		for field in fields:

			# crete plot destination
			destination = '{}/Diagnosis_{}.png'.format(folder, field)

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8,8))

			# construct date
			date = str(data['date'][0][0])
			date = '{}m{}{}'.format(date[:4], date[4:6], date[6:8])
			time = str(data['time'][0][0])
			time = '{}:{} CST'.format(int(time[:2]) - 6, time[2:4])
			scan = 'S{}'.format(self._pad(data['scan'][0][0], 3))

			# create title
			title = '{} {} {} {}\n Model - Onefluxnet vs {}'.format(self.target, date, time, scan, field)
			pyplot.title(title)

			# add labels
			pyplot.xlabel(field)
			pyplot.ylabel('{} difference'.format(self.target))

			# plot data
			pyplot.plot(data[field], difference, 'g.', markersize=2)

			# save plot
			pyplot.savefig(destination)
			pyplot.clf()

		return None

	def distribute(self, bins=50):
		"""Generate comparative histograms of truth and prediction distributions:

		Arguments:
			bins: int, number of bins

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/histograms'.format(self.folder)
		self._make(folder)

		# get training matrix and make predictions
		matrix, _, _ = self.materialize(self.training['sites'])
		prediction, _, truth, _ = self.predict(matrix)

		# Create a histogram
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))
		options = {'bins': bins, 'edgecolor': 'black', 'label': ['truth', 'prediction'], 'color': ['blue', 'green']}
		pyplot.hist([truth, prediction], **options)

		# add legend
		pyplot.legend(loc='upper right')

		# Add titles and labels
		title = 'histogram of truth vs prediction'
		pyplot.title(title)
		pyplot.xlabel('value')
		pyplot.ylabel('counts')

		# save the plot
		destination = '{}/histogram_truth_prediction.png'.format(folder)
		pyplot.savefig(destination)
		pyplot.clf()

		# print status
		self._print('plotted {}'.format(destination))

		return None

	def err(self, scan=10):
		"""Run a random forest against the model error.

		Arguments:
			scan: int, scan number

		Returns:
			None
		"""

		# make folders
		folder = '{}/reports'.format(self.folder)
		self._make(folder)

		# get training matrix for particular scan
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, data, constraints = self.materialize(*parameters)

		# construct valid data
		valids = self._constrain(data, constraints)

		# make predictions
		prediction, deviation, truth, _ = self.predict(matrix)

		# add difference to valids
		valids.update({'error': (prediction - valids['gpp_fluxsat'].squeeze()).reshape(-1, 1)})

		# remove all 1-d arrays
		valids = {field: array for field, array in valids.items() if array.shape[0] == prediction.shape[0]}

		# view data
		self._view(valids)

		# get secondary axis sizes
		sizes = {field: array.shape[1] for field, array in valids.items()}

		# begin training matrix
		train = {}

		# for each field
		for field, array in valids.items():

			# add to training
			train.update({'{}|{}'.format(field, self._pad(index)): array[:, index] for index in range(sizes[field])})

		# create report destination and plant tree
		self.plant(train, 'error|00', '{}/gpp_error_random_forest.txt'.format(folder))

		return None

	def examine(self, points=None, margin=1, scans=(1, 19), apply=True, tag='', fraction=0.01):
		"""Inspect the model across a specific variable at a specific point, and compare with regression.

		Arguments:
			points: numpy array, list of specific points for all variables
			margin: float, margin in stdevs
			scans: tuple of ints, the scan bracket
			apply: apply specific trained indices?
			tag: str, tag for title

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/examine'.format(self.folder)
		self._make(folder)

		# set up matrices and fractions
		matrices = []
		fractions = []

		# # make scan blocks in sets of three
		# indices = list(range(*scans))
		# size = 3
		# chunks = math.ceil(len(indices) / size)
		# pairs = [(index * size, size + index * size) for index in range(chunks)]
		# blocks = [indices[one:two] for one, two in pairs]

		# for each day
		for month, day in self.training['days']:

			# for each block of scans
			for scan in range(*scans):

				# get training matrix
				matrix, _, _ = self.materialize([(month, day)], (scan, scan + 1))

				# if there was a matrix
				if matrix is not None:

					# create mask for fraction
					mask = numpy.random.rand(matrix.shape[0]) < fraction

					# append
					matrices.append(matrix)
					fractions.append(mask)

		# create arrays
		matrix = numpy.vstack(matrices)
		fractions = numpy.hstack(fractions)

		# if limiting to selected indices
		if apply:

			# retrieve
			selection = self.details['samples/selected_indices']

			# create boolean for indices
			indices = numpy.zeros(fractions.shape).astype(bool)
			indices[selection] = True
			selectionii = indices * fractions
			matrix = matrix[selectionii]

		# normalize matrix for regressions
		inputs = matrix[:, :-1]
		outputs = matrix[:, -1]
		average = inputs.mean(axis=0)
		spread = inputs.std(axis=0)
		inputs = (inputs - average) / spread

		# create linear regressor and fit
		regressor = LinearRegression()
		regressor.fit(inputs, outputs.reshape(-1, 1))

		# create polynomial fit
		polymerizer = PolynomialFeatures(degree=3, include_bias=True)
		polynomial = polymerizer.fit_transform(inputs)
		regressorii = LinearRegression()
		regressorii.fit(polynomial, outputs.reshape(-1, 1))

		# if no points given
		if points is None:

			# get points from clustering if not given
			points = points or self._cluster(matrix, 5, [])

		# # set variable names
		names = [feature.replace('|', ' ') for feature in self.features]

		# for each variable
		for variable, name in zip(self.features[:-1], names):

			# set units
			units = '-'

			# get column position for variable of interest
			column = self.features.index(variable)

			# create percentiles
			percentiles = numpy.array([numpy.percentile(matrix[:, column], percentile) for percentile in range(101)])

			# for each point
			for index, point in enumerate(points):

				# create probe matrix from medians for all features but variable in question
				point = point.astype('float32')
				probe = numpy.array([point.copy()] * 101)
				probe[:, column] = percentiles

				# predict values from the model
				prediction, deviation, _, _ = self.predict(probe)
				predictionii, _, _, _, = self.predict(point.reshape(1, -1))

				# predict at point
				ends = numpy.array([point.copy()] * 2)
				ends[0][column] = percentiles[0]
				ends[1][column] = percentiles[-1]
				regression = regressor.predict((ends[:, :-1] - average) / spread)

				# predict polynomial regression at point
				series = numpy.array([point.copy()] * len(percentiles))
				series[:, column] = percentiles
				polynomial = polymerizer.transform((series[:, :-1] - average) / spread)
				regressionii = regressorii.predict(polynomial)

				# begin plot
				pyplot.clf()
				pyplot.figure(figsize=(8, 8))

				# create title
				tagii = (tag or 'point') + ' {}'.format(self._pad(index))

				vector = '[ ' + ' '.join(['{:.3f}'.format(quantity) for quantity in point[:-1]]) + ' ]'
				formats = (name, tagii, vector)
				title = 'Onefluxnet model, {}, {}\n{}'.format(*formats)
				pyplot.title(title)

				# add labels
				pyplot.xlabel('{} ( {} )'.format(name, units))
				pyplot.ylabel('{} ( {} )'.format(self.target, self.unit))

				# set limits
				pyplot.ylim(self.limits['scale'])

				# Sample data
				abscissa = percentiles
				ordinate = prediction

				# plot regression line
				pyplot.plot(ends[:, column], regression, 'g--', label='linear fit', linewidth=4)

				# plot polynomial line
				pyplot.plot(abscissa, regressionii, 'b--', label='polynomial fit', linewidth=4)

				# create line plots
				pyplot.plot(abscissa, ordinate, 'co-', label='model')
				pyplot.plot(abscissa, ordinate - deviation, 'c--')
				pyplot.plot(abscissa, ordinate + deviation, 'c--')

				# plot prediction and truth
				# pyplot.plot([row[index]], [truth], 'gX', markersize=15, label='truth')
				pyplot.plot([point[column]], [predictionii], 'rX', markersize=15, label='prediction')

				# copy matrix
				xerox = matrix.copy()
				mean = matrix.mean(axis=0)
				deviation = matrix.std(axis=0)

				# # get percents
				# lower = numpy.percentile(xerox, 50 - margin, axis=0)
				# upper = numpy.percentile(xerox, 50 + margin, axis=0)

				# print final sahape
				self._print('{} samples: {}'.format(variable, xerox.shape))

				# calculate differences from median for color scale
				difference = (((xerox - mean) / deviation) - ((point - mean) / deviation)) ** 2
				difference[:, column] = 0
				difference = difference.sum(axis=1)

				# add training points
				# label = 'samples +/- {} std'.format(str(margin))
				label = 'training'
				pyplot.scatter(xerox[:, column], xerox[:, -1], c=difference, cmap='gray', s=10, marker='.', label=label)

				# add legend
				# middle = (percentiles[0] + percentiles[100]) / 2
				# location = self._anchor((point[column], predictionii), (middle, prediction))
				pyplot.legend(loc='upper right')

				# save plot
				tagii = tagii.replace(' ', '_').replace(',', '').replace(':', '_')
				formats = (folder, variable, self._pad(index), tagii, self.session, self.tag)
				destination = '{}/examine_{}_{}_{}_{}_{}.png'.format(*formats)
				pyplot.savefig(destination)
				pyplot.clf()

		return None

	def exhibit(self, scan=99, dependent=None, independent=None):
		"""Visualize all datasets using PCA and different colors for each.

		Arguments:
			scan: int, scan number
			variables: list of str, dependent and independent variables for scatters

		Returns:
			None
		"""

		# set default variables
		dependent = dependent or 'tropomi_vertical_column_no2'
		independent = independent or 'reflectance_pcas|05'

		# make shapely folder
		folder = '{}/exhibit'.format(self.folder)
		self._make(folder)

		# collect training days
		days = self._daze(validation=False)

		# begin matrices and scores
		matrices = []
		scores = []

		# set colors
		colors = ['magenta', 'red', 'orange', 'lightgreen']
		colors += ['green', 'lightblue', 'blue', 'violet']
		colors += ['indigo', 'black']

		# for each day
		for month, day in days:

			# get training matrix for particular scan
			parameters = [(month, day)], (scan, scan + 1)
			matrix, features, data, constraints = self.materialize(*parameters)

			# add to matrices
			matrices.append(matrix)

			# get score
			_, _, _, score = self.predict(matrix)
			scores.append(score)

		# get scaling information
		minimum = self.details['scaling/input_min']
		maximum = self.details['scaling/input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		inputs = [(matrix[:, :-1] - minimum) / scale for matrix in matrices]

		# create block of all inputs
		block = numpy.vstack(inputs)

		# fit PCA on inputs and get coefficients
		decomposer = PCA(n_components=2)
		decomposer.fit(block)
		coefficients = decomposer.components_.transpose(1, 0)

		# get representation
		representations = [numpy.dot(matrix, coefficients) for matrix in inputs]

		# make labels
		labels = self._represent(coefficients, features)

		# get minimum and maximums
		margin = 0.2
		minimum = min([representation[:, 0].min() for representation in representations]) - margin
		minimumii = min([representation[:, 1].min() for representation in representations]) - margin
		maximum = max([representation[:, 0].max() for representation in representations]) + margin
		maximumii = max([representation[:, 1].max() for representation in representations]) + margin

		# store components
		data = {'2d_pca': coefficients}
		data.update({'minimum': minimum, 'maximum': maximum, 'minimumii': minimumii, 'maximumii': maximumii})
		self._make('{}/pca'.format(self.folder))
		self.spawn('{}/pca/2d_pca_components.h5'.format(self.folder), data)

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# for each dataset
		for index, representation in enumerate(representations):

			# Sample data
			abscissa = representation[:, 0]
			ordinate = representation[:, 1]

			# create scatter plot
			pyplot.scatter(abscissa, ordinate, c=colors[index], cmap='viridis', s=0.1, label=days[index])

		# make labels
		pyplot.xlabel('PCA 0 ( {} )'.format(labels[0]))
		pyplot.ylabel('PCA 1 ( {} )'.format(labels[1]))
		pyplot.title('PCA components vs day, scan {}'.format(scan))
		pyplot.legend(markerscale=20)

		# save
		pyplot.savefig('{}/pca_exhibit.png'.format(folder))
		pyplot.clf()

		# begin median plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# for each dataset
		for index, representation in enumerate(representations):

			# Sample data
			abscissa = numpy.percentile(representation[:, 0], 50).reshape(-1)
			ordinate = numpy.percentile(representation[:, 1], 50).reshape(-1)

			# create scatter plot
			pyplot.scatter(abscissa, ordinate, c=colors[index], cmap='viridis', s=20, label=days[index])

			# annotate
			text = '{} ( {:.2f} )'.format(days[index], scores[index])
			pyplot.annotate(xy=(abscissa,  ordinate), text=text)

			print(text, abscissa, ordinate)

		# make labels
		pyplot.xlabel('PCA 0 ( {} )'.format(labels[0]))
		pyplot.ylabel('PCA 1 ( {} )'.format(labels[1]))
		pyplot.title('Median PCA components vs day, scan {}'.format(scan))
		# pyplot.legend(markerscale=1)

		# save
		pyplot.savefig('{}/pca_exhibit_median.png'.format(folder))
		pyplot.clf()

		# begin scatter plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# for each dataset
		for index, matrix in enumerate(matrices):

			# Sample data
			abscissa = matrix[:, features.index(independent)]
			ordinate = matrix[:, features.index(dependent)]

			# create scatter plot
			pyplot.scatter(abscissa, ordinate, c=colors[index], cmap='viridis', s=1, label=days[index])

			# calculate regression line
			slope, intercept, _, _, _ = scipy.stats.linregress(abscissa, ordinate)

			# plot line
			horizontal = [abscissa.min(), abscissa.max()]
			vertical = [intercept + slope * point for point in horizontal]
			pyplot.plot(horizontal, vertical, linestyle='dashed', color=colors[index])

		# make labels
		pyplot.xlabel(independent)
		pyplot.ylabel(dependent)
		pyplot.title('{} vs {}, scan {}'.format(dependent, independent, scan))
		pyplot.legend(markerscale=10)

		# save
		pyplot.savefig('{}/pca_exhibit_scatters_{}.png'.format(folder, independent))
		pyplot.clf()

		return None

	def explain(self, scan=10, instances=1, samples=500):
		"""Perform shapley study.

		Arguments:
			scan: int, scan number
			instances: int, number of instances for model predictions
			samples: int, number of samples to use

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/explanation'.format(self.folder)
		self._make(folder)

		# get training matrix for particular scan
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, _, _ = self.materialize(*parameters)

		# get scaling information
		minimum = self.details['scaling/input_min']
		maximum = self.details['scaling/input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		inputs = (matrix[:, :-1] - minimum) / scale

		# get pca coefficients
		coefficients = self.details['scaling/pca_coefficients']

		# pare down sample number
		numpy.random.seed(42)
		xerox = inputs.copy()
		indices = numpy.random.choice(xerox.shape[0], size=samples, replace=False)
		xerox = xerox[indices, :]

		# define modeler
		def modeling(matrix): return self.model(numpy.dot(matrix, coefficients).astype('float32'))

		# create explainer
		explainer = shap.Explainer(modeling, xerox)
		importance = explainer(xerox)
		importance.feature_names = self.features[:-1]

		# set plot size
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# for ten samples
		for sample in range(10):

			# visualize decision plot
			shap.plots.decision(importance.base_values[sample], importance.data, feature_names=importance.feature_names)
			pyplot.savefig('{}/explanation/decision_{}.png'.format(folder, sample), bbox_inches='tight')
			pyplot.clf()

			# visualize waterfall plot
			shap.plots.waterfall(importance[sample], max_display=20)
			pyplot.savefig('{}/explanation/waterfall_{}.png'.format(folder, sample), bbox_inches='tight')
			pyplot.clf()

# 		# for each Feature
# 		for name in features[:-1]:
#
		# make scatter plot
		shap.plots.scatter(importance[:, 0], color=importance)
		pyplot.savefig('{}/scatter.png'.format(folder), bbox_inches='tight')
		pyplot.clf()

		# visualize beeswarm plot
		shap.plots.beeswarm(importance, max_display=20)
		pyplot.savefig('{}/beeswarm.png'.format(folder), bbox_inches='tight')
		pyplot.clf()

		# visualize bar plot
		shap.plots.bar(importance, max_display=20)
		pyplot.savefig('{}/bar.png'.format(folder), bbox_inches='tight')
		pyplot.clf()

		return explainer, importance

	def follow(self, scans=(6, 14), radius=2):
		"""Follow the diurnal gpp changes at specific sites.

		Arguments
			scans: tuple of ints, the scan numbers
			radius: radius in degrees for closeness

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/follow'.format(self.folder)
		self._make(folder)

		# gather all study sites
		sites = self._know('Onefluxnet/sites/study_sites.txt')

		# get landforms dictionary
		forms = self._acquire('Land_Types/land.yaml')

		# begin datasets and matrices
		latitudes = []
		longitudes = []
		lands = []
		matrices = []

		# for each scan
		for scan in range(*scans):

			# get training matrix for particular scan
			parameters = ([(self.month, self.day)], (scan, scan + 1))
			matrix, datum, constraints = self.materialize(*parameters, constrain=False)

			# constrain based on clouds
			mask = (datum['cloud_fraction'].squeeze() <= self.limits['cloud'][1])
			matrix = matrix[mask]

			# apply constraints to data
			datum = self._constrain(datum, mask)

			# get mirror and track indices
			mirror = datum['mirror'].squeeze().astype(int)
			track = datum['track'].squeeze().astype(int)

			# create latitude data
			latitude = numpy.ones((self.mirror, self.track)) * self.fill
			latitude[mirror, track] = datum['latitude'].squeeze()
			latitudes.append(latitude)

			# create longitude data
			longitude = numpy.ones((self.mirror, self.track)) * self.fill
			longitude[mirror, track] = datum['longitude'].squeeze()
			longitudes.append(longitude)

			# create land type data
			land = numpy.ones((self.mirror, self.track)) * self.fill
			land[mirror, track] = datum['land_type'].squeeze()
			lands.append(land)

			# create matrix panel
			panel = numpy.ones((self.mirror, self.track, matrix.shape[1])) * self.fill
			panel[mirror, track] = matrix
			matrices.append(panel)

		# create arrays
		latitudes = numpy.array(latitudes)
		longitudes = numpy.array(longitudes)
		lands = numpy.array(lands)
		matrices = numpy.array(matrices)

		# get the average of latitudes and longitudes
		latitudes = latitudes.mean(axis=0)
		longitudes = longitudes.mean(axis=0)
		lands = lands.mean(axis=0).astype(int)

		# reduce to finites
		mask = numpy.isfinite(latitudes) & numpy.isfinite(longitudes)

		# apply to arrays
		latitudes = latitudes[mask]
		longitudes = longitudes[mask]
		lands = lands[mask]
		matrices = numpy.array([matrices[index][mask] for index in range(matrices.shape[0])])

		# for each site
		for site in sites:

			# retrieve the coordinates
			latitude = self.catalog[site]['latitude']
			longitude = self.catalog[site]['longitude']

			# calculate squared distances
			distances = (latitudes - latitude) ** 2 + (longitudes - longitude) ** 2

			# get all examples within radius
			mask = (distances < radius ** 2)

			# if nonzero
			if mask.sum() > 0:

				# get the closest
				closest = numpy.argmin(distances[mask])

				# get the matrix entry
				matrix = numpy.array([matrices[index][mask][closest] for index in range(matrices.shape[0])])

				# get the predictions
				prediction, deviation, truth, _ = self.predict(matrix)

				# create time abscssia
				abscissa = [scan + 2 for scan in range(*scans)]

				# begin plot
				pyplot.clf()
				pyplot.figure(figsize=(8, 8))

				# set title and labels
				formats = [self.date, self.target, site, self._orient(latitude), self._orient(longitude, east=True)]
				formats += [forms[lands[mask][closest]]]
				title = 'TEMPO {}, diurnal {}, site {}, {}, {} ( {} )'.format(*formats)
				pyplot.title(title)
				pyplot.xlabel('hour')
				pyplot.ylabel('{} ( {} )'.format(self.target, self.unit))

				# plot truth
				pyplot.plot(abscissa, truth, 'bo-', label='Onefluxnet')

				# plot prediction and deviations
				pyplot.plot(abscissa, prediction, 'go-', label='Prediction')
				pyplot.plot(abscissa, prediction - deviation, 'g--')
				pyplot.plot(abscissa, prediction + deviation, 'g--')

				# add legend
				pyplot.legend(loc='upper right')

				# save
				formats = (folder, self.date, self._orient(latitude), self._orient(longitude, east=True), site)
				destination = '{}/Hourly_GPP_{}_{}_{}_{}.png'.format(*formats)
				pyplot.savefig(destination)
				pyplot.clf()

				# print status
				self._print('plotted {}.'.format(destination))

				# for each scan
				predictions = []
				deviations = []
				truths = []
				deviationsii = []
				for index in range(matrices.shape[0]):

					# get the matrix entries for the 2 degree radius
					matrix = matrices[index][mask]

					# get the predictions
					prediction, deviation, truth, _ = self.predict(matrix)

					# average across all samples in the area
					deviation = prediction.std(axis=0)
					deviationii = truth.std(axis=0)
					prediction = prediction.mean(axis=0)
					truth = truth.mean(axis=0)

					# append to arrays
					predictions.append(prediction)
					truths.append(truth)
					deviations.append(deviation)
					deviationsii.append(deviationii)

				# crate arrays
				predictions = numpy.array(predictions)
				truths = numpy.array(truths)
				deviations = numpy.array(deviations)
				deviationsii = numpy.array(deviationsii)

				# create time abscssia
				abscissa = [scan + 2 for scan in range(*scans)]

				# begin plot
				pyplot.clf()
				pyplot.figure(figsize=(8, 8))

				# set title and labels
				formats = [self.date, self.target, site, self._orient(latitude), self._orient(longitude, east=True)]
				# formats += [forms[lands[mask][closest]]]
				title = 'TEMPO {}, diurnal {}, site {}, {}, {} ( 2 deg radius avg )'.format(*formats)
				pyplot.title(title)
				pyplot.xlabel('hour')
				pyplot.ylabel('{} ( {} )'.format(self.target, self.unit))

				# plot truth
				pyplot.plot(abscissa, truths, 'bo-', label='Onefluxnet')
				pyplot.plot(abscissa, truths - deviationsii, 'b--')
				pyplot.plot(abscissa, truths + deviationsii, 'b--')

				# plot prediction and deviations
				pyplot.plot(abscissa, predictions, 'go-', label='Prediction')
				pyplot.plot(abscissa, predictions - deviations, 'g--')
				pyplot.plot(abscissa, predictions + deviations, 'g--')

				# add legend
				pyplot.legend(loc='upper right')

				# save
				formats = (folder, self.date, self._orient(latitude), self._orient(longitude, east=True), site)
				destination = '{}/Hourly_GPP_Avg_{}_{}_{}_{}.png'.format(*formats)
				pyplot.savefig(destination)
				pyplot.clf()

				# print status
				self._print('plotted {}.'.format(destination))

		return None

	def flux(self, scans=(6, 14)):
		"""Compare average of sythetic data to fluxsat data.

		Arguments:
			scans: tuple of ints, the scan brackets

		Returns:
			None
		"""

		# make troubleshooting folder
		folder = '{}/flux'.format(self.folder)
		self._make(folder)

		# begin collection of gpp data
		fluxes = []
		synthetics = []
		latitudes = []
		longitudes = []
		corners = []
		cornersii = []

		# for each scan
		for scan in range(*scans):

			# get training matrix for particular scan
			parameters = ([(self.month, self.day)], (scan, scan + 1))
			matrix, datum, constraints = self.materialize(*parameters, constrain=True, exclude=True)

			# apply constraints to data
			datum = self._constrain(datum, constraints)

			# get mirror and track indices
			mirror = datum['mirror'].squeeze().astype(int)
			track = datum['track'].squeeze().astype(int)

			# create latitude data
			latitude = numpy.ones((self.mirror, self.track)) * self.fill
			latitude[mirror, track] = datum['latitude'].squeeze()
			latitudes.append(latitude)

			# create longitude data
			longitude = numpy.ones((self.mirror, self.track)) * self.fill
			longitude[mirror, track] = datum['longitude'].squeeze()
			longitudes.append(longitude)

			# create latitude bounds data
			corner = numpy.ones((self.mirror, self.track, 4)) * self.fill
			corner[mirror, track] = datum['latitude_bounds'].squeeze()
			corners.append(corner)

			# create longitude bounds data
			cornerii = numpy.ones((self.mirror, self.track, 4)) * self.fill
			cornerii[mirror, track] = datum['longitude_bounds'].squeeze()
			cornersii.append(cornerii)

			# create fluxsat data
			flux = numpy.ones((self.mirror, self.track)) * self.fill
			flux[mirror, track] = datum['gpp_fluxsat'].squeeze()
			fluxes.append(flux)

			# create synthetics data
			synthetic = numpy.ones((self.mirror, self.track)) * self.fill
			synthetic[mirror, track] = datum['gpp_onefluxnet'].squeeze()
			synthetics.append(synthetic)

		# average all arrays
		fluxes = numpy.array(fluxes).mean(axis=0)
		synthetics = numpy.array(synthetics).mean(axis=0)
		latitudes = numpy.array(latitudes).mean(axis=0)
		longitudes = numpy.array(longitudes).mean(axis=0)
		corners = numpy.array(corners).mean(axis=0)
		cornersii = numpy.array(cornersii).mean(axis=0)

		# generate difference
		differences = synthetics - fluxes

		# mask for valid data
		mask = self._mask(fluxes) & self._mask(synthetics)

		# apply mask
		fluxes = fluxes[mask]
		synthetics = synthetics[mask]
		differences = differences[mask]
		latitudes = latitudes[mask]
		longitudes = longitudes[mask]
		corners = corners[mask]
		cornersii = cornersii[mask]

		# set arrays
		arrays = [fluxes, synthetics, differences]

		# set titles
		titles = ['Fluxsat {}, {}\n'.format(self.target, self.date)]
		titles += ['Averaged Synthetic {}, {}\n'.format(self.target, self.date)]
		titles += ['Averaged Synthetic - Fluxsat {}, {}\n'.format(self.target, self.date)]

		# set stubs
		stubs = ['flux', 'synthetic', 'difference']

		# get difference scale
		minimum = numpy.percentile(differences, 2)
		maximum = numpy.percentile(differences, 98)
		absolute = max([abs(minimum), abs(maximum)])

		# set scales
		scales = [self.limits['scale'], self.limits['scale'], (-absolute, absolute)]

		# set gradients
		gradients = ['plasma', 'plasma', 'coolwarm']

		# for each array
		for array, title, stub, scale, gradient in zip(arrays, titles, stubs, scales, gradients):

			# begin graphs
			graphs = []

			# set destination
			formats = (folder, self.date, self.session, self.tag, stub)
			destination = '{}/Fluxsat_Comparison_{}_{}_{}_{}.png'.format(*formats)

			# begin GPP truth plot
			unit = self.unit
			bounds = [(-125, -60), (15, 55)]
			labels = ['', '']

			# make cluster graph
			options = {'polygons': True, 'corners': [corners, cornersii]}
			parameters = [longitudes, latitudes, array, bounds, scale]
			parameters += [gradient, title, unit, labels]
			graph = self._graph(*parameters, **options)
			graphs.append(graph)

			# create plot
			self._paint(destination, graphs)

		return None

	def grid(self):
		"""Create the high resolution ABI grid from the low resolution ABI grid.

		Arguments:
			None

		Returns:
			None
		"""

		# get the grid file
		hydra = Hydra('ABI_Grid')
		hydra.ingest(0)

		# get the latitude and longitude
		latitude = hydra.grab('latitude')
		longitude = hydra.grab('longitude')

		# get shape
		shape = latitude.shape

		# create the low resolution indices
		low = numpy.array(range(shape[0]))
		lowii = numpy.array(range(shape[1]))

		# create the middle resolution indices, offset by 0.25
		middle = numpy.array([(index / 2) - 0.25 for index in range(shape[0] * 2)])
		middleii = numpy.array([(index / 2) - 0.25 for index in range(shape[1] * 2)])

		# create the high resolution indices, offset by 0.25
		high = numpy.array([(index / 4) - 0.375 for index in range(shape[0] * 4)])
		highii = numpy.array([(index / 4) - 3.375 for index in range(shape[1] * 4)])

		# create mesh grid
		mesh, meshii = numpy.meshgrid(middle, middleii, indexing='ij')

		# interpolate middle latitude and longitude
		latitudeii = self._interpolate(latitude, low, lowii, mesh, meshii, method='linear')
		longitudeii = self._interpolate(longitude, low, lowii, mesh, meshii, method='linear')

		# create mesh grid
		mesh, meshii = numpy.meshgrid(high, highii, indexing='ij')

		# interpolate high latitude and longitude
		latitudeiii = self._interpolate(latitude, low, lowii, mesh, meshii, method='linear')
		longitudeiii = self._interpolate(longitude, low, lowii, mesh, meshii, method='linear')

		# create file
		data = {'latitude_low' : latitude, 'longitude_low': longitude}
		data.update({'latitude_mid': latitudeii, 'longitude_mid': longitudeii})
		data.update({'latitude_high': latitudeiii, 'longitude_high': longitudeiii})
		destination = 'ABI_High_Grid/ABI_high_resolution_COTUS_fixed_grid.h5'
		hydra.spawn(destination, data)

		return None

	def histolyze(self, scans=(6, 14), variable='gpp_onefluxnet', position=0, bins=50, limits=None, constrain=False):
		"""Generate a histogram of a variable:

		Arguments:
			scan: int, the scan number
			variable: str, variable name
			position: int, the index of a multi-column variable
			bins: int, number of bins
			limits: tuple of floats, the limits of the data
			constrain: apply constraints?

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/histograms'.format(self.folder)
		self._make(folder)

		# get training matrix for particular scan
		parameters = ([(self.month, self.day)], scans)
		_, data, constraints = self.materialize(*parameters)

		# if constraining
		if constrain:

			# apply constraints
			data = self._constrain(data, constraints)

		# get data
		datum = data[variable][:, position]

		# remove nans
		mask = numpy.isfinite(datum)
		datum = datum[mask]

		# if given a limit
		if limits:

			# apply limit
			mask = (datum >= limits[0]) & (datum <= limits[1])
			datum = datum[mask]

		# print bounds
		self._print(datum.min(), datum.max())
		self._print(datum.shape)

		# Create a histogram
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))
		pyplot.hist(datum, bins=bins, edgecolor='black')

		# Add titles and labels
		title = 'histogram of {}:{} for {}'.format(variable, position, self.date)
		pyplot.title(title)
		pyplot.xlabel('value')
		pyplot.ylabel('counts')

		# save the plot
		formats = (folder, variable, self.date, self._pad(scans[0]), self._pad(scans[1]), self._pad(position))
		destination = '{}/histogram_{}_{}_{}_{}_{}.png'.format(*formats)
		pyplot.savefig(destination)
		pyplot.clf()

		# print status
		self._print('plotted {}'.format(destination))

		return None

	def inspect(self, scan=10, bounds=[(31, 34), (-106, -100)], point=None, variables=None, margin=1.5):
		"""Inspect the model around a validation point with high error.

		Arguments:
			scan: int, scan number
			bounds: list of float tuples, the geographic bounds
			point: tuple of floats, latitude and longitude of particular point
			variables: list of str, the variables to check
			margin: int, percent margin

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/inspection'.format(self.folder)
		self._make(folder)

		# collect training days
		days = self.training['days']

		# get training matrix
		parameters = (days, (scan, scan + 1))
		matrix, data, constraints = self.materialize(*parameters)

		# set default variables
		variables = variables or self.features[:-1]

		# get specific day matrix
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrixii, dataii, constraintsii = self.materialize(*parameters)
		dataii = self._constrain(dataii, constraintsii)

		# if a point is given
		if point:

			# reconstruct bounds based on particular point
			bounds = [(point[0] - 0.001, point[0] + 0.001), (point[1] - 0.001, point[1] +  0.001)]

		# reduce matrixii to bounding box
		mask = (dataii['latitude'] >= bounds[0][0]) & (dataii['latitude'] <= bounds[0][1])
		mask = mask & (dataii['longitude'] >= bounds[1][0]) & (dataii['longitude'] <= bounds[1][1])
		mask = mask.squeeze()
		matrixii = matrixii[mask]

		# predict values from the model
		predictions, _, truths, _ = self.predict(matrixii)

		# get error between prediction and truth
		error = predictions - truths

		# get the sample with highest absolute error, and corresponding matrix row
		sample = numpy.argmax(abs(error))
		row = matrixii[sample]
		prediction = predictions[sample]
		truth = truths[sample]
		latitude = dataii['latitude'].squeeze()[mask][sample]
		longitude = dataii['longitude'].squeeze()[mask][sample]
		self._print('latitude, longitude: {}, {}'.format(latitude, longitude))

		# for each variable
		for variable in variables:

			# get the index of the feature
			index = self.features.index(variable)

			# create percentiles
			percentiles = numpy.array([numpy.percentile(matrix[:, index], percentile) for percentile in range(101)])

			# create probe matrix from medians for all features but variable in question
			probe = numpy.array([row.copy()] * 101)
			probe[:, index] = percentiles

			# predict values from the model
			predictionii, deviationii, _, _ = self.predict(probe)

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8, 8))

			# create title
			formats = (self.date, self._orient(latitude), self._orient(longitude, east=True), variable)
			title = '{} at {}, {}, {}'.format(*formats)
			pyplot.title(title)

			# add labels
			pyplot.xlabel(variable)
			pyplot.ylabel(self.features[-1])

			# set limits
			minimum = min([self.limits['scale'][0], truth - 1])
			maximum = max([self.limits['scale'][1], truth + 1])
			pyplot.ylim(minimum, maximum)

			# Sample data
			abscissa = percentiles
			ordinate = predictionii

			# create line plots
			pyplot.plot(abscissa, ordinate, 'co-', label='model')
			pyplot.plot(abscissa, ordinate - deviationii, 'c--')
			pyplot.plot(abscissa, ordinate + deviationii, 'c--')

			# plot prediction and truth
			pyplot.plot([row[index]], [truth], 'gX', markersize=15, label='truth')
			pyplot.plot([row[index]], [prediction], 'rX', markersize=15, label='prediction')

			# copy matrix
			xerox = matrix.copy()

			# # get percents
			# lower = numpy.percentile(xerox, 50 - margin, axis=0)
			# upper = numpy.percentile(xerox, 50 + margin, axis=0)

			# get standard deviation
			deviation = xerox.std(axis=0)

			# for each feature
			for position, feature in enumerate(self.features[:-1]):

				# if feature not varible of interest
				if feature != variable:

					# subset matrix
					buffer = deviation[position] * margin
					mask = (xerox[:, position] >= row[position] - buffer)
					mask = mask & (xerox[:, position] <= row[position] + buffer)
					xerox = xerox[mask]

			# print final sahape
			self._print('{} samples: {}'.format(variable, xerox.shape))

			# calculate differences from median for color scale
			difference = abs(xerox - row)
			difference[:, index] = 0
			difference = difference.sum(axis=1)

			# add truth points within median margins
			label = 'samples +/- {} std'.format(str(margin))
			pyplot.scatter(xerox[:, index], xerox[:, -1], c=difference, cmap='gray', s=25, marker='x', label=label)

			# add legend
			middle = (percentiles[0] + percentiles[100]) / 2
			location = self._anchor((row[index], truth), (middle, prediction))
			pyplot.legend(loc=location)

			# save plot
			destination = '{}/inspection_{}_{}_{}_{}.png'.format(folder, *formats)
			pyplot.savefig(destination)
			pyplot.clf()

		return None

	def inventory(self, products=None):
		"""Create inventory of training day holdings.

		Arguments:
			products: list of str, the directories

		Returns:
			None
		"""

		# make inventory folder
		self._make('{}/Inventory'.format(self.sink))

		# set default folders
		products = ['GPP_Inputs', 'TEMPO_RAD_L1', 'TEMPO_CLDO4_L2', 'TEMPO_NO2_L2', 'Nitro_Inputs']
		products += ['Synthetic_GPP', 'Mosaic_GPP', 'TEMPO_RAD_Smooth']

		# maximum length of products
		lengths = [len(product) for product in products]
		maximum = max(lengths)

		# begin inventory
		inventory = []

		# for each product folder
		for product in products:

			# get list of year subfolders
			years = self._see('{}/{}'.format(self.sink, product))
			years = [self._file(folder) for folder in years]

			# for each year subfolder
			for year in years:

				# get month folders
				months =  self._see('{}/{}/{}'.format(self.sink, product, year))
				months = [self._file(folder) for folder in months]

				# for each month
				for month in months:

					# get day folders
					days = self._see('{}/{}/{}/{}'.format(self.sink, product, year, month))
					days = [self._file(folder) for folder in days]

					# for each day
					for day in days:

						# make hydra
						formats = (self.sink, product, year, self._pad(month), self._pad(day))
						hydra = Hydra('{}/{}/{}/{}/{}'.format(*formats), show=False)

						# get all granules
						granules = [self._search('S[0-9]{3}G[0-9]{2}', path) for path in hydra.paths]

						# group by scan
						scans = self._group(granules, lambda granule: granule[:4])

						# for each scan
						for scan, members in scans.items():

							# reduce members and sort
							members = [member[-2:] for member in members]
							members.sort()
							members = ' '.join(members)

							# add element to inventory
							date = '{}-{}-{}:  '.format(year, month, day)
							tab = ' ' * (maximum - len(product))
							element = [date, '{}:  '.format(scan), '{}:  {}'.format(product, tab), members]
							inventory.append(element)

		# for each field except members
		for index in (2, 1, 0):

			# sort inventory
			inventory.sort(key=lambda element: element[index])

		# begin secondary inventory
		inventoryii = [[entry for entry in inventory[0]]]

		# for each element
		for index, element in enumerate(inventory):

			# if not the first
			if index > 0:

				# copy element
				elementii = [entry for entry in element]

				# if date is the same as above
				if element[0] == inventory[index - 1][0]:

					# make it blank instead
					elementii[0] = ' ' * len(element[0])

				# if scan is the same as above
				if element[1] == inventory[index - 1][1]:

					# make it blank instead
					elementii[1] = ' ' * len(element[1])

				# add to secondar inventory
				inventoryii.append(elementii)

		# begin report
		report = ['Training Data Inventory']

		# or each element in the inventory
		for elementii in inventoryii:

			# append to report
			line = '{}{}{}{}'.format(*elementii)
			report.append(line)

			# print to screen
			self._print(line)

		# jot report
		self._jot(report, '{}/Inventory/training_inventory.txt'.format(self.sink))

		return None

	def land(self, scan=10):
		"""Create land type based scatter plots.

		Arguments:
			scan: int, scan number

		Returns:
			None
		"""

		# make folders
		folder = '{}/land'.format(self.folder)
		self._make(folder)

		# get training matrix for particular scan
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, data, constraints = self.materialize(*parameters)

		# apply constraints to data
		data = self._constrain(data, constraints)

		# make predictions
		prediction, deviation, truth, score = self.predict(matrix)

		# get land type
		land = data['land_type']

		# get landforms dictionary
		names = self._acquire('Land_Types/land.yaml')

		# begin graphs
		graphs = []

		# for each land type, excluding water
		for index in range(1, 17):

			# apply screen
			mask = (land == index).squeeze()
			truthii = truth[mask]
			predictionii = prediction[mask]

			# calculate r2 score
			score = r2_score(truthii, predictionii)

			# calculate regression line
			slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(truthii, predictionii)

			# perform binning
			mesh, meshii, counts = self._bin(truth, prediction, samples, bracket)

			# construct samples title depending on instances
			name = names[index]
			title = 'Prediction vs FluxSat {}\n{} ( {} ), $R^2$ = {:.2f}'.format(self.date, name, index, score)

			# construct samples graph
			unit = 'samples'
			labels = ['{} TROPOMI'.format(self.target), '{} prediction'.format(self.target)]
			boundsii = [(0, self.limits['scale'][1]), (0, self.limits['scale'][1])]
			scaleiii = (1, 100)
			points = [0, self.limits['scale'][1]]
			regression = [intercept + point * slope for point in points]
			line = (points, points, 'k-')
			lineii = (points, regression, 'k--')
			lines = [line, lineii]
			options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.5, 'polygons': False}
			graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
			graphs.append(graph)

		# for each block
		for first, last in ((0, 4), (4, 8), (8, 12), (12, 16)):

			# plot land type scatters
			space = {'hspace': 0.6}
			formats = (folder, self.date, self._pad(first), self._pad(last))
			destination = '{}/land_type_scatters_{}_{}_{}.png'.format(*formats)
			self._paint(destination, graphs[first:last], space=space)

		return None

	def materialize(self, days, scans=(10, 11), granules=(1, 10), constrain=True, exclude=False, reservoir=None):
		"""Form training matrix from a set of input files.

		Arguments:
			days: list of (int, int) tuples, the month and day
			scans: tuple of ints, the scan bracket
			granules: tuple of ints, the granule bracket
			constrain: boolean, apply constraints to matrix?
			exclude: boolean, exclude truths from limits?
			reservoir: str, name of folder from which to grab data

		Returns:
			numpy array, dict of numpy arrays
		"""

		# set features
		features = self.features

		# set reservoir
		reservoir = reservoir or 'GPP_Inputs'

		# map fields to yaml limits
		limits = {'latitude': 'latitude', 'longitude': 'longitude'}
		limits.update({'solar_zenith_angle': 'zenith', 'viewing_zenith_angle': 'view'})
		limits.update({'cloud_fraction': 'cloud', 'snow_fraction': 'snow'})
		limits.update({'water_mask': 'water'})
		limits.update({'max_rad_quality_high': 'quality', 'max_rad_quality_low': 'quality'})
		limits.update({'max_irr_quality_high': 'quality', 'max_rad_quality_low': 'quality'})

		# begin reservoirs
		matrix = []
		constraints = []
		data = {}

		# for each day
		for month, day in days:

			# create hydra
			formats = (self.sink, reservoir, self.year, self._pad(month), self._pad(day))
			hydra = Hydra('{}/{}/{}/{}/{}'.format(*formats), show=False)

			# create labels for scans
			labels = [self._label(scan, granule) for scan in range(*scans) for granule in range(*granules)]

			# subset paths
			paths = [path for path in hydra.paths if any([label in path for label in labels])]
			paths.sort()

			# for each path
			for path in paths:

				# ingest path
				hydra.ingest(path)

				# extract data
				extract = hydra.extract()

				# separate extract based on dimensionality
				ones = {field: array for field, array in extract.items() if len(array.shape) == 1}
				twos = {field: array for field, array in extract.items() if len(array.shape) == 2}
				threes = {field: array for field, array in extract.items() if len(array.shape) == 3}

				# remove 1-d arrays
				extract = {field: array for field, array in extract.items() if field not in ones.keys()}

				# reshape all 2 and 3-d arrays
				extract.update({field: array.reshape(-1, 1) for field, array in twos.items()})
				extract.update({field: array.reshape(-1, array.shape[2]) for field, array in threes.items()})

				# get inputs
				block = self._forge(extract, features)

				# begin constraint
				constraint = numpy.ones(extract['latitude'].shape).astype(bool)

				# for each limit
				for field, limit in limits.items():

					# add lower and upper limit constraints
					constraint = constraint & (extract[field] >= self.limits[limit][0])
					constraint = constraint & (extract[field] <= self.limits[limit][1])

				# if excluding truth from limits:
				if exclude:

					# check for nans in truth
					constraint = constraint & (self._mask(block[:, :-1].sum(axis=1)).reshape(-1, 1))
					constraint = constraint & (self._mask(block[:, :-1].min(axis=1)).reshape(-1, 1))
					constraint = constraint & (self._mask(block[:, :-1].max(axis=1)).reshape(-1, 1))
					constraint = constraint.squeeze()

				# otherwise
				else:

					# check for nans in truth
					constraint = constraint & (self._mask(block.sum(axis=1)).reshape(-1, 1))
					constraint = constraint & (self._mask(block.min(axis=1)).reshape(-1, 1))
					constraint = constraint & (self._mask(block.max(axis=1)).reshape(-1, 1))
					constraint = constraint.squeeze()

				# if apply constraints
				if constrain:

					# apply constraints
					block = block[constraint]

				# add 1-d arrays back
				extract.update({field: array.reshape(1, -1) for field, array in ones.items()})

				# add to reservoirs
				[data.setdefault(field, []).append(array) for field, array in extract.items()]
				matrix.append(block)
				constraints.append(constraint)

		# try to
		try:

			# stack into matrix
			matrix = numpy.vstack(matrix)
			constraints = numpy.hstack(constraints)
			data = {field: numpy.vstack(arrays) for field, arrays in data.items()}

		# unless the matrix list is empty
		except ValueError:

			# in which case, pass Nones
			matrix = None
			constraints = None
			data = None

		return matrix, data, constraints

	def neighbor(self, scan=10, samples=1000, fraction=0.5, metric='euclidean', number=None):
		"""Run a KNN regressor to approximate irreducible error.

		Arguments:
			scan: int, scan number
			samples: int, number of samples to use
			fraction: float, fraction for training, test split
			metric: str, similarity metric
			number: int, number of nearest neighbors

		Returns:
			None
		"""

		# default number to sqrt of samples
		number = number or int(numpy.sqrt(samples * fraction))

		# create a set of ten numbers, beginning with 5
		numbers = numpy.linspace(5, number, 10).astype(int)

		# make folders
		folder = '{}/reports'.format(self.folder)
		folderii = '{}/neighbors'.format(self.folder)
		self._make(folder)
		self._make(folderii)

		# begin error percents
		percents = []

		# for each number
		for number in numbers:

			# begin report
			report = [self._print('KNN Nearest Neighbors regressor...')]

			# get training matrix for particular scan
			parameters = ([(self.month, self.day)], (scan, scan + 1))
			matrix, _, _ = self.materialize(*parameters)

			# get scaling information
			minimum = self.details['scaling/input_min']
			maximum = self.details['scaling/input_max']
			scale = maximum - minimum

			# print size
			report.append(self._print('matrix shape: {}'.format(matrix.shape)))

			# calculate total variables
			variance = numpy.var(matrix[:, -1])
			report.append(self._print('total variance: {}'.format(variance)))

			# shuffle the matrix
			numpy.random.shuffle(matrix)

			# get the splitting index
			split = int(samples * fraction)

			# remove truth and apply scaling
			truth = matrix[:split, -1]
			inputs = (matrix[:split, :-1] - minimum) / scale

			# remove test set and apply scaling
			truthii = matrix[split:samples, -1]
			inputsii = (matrix[split:samples, :-1] - minimum) / scale

			# print train and test sizes
			report.append(self._print('training matrix shape: {}'.format(inputs.shape)))
			report.append(self._print('testing matrix shape: {}'.format(inputsii.shape)))

			# create the knn regressor and fit
			report.append(self._print('training knn with {} neighbors, {} metric...'.format(number, metric)))
			regressor = KNeighborsRegressor(n_neighbors=number, metric=metric)
			regressor.fit(inputs, truth)

			# predict on test data and calculate residuals
			prediction = regressor.predict(inputsii)
			residuals = truthii - prediction

			# estimate irreducible error from variance of residuatlw
			error = numpy.var(residuals)
			percent = 100 * (error / variance)
			report.append(self._print('irreducible error: {}'.format(error)))
			report.append(self._print('irreducible error: {} %'.format(percent)))

			# append percent
			percents.append(percent)

			# jot report
			formats = (folder, self.date, metric, samples, number)
			destination = '{}/knn_neighbors_estimate_{}_{}_{}_{}.txt'.format(*formats)
			self._jot(report, destination)

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# plot percents versus number
		pyplot.title('KNN nearest neighbors for scan {}, {}'.format(scan, self.date))

		# label
		pyplot.xlabel('neighbors ( k )')
		pyplot.ylabel('irreducible error ( % )')

		# plot
		pyplot.plot(numbers, percents, 'b-')

		# save figure
		pyplot.savefig('{}/nearest_neighbors_{}_{}.png'.format(folderii, self.date, scan))
		pyplot.clf()

		return None

	def picture(self, scan=10, constrain=True):
		"""Picture the GPP across the entire map.

		Arguments:
			scan: int, scan number
			constrain: boolean, apply constraints?

		Returns:
			None
		"""

		# make troubleshooting folder
		folder = '{}/picture'.format(self.folder)
		self._make(folder)

		# get data set, excluding truth from constraints
		options = {'constrain': constrain, 'exclude': True}
		matrix, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1), **options)

		# if constraining:
		if constrain:

			# apply constraints
			data = self._constrain(data, constraints)

		# get predictions
		prediction, _, _, _ = self.predict(matrix)

		# get geo coordinates
		latitude = data['latitude']
		longitude = data['longitude']
		corners = data['latitude_bounds']
		cornersii = data['longitude_bounds']

		# set scales
		scales = {'gpp_onefluxnet': (0, 35), 'gpp_fluxsat': (0, 35), 'short_wave': (0, 1000)}
		scales.update({'onefluxnet_deviation': (0, 10), 'gpp_mosaic': (0, 35)})

		# begin graphs
		graphs = []

		# create destination
		destination = '{}/Picture_{}_{}_{}_{}.png'.format(folder, self.date, self._pad(scan), self.session, self.tag)

		# create time
		time = str(data['time'][0][0])
		time = '{}:{} CST'.format(int(time[:2]) - 6, time[2:4])

		# construct percent difference plot
		symbol = '\u2264'
		formats = (self.target, self.date, time, symbol, self.limits['cloud'][1])
		title = 'TEMPO {}, {} {}, crf {} {}\n'.format(*formats)
		unit = self.unit
		bounds = [(-125, -60), (20, 50)]

		# create valid mask, also removing clouds
		mask = self._mask(prediction).squeeze()

		# set gradient
		gradient = 'plasma'

		# construct scale
		scale = self.limits['scale']

		# set categories to None
		categories = None

		# make plot
		labels = ['', '']
		options = {'polygons': True, 'corners': [corners[mask], cornersii[mask]], 'bar': True, 'categories': categories}
		parameters = (longitude[mask], latitude[mask], prediction[mask], bounds, scale, gradient, title, unit, labels)
		graph = self._graph(*parameters, **options)
		graphs.append(graph)

		# create plot
		self._paint(destination, graphs)

		return None

	def point(self, scan=10, latitude=35, longitude=-90, site=None):
		"""Get a matrix point from the dataset.

		Arguments:
			scan: int, the scan of interest
			latitude: float, latitude of interest
			longitue: float, longitude of interest

		Returns:
			numpy array, str tuple, the points and a descriptive tag
		"""

		# if given a site
		if site:

			# override latitude and longitude with site
			latitude = self.catalog[site]['latitude']
			longitude = self.catalog[site]['longitude']

		# get the matrix and data
		matrix, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1))
		data = self._constrain(data, constraints)

		# construct mask
		index = self._pin([latitude, longitude], [data['latitude'], data['longitude']])[0][0]

		# get points
		points = matrix[index: index + 1]

		# begin data subset
		subset = {}

		# for each field
		for field in data.keys():

			# try to
			try:

				# subset data
				subset[field] = data[field][index: index + 1]

			# unless problem
			except IndexError:

				# in which case, skip subsetting
				subset[field] = data[field]

		# create description
		formats = (scan, self._orient(latitude), self._orient(longitude, east=True))
		tag = 'scan {}, {}, {}'.format(*formats)

		# if given a site
		if site:

			# add to tag
			tag += ', {}'.format(site)

		return points, tag, subset

	def predict(self, matrix, instances=None):
		"""Use the model to make predictions on a matrix.

		Arguments:
			model: tensorflow model instance
			details: dict of model information

		Returns:
			tuple of numpy arrays, the truth, the averaged predictions, and deviation
		"""

		# set default instances
		instances = instances or self.instances

		# transform the input data
		inputs, truth = self._transform(matrix)

		# get model outputs
		outputs = []
		for _ in range(instances):

			# get outputs
			output = self.model(inputs)
			output = output.numpy().squeeze()
			outputs.append(output)

		# create array
		outputs = numpy.array(outputs)

		# if outputs are 1-D
		if len(outputs.shape) < 2:

			# reshape for second dimension
			outputs = outputs.reshape(-1, 1)

		# translate the predictions from the raw outputs
		predictions = self._translate(outputs)

		# take mean and stdev of predictions
		average = numpy.array(predictions).mean(axis=0)
		spread = numpy.array(predictions).std(axis=0)

		# make sure they are finite
		mask = (numpy.isfinite(truth) & numpy.isfinite(average))

		# default score to 0
		score = 0

		# if there are samples available
		if mask.sum() > 1:

			# compute r^2 score on mean of predictions
			score = r2_score(truth[mask], average[mask])

		return average, spread, truth, score

	def prepare(self, scans=(2, 3), granules=(1, 2)):
		"""Prepare training data files for TEMPO.

		Arguments:
			scans: tuple of ints, the scan bracket
			granules: tuple of ints, the granule bracket

		Returns:
			None
		"""

		# construct scan labels
		labels = [self._label(scan, granule) for scan in range(*scans) for granule in range(*granules)]

		# for each folder
		for folder in ('GPP_Inputs', 'TEMPO_RAD_Smooth'):

			# create destination folders
			self._make('{}/{}'.format(self.sink, folder))
			self._make('{}/{}/{}'.format(self.sink, folder, self.year))
			self._make('{}/{}/{}/{}'.format(self.sink, folder, self.year, self._pad(self.month)))
			self._make('{}/{}/{}'.format(self.sink, folder, self.stub))

		# construct hydras for data ingestion
		radiances = Hydra('{}/TEMPO_RAD_L1/{}'.format(self.sink, self.stub), show=False)
		clouds = Hydra('{}/TEMPO_CLDO4_L2/{}'.format(self.sink, self.stub), show=False)
		irradiances = Hydra('{}/TEMPO_IRR_L1/{}'.format(self.sink, self.stub), show=False)
		fluxes = Hydra('{}/FluxSat/{}'.format(self.sink, self.year), show=False)
		radiations = Hydra('{}/PAR'.format(self.sink), show=False)
		waters = Hydra('{}/WaterMask'.format(self.sink), show=False)
		components = Hydra('{}/TEMPO_PCs'.format(self.sink), show=False)
		synthetics = Hydra('{}/Synthetic_GPP/{}'.format(self.sink, self.stub))
		mosaics = Hydra('{}/Mosaic_GPP/{}'.format(self.sink, self.stub))

		# filter radiance paths
		paths = [path for path in radiances.paths if any([label in path for label in labels])]
		for path in paths:

			# prints status
			self._stamp('ingesting {} and related paths...'.format(path), initial=True)

			# get date from path
			search = '[0-9]{8}T[0-9]{6}Z'
			date = re.findall(search, path)[0]

			# get granule from path
			search = 'S[0-9]{3}G[0-9]{2}'
			granule = re.findall(search, path)[0]

			# begin data with time and granule information
			data = {'date': numpy.array([int(date[0:8])]), 'time': numpy.array([int(date[9:15])])}
			data.update({'scan': numpy.array([int(granule[1:4])]), 'granule': numpy.array([int(granule[5:7])])})

			# ingest the path
			radiances.ingest(path)

			# get geolocations
			latitude = radiances.grab('latitude')
			longitude = radiances.grab('longitude')
			bounds = radiances.grab('latitude_bounds')
			boundsii = radiances.grab('longitude_bounds')

			# get shape
			shape = latitude.shape

			# begin data with geolocation information
			data.update({'latitude': latitude, 'longitude': longitude})
			data.update({'latitude_bounds': bounds, 'longitude_bounds': boundsii})

			# get the seconds since 1-6-1980
			seconds = radiances.grab('time')

			# create local time, and add to data
			time = self._localize(seconds, longitude)
			data.update({'local_time': time})

			# get grid positions, and create mesh
			mirror = radiances.grab('mirror_step')
			track = radiances.grab('xtrack')

			# create grids and add to data
			mirror, track = numpy.meshgrid(mirror, track, indexing='ij')
			data.update({'mirror': mirror, 'track': track})

			# get angles
			zenith = radiances.grab('solar_zenith_angle')
			view = radiances.grab('viewing_zenith_angle')
			azimuth = radiances.grab('solar_azimuth_angle')
			azimuthii = radiances.grab('viewing_azimuth_angle')

			# add angles to data
			data.update({'solar_zenith_angle': zenith, 'viewing_zenith_angle': view})
			data.update({'relative_azimuth_angle': azimuthii - azimuth})

			# get surface information
			ground = radiances.grab('ground_pixel_quality_flag')
			snow = radiances.grab('snow_ice_fraction')

			# get land class data from ground pixel bits 16 - 23
			bits = (1 << (23 - 16 + 1)) - 1
			land = (ground >> 16) & bits
			data.update({'land_type': land, 'snow_fraction': snow})

			# get rgb information
			red = radiances.grab('red')
			green = radiances.grab('green')
			blue = radiances.grab('blue')

			# stack into colors, and clip at 1
			colors = numpy.dstack([red, green, blue])
			colors = numpy.where(colors <= 1, colors, 1)
			data.update({'rgb': colors})

			# for each band
			for band in (1, 2):

				# for each cardinal
				for cardinal in ('east', 'west'):

					# add abi data
					search = 'ABI_Band_{}_{}'.format(band, cardinal.capitalize())
					data.update({'abi_band{}_{}'.format(band, cardinal): radiances.grab(search)})

			# get radiance data
			high = radiances.grab('band_290_490/radiance')
			low = radiances.grab('band_540_740/radiance')
			shorts = radiances.grab('band_290_490/nominal_wavelength')
			longs = radiances.grab('band_540_740/nominal_wavelength')

			# get quality flags
			quality = radiances.grab('band_290_490/pixel_quality_flag')
			qualityii = radiances.grab('band_540_740/pixel_quality_flag')

			# take maximum quality reading in radiance ranges
			limits = self.limits['high']
			limitsii = self.limits['low']
			flag = numpy.where(((shorts >= limits[0]) & (shorts <= limits[1])), quality, 0).max(axis=2)
			flagii = numpy.where(((longs >= limitsii[0]) & (longs <= limitsii[1])), qualityii, 0).max(axis=2)

			# add to data
			data.update({'max_rad_quality_high': flag, 'max_rad_quality_low': flagii})

			# get the irradiance path
			irradiances.ingest(0)

			# get irradiance data
			highii = irradiances.grab('band_290_490/radiance').squeeze()
			lowii = irradiances.grab('band_540_740/radiance').squeeze()
			shortsii = irradiances.grab('band_290_490/nominal_wavelength').squeeze()
			longsii = irradiances.grab('band_540_740/nominal_wavelength').squeeze()

			# get irradiance qualifiers
			qualifiers = irradiances.grab('band_290_490/pixel_quality_flag').squeeze()
			qualifiersii = irradiances.grab('band_540_740/pixel_quality_flag').squeeze()

			# take maximum quality reading in radiance ranges
			limits = self.limits['high']
			limitsii = self.limits['low']
			flag = numpy.where(((shortsii >= limits[0]) & (shortsii <= limits[1])), qualifiers, 0).max(axis=1)
			flagii = numpy.where(((longsii >= limitsii[0]) & (longsii <= limitsii[1])), qualifiersii, 0).max(axis=1)

			# tile qualifiers to match shape
			flag = numpy.tile(flag, (shape[0], 1))
			flagii = numpy.tile(flagii, (shape[0], 1))

			# add to data
			data.update({'max_irr_quality_high': flag, 'max_irr_quality_low': flagii})

			# ingest pca components
			components.ingest()

			# get pca coefficents
			coefficients = components.grab('PCA_Coeff')

			# print statuc
			self._stamp('smoothing reflectances...')

			# smooth and reduce into reflectance pcas
			parameters = [high, low, shorts, longs]
			parameters += [highii, lowii, shortsii, longsii]
			parameters += [zenith, coefficients]
			reduction, reflectance, grid = self.smooth(*parameters)

			# add to data
			data.update({'reflectance_pcas': reduction})

			# create secondary data for smoothed radiances
			dataii = {'smoothed_reflectances': reflectance, 'reflectance_wavelengths': grid}

			# ingest the cloud path matching the radiance path
			clouds.ingest(date)

			# get cloud data
			cloud = clouds.grab('cloud_fraction')

			# add to reservoir
			data.update({'cloud_fraction': cloud})

			# print status
			self._stamp('interpolating fluxsat...')

			# ingest the relevant FluxSat data
			fluxes.ingest('{}{}'.format(self.year, self._pad(self.month)))

			# get fluxsat data, reversing latitude
			latitudeii = fluxes.grab('lat')[::-1]
			longitudeii = fluxes.grab('lon')
			productivity = fluxes.grab('GPP')[self.day - 1][::-1]
			uncertainty = fluxes.grab('GPP_uncertainty')[self.day - 1][::-1]

			# interpolate gpp onto tempo coordinates
			interpolation = self._interpolate(productivity, latitudeii, longitudeii, latitude, longitude)
			data.update({'gpp_fluxsat': interpolation})

			# interpolate gpp onto tempo coordinates
			interpolation = self._interpolate(uncertainty, latitudeii, longitudeii, latitude, longitude)
			data.update({'fluxsat_uncertainty': interpolation})

			# ingest the watermask data
			waters.ingest(0)

			# get water mask, reversing latitude ( at same latitude and longitude as GPP )
			water = waters.grab('WaterMask').astype(float)[::-1]

			# interpolate water mask onto tempo coordinates
			interpolation = self._interpolate(water, latitudeii, longitudeii, latitude, longitude)
			data.update({'water_mask': interpolation})

			# ingest PAR
			radiations.ingest(0)

			# get PAR latitudes and clear sky par, reversing direction for low to high
			slats = radiations.grab('lat_grid_par')[::-1]
			radiation = radiations.grab('par_clearsky')[self.day - 1][self.month - 1][::-1]

			# interpolate par onto tempo grid
			interpolation = numpy.interp(latitude, slats, radiation)
			data.update({'clearsky_par': interpolation})

			# print status
			self._stamp('interpolating ABI baseliner data...')

			# interpolate nearest abi data on tempo grid, and remove negativew
			baselines, wavelengths = self.baseline(path, latitude, longitude)
			baselines = numpy.where(baselines >= 0, baselines, self.fill)
			data.update({'abi_radiance': baselines, 'abi_wavelengths': wavelengths})

			# add synthetic data
			synthetics.ingest(date)
			fields = ['short_wave', 'mcd43d_bands', 'gpp_onefluxnet_high', 'onefluxnet_high_deviation']
			fields += ['mcd43c_bands', 'gpp_onefluxnet_low', 'onefluxnet_low_deviation']
			data.update({field: synthetics.grab(field) for field in fields})

			# add modis bands
			pairs = [(620, 670), (841, 876), (459, 479), (545, 565)]
			pairs += [(1230, 1250), (1628, 1652), (2105, 2155)]
			data.update({'mcd43_wavelengths': numpy.array(pairs)})

			# add mosaic data
			mosaics.ingest(date)
			data.update({'gpp_mosaic': mosaics.grab('gpp_mosaic')})

			# fill values
			data = {name: self._fill(array) for name, array in data.items()}

			# check out data
			self._view(data)

			# stash data
			destination = '{}/GPP_Inputs/{}/GPP_Inputs_{}_{}.h5'.format(self.sink, self.stub, date, granule)
			self.spawn(destination, data)

			# fill values for smoothed radiance
			dataii = {name: self._fill(array) for name, array in dataii.items()}

			# check out data
			self._view(dataii)

			# stash data
			formats = (self.sink, self.stub, date, granule)
			destinationii = '{}/TEMPO_RAD_Smooth/{}/TEMPO_RAD_Smooth_{}_{}.h5'.format(*formats)
			self.spawn(destinationii, dataii)

			# print status
			self._stamp('prepared')

		return None

	def present(self, destination, truth, prediction, deviation, valids, data, instances, chart=True):
		"""Make presentation plots.

		Arguments:
			destination: str, destination filepath
			truth: array of gpp fluxsat data
			prediction: NN model prediction
			deviation: NN model stdev
			valids: dict of valid data
			data: dict of all data
			instances: int, number of prediction instances
			chart: boolean, plot maps?

		Returns:
			None
		"""

		# begin status
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase preevious copy
		self._clean(destination, force=True)

		# extract date
		date = str(data['date'][0][0])
		date = '{}m{}{}'.format(date[:4], date[4:6], date[6:8])

		# create time for title
		time = data['time'][0][0]
		if time < 70000:

			# add to 2400
			time += 240000

		# make string
		time = str(time)
		time = '{}:{} CST'.format(int(time[:2]) - 6, time[2:4])

		# grab data
		latitude = valids['latitude']
		longitude = valids['longitude']
		cloud = valids['cloud_fraction']
		uncertainty = valids['fluxsat_uncertainty']
		corners = valids['latitude_bounds']
		cornersii = valids['longitude_bounds']

		# caluclate differencd
		difference = prediction - truth

		# calculate r2 score
		score = r2_score(truth, prediction)

		# calculate regression line
		slope, intercept, _, _, _ = scipy.stats.linregress(truth, prediction)

		# create titles
		titles = ['{} Truth {} {}\n'.format(self.target, date, time)]
		titles += ['{} Prediction {} {}\n'.format(self.target, date, time)]
		titles += ['Prediction vs Truth\n( R^2 = {} )'.format(self._round(score, 2))]
		titles += ['Percent Difference\n']

		# calculate differences
		difference = prediction - truth
		difference = numpy.where(numpy.isfinite(difference), difference, 0)

		# perform binning
		samples = 100
		bracket = self.limits['scale']
		mesh, meshii, counts = self._bin(truth, prediction, samples, bracket)

		# get minimum and maximum tracer values
		minimum = min([truth.min(), prediction.min()])
		maximum = max([truth.max(), prediction.max()])

		# set percent bracket
		absolute = max([abs(numpy.percentile(difference, 5)), abs(numpy.percentile(difference, 95))])

		# if plotting maps:
		if chart:

			# set boundaries
			boundaries = [[(-125, -70), (25, 50)]]
			boundaries += [[(-100, -80), (28, 40)]]
			boundaries += [[(-95, -85), (29, 36)]]

			# set alphas
			alphas = [1.0, 1.0, 1.0]

			# set longitude ticks
			ticks = [(-120, -110, -100, -90, -80, -70)]
			ticks += [(-100, -95, -90, -85, -80)]
			ticks += [(-95, -90, -85)]

			# set longitude tickmarks
			marks = [[self._orient(entry, east=True).strip('0') for entry in tick] for tick in ticks]

			# set latitude ticks
			ticksii = [(30, 35, 40, 45, 50)]
			ticksii += [(30, 35, 40)]
			ticksii += [(30, 32, 34)]

			# set latitude tickmarks
			marksii = [[self._orient(entry).strip('0') for entry in tick] for tick in ticksii]

			# set boundaries
			zipper = zip(boundaries, alphas, ticks, marks, ticksii, marksii)
			for bounds, alpha, tick, mark, tickii, markii in zipper:

				# construct geo tag
				formats = [self._orient(bounds[0][0], east=True), self._orient(bounds[0][1], east=True)]
				formats += [self._orient(bounds[1][0]), self._orient(bounds[1][1])]
				tag = '{}{}{}{}'.format(*formats)

				# replace destination
				destinationii = destination.replace('.png', '_{}.png'.format(tag))
				destinationiii = destination.replace('.png', '_uncertainty_{}.png'.format(tag))

				# set bounds
				scale = (minimum, maximum)
				scaleii = (-absolute, absolute)

				# begin graphs
				graphs = []

				# construct GPP truth plot
				title = '{} FluxSat {} {}'.format(self.target, date, time)
				unit = self.unit
				labels = ['', '']
				options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				graph = self._graph(longitude, latitude, truth, bounds, scale, 'plasma', title, unit, labels, **options)
				graphs.append(graph)

				# construct GPP prediction plot
				title = '{} Prediction {} {}'.format(self.target, date, time)
				unit = self.unit
				labels = ['', '']
				options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				parameters = (longitude, latitude, prediction, bounds, scale, 'plasma', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# unpack reflectivity
				latitudeii = data['latitude']
				longitudeii = data['longitude']
				norths = data['latitude_bounds']
				easts = data['longitude_bounds']
				colors = data['rgb']
				land = data['land_type']

				# clip boundaries
				colors = numpy.where(colors > 1, 1, colors)
				colors = numpy.where(colors < 0, 0, colors)
				mask = (numpy.isfinite(latitudeii)) & (numpy.isfinite(longitudeii)) & (land != 0)
				mask = mask & (latitudeii > (-longitudeii - 95))
				mask = mask.squeeze()
				colors = colors[mask]
				latitudeii = latitudeii[mask]
				longitudeii = longitudeii[mask]
				norths = norths[mask]
				easts = easts[mask]

				# construct rgb plot
				title = 'RGB Map {} {}'.format(date, time)
				unit = self.unit
				labels = ['', '']
				options = {'polygons': True, 'corners': [norths, easts], 'globe': True, 'colorbar': False}
				options.update({'reflectivity': True, 'ticks': tick, 'alpha': alpha})
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				parameters = (longitudeii, latitudeii, colors, bounds, scaleii, 'plasma', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# construct percent difference plot
				title = 'Difference ( Prediction - FluxSat )'
				unit = self.unit
				labels = ['', '']
				options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				parameters = (longitude, latitude, difference, bounds, scaleii, 'coolwarm', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# construct percent difference plot
				title = 'FluxSat Uncertainty {}'.format(date)
				unit = self.unit
				labels = ['', '']
				scaleiii = (0, 3)
				options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				parameters = (longitude, latitude, uncertainty, bounds, scaleiii, 'plasma', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# construct percent difference plot
				title = 'Prediction Stdev {} {}'.format(date, time)
				unit = self.unit
				labels = ['', '']
				scaleiii = (0, 3)
				options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				parameters = (longitude, latitude, deviation, bounds, scaleiii, 'plasma', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# create plots
				self._paint(destinationii, graphs[:4])
				space = {'top': 0.88, 'bottom': 0.28, 'wspace': 0.35, 'right': 0.95}
				self._paint(destinationiii, graphs[4:6], size=(16, 8), space=space, gap=0.11)

		# begin scatter plots
		scatters = []

		# set cloud sceens
		fractions = [(0, 1.0), (0, 0.25), (0.25, 0.5), (0.5, 0.75), (0.75, 1.0)]
		screens = [(one * self.limits['cloud'][1], two * self.limits['cloud'][1]) for one, two in fractions]

		# for each screen
		for screen in screens:

			# create less than or equal symbol
			symbol = '\u2264'

			# unpack screen
			low, high = screen

			# apply screen
			mask = (cloud >= low) & (cloud <= high)
			mask = mask.squeeze()
			truthii = truth[mask]
			predictionii = prediction[mask]
			cloudii = cloud[mask].squeeze()
			differenceii = difference[mask].squeeze()

			# calculate r2 score
			score = r2_score(truthii, predictionii)

			# calculate regression line
			slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(truthii, predictionii)

			# perform binning
			samples = 100
			bracket = self.limits['scale']
			mesh, meshii, counts = self._bin(truthii, predictionii, samples, bracket)

			# construct samples title depending on instances
			scan = int(self._file(destination).split('_')[3])
			formats = (low, symbol, symbol, high, date, time, scan, self._round(score, 2))
			title = 'TEMPO vs Onefluxnet ( {:.2f} {} CRF {} {:.2f} )\n{} {} ({}) ( $R^2$ = {:.2f} )'.format(*formats)

			# construct samples graph
			unit = 'samples'
			labels = ['{} Onefluxnet'.format(self.target), 'TEMPO {} prediction'.format(self.target)]
			boundsii = [self.limits['scale'], self.limits['scale']]
			scaleiii = (1, 100)
			points = [0, truthii.max()]
			regression = [intercept + point * slope for point in points]
			line = (points, points, 'k-')
			lineii = (points, regression, 'k--')
			lines = [line, lineii]
			options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.5, 'polygons': False}
			graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
			scatters.append(graph)

			# if full cloud screen
			if screen == screens[0]:

				# zipper = zip(truthii, predictionii, differenceii, cloudii)
				# for i, j, k, l in list(zipper)[:100]:
				# 	print(i, j, k, l)

				# calculate r2 score
				score = r2_score(cloudii, differenceii)

				# calculate regression line
				slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(cloudii, differenceii)

				# remove outliers
				lower = numpy.percentile(differenceii, 0)
				upper = numpy.percentile(differenceii, 100)

				# perform binning
				samples = 100
				bracket = self.limits['cloud']
				mesh, meshii, counts = self._bin(truthii, predictionii, samples, bracket)

				# construct samples title depending on instances
				title = 'Prediction - FluxSat vs CRF\n{} {}'.format(date, time)

				# set limits for scatter
				minimum = lower
				maximum = upper

				# construct samples graph
				unit = 'samples'
				labels = ['CRF', 'Prediction - FluxSat']
				boundsii = [self.limits['cloud'], (minimum, maximum)]
				scaleiii = (1, 100)
				points = self.limits['cloud']
				pointsii = [0, 0]
				regression = [intercept + point * slope for point in points]
				line = (points, pointsii, 'k-')
				lineii = (points, regression, 'k--')
				lines = [line, lineii]
				options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.5, 'polygons': False}
				options.update({'marker': 0.1})
				graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
				scatters.append(graph)

		# plot scatters
		space = {'top': 0.88, 'bottom': 0.24, 'wspace': 0.36, 'right': 0.96}
		destinationii = destination.replace('.png', '_crf_{}.png'.format(int(self.limits['cloud'][1] * 10)))
		self._paint(destinationii, scatters[0:2], size=(16, 8), space=space, gap=0.12)

		# plot crf scatters
		space = {'hspace': 0.6}
		destinationiii = destination.replace('.png', '_scatters_{}.png'.format(int(self.limits['cloud'][1] * 10)))
		self._paint(destinationiii, scatters[2:6], space=space)

		return None

	def probe(self, scan=10, variables=None, margin=25):
		"""Probe model as a function of independent variable, with median values at all others.

		Arguments:
			scan: int, scan number
			variable: str, dependent variable
			margin: int, perentile margin

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/probe'.format(self.folder)
		self._make(folder)

		# collect training days
		days = self._daze(validation=False)

		# begin matrices and scores
		matrices = []

		# for each day
		for month, day in days:

			# get training matrix for particular scan
			parameters = [(month, day)], (scan, scan + 1)
			matrix, data, constraints = self.materialize(*parameters)

			# add to matrices
			matrices.append(matrix)

		# set default variable
		variables = variables or self.features[:-1]

		# create block of all inputs
		block = numpy.vstack(matrices)

		# get the median of the matrix, as well as the lower and upper bounds
		median = numpy.median(block, axis=0)
		lower = numpy.percentile(block, 50 - margin, axis=0)
		upper = numpy.percentile(block, 50 + margin, axis=0)

		# for each variable
		for variable in variables:

			# get the index of the feature
			index = self.features.index(variable)

			# create percentiles
			percentiles = numpy.array([numpy.percentile(block[:, index], percentile) for percentile in range(101)])

			# create probe matrix from medians for all features but variable in question
			probe = numpy.array([median] * 101)
			probe[:, index] = percentiles

			# predict values from the model
			prediction, deviation, _, _ = self.predict(probe)

			# for each dataset
			for day, matrix in zip(days, matrices):

				# begin plot
				pyplot.clf()
				pyplot.figure(figsize=(8, 8))

				# create title
				date = '{}m{}{}'.format(self.year, self._pad(day[0]), self._pad(day[1]))
				title = '{} {} at medians, +\- {}th percentile'.format(date, variable, margin)
				pyplot.title(title)

				# add labels
				pyplot.xlabel(variable)
				pyplot.ylabel(self.features[-1])

				# set limits
				minimum = block[:, -1].min(axis=0)
				maximum = block[:, -1].max(axis=0)
				buffer = (maximum - minimum) * 0.05
				pyplot.ylim(*self.limits['scale'])

				# Sample data
				abscissa = percentiles
				ordinate = prediction

				# create line plots
				pyplot.plot(abscissa, ordinate, 'bo-')
				pyplot.plot(abscissa, ordinate - deviation, 'b--')
				pyplot.plot(abscissa, ordinate + deviation, 'b--')

				# copy matrix
				xerox = matrix.copy()

				# for each feature
				for position, feature in enumerate(self.features[:-1]):

					# if feature not varible of interest
					if feature != variable:

						# subset matrix
						mask = (xerox[:, position] >= lower[position]) & (xerox[:, position] <= upper[position])
						xerox = xerox[mask]

				# calculate differences from median for color scale
				difference = abs(xerox - median)
				difference[:, index] = 0
				difference = difference.sum(axis=1)

				# add truth points within median margins
				pyplot.scatter(xerox[:, index], xerox[:, -1], c=difference, cmap='gray', s=25, marker='x')

				# save plot
				destination = '{}/probe_{}_{}_{}_{}.png'.format(folder, date, self.session, self.tag, variable)
				pyplot.savefig(destination)
				pyplot.clf()

		return None

	def radiate(self, scan=10, margin=1, maps=True):
		"""Troubleshooot abi data by plotting tempo vs abi.

		Arguments:
			scan: int, scan number
			margin: float, percentile margin
			maps: boolean, plot maps?

		Returns:
			None
		"""

		# make troubleshooting folder
		folder = '{}/radiation'.format(self.folder)
		self._make(folder)

		# get combined data set
		_, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1))
		valids = data

		# get input abi data
		baseline = valids['abi_radiance']
		wavelengths = valids['abi_wavelengths'][0]

		# get abi bands
		east = valids['abi_band1_east']
		eastii = valids['abi_band2_east']
		west = valids['abi_band1_west']
		westii = valids['abi_band2_west']

		# set up conversion, W/m2 um sr = ph/s cm2 nm sr * 10^7 hc / wl(m) = 10^13 hc / wl(um)
		planck = 6.626e-34
		light = 2.998e8
		units = 1e13
		factor = units * planck * light

		# apply conversion
		east = east * factor / wavelengths[0]
		west = west * factor / wavelengths[0]
		eastii = eastii * factor / wavelengths[1]
		westii = westii * factor / wavelengths[1]

		# stack bands
		easts = numpy.hstack([east, eastii])
		wests = numpy.hstack([west, westii])

		# get geocoordinates
		latitude = valids['latitude']
		longitude = valids['longitude']
		corners = valids['latitude_bounds']
		cornersii = valids['longitude_bounds']

		# get angles
		zenith = valids['solar_zenith_angle']
		view = valids['viewing_zenith_angle']
		azimuth = valids['relative_azimuth_angle']

		# add 0 for nans
		mask = self._mask(baseline)
		baseline = numpy.where(mask, baseline, 0)

		# make mask
		mask = self._mask(easts)
		easts = numpy.where(mask, easts, 0)

		# make mask
		mask = self._mask(wests)
		wests = numpy.where(mask, wests, 0)

		# set scales
		scales = [(60, 460), (0, 600), (40, 240), (0, 30)]
		scales += [(5, 40), (0, 10), (0, 3), (1, 6)]
		scales += [(3, 15), (5, 25), (12, 82), (20, 60)]
		scales += [(20, 140), (30, 150), (30, 160), (40, 120)]

		# for band
		for band in (0, 1):

			# for east and west
			modes = ('east', 'west')
			arrays = (easts, wests)
			for array, mode in zip(arrays, modes):

				# create plot
				pyplot.clf()
				_ = pyplot.figure(figsize=(8, 8))

				# make title and labels
				pyplot.title('Band {}, mode: {}'.format(band, mode))
				pyplot.xlabel('ABI')
				pyplot.ylabel('TEMPO')

				# plot
				pyplot.plot(baseline[:, band], array[:, band], 'g.', markersize=0.1)

				# savefigure
				pyplot.savefig('{}/band_{}_mode_{}.png'.format(folderii, band, mode))

				# clear figure
				pyplot.clf()

		# for band
		for band in (0, 1):

			# for east and west
			modes = ('east', 'west')
			arrays = (easts, wests)
			for array, mode in zip(arrays, modes):

				# for each angle
				names = ('solar_zenith', 'viewing_zenith', 'relative_azimuth')
				angles = (zenith, view, azimuth)
				for name, angle in zip(names, angles):

					# get difference
					difference = baseline[:, band] - array[:, band]

					# create plot
					pyplot.clf()
					pyplot.figure(figsize=(8, 8))

					# make title
					pyplot.title('Band {}, mode: {}, {}'.format(band, mode, name))
					pyplot.xlabel(name)
					pyplot.ylabel('ABI - TEMPO')

					# plot
					pyplot.plot(angle.squeeze(), difference, 'g.', markersize=0.1)

					# savefigure
					pyplot.savefig('{}/band_{}_mode_{}_{}.png'.format(folderii, band, mode, name))

					# clear figure
					pyplot.clf()

		# if plotting maps:
		if maps:

			# for band
			for band in (0, 1):

				# begin graphs
				graphs = []

				# create destination
				destination = '{}/abi_band_{}_east_diff.png'.format(folder, band)

				# construct percent difference plot
				title = 'TEMPO - ABI band {} east\n'.format(band)
				unit = '$W / m^2 um sr$'
				bounds = [(-125, -60), (15, 55)]

				# construct differece
				difference = baseline[:, band] - easts[:, band]

				# construct scale
				minimum = numpy.percentile(difference, margin)
				maximum = numpy.percentile(difference, 100 - margin)
				absolute = max([abs(minimum), abs(maximum)])
				scale = (-absolute, absolute)
				scale = (-50, 50)
				labels = ['', '']
				options = {'polygons': True, 'corners': [corners, cornersii], 'bar': True}
				parameters = (longitude, latitude, difference, bounds, scale, 'coolwarm', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# create plot
				self._paint(destination, graphs)

			# for band
			for band in (0, 1):

				# for east and west
				modes = ('east', 'west')
				arrays = (easts, wests)

				# for each array
				for array, mode in zip(arrays, modes):

					# begin graphs
					graphs = []

					# create destination
					destination = '{}/abi_band_{}_{}.png'.format(folder, band, mode)

					# construct percent difference plot
					title = 'ABI band {} {}\n'.format(mode, band)
					unit = '$W / m^2 um sr$'
					bounds = [(-125, -60), (15, 55)]
					minimum = numpy.percentile(array[:, band], margin)
					maximum = numpy.percentile(array[:, band], 100 - margin)
					scale = (minimum, maximum)
					scale = scales[band]
					labels = ['', '']
					options = {'polygons': True, 'corners': [corners, cornersii], 'bar': True}
					parameters = (longitude, latitude, array[:, band], bounds, scale, 'plasma', title, unit, labels)
					graph = self._graph(*parameters, **options)
					graphs.append(graph)

					# create plot
					self._paint(destination, graphs)

			# for each wavelength
			for wave in range(16):

				# begin graphs
				graphs = []

				# create destination
				destination = '{}/abi_wavelength_{}.png'.format(folder, self._pad(wave))

				# construct percent difference plot
				title = 'ABI wavelength {:.2f} um\n'.format(wavelengths[wave])
				unit = '$W / m^2 um sr$'
				bounds = [(-125, -60), (15, 55)]
				minimum = numpy.percentile(baseline[:, wave], margin)
				maximum = numpy.percentile(baseline[:, wave], 100 - margin)
				scale = (minimum, maximum)
				scale = scales[wave]
				labels = ['', '']
				options = {'polygons': True, 'corners': [corners, cornersii], 'bar': True}
				parameters = (longitude, latitude, baseline[:, wave], bounds, scale, 'plasma', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# create plot
				self._paint(destination, graphs)

		return None

	def reflect(self, scan=10, wave=620):
		"""Make maps of short wave radiaation and compare with tempo reflectance.

		Arguments:
			scan: int, scan number
			wave: float, the wavelength of interest

		Returns:
			None
		"""

		# make folder
		folder = '{}/reflect'.format(self.folder)
		self._make(folder)

		# get data set
		_, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1), exclude=True)

		# apply constraints
		data = self._constrain(data, constraints)

		# get hydra fro reflectances
		hydra = Hydra('TEMPO_RAD_Smooth/{}'.format(self.stub))

		# begin collection of reflectances
		reflectances = []

		# for each granule
		for granule in range(1, 10):

			# get label an ingest
			label = self._label(scan, granule)
			hydra.ingest(label)

			# get wavelength
			wavelength = hydra.grab('reflectance_wavelengths')
			index = wavelength.tolist().index(wave)

			# get reflectance and subset wavelength
			reflectance = hydra.grab('smoothed_reflectances')
			reflectance = reflectance[:, :, index].reshape(-1, 1)
			reflectances.append(reflectance)

		# form array and apply constraints
		reflectances = numpy.vstack(reflectances)
		reflectances = reflectances[constraints]

		# get geo coordinates
		latitude = data['latitude']
		longitude = data['longitude']
		corners = data['latitude_bounds']
		cornersii = data['longitude_bounds']

		# get short wave data
		shorts = data['short_wave']

		# set scales and units
		scales = {'short': (100, 1100), 'reflectance': (0, 0.15)}
		units = {'short': '$ W/m^2 $', 'reflectance': '-'}

		# set names
		names = {'short': 'GMAO short wave', 'reflectance': 'TEMPO reflectance {}nm'.format(wave)}

		# set arrays
		arrays = {'short': shorts, 'reflectance': reflectances}

		# for each variable
		for variable in names.keys():

			# begin graphs
			graphs = []

			# create destination
			destination = '{}/{}_{}_{}.png'.format(folder, self.date, self._pad(scan), variable)

			# construct percent difference plot
			title = '{} {} scan {}\n'.format(names[variable], self.date, self._pad(scan))
			unit = units[variable]
			bounds = [(-125, -60), (20, 50)]

			# create valid mask
			array = arrays[variable]
			mask = self._mask(array).squeeze()

			# set gradient
			gradient = 'plasma'

			# get scale
			scale = scales[variable]

			# make plot
			labels = ['', '']
			options = {'polygons': True, 'corners': [corners[mask], cornersii[mask]], 'bar': True}
			parameters = (longitude[mask], latitude[mask], array[mask], bounds, scale, gradient, title, unit, labels)
			graph = self._graph(*parameters, **options)
			graphs.append(graph)

			# create plot
			self._paint(destination, graphs)

		# calculate regression line and r^2 score
		abscissa = arrays['short'].squeeze()
		ordinate = arrays['reflectance'].squeeze()
		slope, intercept, _, _, _ = scipy.stats.linregress(abscissa, ordinate)
		score = r2_score(abscissa, ordinate)

		print(slope, intercept)

		# begin scatter plot
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# set title and labels
		title = '{} vs {} ( $R^2$ = {:.2f} )'.format(names['short'], names['reflectance'], score)
		pyplot.title(title)
		pyplot.xlabel(names['short'])
		pyplot.ylabel(names['reflectance'])
		pyplot.xlim(*scales['short'])
		pyplot.ylim(*scales['reflectance'])

		# plot prediction and deviations
		pyplot.plot(abscissa, ordinate, 'g.', markersize=0.1)

		# plot regression line
		line = [slope * point + intercept for point in scales['short']]
		pyplot.plot(scales['short'], line, 'k--')

		# save
		destination = '{}/Scatter_plot.png'.format(folder)
		pyplot.savefig(destination)
		pyplot.clf()

		return None

	def repair(self, scan=None, baseline=False):
		"""Repair input files by adding new fields.

		Arguments:
			baseline: boolean, redo baseline data?

		Returns:
			None
		"""

		# create smoothed data folders
		for folder in ('TEMPO_RAD_Smooth',):

			# create destination folders
			self._make('{}/{}'.format(self.sink, folder))
			self._make('{}/{}/{}'.format(self.sink, folder, self.year))
			self._make('{}/{}/{}/{}'.format(self.sink, folder, self.year, self._pad(self.month)))
			self._make('{}/{}/{}'.format(self.sink, folder, self.stub))

		# get all tempo l1b files
		hydraii = Hydra('{}/TEMPO_RAD_L1/{}'.format(self.sink, self.stub))

		# get all inputs files
		hydra = Hydra('{}/GPP_Inputs/{}'.format(self.sink, self.stub))
		paths = hydra.paths

		# if scan number given
		if scan:

			# create labels and subset paths
			label = self._label(scan)
			paths = [path for path in paths if label in path]

		# for each path
		for path in paths:

			# ingest the path
			hydra.ingest(path)

			# extract data
			data = hydra.extract()

			# get date from path
			search = '[0-9]{8}T[0-9]{6}Z'
			date = re.findall(search, path)[0]

			# get granule from path
			search = 'S[0-9]{3}G[0-9]{2}'
			granule = re.findall(search, path)[0]

			# begin data with time and granule information
			data.update({'date': numpy.array([int(date[0:8])]), 'time': numpy.array([int(date[9:15])])})
			data.update({'scan': numpy.array([int(granule[1:4])]), 'granule': numpy.array([int(granule[5:7])])})

			# get shape
			shape = data['latitude'].shape

			# get irradiance qualifiers
			qualifier = data['max_irr_quality_high']
			qualifierii = data['max_irr_quality_low']

			# if qualifiers are 1-d
			if len(qualifier.shape) == 1:

				# tile qualifiers to match shape
				qualifier = numpy.tile(qualifier, (shape[0], 1))
				qualifierii = numpy.tile(qualifierii, (shape[0], 1))

				# update data
				data.update({'max_irr_quality_high': qualifier, 'max_irr_quality_low': qualifierii})

			# try to
			try:

				# access l1b data
				hydraii.ingest(date)

				# get the seconds since 1-6-1980
				seconds = hydraii.grab('time')

				# get the longitude
				longitude = data['longitude']

				# create local time, and add to data
				time = self._localize(seconds, longitude)
				data.update({'local_time': time})

				# for each band
				for band in (1, 2):

					# for each cardinal
					for cardinal in ('east', 'west'):

						# add abi data
						search = 'ABI_Band_{}_{}'.format(band, cardinal.capitalize())
						data.update({'abi_band{}_{}'.format(band, cardinal): hydraii.grab(search)})

			# unless TEMPOL1B not available
			except IndexError:

				# print status
				self._print('TEMPO_L1_RAD not available')

			# if relative azimuth in keys
			if 'relative_azimuth' in data.keys():

				# replace relative azimuth with angle name
				data['relative_azimuth_angle'] = data['relative_azimuth']
				del data['relative_azimuth']

			# if redoing baseline
			if baseline:

				# redo baseline data
				latitude = hydra.grab('latitude')
				longitude = hydra.grab('longitude')
				baselines, wavelengths = self.baseline(path, latitude, longitude)
				baselines = numpy.where(baselines >= 0, baselines, self.fill)
				data.update({'abi_radiance': baselines, 'abi_wavelengths': wavelengths})

			# add synthetic data
			synthetics = Hydra('{}/Synthetic_GPP/{}'.format(self.sink, self.stub))
			synthetics.ingest(date)
			fields = ['short_wave', 'mcd43_bands', 'gpp_onefluxnet', 'onefluxnet_deviation']
			data.update({field: synthetics.grab(field) for field in fields})

			# add modis bands
			pairs = [(620, 670), (841, 876), (459, 479), (545, 565)]
			pairs += [(1230, 1250), (1628, 1652), (2105, 2155)]
			data.update({'mcd43_wavelengths': numpy.array(pairs)})

			# add mosaic data
			mosaics = Hydra('{}/Mosaic_GPP/{}'.format(self.sink, self.stub))
			mosaics.ingest(date)
			data.update({'gpp_mosaic': mosaics.grab('gpp_mosaic')})

			# fill values
			data = {name: self._fill(array) for name, array in data.items()}

			# try to
			try:

				# get smoothed reflectances
				fields = ('smoothed_reflectances', 'reflectance_wavelengths')
				dataii = {field: data[field] for field in fields}

				# for each field
				for field in fields:

					# excise from data
					del data[field]

				# stash
				self.spawn(path.replace('GPP_Inputs', 'TEMPO_RAD_Smooth'), dataii)

			# unless not found
			except KeyError:

				# pass
				self._print('smoothed reflectances not found')

			# restash file
			self.spawn(path, data)

		return None

	def run(self, predictors, targets, validation):
		"""Define and run the Bayesian model.

		Arguments:
			predictors: numpy array, inputs matrix
			targets: numpy array, outputs vector
			validation: list of numpy array, the validation set
			epochs: number of training epochs

		Returns:
			None
		"""

		# define abbreviations
		optimizers = tensorflow_addons.optimizers
		layers = tensorflow_probability.layers
		backend = tensorflow.keras.backend

		# unpack shape
		samples, features = predictors.shape

		# define batch size and kernel weight
		batch = self.training['batch']
		weight = batch / samples

		# if not using weights
		if not self.architecture['weight']:

			# remove weight
			weight = 0

		# set up bayesian layers
		prior = self._prioritize
		posterior = self._post

		# set activation functions if not plain strings
		activations = {'leaky': tensorflow.keras.layers.LeakyReLU(alpha=0.3)}
		activations.update({'softsign': tensorflow.keras.activations.softsign})
		activations.update({'bent': self._bend})

		# set up beginning input layer
		input = tensorflow.keras.Input(shape=(features))
		output = input

		# for each hidden layer
		for hiddens, activation in zip(self.architecture['hiddens'], self.architecture['activations']):

			# set activation
			activation = activations.get(activation, activation)

			# if using variational
			if self.architecture['bayesian']:

				# add variational layer
				options = {'kl_weight': weight, 'activation': activation}
				output = layers.DenseVariational(hiddens, posterior, prior, **options)(output)

			# otherwise
			else:

				# add normal dense layer
				options = {'activation': activation}
				output = tensorflow.keras.layers.Dense(hiddens, **options)(output)

			# if dropping out
			if self.architecture['dropout'] > 0:

				# add dropout layer
				output = tensorflow.keras.layers.Dropout(rate=self.architecture['dropout'])(output)

		# if bayesian
		if self.architecture['bayesian']:

			# set activation
			activation = activations.get(self.architecture['final'], self.architecture['final'])

			# add final mean, deviation layers
			output = tensorflow.keras.layers.Dense(units=2, activation=activation)(output)
			output = layers.IndependentNormal(1)(output)

		# otherwise
		else:

			# set activation
			activation = activations.get(self.architecture['final'], self.architecture['final'])

			# add normal output
			output = tensorflow.keras.layers.Dense(units=1, activation=activation)(output)

		# if using cyclic learning rate
		if self.training['cyclic']:

			# define cyclical learning rate
			def scaling(timepoint): return 1 / (2 ** (timepoint - 1))
			learning = self.training['learning']
			options = {'initial_learning_rate': float(learning[0])}
			options.update({'maximal_learning_rate': float(learning[1])})
			options.update({'scale_fn': scaling, 'step_size': 5 * math.ceil(samples / batch)})
			learning = optimizers.CyclicalLearningRate(**options)

		# otherwise
		else:

			# use constant learning rate from initial
			learning = float(self.training['learning'][0])

		# set loss functions
		losses = {'custom': self._lose, 'bayes': self._like}
		loss = losses.get(self.training['loss'], self.training['loss'])

		# begin learning rate accumulation
		rates = []

		# crate callback to log and store learning rates
		monitoring = lambda batch, logs: rates.append(backend.get_value(model.optimizer.learning_rate))
		monitor = tensorflow.keras.callbacks.LambdaCallback(on_batch_end=monitoring)

		# build model
		epochs = self.training['epochs']
		metrics = ['mae', 'mse', 'acc', 'mape']
		model = tensorflow.keras.Model(input, output, name="Bayesian")
		model.compile(loss=loss, optimizer=tensorflow.optimizers.Adam(learning), metrics=metrics)
		options = {'epochs': epochs, 'batch_size': batch, 'validation_data': validation}
		options.update({'callbacks': [monitor], 'shuffle': True})
		history = model.fit(predictors, targets, **options)
		model.save('{}/TEMPO_GPP_Model'.format(self.folder))

		# clear session
		tensorflow.keras.backend.clear_session()

		# store history
		self._dump(history.history, '{}/history.json'.format(self.folder))

		# store learning rates
		rates = numpy.array(rates).astype('float32').tolist()
		learn = {'rates': rates, 'samples': samples}
		self._dump(learn, '{}/rates.json'.format(self.folder))

		# analyze history
		self.chronicle()

		return None

	def schedule(self, scans=(1, 19), samples=10, dot=1.0):
		"""Create scatter plots of predictions versus truths at local hours.

		Arguments:
			scans: tuple of ints, the scans
			samples: samples upper limit
			dot: float, pixel size of each dot

		Returns:
			None
		"""

		# make plots folder
		folder = '{}/schedule'.format(self.folder)
		self._make(folder)

		# get the matrix for all scans and constrain data
		parameters = ([(self.month, self.day)], scans)
		matrix, data, constraints = self.materialize(*parameters, constrain=True, exclude=True)
		data = self._constrain(data, constraints)

		# get predictions
		predictions, _, truths, _ = self.predict(matrix)

		# get the local times
		time = data['local_time'].squeeze()

		# for every hour
		for hour in range(0, 24):

			# create mask for +/- 30 min
			mask = ((time >= hour - 0.5) & (time <= hour + 0.5))
			mask = mask & (numpy.isfinite(predictions)) & (numpy.isfinite(truths))

			# subset the data
			prediction = predictions[mask]
			truth = truths[mask]

			# calculate r2 score
			score = r2_score(truth, prediction)

			# calculate regression line
			slope, intercept, _, _, _ = scipy.stats.linregress(truth, prediction)

			# set scaling bracket
			bracket = (0, 35)
			mesh, meshii, counts = self._bin(truth, prediction, samples, bracket)

			# begin graphs
			graphs = []

			# construct samples title depending on instances
			platform = 'Onefluxnet'
			date = self.date
			formats = (platform, date, hour, score)
			title = 'Prediction vs {}\n{} {}:00 +/- 30 min ( $R^2$ = {:.2f} )'.format(*formats)

			# construct samples graph
			unit = 'samples'
			labels = ['{} truth'.format(self.target), '{} prediction'.format(self.target)]
			boundsii = [bracket, bracket]
			scaleiii = (1, samples)
			points = bracket
			regression = [intercept + point * slope for point in points]
			line = (points, points, 'k-')
			lineii = (points, regression, 'k--')
			lines = [line, lineii]
			options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': dot, 'polygons': False}
			graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
			graphs.append(graph)

			# create graph
			margins = {'bottom': 0.18, 'top': 0.90, 'left': 0.12, 'right': 0.93}
			destination = '{}/Local_hour_scatter_plot_{}.png'.format(folder, self._pad(hour))
			self._paint(destination, graphs, size=(8, 8), space=margins)

		return None

	def scope(self, scans=(6, 14), radius=0.1):
		"""Scope out the sites where there are valid tempo pixels close by.

		Arguments:
			scans: tuple of ints, the scans
			radius: float, the distance tolerance in degrees.

		Returns:
			None
		"""

		# set up reports folder
		folder = '{}/reports'.format(self.folder)
		self._make(folder)

		# begin collections
		collection = {site: [] for site in self.catalog.keys()}

		# for each scan
		for scan in range(*scans):

			# get data
			_, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1))
			data = self._constrain(data, constraints)

			# for each site
			for site in collection.keys():

				# get the coordiates
				latitude = self.catalog[site]['latitude']
				longitude = self.catalog[site]['longitude']

				# find the closest temp pixel
				pixel = self._pin([latitude, longitude], [data['latitude'], data['longitude']])[0]

				# calculate the squared distance
				square = (latitude - data['latitude'][pixel]) ** 2 + (longitude - data['longitude'][pixel]) ** 2

				# if less than radius
				if square < radius ** 2:

					# add scan to site lists
					collection[site].append(scan)

		# determine total number of scans
		total = scans[1] - scans[0]

		# begin site lists
		sites = {number: [] for number in range(total + 1)}

		# for each site
		for site in collection.keys():

			# add to siteslist based on number of pixels
			length = len(collection[site])
			sites[length].append(site)

		# begin report
		report = ['site list of tempo pixels within {}'.format(radius)]
		report.append('date: {}, scans: {}'.format(self.date, scans))

		# for each length
		for length in sites.keys():

			# add list
			report.append('{} sites:'.format(length))
			report.append(str(sites[length]))

		# dump report
		formats = (folder, self.date, self._pad(scans[0]), self._pad(scans[1]))
		self._jot(report, '{}/valid_sites_within_proximity_{}_{}_{}.txt'.format(*formats))

		return None

	def seek(self, scans=(6, 14)):
		"""Follow the diurnal gpp changes for specific landforms.

		Arguments
			scans: tuple of ints, the scan numbers

		Returns:
			None
		"""

		# make plots folder
		folder = '{}/seek'.format(self.folder)
		self._make(folder)

		# get landforms dictionary
		forms = self._acquire('Land_Types/land.yaml')

		# begin datasets and matrices
		lands = []
		matrices = []
		latitudes = []
		longitudes = []
		corners = []
		cornersii = []
		times = []
		truths = []

		# for each scan
		for scan in range(*scans):

			# get training matrix for particular scan
			parameters = ([(self.month, self.day)], (scan, scan + 1))
			matrix, datum, constraints = self.materialize(*parameters, constrain=True, exclude=True)

			# apply constraints to data
			datum = self._constrain(datum, constraints)

			# get mirror and track indices
			mirror = datum['mirror'].squeeze().astype(int)
			track = datum['track'].squeeze().astype(int)

			# create land type data
			land = numpy.ones((self.mirror, self.track)) * self.fill
			land[mirror, track] = datum['land_type'].squeeze()
			lands.append(land)

			# create latitude data
			latitude = numpy.ones((self.mirror, self.track)) * self.fill
			latitude[mirror, track] = datum['latitude'].squeeze()
			latitudes.append(latitude)

			# create longitude data
			longitude = numpy.ones((self.mirror, self.track)) * self.fill
			longitude[mirror, track] = datum['longitude'].squeeze()
			longitudes.append(longitude)

			# create local time data
			time = numpy.ones((self.mirror, self.track)) * self.fill
			time[mirror, track] = datum['local_time'].squeeze()
			times.append(time)

			# create fluxsat truth data
			truth = numpy.ones((self.mirror, self.track)) * self.fill
			truth[mirror, track] = datum['gpp_fluxsat'].squeeze()
			truths.append(truth)

			# create latitude bounds data
			corner = numpy.ones((self.mirror, self.track, 4)) * self.fill
			corner[mirror, track] = datum['latitude_bounds'].squeeze()
			corners.append(corner)

			# create longitude bounds data
			cornerii = numpy.ones((self.mirror, self.track, 4)) * self.fill
			cornerii[mirror, track] = datum['longitude_bounds'].squeeze()
			cornersii.append(cornerii)

			# create matrix panel
			panel = numpy.ones((self.mirror, self.track, matrix.shape[1])) * self.fill
			panel[mirror, track] = matrix
			matrices.append(panel)

		# create land array and mask for locations with all timepoints, using mode to reduce
		lands = numpy.array(lands).astype(int)
		mask = (abs(lands.sum(axis=0)) < 200)
		lands = numpy.array([land[mask] for land in lands])
		lands = scipy.stats.mode(lands, axis=0)[0]

		# apply to coordinates and average
		latitudes = numpy.array([latitude[mask] for latitude in latitudes]).mean(axis=0)
		longitudes = numpy.array([longitude[mask] for longitude in longitudes]).mean(axis=0)
		truths = numpy.array([truth[mask] for truth in truths]).mean(axis=0)
		corners = numpy.array([corner[mask] for corner in corners]).mean(axis=0)
		cornersii = numpy.array([cornerii[mask] for cornerii in cornersii]).mean(axis=0)

		# apply to all matrices and local times
		matrices = numpy.array([matrix[mask] for matrix in matrices])
		times = numpy.array([time[mask] for time in times])

		# get all predictions and take mean
		predictions = numpy.array([self.predict(matrix)[0] for matrix in matrices])
		peaks = numpy.array(predictions).max(axis=0)

		# for each site
		for code, form in forms.items():

			# construct landform mask & finite sample mask
			mask = (lands == code) & numpy.isfinite(peaks)

			# if finite number of sample
			if mask.sum() > 10:

				# apply mask to arrays
				latitude = latitudes[mask]
				longitude = longitudes[mask]
				corner = corners[mask]
				cornerii = cornersii[mask]
				peak = peaks[mask]
				truth = truths[mask]

				# perform clustering
				matrix = numpy.hstack([longitude.reshape(-1, 1), latitude.reshape(-1, 1)])
				clusterer = KMeans(n_clusters=2, random_state=42)
				clusterer.fit(matrix)

				# extract labels and centroids
				centroids = clusterer.cluster_centers_
				clusters = clusterer.labels_

				# begin graphs
				graphs = []

				# for each cluster
				for cluster, centroid in zip((0, 1), centroids):

					# get the subset that has the cluster label
					maskii = (clusters == cluster)

					# set up longitude percentile meridians
					percentiles = (0, 33, 66, 100)
					meridians = [numpy.percentile(longitude[maskii], percentile) for percentile in percentiles]

					# begin abscissas and ordinates
					abscissas = []
					ordinates = []
					spreads = []
					fluxes = []

					# for each pair of meridians
					for west, east in zip(meridians[:-1], meridians[1:]):

						# get mask for the longitudes within pair
						maskiii = (longitude[maskii] >= west) & (longitude[maskii] <= east)

						# begin averages
						averages = []
						deviations = []
						hours = []

						# for each scan
						for index in range(scans[1] - scans[0]):

							# add average prediction
							average = (predictions[index][mask][maskii][maskiii]).mean()
							deviation = (predictions[index][mask][maskii][maskiii]).std()
							hour = (times[index][mask][maskii][maskiii]).mean()

							# append
							averages.append(average)
							deviations.append(deviation)
							hours.append(hour)

						# add to absiccsas and ordiantes
						abscissas.append(numpy.array(hours))
						ordinates.append(numpy.array(averages))
						spreads.append(numpy.array(deviations))

						# append mean truth
						flux = truth[maskii][maskiii].mean()
						fluxes.append(flux)

					# begin GPP truth plot
					title = 'peak {} {} {} ( cluster {} )\n'.format(self.target, self.date, form, cluster)
					unit = self.unit
					bounds = [(-125, -60), (15, 55)]
					labels = ['', '']
					scale = self.limits['scale']

					# create meridian lines and centroid
					# lines = [([meridian, meridian], bounds[1], 'k--') for meridian in meridians]
					lines = [([centroid[0], centroid[0]], [centroid[1], centroid[1]], 'bx')]

					# make cluster graph
					options = {'polygons': True, 'corners': [corner[maskii], cornerii[maskii]], 'lines': lines}
					options.update({'marker': 10})
					parameters = [longitude[maskii], latitude[maskii], peak[maskii], bounds, scale]
					parameters += ['plasma', title, unit, labels]
					graph = self._graph(*parameters, **options)
					graphs.append(graph)

					# begin diurnal plot
					formats = [self.date, self.target, form, int(maskii.sum())]
					title = 'TEMPO {}, diurnal {}, ( {} )\n( {} samples )'.format(*formats)
					unit = ''
					blank = numpy.array([0])
					labels = ['local time', '{} ( {} )'.format(self.target, self.unit)]
					bounds = [(7, 17), self.limits['scale']]
					scale = (0, 10)

					# construct gpp lines
					colors = ('ro-', 'go-', 'bo-')
					zipper = zip(abscissas, ordinates, colors)
					lines = [(abscissa, ordinate, color) for abscissa, ordinate, color in zipper]

					# construct deviation lines
					colors = ('r--', 'g--', 'b--')
					zipper = zip(abscissas, ordinates, spreads, colors)
					lines += [(abscissa, ordinate - spread, color) for abscissa, ordinate, spread, color in zipper]
					zipper = zip(abscissas, ordinates, spreads, colors)
					lines += [(abscissa, ordinate + spread, color) for abscissa, ordinate, spread, color in zipper]

					# construct truth lines
					colors = ('rx', 'gx', 'bx')
					zipper = zip(fluxes, colors)
					lines += [([bounds[0][0] + 0.5, bounds[0][0] + 0.5], [flux, flux], color) for flux, color in zipper]

					# make plot
					options = {'globe': False, 'lines': lines, 'polygons': False, 'colorbar': False, 'marker': 10}
					graph = self._graph(blank, blank, blank, bounds, scale, 'plasma', title, unit, labels, **options)
					graphs.append(graph)

				# paint graphs
				destination = '{}/Diurnal_Plot_{}_{}_{}_{}.png'.format(folder, self.date, self.session, self.tag, form)
				self._paint(destination, graphs)

		return None

	def sense(self, scan=99, isolate=False):
		"""Perform sensitivity study, by removal of each feature.

		Arguments:
			scan: int, scan number
			isolate: perfrom isolation study?

		Returns:
			None
		"""

		# set mode
		mode = 'sensitivity'

		# if isolating
		if isolate:

			# set mode to isolation
			mode = 'isolation'

		# make reports folder
		folder = '{}/reports'.format(self.folder)
		self._make(folder)

		# get training matrix for particular scan
		matrix, _, _ = self.materialize([(self.month, self.day)], (scan, scan + 1))

		# begin report and scores collection
		report = ['{} report for {}\n'.format(mode.capitalize(), folder)]
		scores = []

		# rename last feature from target to all predictors
		features = self.features.copy()
		features[-1] = 'all predictors'

		# for each feature
		for feature in range(matrix.shape[1]):

			# copy inputs
			xerox = matrix.copy()

			# if feature is valid
			if feature < matrix.shape[1] - 2:

				# if mode is isolation
				if isolate:

					# replace all features but index with median
					xerox[:, :-1] = numpy.array([numpy.percentile(matrix[:, :-1], 50, axis=0)] * matrix.shape[0])
					xerox[:, feature] = matrix[:, feature]

				# otherwise
				else:

					# replace feature index with median
					xerox[:, feature] = numpy.percentile(matrix[:, feature], 50)

			# make predictions
			prediction, deviation, truth, score = self.predict(xerox)

			# print r^2 score
			pair = (features[feature], score)
			self._print('{}: {}'.format(*pair))

			# add scores
			scores.append(pair)

		# sort scores, reversing for isolation
		scores.sort(key=lambda pair: pair[1], reverse=isolate)

		# for each score
		for pair in scores:

			# add to report
			report.append('{}: {}'.format(*pair))

		# jot to reports folder
		self._jot(report, '{}/{}.txt'.format(folder, mode))

		return None

	def smooth(self, high, low, shorts, longs, highii, lowii, shortsii, longsii, zenith, coefficients):
		"""Smooth and decompose reflectances into PCA components:

		Arguments:
			high: numpy array, radiance high frequencies
			low: numpy array, radiance low frequencies
			shorts: numpy array, radiance short wavelengths
			longs: numpy array, radiance log wavelengths
			highii: numpy array, irradiance high
			lowii: numpy array, irradiance low
			shortsii: numpy array, irradiance short wavelengths
			longsii: numpy array, irradiance long wavelengths
			zenith: numpy array, radiance solar zenith angle
			coefficients: numpy array, pca coefficents

		Returns:
			numpy array, the reflectance pcas
		"""

		# set gridding attributes
		nodes = 5
		width = 2
		widthii = 1 / nodes

		# construct wavelength grid
		grid = numpy.array(range(self.limits['high'][0], self.limits['high'][1] + 1))
		gridii = numpy.array(range(self.limits['low'][0], self.limits['low'][1] + 1))

		# construct finely spaced grids ( 0.2 nm )
		fine = numpy.array(range((self.limits['high'][0] - 1) * nodes, self.limits['high'][1] * nodes + 1)) / nodes
		fineii = numpy.array(range((self.limits['low'][0] - 1) * nodes, self.limits['low'][1] * nodes + 1)) / nodes

		# construct boxcar
		boxcar = numpy.ones(int(width / widthii)) / int(width / widthii)

		# for each mirror step in reflectance data
		reflectance = []
		reflectanceii = []
		for step in range(high.shape[0]):

			# if even number
			if step % 10 == 0:

				# print status
				print('interpolating {}...'.format(step))

			# for each scan
			panel = []
			panelii = []
			for scan in range(high.shape[1]):

				# set pixel
				pixel = (step, scan)

				# create masks for valid data
				mask = self._mask(highii[scan])
				maskii = self._mask(lowii[scan])

				# interpolate irradiance onto radiance wavelengths
				options = {'kind': 'linear', 'fill_value': 'extrapolate', 'bounds_error': False}
				interpolator = scipy.interpolate.interp1d(shortsii[scan][mask], highii[scan][mask], **options)
				irradiance = interpolator(shorts[scan])
				interpolatorii = scipy.interpolate.interp1d(longsii[scan][maskii], lowii[scan][maskii], **options)
				irradianceii = interpolatorii(longs[scan])

				# divide by irradiance
				ratio = high[pixel] / irradiance
				ratioii = low[pixel] / irradianceii

				# get mask for valid data
				mask = self._mask(ratio)
				maskii = self._mask(ratioii)

				# interpolate to fine grid
				interpolator = scipy.interpolate.interp1d(shorts[scan][mask], ratio[mask], **options)
				interpolation = interpolator(fine)
				interpolatorii = scipy.interpolate.interp1d(longs[scan][maskii], ratioii[maskii], **options)
				interpolationii = interpolatorii(fineii)

				# convolve onto course grid
				convolution = scipy.signal.convolve(interpolation, boxcar, mode='same')
				convolutionii = scipy.signal.convolve(interpolationii, boxcar, mode='same')

				# get indices of wavelengths that also in the grid
				indices = numpy.where(numpy.isin(fine, grid))
				indicesii = numpy.where(numpy.isin(fineii, gridii))

				# add to reflectances
				panel.append(numpy.array(convolution)[indices])
				panelii.append(numpy.array(convolutionii)[indicesii])

			# append panels
			reflectance.append(panel)
			reflectanceii.append(panelii)

		# construct arrays
		reflectance = numpy.array(reflectance)
		reflectanceii = numpy.array(reflectanceii)

		# divide by cosine of solar zenith angle
		cosine = numpy.cos(numpy.radians(zenith)).reshape(*zenith.shape, 1)
		reflectance = reflectance / cosine
		reflectanceii = reflectanceii / cosine

		# concatenate grids
		grids = numpy.hstack([grid, gridii])
		reflectances = numpy.dstack([reflectance, reflectanceii])

		# excise certainn wavelengths
		indices = (grids > 350) & ((grids < 371) | (grids > 375))
		reflectances = reflectances[:, :, indices]
		grids = grids[indices]

		# print status
		self._print('performing pca reduction...')

		# set up blocks
		number = 20
		size = int(numpy.ceil(reflectance.shape[0] / number))
		brackets = [(index * size, size + index * size) for index in range(number)]
		reductions = []
		for bracket in brackets:

			# print bracket
			self._print('bracket {}...'.format(bracket))

			# reduce to pcas ( keep 50 )
			reduction = numpy.dot(reflectances[bracket[0]: bracket[1]], coefficients)
			reduction = reduction[:, :, :50]

			# add to reductions
			reductions.append(reduction)

		# create array
		reductions = numpy.vstack(reductions)

		# delete intermediates
		del reflectance
		del reflectanceii
		gc.collect()

		return reductions, reflectances, grids

	def sow(self, scans=(10, 11), granules=(1, 10)):
		"""Gather up training data into a matrix, and run a random forest.

		Arguments:
			scans: tuple of ints, the scans to use
			granules: tuple 0f ints, the granules bracket

		Returns:
			None
		"""

		# create folder
		folder = '{}/reports'.format(self.folder)
		self._make(folder)

		# construct training days
		days = self.training['days']

		# construct matrix
		matrix, _, _ = self.materialize(days, scans, granules)

		# create data
		data = {feature: matrix[:, index] for index, feature in enumerate(self.features)}

		# create report destination and plant tree
		self.plant(data, self.features[-1], '{}/gpp_random_forest.txt'.format(folder))

		return None

	def speculate(self, sites=['CA-Cbo'], tag='', forest=False):
		"""Reproduce the spectra at a site and color by GPP error.

		Arguments:
			sites: list of str, the sites to analyze
			tag: str, tag for study, defaults to first site name
			forest: boolean, run random forest?

		Returns:
			None
		"""

		# create folder
		folder = '{}/speculate'.format(self.folder)
		self._make(folder)

		# create reports folder
		folderii = '{}/reports'.format(self.folder)
		self._make(folderii)

		# connect to data
		hydra = Hydra('{}/GPP_Confirmation/{}'.format(self.sink, self.stub))

		# begin reservoirs
		matrices = []
		predictions = []
		truths = []
		times = []

		# begin angle and cloud reservoirs
		angles = []
		clouds = []

		# for each site
		for site in sites:

			# ingest data
			hydra.ingest(site)
			matrix = hydra.grab('tempo_matrix')
			prediction = hydra.grab('tempo_prediction')
			truth = hydra.grab('tempo_truth')
			time = hydra.grab('tempo_times')
			cloud = hydra.grab('tempo_cloud')
			angle = hydra.grab('tempo_angles')

			# append
			matrices.append(matrix)
			predictions.append(prediction)
			truths.append(truth)
			times.append(time)
			clouds.append(cloud)
			angles.append(angle)

		# construct arrays
		matrices = numpy.vstack(matrices)
		predictions = numpy.hstack(predictions)
		truths = numpy.hstack(truths)
		times = numpy.hstack(times)
		clouds = numpy.hstack(clouds)
		angles = numpy.hstack(angles)

		# calculate errors
		errors = predictions - truths

		# extract reflectance pcas
		matricesii = matrices[:, :self.limits['pca']]

		# get wavelengths
		hydraii = Hydra('{}/TEMPO_RAD_Smooth/{}'.format(self.sink, self.stub))
		hydraii.ingest()
		wavelengths = hydraii.grab('reflectance_wavelengths')

		# get PCA coefficients
		hydraiii = Hydra('{}/TEMPO_PCs'.format(self.sink))
		hydraiii.ingest()
		coefficients = hydraiii.grab('PCA_Coeff')

		# reconstruct spectra
		transpose = coefficients.transpose(1, 0)[:self.limits['pca']]
		spectra = numpy.matmul(matricesii, transpose)

		# sort by absolute error
		order = numpy.argsort(abs(errors))
		errorsii = errors[order]
		spectraii = spectra[order]
		timesii = times[order]

		# set modes
		modes = ('error', 'time')

		# set gradients
		gradients = {'error': 'coolwarm', 'time': 'terrain_r'}
		selections = {'error': list(range(256)), 'time': list(range(32, 224))}

		# set color scales
		absolute = max([abs(errors.max()), abs(errors.min())])
		absoluteii = max([abs(times.min() - 12), abs(times.max() - 12)])
		scales = {'error': (-absolute, absolute), 'time': (12 - absoluteii, 12 + absoluteii)}

		# set colorbar lables
		labels = {'error': '{} error ( {} )'.format(self.target, self.unit), 'time': 'local time ( hr )'}

		# set intensities
		intensities = {'error': errorsii, 'time': timesii}

		# for each mode
		for mode in modes:

			# determine color scale
			gradient = gradients[mode]
			scale = scales[mode]
			normal = matplotlib.colors.Normalize(*scale)
			colors = pyplot.get_cmap(gradient)
			colors = colors(numpy.array(selections[mode]))
			colors = matplotlib.colors.ListedColormap(colors)
			scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8,8))

			# title plot
			tag = tag or sites[0]
			pyplot.title('{} by spectra, {}, {}'.format(mode, self.date, tag))
			pyplot.xlabel('wavelength ( nm )')
			pyplot.ylabel('normalized reflectance')

			# for each spectrum
			for spectrum, intensity in zip(spectraii, intensities[mode]):

				# plot line
				color = colors(normal(intensity))
				pyplot.plot(wavelengths, spectrum, color=color, marker=None)

			# add colorbar
			bar = pyplot.colorbar(scalar, ax=pyplot.gca())
			bar.set_label(labels[mode])

			# save figure
			formats = (folder, tag, mode, self.date, self.session, self.tag)
			destination = '{}/speculate_{}_{}_{}_{}_{}.png'.format(*formats)
			pyplot.savefig(destination)
			self._print('plotted {}.'.format(destination))

		# set waves and colors
		waves = [(420, 'purple'), (480, 'blue'), (545, 'green')]
		waves += [(584, 'orange'), (670, 'red'), (710, 'gray')]

		# begin wavelength plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# title plot
		tag = tag or sites[0]
		pyplot.title('wavelengths, {}, {}'.format(self.date, tag))
		pyplot.xlabel('hour')
		pyplot.ylabel('normalized reflectance')
		pyplot.ylim(0, spectra.max())

		# for each wavelength
		for wave, color in waves:

			# get position in wavelengths
			position = self._pin(wave, wavelengths)[0][0]

			# plot line
			pyplot.plot(times, spectra[:, position], color=color, marker='o', label='{}nm'.format(str(wave)))

		# for each error
		for error, time in zip(errors, times):

			# default hue to red
			hue = 'magenta'

			# if error is negative
			if error < 0:

				# set to blue
				hue = 'cyan'

			# set size
			size = abs(int(error)) * 50

			# plot marker
			pyplot.scatter(time, 0.0, s=size, c=hue)

		# add legend
		pyplot.legend(loc='upper left')

		# save figure
		formats = (folder, tag, self.date, self.session, self.tag)
		destination = '{}/speculate_{}_wave_{}_{}_{}.png'.format(*formats)
		pyplot.savefig(destination)
		self._print('plotted {}.'.format(destination))

		# set up functions
		def indexing(one, two): return (one - two) / (one + two)
		def enhancing(one, two, three): return 2.5 * (one - two) / (one + 6 * two - 7.5 * three + 1)

		# set up indices
		indices = [('ndvi', [710, 670], indexing, 'red'), ('ccl', [530, 670], indexing, 'orange')]
		indices += [('pri', [530, 570], indexing, 'green'), ('evi', [710, 670, 480], enhancing, 'blue')]

		# begin indices plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# title plot
		tag = tag or sites[0]
		pyplot.title('vegetation indices, {}, {}'.format(self.date, tag))
		pyplot.xlabel('hour')
		pyplot.ylabel('index')
		pyplot.ylim(0, 0.6)

		# for each spectrum
		for name, group, function, color in indices:

			# get position in wavelengths
			positions = [self._pin(wave, wavelengths)[0][0] for wave in group]

			# retrieve elements
			elements = [spectra[:, position] for position in positions]

			# plot line
			pyplot.plot(times, function(*elements), color=color, marker='o', label=name)

		# for each error
		for error, time in zip(errors, times):

			# default hue to red
			hue = 'magenta'

			# if error is negative
			if error < 0:

				# set to blue
				hue = 'cyan'

			# set size
			size = abs(int(error)) * 50

			# plot marker
			pyplot.scatter(time, 0, s=size, c=hue)

		# add legend
		pyplot.legend(loc='upper left')

		# save figure
		formats = (folder, tag, self.date, self.session, self.tag)
		destination = '{}/speculate_{}_indices_{}_{}_{}.png'.format(*formats)
		pyplot.savefig(destination)
		self._print('plotted {}.'.format(destination))

		# begin cloud plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# title plot
		tag = tag or sites[0]
		pyplot.title('clouds, scattering angle, {}, {}'.format(self.date, tag))
		pyplot.xlabel('hour')
		pyplot.ylabel('cloud fraction, cos ( scattering angle )')
		pyplot.ylim(-0.1, 1.1)

		# plot cloud fractions
		pyplot.plot(times, clouds, color='b', marker='o', label='cloud fraction')

		# plot scattering angle
		cosines = numpy.cos(numpy.radians(angles))
		pyplot.plot(times, cosines, color='r', marker='o', label='cos ( scattering angle )')

		# for each error
		for error, time in zip(errors, times):

			# default hue to red
			hue = 'magenta'

			# if error is negative
			if error < 0:

				# set to blue
				hue = 'cyan'

			# set size
			size = abs(int(error)) * 50

			# plot marker
			pyplot.scatter(time, -1.1, s=size, c=hue)

		# add legend
		pyplot.legend(loc='upper left')

		# save figure
		formats = (folder, tag, self.date, self.session, self.tag)
		destination = '{}/speculate_{}_cloud_{}_{}_{}.png'.format(*formats)
		pyplot.savefig(destination)
		self._print('plotted {}.'.format(destination))

		# if using random forest
		if forest:

			# set up training data
			train = {feature: matrices[:, index] for index,feature in enumerate(self.features[:-1])}
			train.update({'error|00': errors})

			# create report destination and plant tree
			self.plant(train, 'error|00', '{}/gpp_pca_error_random_forest.txt'.format(folderii))

			# set up training data
			train = {'{}nm'.format(wave): spectra[:, index] for index, wave in enumerate(wavelengths)}
			train.update({'error|00': errors})

			# create report destination and plant tree
			self.plant(train, 'error|00', '{}/gpp_wave_error_random_forest.txt'.format(folderii))

		return None

	def streak(self, scan=8, quantities=(25.75469, 25.75470)):
		"""Plot the locations of a streak value.

		Arguments:
			scan: int, the scan number
			quantities: tuple of floats, the value range

		Returns:
			None
		"""

		# make folder
		folder = '{}/streaks'.format(self.folder)
		self._make(folder)

		# get matrix and data
		matrix, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1))
		data = self._constrain(data, constraints)

		# get mask for truth range
		truth = matrix[:, -1]
		mask = (truth >= quantities[0]) & (truth <= quantities[1])

		# apply to data
		data = self._constrain(data, mask)

		# get latitudes and longitudes
		latitude = data['latitude']
		longitude = data['longitude']

		# Create a plot
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# Add titles and labels
		title = 'locations of {:.2f}, {:.2f}, scan {}, {}'.format(*quantities, scan, self.date)
		pyplot.title(title)
		pyplot.xlabel('longitude')
		pyplot.ylabel('latitude')

		# set limits
		pyplot.xlim(-110, -60)
		pyplot.ylim(10, 60)

		# plot
		pyplot.plot(longitude, latitude, 'b.', ms=1)

		# save the plot
		destination = '{}/map_streaks_{}_{}.png'.format(folder, self.date, self._pad(scan))
		pyplot.savefig(destination)
		pyplot.clf()

		return None

	def surround(self, site='CA-Cbo', scan=6, radius=0.1):
		"""Get the data surrounding a particular site.

		Arguments:
			None

		Returns:
			dict of data
		"""

		# get the original data
		_, data, _ = self.materialize([(self.month, self.day)], (scan, scan + 1))

		# get the site coordinates
		latitude = self.catalog[site]['latitude']
		longitude = self.catalog[site]['longitude']

		# generate mask for squared distance
		mask = ((latitude - data['latitude']) ** 2 <= radius ** 2)
		mask = mask & ((longitude - data['longitude']) ** 2 <= radius ** 2)
		mask = mask.squeeze()

		# apply mask
		data = self._constrain(data, mask)

		return data

	def train(self):
		"""Gather up training data into a matrix, and run neural network.

		Arguments:
			None

		Returns:
			None
		"""

		# clear models
		self.model = None
		self.details = None
		self.folder = None
		self.session = None

		# read in features from yanml
		self._feature()

		# get current date
		today = datetime.datetime.today().date()
		date = '{}m{}{}'.format(today.year, self._pad(today.month), self._pad(today.day))

		# create folder
		folder = '{}/GPP_Models/GPP_NN_{}_{}'.format(self.sink, date, self.tag)
		self._make(folder)
		self.folder = folder

		# begin data details
		data = {'training/{}'.format(field): numpy.array(datum) for field, datum in self.training.items()}
		data.update({'validation/{}'.format(field): numpy.array(datum) for field, datum in self.validation.items()})
		data.update({'architecture/{}'.format(field): numpy.array(datum) for field, datum in self.architecture.items()})
		data.update({'limits/{}'.format(field): numpy.array(datum) for field, datum in self.limits.items()})

		# construct matrix
		days = self.training['days']
		scans = self.training['scans']
		granules = self.training['granules']

		# set up matrices and indices
		matrices = []
		indices = []
		lengths = []

		# for each training date
		for month, day in days:

			# get the matrix
			matrix, _, _ = self.materialize([(month, day)], scans, granules)

			# if randomly sampling
			if self.training['random']:

				# randomly sample indices
				size = int(self.training['fraction'] * matrix.shape[0])
				selection = numpy.random.randint(0, matrix.shape[0], size=(size,))
				selection = numpy.array([index for index in selection if index < matrix.shape[0]])

			# otherwise
			else:

				# take samples evenly spread
				increment = int(1 / self.training['fraction'])
				selection = [position * increment for position in range(matrix.shape[0])]
				selection = numpy.array([index for index in selection if index < matrix.shape[0]])

			# append matrix and indices, and keep track of original matrix lengths
			indices.append(selection + sum(lengths))
			lengths.append(matrix.shape[0])
			matrices.append(matrix[selection])

		# reform matrix and indices
		matrix = numpy.vstack(matrices)
		indices = numpy.hstack(indices)

		# add indices to data
		data.update({'samples/selected_indices': indices})

		# get the minimums and maximums
		minimum = matrix.min(axis=0)
		maximum = matrix.max(axis=0)
		mean = matrix.mean(axis=0)
		deviation = matrix.std(axis=0)

		# add data for scaling factors
		data.update({'scaling/input_min': minimum[:-1], 'scaling/input_max': maximum[:-1]})
		data.update({'scaling/output_min': minimum[-1], 'scaling/output_max': maximum[-1]})
		data.update({'scaling/input_mean': mean[:-1], 'scaling/input_std': deviation[:-1]})
		data.update({'scaling/output_mean': mean[-1], 'scaling/output_std': deviation[-1]})

		# add features list
		data.update({'features': numpy.array(self.features)})

		# if normalizing
		if self.training['normalize']:

			# apply normalization to traiing matrix
			matrix = (matrix - mean) / deviation

		# otherwise
		else:

			# apply scaling to training matrix
			scale = maximum - minimum
			matrix = (matrix - minimum) / scale

		# fit PCA on inputs and get coefficients, and transpose them
		decomposer = PCA(n_components=matrix.shape[1] - 1)
		decomposer.fit(matrix[:, :-1])
		coefficients = decomposer.components_.transpose(1, 0)

		# add to data
		data.update({'scaling/pca_coefficients': coefficients})

		# separate into predictors and targets
		targets = matrix[:, -1]
		predictors = matrix[:, :-1]

		# if using pca decomposition
		if self.training['decompose']:

			# apply coefficients
			predictors = numpy.dot(predictors, coefficients)

		# construct validation matrix
		daysii = self.validation['days']

		# set up matrices and indices
		matricesii = []
		indicesii = []
		lengthsii = []

		# for each training date
		for month, day in daysii:

			# get the matrix
			matrixii, _, _ = self.materialize([(month, day)], scans, granules)

			# if randomly sampling
			if self.training['random']:

				# randomly sample indices
				size = int(self.training['fraction'] * matrixii.shape[0])
				selection = numpy.random.randint(0, matrixii.shape[0], size=(size,))
				selection = numpy.array([index for index in selection if index < matrixii.shape[0]])

			# otherwise
			else:

				# take samples evenly spread
				increment = int(1 / self.training['fraction'])
				selection = [position * increment for position in range(matrixii.shape[0])]
				selection = numpy.array([index for index in selection if index < matrixii.shape[0]])

			# append matrix and indices, and keep track of original matrix sizes
			indicesii.append(selection + sum(lengthsii))
			lengthsii.append(matrixii.shape[0])
			matricesii.append(matrixii[selection])

		# reform matrix and indices
		matrixii = numpy.vstack(matricesii)
		indicesii = numpy.hstack(indices)

		# add indices to data
		data.update({'samples/validation_indices': indicesii})

		# if normalizing
		if self.training['normalize']:

			# apply normalization to validation matrix
			matrixii = (matrixii - mean) / deviation

		# otherwise
		else:

			# apply scaling to training matrix
			scale = maximum - minimum
			matrixii = (matrixii - minimum) / scale

		# separate into predictors and targets
		targetsii = matrixii[:, -1]
		predictorsii = matrixii[:, :-1]

		# if using pca decomposition
		if self.training['decompose']:

			# apply coefficients
			predictorsii = numpy.dot(predictorsii, coefficients)

		# if randomly sampling
		if self.validation['random']:

			# randomly sample indices
			size = int(self.validation['fraction'] * predictorsii.shape[0])
			indicesii = numpy.random.randint(0, predictorsii.shape[0], size=(size,))

		# otherwise
		else:

			# take samples evenly spread
			increment = int(1 / self.validation['fraction'])
			indicesii = [position * increment + 1 for position in range(predictorsii.shape[0])]
			indicesii = numpy.array([index for index in indicesii if index < predictorsii.shape[0]])

		# create validation set
		validation = [predictorsii, targetsii]

		# stash
		destination = '{}/Model_Scaling.h5'.format(folder)
		self.spawn(destination, data)

		# run the model
		self._print('matrix: {}'.format(predictors.shape))
		self.run(predictors, targets, validation)

		# reload the mode
		self._retrieve()

		return None

	def trouble(self, latitude=37, longitude=-80, scans=(6, 14)):
		"""Troubleshoot synthetic data by generating synthetic matrices.

		Arguments:
			latitude: float, closest latitude
			longitude; float, closest longitude
			scans: tuple of ints, the scan brackets.

		Returns:
			tuple of numpy array, the matrix
		"""

		# begin collection
		matrix = []

		# for each scan
		for scan in range(*scans):

			# get matrix and data
			_, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1))
			data = self._constrain(data, constraints)

			# find index closest to latitude and longitude
			closest = self._pin([latitude, longitude], [data['latitude'].squeeze(), data['longitude'].squeeze()])[0]

			print(data['latitude'][closest], data['longitude'][closest])

			# assemble row
			row = [data['mcd43_bands'][closest], data['short_wave'][closest], data['gpp_onefluxnet'][closest]]

			[print(entry.shape) for entry in row]

			row = numpy.hstack(row)
			matrix.append(row)

		# create array
		matrix = numpy.array(matrix)

		return matrix

	def validate(self, scan=10, apply=False, verify=True, present=False, platform=None, large=False, dot=0.1):
		"""Produce plots of one scan to validate the NN model.

		Arguments:
			scan: int, scan number
			apply: boolean, apply selected indices?
			verify: boolean, make verifiation plots?
			presernt: boolean, make presentation plots?
			platform: str, name of satellite
			large: boolean, make single panel plots?
			dot; float, pixel size for scatter plot

		Returns:
			None
		"""

		# set default platform
		platform = platform or 'Onefluxnet'

		# make folders
		folder = '{}/validation'.format(self.folder)
		folderii = '{}/presentation'.format(self.folder)
		self._make(folder)
		self._make(folderii)

		# get training matrix for particular scan
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, data, constraints = self.materialize(*parameters)

		# construct valid data
		valids = self._constrain(data, constraints)

		# if applying sampled indices:
		if apply:

			# get selected indices
			indices = self.details['samples/selected_indices']

			# get fields with same shape as truth and apply indices
			fields = [field for field, array in valids.items() if array.shape[0] == matrix[:, -1].shape[0]]
			valids.update({field: valids[field][indices] for field in fields})

			# apply indices to inputs and truth
			matrix = matrix[indices]

		# make predictions
		prediction, deviation, truth, score = self.predict(matrix)

		# print r^2 score
		self._print('score: {}'.format(score))

		# plot model errors
		self.diagnose(truth, prediction, valids)

		# if making verification plot
		if verify:

			# create plot destination
			formats = [folder, self.date,  self._pad(scan), int(data['time'][0][0]), self.session]
			formats += [self.tag, self.instances]
			destination = '{}/GPP_verify_{}_{}_{}_{}_{}_{}.png'.format(*formats)

			# if site in validation set
			if [self.month, self.day] in self.validation['days']:

				# replace destination with validation
				destination = destination.replace('verify', 'validate')

			# verify with plot
			parameters = (destination, truth, prediction, valids)
			options = {'platform': platform, 'large': large, 'dot': dot}
			self.verify(*parameters, **options)

		# if making presentation plots
		if present:

			# create plot destination
			formats = (folderii, self.date, self._pad(scan), data['time'][0][0], self.instances)
			destination = '{}/GPP_present_{}_{}_{}_{}.png'.format(*formats)

			# verify with plot
			parameters = (destination, truth, prediction, deviation, valids, data, self.instances)
			self.present(*parameters, chart=False)

		return None

	def verify(self, destination, truth, prediction, data, samples=10, dot=0.1, platform=None, large=False):
		"""Plot a validation sample.

		Arguments:
			destination: str, destination filepath
			truth: ground truth observations
			prediction: NN predictions
			data: dict of numpy arrays
			samples: int, largest number of samples for scatter plots
			platform: name of satellite
			large: boolean, make large vesion of plots?
			dot: size of scatter plot pixel

		Returns:
			None
		"""

		# set default platform
		platform = platform or 'Onefluxnet'

		# begin status
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase preevious copy
		self._clean(destination, force=True)

		# calculate r2 score
		score = r2_score(truth, prediction)

		# calculate regression line
		slope, intercept, _, _, _ = scipy.stats.linregress(truth, prediction)

		# create date for title
		date = str(data['date'][0][0])
		date = '{}m{}{}'.format(date[:4], date[4:6], date[6:8])

		# create time for title
		time = data['time'][0][0]
		if time < 70000:

			# add to 2400
			time += 240000

		# make string
		time = str(time)
		time = '{}:{} CST'.format(int(time[:2]) - 6, time[2:4])

		# calculate differences
		difference = prediction - truth

		# set scaling bracket
		bracket = self.limits['scale']

		# perform binning
		mesh, meshii, counts = self._bin(truth, prediction, samples, bracket)

		# get minimum and maximum tracer values
		minimum = min([truth.min(), prediction.min()])
		maximum = max([truth.max(), prediction.max()])

		# set percent bracket
		absolute = max([abs(numpy.percentile(difference, 2)), abs(numpy.percentile(difference, 98))])

		# set bounds
		bounds = [(-125, -60), (15, 55)]
		scale = (minimum, maximum)
		scale = self.limits['scale']
		scaleii = (-absolute, absolute)

		# begin graphs
		graphs = []

		# get data
		latitude = data['latitude']
		longitude = data['longitude']
		corners = data['latitude_bounds']
		cornersii = data['longitude_bounds']

		# construct GPP truth plot
		title = '{} {} {} {}\n'.format(self.target, platform, date, time)
		unit = self.unit
		labels = ['', '']
		options = {'polygons': True, 'corners': [corners, cornersii]}
		graph = self._graph(longitude, latitude, truth, bounds, scale, 'plasma', title, unit, labels, **options)
		graphs.append(graph)

		# construct GPP prediction plot
		title = '{} Prediction {} {}\n'.format(self.target, date, time)
		unit = self.unit
		labels = ['', '']
		options = {'polygons': True, 'corners': [corners, cornersii]}
		graph = self._graph(longitude, latitude, prediction, bounds, scale, 'plasma', title, unit, labels, **options)
		graphs.append(graph)

		# construct samples title depending on instances
		formats = (platform, date, time, score)
		title = 'Prediction vs {}, ( 1 instance )\n{} {} ( $R^2$ = {:.2f} )'.format(*formats)
		if self.instances > 1:

			# add instances
			formats = (platform, self.instances, date, time, score)
			title = 'Prediction vs {} ( avg of {} instances )\n{} {} ( R^2 = {:.2f} )'.format(*formats)

		# construct samples graph
		unit = 'samples'
		labels = ['{} truth'.format(self.target), '{} prediction'.format(self.target)]
		boundsii = [self.limits['scale'], self.limits['scale']]
		scaleiii = (1, samples)
		points = self.limits['scale']
		regression = [intercept + point * slope for point in points]
		line = (points, points, 'k-')
		lineii = (points, regression, 'k--')
		lines = [line, lineii]
		# lines = [line]
		options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': dot, 'polygons': False}
		graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
		graphs.append(graph)

		# construct percent difference plot
		title = 'Model - {}, {} {}\n'.format(platform, date, time)
		unit = self.unit
		labels = ['', '']
		options = {'polygons': True, 'corners': [corners, cornersii]}
		graph = self._graph(longitude, latitude, difference, bounds, scaleii, 'coolwarm', title, unit, labels, **options)
		graphs.append(graph)

		# create plot
		self._paint(destination, graphs)

		# if large plots:
		if large:

			# for each graph
			for index, graph in enumerate(graphs):

				# plot each one
				self._paint(destination.replace('.png', '_{}.png'.format(index)), [graph], size=(16, 16))

		return None

	def visualize(self, scan=10, variables=None):
		"""Visualize the dataset with PCA.

		Arguments:
			scan: int, scan number
			variables: list of str, the variables to plot

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/visualization'.format(self.folder)
		self._make(folder)

		# get training matrix for particular scan
		parameters = [(self.month, self.day)], (scan, scan + 1)
		matrix, data, constraints = self.materialize(*parameters)

		# make predictions
		prediction, _, truth, score = self.predict(matrix)
		prediction = prediction.reshape(-1, 1)
		truth = truth.reshape(-1, 1)

		# apply constraints
		data = self._constrain(data, constraints)

		# get scaling information
		minimum = self.details['scaling/input_min']
		maximum = self.details['scaling/input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		inputs = (matrix[:, :-1] - minimum) / scale

		# default bounds
		bounds = None
		boundsii = None

		# try to
		try:

			# get pca components already stored
			hydra = Hydra('{}/pca'.format(folder))
			hydra.ingest()
			coefficients = hydra.grab('2d_pca')
			bounds = (hydra.grab('minimum'), hydra.grab('maximum'))
			boundsii = (hydra.grab('minimumii'), hydra.grab('maximumii'))

		# unless not made
		except IndexError:

			# fit PCA on inputs and get coefficients
			decomposer = PCA(n_components=2)
			decomposer.fit(inputs)
			coefficients = decomposer.components_.transpose(1, 0)

		# get representation
		representation = numpy.dot(inputs, coefficients)

		# make labels
		labels = self._represent(coefficients, self.features)

		# if no set variables
		if not variables:

			# set heatmap variables
			variables = ['tropomi_vertical_column_no2', 'solar_zenith_angle', 'cloud_fraction']
			variables += ['land_type', 'latitude', 'longitude', 'tropomi_quality_assurance']
			variables += ['viewing_zenith_angle', 'tempo_vertical_column_no2']

		# set color scales
		scales = {'tropomi_vertical_column_no2': self.limits['scale']}
		scales.update({'prediction': self.limits['scale']})

		# add predictions
		data['prediction'] = prediction
		variables = variables + ['prediction']

		# add error
		data['error'] = truth - prediction
		variables = variables + ['error']

		# for each variable
		for variable in variables:

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8,8))

			# Sample data
			abscissa = representation[:, 0]
			ordinate = representation[:, 1]
			colors = data[variable]

			# normalize colors
			normal = matplotlib.colors.Normalize(vmin=colors.min(), vmax=colors.max(), clip=True)

			# check for scale
			scale = scales.get(variable, None)

			# if scale available
			if scale:

				# clip colors
				normal = matplotlib.colors.Normalize(vmin=scale[0], vmax=scale[1], clip=True)

				# clip colors
				colors = numpy.where(colors <= scale[0], scale[0], colors)
				colors = numpy.where(colors >= scale[1], scale[1], colors)

			# create scatter plot
			pyplot.scatter(abscissa, ordinate, c=colors, cmap='viridis', norm=normal, s=0.05)  # Use 'c' for colors and 'cmap' for colormap
			pyplot.colorbar(label=variable)  # Add a colorbar to indicate the scale
			pyplot.xlabel('PCA 0 ( {} )'.format(labels[0]))
			pyplot.ylabel('PCA 1 ( {} )'.format(labels[1]))
			pyplot.title('PCA components vs {}\nscan {}, {}, {:.3f}'.format(variable, scan, self.date, score))

			# if bounds are set
			if bounds:

				# set limits
				pyplot.xlim(*bounds)
				pyplot.ylim(*boundsii)

			# save
			pyplot.savefig('{}/pca_{}_{}.png'.format(folder, self.date, variable))
			pyplot.clf()

		return None

	def wave(self, scan=10, granule=7, parallel=31, meridian=-105):
		"""Make a plot of wavelengths to verify smoothing.

		Arguments:
			scan: int, scan number
			granule int, granule number
			parallel: float, the closest latitude
			meridian: float, the closest longitude

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folders
		folderii = '{}/wave'.format(folder)
		self._make(folderii)

		# get data
		radiances = Hydra('{}/TEMPO_RAD_L1/{}'.format(self.sink, self.stub), show=False)
		irradiances = Hydra('{}/TEMPO_IRR_L1/{}'.format(self.sink, self.stub), show=False)
		nitrogens = Hydra('{}/Nitro_Inputs/{}'.format(self.sink, self.stub), show=False)
		components = Hydra('{}/Nitro_PCs'.format(self.sink), show=False)
		productivity = Hydra('{}/GPP_Inputs/{}'.format(self.sink, self.stub), show=False)

		# make label
		label = self._label(scan, granule)

		# ingest the files
		radiances.ingest(label)
		irradiances.ingest()
		nitrogens.ingest(label)
		productivity.ingest(label)
		components.ingest()

		# get radiance data
		latitude = radiances.grab('latitude')
		longitude = radiances.grab('longitude')
		zenith = radiances.grab('solar_zenith_angle')
		view = radiances.grab('viewing_zenith_angle')
		azimuth = radiances.grab('solar_azimuth_angle')
		azimuthii = radiances.grab('viewing_azimuth_angle')
		high = radiances.grab('band_290_490/radiance')
		shorts = radiances.grab('band_290_490/nominal_wavelength')

		# get irradiance data
		highii = irradiances.grab('band_290_490/radiance').squeeze()
		shortsii = irradiances.grab('band_290_490/nominal_wavelength').squeeze()

		# get pca coefficents
		coefficients = components.grab('no2_pca_coefficients')
		wavelengths = components.grab('wavelength')

		# get scanline closest to latitude
		mirror, track = self._pin([parallel, meridian], [latitude, longitude])[0]

		# get decomposition and recompose
		smooth = productivity.grab('smoothed_reflectances')[mirror, track]
		wavelengthsii = productivity.grab('reflectance_wavelengths')
		decomposition = nitrogens.grab('no2_reflectance_pcas')[mirror, track]
		composition = numpy.dot(coefficients, decomposition.reshape(-1, 1))

		print('decomposition: {}'.format(decomposition.shape))
		print('coefficients: {}'.format(coefficients.shape))
		print('composition: {}'.format(composition.shape))

		# subset radiance and irradiance
		high = high[mirror, track]
		shorts = shorts[track]
		highii = highii[track]
		shortsii = shortsii[track]

		# set gridding attributes
		nodes = 5
		width = 2
		widthii = 1 / nodes

		# construct wavelength grid
		grid = numpy.array(range(self.limits['high'][0], self.limits['high'][1] + 1))

		# construct finely spaced grids ( 0.2 nm )
		fine = numpy.array(range((self.limits['high'][0] - 1) * nodes, self.limits['high'][1] * nodes + 1)) / nodes

		# construct boxcar
		boxcar = numpy.ones(int(width / widthii)) / int(width / widthii)

		# create masks for valid data
		mask = self._mask(highii)

		# interpolate irradiance onto radiance wavelengths
		options = {'kind': 'linear', 'fill_value': 'extrapolate', 'bounds_error': False}
		interpolator = scipy.interpolate.interp1d(shortsii[mask], highii[mask], **options)
		irradiance = interpolator(shorts)

		# divide by irradiance
		ratio = high / irradiance

		print('high')
		print(high[35:100])
		print('irr')
		print(irradiance[35:100])
		print('ratio')
		print(ratio[35:100])

		# get mask for valid data
		mask = self._mask(ratio)

		# interpolate to fine grid
		interpolator = scipy.interpolate.interp1d(shorts[mask], ratio[mask], **options)
		interpolation = interpolator(fine)

		# convolve onto course grid
		convolution = scipy.signal.convolve(interpolation, boxcar, mode='same')

		# get indices of wavelengths that also in the grid
		indices = numpy.where(numpy.isin(fine, grid))
		reflectance = convolution[indices]

		# normalize by zenith angle
		cosine = reflectance / numpy.cos(numpy.radians(zenith[mirror, track]))

		print(fine.shape, ratio.shape)
		print(reflectance.shape, grid.shape)

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# add text
		formats = (self.date, label, mirror, track, self._orient(parallel), self._orient(meridian, east=True))
		pyplot.title('Smoothed reflectance for {}, {}, {}, {}, ( {}, {} )'.format(*formats))
		pyplot.xlabel('wavelength')
		pyplot.ylabel('reflectance'.format(self.target, self.unit))

		# # set bounds
		# pyplot.xlim(*bounds)
		# pyplot.ylim(*self.limits['scale'])

		# plot reflectance
		pyplot.plot(fine, interpolation, 'b-', linewidth=1, label='unsmoothed')

		# plot smoothed reflectance
		pyplot.plot(grid, reflectance, 'g-', linewidth=1, label='smoothed')

		# plot zenith adjusted reflectance
		pyplot.plot(grid, cosine, 'r-', linewidth=1, label='zenith adjusted')

		# # plot smoothed
		# pyplot.plot(wavelengthsii, smooth, 'm-', linewidth=1, label='no2 smoothed')
		#
		# plot recomposition
		pyplot.plot(wavelengths, composition, 'k-', linewidth=1, label='pca recomposition')

		# add legend
		pyplot.legend(loc='upper left')

		# save
		pyplot.savefig('{}/Reflectance_{}_{}_{}_{}_{}_{}.png'.format(folderii, *formats))
		pyplot.clf()

		# tranpose coefficients to get coponent vectors
		vectors = coefficients.transpose(1, 0)

		# for each vector
		for index, vector in enumerate(vectors[:10]):

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8, 8))

			# add text
			formats = (index, self.date, label, mirror, track, self._orient(parallel), self._orient(meridian, east=True))
			pyplot.title('PCA Vector: {} for {}, {}, {}, {}, ( {}, {} )'.format(*formats))
			pyplot.xlabel('wavelength')
			pyplot.ylabel('pca coefficient'.format(self.target, self.unit))

			# plot vector
			pyplot.plot(wavelengths, vector, 'b-', linewidth=1, label='pca vector')

			# add legend
			pyplot.legend(loc='upper left')

			# save
			pyplot.savefig('{}/Vector_{}_{}_{}_{}_{}_{}_{}.png'.format(folderii, *formats))
			pyplot.clf()

		return None

	def window(self, sites=['US-SRM'], scan=6, radius=0.1):
		"""Examine overlap of modis and merra data with tempo pixels.

		Arguments:
			sites: list of str, site names
			scan: int, the scan number
			radius: float, bounds of window in degrees

		Returns:
			None
		"""

		# make folder
		folder = '{}/window'.format(self.folder)
		self._make(folder)

		# get tempo data
		_, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1))
		data = self._constrain(data, constraints)

		# determine high or low resolution mode
		modes = {'low': 'high', 'high': 'low'}
		mode = self.features[-1].split('_')[-1]
		modeii = modes[mode]

		# for each site
		for site in sites:

			# get latitude and longitude of site
			latitude = self.catalog[site]['latitude']
			longitude = self.catalog[site]['longitude']

			# get all tempo pixels within window of site
			mask = (data['latitude'] >= latitude - radius) & (data['latitude'] <= latitude + radius)
			mask = mask & (data['longitude'] >= longitude - radius) & (data['longitude'] <= longitude + radius)
			mask = mask.squeeze()

			# apply mask
			dataii = self._constrain(data, mask)

			# get average local time
			hour = dataii['local_time'].mean()

			# if hour less than 0
			if hour < 0:

				# add 24
				hour += 24

			# open the window file
			hydra = Hydra('{}/Onefluxnet_Window/{}'.format(self.sink, self.stub))
			hydra.ingest(site)

			# get the index of the nearst time
			index = self._pin(hour, hydra.grab('hour'))[0][0]

			# set variables
			variables = ['red', 'nir', 'blue', 'green', 'swir1', 'swir2', 'swir3', 'sw', 'gpp']
			units = {'sw': '$W/m^2$', 'gpp': self.unit}

			# get synthetic data
			latitudes = hydra.grab('latitude_{}'.format(mode))
			longitudes = hydra.grab('longitude_{}'.format(mode))
			albedo = hydra.grab('albedo_{}'.format(mode))
			wave = hydra.grab('wave_{}'.format(mode))[index]
			productivity = hydra.grab('prediction_{}'.format(mode))[index]

			# get synthetic data for secondary mode
			latitudesii = hydra.grab('latitude_{}'.format(modeii))
			longitudesii = hydra.grab('longitude_{}'.format(modeii))
			albedoii = hydra.grab('albedo_{}'.format(modeii))
			waveii = hydra.grab('wave_{}'.format(modeii))[index]
			productivityii = hydra.grab('prediction_{}'.format(modeii))[index]

			# get tempo pixels
			latitudesiii = dataii['latitude']
			longitudesiii = dataii['longitude']

			# get synthetic arrays
			arrays = [array for array in albedo] + [wave] + [productivity]
			arraysii = [array for array in albedoii] + [waveii] + [productivityii]

			# for each variable
			for variable, array, arrayii in zip(variables, arrays, arraysii):

				# begin plot
				pyplot.clf()
				pyplot.figure(figsize=(8, 8))

				# set gradient
				gradient = 'plasma'

				# set labels
				formats = (site, variable, self.date, scan, hour, mode)
				pyplot.title('{} {}, {}, scan {}, hour {:.2f}, ( {} )'.format(*formats))
				pyplot.xlabel('longitude')
				pyplot.ylabel('latitude')

				# set limits
				pyplot.xlim(longitude - radius, longitude + radius)
				pyplot.ylim(latitude - radius, latitude + radius)

				# get the corners of the grid
				corners = self._frame(latitudes, longitudes)
				corner = [corners['northwest'][:, :, 0], corners['northeast'][:, :, 0]]
				corner += [corners['southeast'][:, :, 0], corners['southwest'][:, :, 0]]
				cornerii = [corners['northwest'][:, :, 1], corners['northeast'][:, :, 1]]
				cornerii += [corners['southeast'][:, :, 1], corners['southwest'][:, :, 1]]
				corner = numpy.dstack(corner)
				cornerii = numpy.dstack(cornerii)

				# plot data ( as pinpoints, for the sake of the colorbar )
				mask = (longitudes >= longitude - radius) & (longitudes <= longitude + radius)
				mask = mask & (latitudes >= latitude - radius) & (latitudes <= latitude + radius)

				# get mask for secondary array
				maskii = (longitudesii >= longitude - radius) & (longitudesii <= longitude + radius)
				maskii = maskii & (latitudesii >= latitude - radius) & (latitudesii <= latitude + radius)

				# get minimum and maximum of color scale, using both low and high res data
				minimum = min([array[mask].min(), arrayii[maskii].min()])
				maximum = max([array[mask].max(), arrayii[maskii].max()])

				# plot scatter points to activate colorbar
				options = {'cmap': gradient, 's': 1, 'alpha': 1, 'marker': 's'}
				abscissa = [point for point in longitudes[mask]] + [point for point in longitudesii[maskii]]
				ordinate = [point for point in latitudes[mask]] + [point for point in latitudesii[maskii]]
				colors = [point for point in array[mask]] + [point for point in arrayii[maskii]]
				pyplot.scatter(abscissa, ordinate, c=colors, **options)

				# begin patches
				patches = []

				# for each polygon
				for vertices, verticesii in zip(cornerii.reshape(-1, 4), corner.reshape(-1, 4)):

					# set up patch
					polygon = numpy.array([[first, second] for first, second in zip(vertices, verticesii)])
					patch = matplotlib.patches.Polygon(xy=polygon, linewidth=0, edgecolor=None, alpha=0.9)
					patches.append(patch)

				# plot polygons with colormap
				collection = matplotlib.collections.PatchCollection(patches, cmap=gradient, alpha=0.9)
				collection.set_edgecolor("face")
				collection.set_clim(minimum, maximum)
				collection.set_array(array.flatten())
				polygons = pyplot.gca().add_collection(collection)

				# plot the tempo sites
				pyplot.plot(longitudesiii, latitudesiii, 'kx', ms=20, linewidth=2)

				# plot the onefluxnet site
				pyplot.plot([longitude], [latitude], 'cx', ms=20, linewidth=2)

				# add colorbar
				pyplot.colorbar(label='{} ( {} )'.format(variable, units.get(variable, '-')))

				# save figure
				formats = (folder, site, variable, self._pad(scan), self.date, self.session, self.tag)
				destination = '{}/window_{}_{}_{}_{}_{}_{}.png'.format(*formats)
				pyplot.savefig(destination)
				pyplot.clf()

				# print status
				self._print('plotted {}'.format(destination))

		return None


# if arguments are given
arguments = sys.argv[1:]
if len(arguments) > 0:

	# separate options and arguments
	options = [argument for argument in arguments if argument.startswith('-')]
	arguments = [argument for argument in arguments if not argument.startswith('-')]

	# check for prepare
	if '--prepare' in options:

		# unpack arguments
		sink, year, month, day, scan, scanii, granule, granuleii = arguments

		# set up instance
		tempest = Tempest(int(year), int(month), int(day), sink=sink)

		# prepare data
		tempest.prepare((int(scan), int(scanii)), (int(granule), int(granuleii)))

	# check for collect
	if '--collect' in options:

		# unpack arguments
		sink, year, month, day, scan, scanii, granule, granuleii = arguments

		# set up instance
		tempest = Tempest(int(year), int(month), int(day), sink=sink)

		# prepare data
		tempest.collect((int(scan), int(scanii)), (int(granule), int(granuleii)))

	# check for prepare
	if '--train' in options:

		# unpack arguments
		sink, tag = arguments

		# set up instance
		tempest = Tempest(sink=sink, tag=tag)

		# prepare data
		tempest.train()