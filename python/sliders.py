# sliders.py for combining pngs into a pdf slideshow

# import reload
from importlib import reload

# import system tools
import os
import json
import shutil

# import Core
from cores import Core

# import datetime
from datetime import datetime, timedelta
from time import time, sleep

# import numpy and math
import numpy
import math

# image manipulation
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter, PdfFileMerger, PdfWriter, PdfReader
from pdf2image import convert_from_path

# import reportlab instead of pdf writer
from reportlab.pdfgen import canvas


# create Slider class
class Slider(Core):
    """Class Slider to create slides.

    Inherits from:
        Core
    """

    def __init__(self, source):
        """Initialize a slider instance.

        Arguments:
            folder: str, folder name of pngs
        """

        # initialize the base Core instance
        Core.__init__(self)

        # set directory
        self.source = source

        # establish directory structure
        self._establish()

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Slider instance at: {} >'.format(self.source)

        return representation

    def _establish(self):
        """Establish directory structure for output files.

        Arguments:
            None

        Returns:
            None
        """

        # make list of destinations
        destinations = [self.source]

        # make directories if not already made
        for destination in destinations:

            # try to
            try:

                # access the contenst
                _ = os.listdir(destination)

            # unless the directory does not exit
            except FileNotFoundError:

                # in which case, make it
                os.mkdir(destination)

        return None

    def arrange(self, bypass=False, order=None, sequence=False):
        """Arrange slides in order.

        Arguments:
            bypass: boolean, bypass choices?
            order: list of file names
            sequence: boolean, order by time sequence?

        Returns:
            None
        """

        # get all original slides
        paths = self._see(self.source)
        paths.sort()

        # if using time order
        if sequence:

            # set bypass to true
            bypass = True

            # get times
            times = [self._ask(path)['modified'] for path in paths]

            # zip with paths and sort by time
            pairs = list(zip(paths, times))
            pairs.sort(key=lambda pair: pair[1])

            # retrieve paths
            paths = [pair[0] for pair in pairs]

        # if given an order
        if order:

            # set bypass to true
            bypass = True

            # create paths
            paths = ['{}/{}'.format(self.source, entry) for entry in order]

        # split off numbers
        originals = [path.split('/')[-1].split(':')[-1] for path in paths]

        # set slides and detours to the full lists by default
        slides = [name for name in originals]
        detours = [path for path in paths]

        # if not bypassing
        if not bypass:

            # begin slides and detours
            slides = []
            detours = []

            # while X is not chosen
            while True:

                # print list of slides
                print('\nslides:')
                [print('{}) {}'.format(str(index).zfill(2), name)) for index, name in enumerate(slides)]

                # print list of originals
                print('\noriginals:')
                [print('{}) {}'.format(str(index).zfill(2), name)) for index, name in enumerate(originals)]

                # get input
                choice = input('next slide? ( X to finish )')

                # check for break condition
                if choice in ('X', 'XXX', 'x', 'xxx'):

                    # break
                    break

                # try to
                try:

                    # convert to int
                    choice = int(choice or 0)

                    # make choice
                    slide = originals[choice]
                    detour = paths[choice]

                    # add original and path to slides and detours
                    slides.append(slide)
                    detours.append(detour)

                    # remove from lists
                    originals = [name for name in originals if name != slide]
                    paths = [path for path in paths if path != detour]

                # unless index error
                except (TypeError, IndexError):

                    # in which case, skip
                    pass

        # rename files
        print('renaming files...')
        for index, (slide, detour) in enumerate(zip(slides, detours)):

            # construct destination
            destination = '{}/{}:{}'.format(self.source, index + 100, slide)

            # rename file
            os.rename(detour, destination)

        return None

    def ask(self, index):
        """Inquire about the size of an image.

        Arguments:
            index: int, index in list

        Returns:
            None
        """

        # get path
        paths = self.extract()
        path = paths[index]

        # open path
        image = Image.open('{}/{}'.format(self.source, path))
        array = numpy.array(image)

        # print shape
        print('{}: {}'.format(path, array.shape))

        return None

    def bring(self, inbound=None):
        """Bring a destop image into the slide images.

        Arguments:
            None

        Returns:
            None
        """

        # set default inbound directory
        inbound = inbound or '../../../Desktop'

        # print desktop contents
        contents = [path for path in self._see(inbound) if '.png' in path or '.pdf' in path]
        contents.sort()
        [print('{}) {}'.format(index, name.split('/')[-1])) for index, name in enumerate(contents)]

        # default choice to first
        index = 0

        # go through choices
        choice = 0
        while choice < 99:

            # get choice, defaulting to index
            choice = int(input('>>?') or index)
            index += 1

            # copy to desktop
            path = contents[choice]
            name = path.split('/')[-1]
            desktop = '{}/{}'.format(self.source, name)
            shutil.copy(path, desktop)

        return None

    def clean(self, names, keep=True):
        """Clean off numberial annotations from file names.

        Arguments:
            paths: the set of path names to remove
            keep: boolean, keep pdf?

        Returns:
            None
        """

        # construct paths
        paths = ['{}/{}'.format(self.source, name) for name in names]

        # reform paths
        for path in paths:

            # if pdf
            if path.endswith('pdf'):

                # and not keeping
                if not keep:

                    # delete file
                    os.remove(path)

            # if DS Store
            elif path.endswith('DS_Store'):

                # delete file
                os.remove(path)

            # otherwise
            else:

                # delete file
                os.remove(path)

        return None

    def crop(self, path, destination, top, bottom, left, right):
        """Crop a plot into a new plot at indices.

        Arguments:
            path: str, relative path to source
            destination: str, relative path to destination
            bottom: int, array index
            top: int, array index
            left: int, array index
            right: int, array index

        Returns:
            None
        """

        # get image as array
        image = Image.open('{}/{}'.format(self.source, path))
        array = numpy.array(image)

        # crop image
        array = array[top: bottom, left: right]

        # save image
        Image.fromarray(array).save('{}/{}'.format(self.source, destination))

        return None

    def encapsulate(self, name):
        """Convert a png to an encapsalated post script.

        Arguments:
            name: str, file name

        Returns:
            None
        """

        # open the image
        image = Image.open('{}/{}'.format(self.source, name))

        # convert to array
        array = numpy.array(image)

        # convert to eps file
        encapsulation = '{}/{}'.format(self.source, name.replace('.png', '.eps'))
        Image.fromarray(array[:, :, :3]).save(encapsulation)

        return None

    def extend(self, path, destination, width):
        """Pad an image by duplicating outer pixel.

            Arguments:
                path: str, relative input path
                destination: str, relative output path
                width: total image width

            Returns:
                None
        """

        # get image as array
        image = Image.open('{}/{}'.format(self.source, path))
        array = numpy.array(image)

        # extend the image
        shape = array.shape
        extension = width - shape[1]

        # get pad
        pad = numpy.ones((shape[0], extension, shape[2]))
        pad = pad * array[:, -2:-1]
        array = numpy.hstack([array, pad]).astype('uint8')

        # save image
        Image.fromarray(array).save('{}/{}'.format(self.source, destination))

        return None

    def extract(self):
        """Extract file path names from directory.

        Arguments:
            None

        Returns:
            list of str, the file names
        """

        # get paths
        paths = [self._file(path) for path in self._see(self.source)]
        paths.sort()

        return paths

    def fling(self, choice=None, outbound=None):
        """Fling a copy of the slide to the desktop for manipulations.

        Arguments:
            choice: int, number of file
            outbound: str, outbound directory

        Returns:
            None
        """

        # set default outbound directory
        outbound = outbound or '../../../Desktop'

        # print directory contents
        contents = self._see(self.source)
        contents.sort()
        contents.sort(key=lambda name: name.split('.')[-1], reverse=True)
        [print('{}) {}'.format(index, name.split('/')[-1])) for index, name in enumerate(contents)]

        # get choice
        choice = choice or int(input('>>?'))

        # copy to desktop
        path = contents[choice]
        name = path.split('/')[-1].split(':')[-1]
        desktop = '{}/{}'.format(outbound, name)
        shutil.copy(path, desktop)

        return None

    def glue(self, destination, *paths, vertical=False):
        """Glue too slides together horizontally.

        Arguments:
            destination: str, name of combined slide
            *paths: unpacked list of image files to glue
            vertical: boolean, stack vertically?

        Returns:
            None
        """

        # get all images
        images = [numpy.array(Image.open('{}/{}'.format(self.source, path))) for path in paths]

        # set defaul as first image
        combination = images[0]

        # if there are two images
        if len(images) == 2:

            # if not vertifal
            if not vertical:

                # combine
                combination = numpy.concatenate([images[0], images[1]], axis=1)

            # if vertical
            if vertical:

                # stack vertically
                combination = numpy.concatenate([images[0], images[1]], axis=0)

        # otherwise, if there are four images
        if len(images) == 3:

            # make two strips
            combination = numpy.concatenate([images[0], images[1], images[2]], axis=1)

        # otherwise, if there are four images
        if len(images) == 4:

            # make two strips
            strip = numpy.concatenate([images[0], images[1]], axis=1)
            stripii = numpy.concatenate([images[2], images[3]], axis=1)
            combination = numpy.concatenate([strip, stripii], axis=0)

        # otherwise, if there are twelve imaeges
        if len(images) == 12:

            # if not vertifal
            if not vertical:

                # make three strips
                strip = numpy.concatenate(images[:4], axis=1)
                stripii = numpy.concatenate(images[4:8], axis=1)
                stripiii = numpy.concatenate(images[8:12], axis=1)
                combination = numpy.concatenate([strip, stripii, stripiii], axis=0)

            # if vertical
            if vertical:

                # make four strips
                strip = numpy.concatenate(images[:3], axis=1)
                stripii = numpy.concatenate(images[3:6], axis=1)
                stripiii = numpy.concatenate(images[6:9], axis=1)
                stripiv = numpy.concatenate(images[9:12], axis=1)
                combination = numpy.concatenate([strip, stripii, stripiii, stripiv], axis=0)

        # resave
        combination = Image.fromarray(combination)
        combination.save('{}/{}'.format(self.source, destination))

        return None

    def grow(self, path, destination, factor=2):
        """Glue too slides together horizontally.

        Arguments:
            path: str, file name
            destination: str, file name
            factor: int, growth factor

        Returns:
            None
        """

        # get image
        image = Image.open('{}/{}'.format(self.source, path))

        # resize
        expansion = image.resize((math.floor(image.width * factor), math.floor(image.height * factor)), Image.ANTIALIAS)

        # save
        expansion.save('{}/{}'.format(self.source, destination))

        return None

    def merge(self, name):
        """Merge all pdfs into a single file.

        Arguments:
            name: str, merged file name

        Returns:
            None
        """

        # grab all pdf files and sort
        pages = self._see(self.source)
        pages = [page for page in pages if page.endswith('.pdf')]
        pages.sort()

        # Create a PDF merger object
        merger = PdfFileMerger()

        # for each page
        for page in pages:

            # append to merger
            merger.append(page)

        # specify the output PDF file
        destination = '{}/{}'.format(self.source, name)

        # write the merged PDF to the output file
        with open(destination, "wb") as pointer:

            # merge
            merger.write(pointer)

        # close the merger object and input PDF files
        merger.close()

        return None

    def mix(self, destination, *paths):
        """Mix too slides together horizontally.

        Arguments:
            destination: str, name of combined slide
            *paths: unpacked list of image files to glue

        Returns:
            None
        """

        # get all images
        images = [numpy.array(Image.open('{}/{}'.format(self.source, path))) for path in paths]

        # get length of images
        length = len(images)

        # get the shape of the first image
        shape = images[0].shape
        rows, columns, colors = shape

        # get width of each column
        height = int(rows / length)

        # create partitions
        partitions = [index * height for index in range(length)] + [rows]
        pairs = list(zip(partitions[:-1], partitions[1:]))

        # create image segments
        segments = [image[pair[0]:pair[1], :, :] for image, pair in zip(images, pairs)]

        # make two strips
        combination = numpy.concatenate(segments, axis=0)

        # resave
        combination = Image.fromarray(combination)
        combination.save('{}/{}'.format(self.source, destination))

        return None

    def pathfind(self, keep=False):
        """Get a list of paths.

        Arguments:
            keep: boolean, keep pdfs?

        Return:
            list of str, the paths
        """

        # get paths
        paths = self._see(self.source)

        # if not keeping pdfs
        if not keep:

            # weed out pdfs
            paths = [path for path in paths if path.endswith('.png')]

        return paths

    def ping(self, name):
        """Convert a pdf path to a png file.

        Argument:
            path: str, file path

        Returns:
            None
        """

        # if pdf
        if name.endswith('.pdf'):

            # create destination
            destination = name.replace('.pdf', '.png')

            # get images
            images = convert_from_path('{}/{}'.format(self.source, name))

            # save top image
            images[0].save('{}/{}'.format(self.source, destination))

        # otherwise
        else:

            # create destination
            destination = name.split('.')[0] + '.png'

            # open the image
            image = Image.open('{}/{}'.format(self.source, name))

            # save as a png
            image.save('{}/{}'.format(self.source, destination))

        return None

    def pull(self, *indices):
        """Pull certain paths.

        Arguments:
            *indices: unpacked list of ints, the file numbers

        Returns:
            list of str, relative file names
        """

        # get the paths
        paths = self.extract()

        # retrieve inidices
        retrieval = [paths[index] for index in indices]

        return retrieval

    def renew(self, keep=True):
        """Clean off numberial annotations from file names.

        Arguments:
            keep: boolean, keep pdf?

        Returns:
            None
        """

        # get all original slides
        paths = self._see(self.source)
        paths.sort()

        # split off numbers
        originals = [path.split('/')[-1].split(':')[-1] for path in paths]

        # reform paths
        for path, original in zip(paths, originals):

            # if pdf
            if path.endswith('pdf'):

                # and not keeping
                if not keep:

                    # delete file
                    os.remove(path)

            # if DS Store
            elif path.endswith('DS_Store'):

                # delete file
                os.remove(path)

            # otherwise
            else:

                # rename path
                self._name(path, original)

        return None

    def select(self, word):
        """Select all paths with a particular word in the path name.

        Arguments:
            word: str, the keyword

        Returns:
            list of str, the relevant paths
        """

        # get paths
        paths = [path for path in self.extract() if word in path]
        paths.sort()

        return paths

    def shrink(self, path, width=1000):
        """Shrink a .png file to a certain size based on a width in pixels.

        Arguments:
            path: str, filepath name
            width: int, width in pixels.

        Returns:
            None
        """

        # open path into image
        image = Image.open('{}/{}'.format(self.source, path))

        # get the size
        size = image.size

        # compute ratio compared to width
        ratio = width / size[0]

        # creete new size
        sizeii = (width, int(ratio * size[1]))

        # resize image
        imageii = image.resize(sizeii)

        # save image
        imageii.save('{}/{}'.format(self.source, path))

        return None

    def show(self):
        """Display all paths.

        Arguments:
            None

        Returns:
            None
        """

        # get paths
        paths = self.extract()

        # display
        self._tell(paths)

        return

    def sidle(self):
        """Add a secondary image to the right of a primary image.

        Arguments (on input):
            primary: png file path, with greenscreen background
            left: int, upper left corner horizontal
            top: int, upper left corner vertical
            right: int, lower right horizontal
            bottom: int, lower right vertical
            secondary: png file path
            upper: int, upper vertical alignment on secondary
            lower: int, lower vertical alignment on secondary
            destination: str, file path

        Returns:
            None
        """

        # print directory contents
        contents = self._see(self.source)
        contents.sort()
        [print('{}) {}'.format(index, name.split('/')[-1])) for index, name in enumerate(contents)]

        # input primary image info
        primary = contents[int(input('\nprimary image? '))]
        left = int(input('left corner? '))
        top = int(input('top corner? '))
        right = int(input('right corner? '))
        bottom = int(input('bottom corner? '))

        # input secondary image info
        secondary = contents[int(input('\nsecondary image? '))]
        upper = int(input('upper alignment? '))
        lower = int(input('lower alignment? '))

        # get destination file name
        destination = input('\nfile name? ')

        # open the background image and the graph
        image = Image.open(secondary)
        graph = Image.open(primary)

        # determine half width
        half = math.floor(graph.width / 2)

        # begin new canvas, same size as original graph
        canvas = numpy.ones((graph.height, graph.width, 4)) * 255
        canvas = canvas.astype('uint8')

        # check image height versus graph height
        ratio = graph.height / image.height

        # if the width is now too big
        if image.width * ratio > half:

            # adjust based on width
            ratio = half / image.width

        # resize image according to ratio
        image = image.resize((math.floor(image.width * ratio), math.floor(image.height * ratio)), Image.ANTIALIAS)

        # calculate margin
        margin = math.floor((graph.height - image.height) / 2)

        # insert onto canvas
        canvas[margin:image.height + margin, half:half + image.width, :] = numpy.array(image)

        # calculate new alignments
        upper = math.floor(upper * ratio)
        lower = math.floor(lower * ratio)

        # calculate ceiling and floor of original graph based on upper, lower distance
        bracket = image.height - (upper + lower)
        bracketii = graph.height - (top + bottom)
        racket = bracket / bracketii

        # take box from graph
        graph = numpy.array(graph)
        insert = graph[:, left:-right, :]
        insert = Image.fromarray(insert)

        # resize graph and insert
        insert = insert.resize((image.width, math.floor(insert.height * racket)), Image.ANTIALIAS)

        # insert at proper point
        ceiling = math.floor(top * racket)
        offset = (margin + upper) - ceiling
        block = canvas[offset:offset + insert.height, :insert.width, :]
        matrix = numpy.array(insert)[:block.shape[0], :block.shape[1], :]
        canvas[offset:offset + insert.height, :insert.width, :] = matrix

        # save graph
        Image.fromarray(canvas).save('{}/{}'.format(self.source, destination))

        return None

    def slip(self, transparency=0.5, green=[245, 245, 245]):
        """Slap a background under a graph at a particlar location.

        Arguments:
            transparency: transparency of background
            green: tuple of int, greenscreen color

        Arguments (on input):
            background: png file path
            foreground: png file path, with greenscreen background
            left: int, upper left corner horizontal
            top: int, upper left corner vertical
            right: int, lower right horizontal
            bottom: int, lower right vertical
            destination: str, file path

        Returns:
            None
        """

        # print directory contents
        contents = self._see(self.source)
        contents.sort()
        [print('{}) {}'.format(index, name.split('/')[-1])) for index, name in enumerate(contents)]

        # input background image info
        background = contents[int(input('\nslip which background image? '))]

        # input foreground image info
        foreground = contents[int(input('\nunder which foreground image? '))]
        left = int(input('left corner? '))
        top = int(input('top corner? '))
        right = int(input('right corner? '))
        bottom = int(input('bottom corner? '))

        # get destination file name
        destination = input('\nnew file name? ')

        # open the background image and the graph
        image = Image.open(background)
        graph = numpy.array(Image.open(foreground))

        # determine insert size
        height = graph.shape[0] - (top + bottom)
        width = graph.shape[1] - (left + right)

        # resize the image
        image = image.resize((width, height), Image.ANTIALIAS)
        array = numpy.array(image)

        # create mask
        hues = array[:, :, :3]

        print(f'hues: {hues.shape}')
        print(hues)

        summation = hues.sum(axis=2)
        mask = summation < sum(green) * 0.9

        print(f'summation: {summation.shape}')
        print(summation)
        print(f'mask: {mask.shape}')
        print(mask)
        print(mask[5:-5, 5:-5])

        # create film
        film = numpy.ones(mask.shape)
        film[mask] = transparency

        # apply transparency
        array[:, :, 3] = array[:, :, 3] * film

        # get the insert
        insert = graph[top:-bottom, left:-right, :]
        superimposition = insert.copy()

        # for each row
        for index, row in enumerate(insert):

            # and each column
            for indexii, pixel in enumerate(row):

                # check for green screen
                if all([color == channel for color, channel in zip(pixel[:3], green)]):

                    # replace pixel
                    superimposition[index][indexii] = array[index][indexii]

        # add back insert to graph
        graph[top:-bottom, left:-right, :] = superimposition

        # save graph
        Image.fromarray(graph).save('{}/{}'.format(self.source, destination))

        return None

    def stitch(self, name, disposal=True, order=None, archive='../bibliotek/archive'):
        """Stitch all pdfs together into a compositie.

        Arguments:
            name: str, name for file
            disposal=True: remove intermediate files?
            order=None: list of str
            archive=None: str, list of archive folder to duplicate into

        Returns:
            None
        """

        # status
        self._stamp('stitching...')

        # append the date to the file
        date = datetime.fromtimestamp(self.now).strftime('%Ym%m%d')
        name = '{}_{}'.format(date, name)

        # load up pdfs
        source = self.source
        pages = os.listdir(source)
        pages = [page for page in pages if '.png' in page and ':' in page]
        paths = ['{}/{}'.format(source, page) for page in pages]

        # convert pngs to temporary pdf files
        ghosts = [path.replace('.png', '__.png') for path in paths]

        # open all images and convert to arrays
        images = [Image.open(path).convert('RGBA') for path in paths]
        arrays = [numpy.array(image) for image in images]
        shapes = [array.shape for array in arrays]
        height = max([shape[0] for shape in shapes])
        width = max([shape[1] for shape in shapes])

        # pad each array
        pads = []
        for array in arrays:

            # get the array shape
            shape = array.shape

            # adjust height to maximum
            pad = numpy.ones((height, shape[1], shape[2])) * 255
            pad = numpy.concatenate([array, pad], axis=0)
            pad = pad[:height]

            # adjust width to maximum
            half = int(width / 2)
            left = numpy.ones((height, half, 4)) * 255
            right = numpy.ones((height, half, 4)) * 255
            pad = numpy.concatenate([left, pad, right], axis=1)
            middle = int(pad.shape[1] / 2)
            pad = pad[:, middle - half: middle + width - half]

            # convert type
            pad = pad.astype('uint8')
            pads.append(pad)

        # for each pad
        for pad, ghost in zip(pads, ghosts):

            # if not already in directory
            if ghost.split('/')[-1] not in os.listdir(source):

                # save image to ghost
                # Image.fromarray(pad).convert('RGB').save(ghost)
                Image.fromarray(pad).save(ghost)

        # sort according to order
        ghosts.sort()
        if order:

            # go through each phrase backwards
            for index, phrase in enumerate(order):

                # sort each tail according to next phrase
                tail = ghosts[index:]
                tail.sort(key=lambda path: fuzz.ratio(path, phrase), reverse=True)
                ghosts = ghosts[:index] + tail

        # initialize writer
        # writer = PdfWriter()

        # initialize canvas
        destination = '{}/{}'.format(source, name)
        document = canvas.Canvas(destination, pagesize=(width, height))

        # go through ghosts
        for ghost in ghosts:

            # print the ghost
            print(ghost)

            # add to canvas
            document.drawImage(ghost, 0, 0, width=width, height=height)
            document.showPage()

        # save
        document.save()

            # # read in ghost
            # # reader = PdfReader(ghost)
            # for page in range(len(reader.pages)):
            #
            #     # # sleep to avoid buffer overlap
            #     # sleep(1)
            #
            #     # add to writer
            #     writer.add_page(reader.pages[page])

        # # Write out the merged PDF
        # destination = '{}/{}'.format(source, name)
        # with open(destination, 'wb') as pointer:
        #
        #     # write file
        #     writer.write(pointer)

        # if archiving
        if archive:

            # copu to archive
            self._copy(destination, archive)

            # # Write out the merged PDF
            # destination = '{}/{}'.format(archive, name)
            # with open(destination, 'wb') as pointer:
            #
            #     # write file
            #     writer.write(pointer)

        # if disposal is true
        if disposal:

            # remove ghost files
            for ghost in ghosts:

                # remove
                os.remove(ghost)

        return None

    def trim(self, path, margin=2):
        """Trim whitespace around a .png file.

        Arguments:
            path: str, filepath name
            margin: int, margin width in pixels.

        Returns:
            None
        """

        # open path into image
        image = Image.open('{}/{}'.format(self.source, path))

        # covert to array and sum across all channels
        array = numpy.array(image)
        summation = array.sum(axis=2)
        transpose = summation.transpose(1, 0)

        # find all rows not all white
        indices = [index for index, row in enumerate(summation) if any([pixel < 240 * 4 for pixel in row])]

        # find all columns not all white
        indicesii = [index for index, column in enumerate(transpose) if any([pixel < 240 * 4 for pixel in column])]

        # crop array
        array = array[indices[0] - margin: indices[-1] + margin, indicesii[0] - margin: indicesii[-1] + margin]

        # save over path
        Image.fromarray(array).save('{}/{}'.format(self.source, path))

        return None