import numpy as np
import pickle
import scipy.io as sio
import print_gpp_output as prtgpp
import gpp_nn as nn
import importlib
import xarray as xr
import numpy.ma as ma
import glob
import os

importlib.reload(prtgpp)
importlib.reload(nn)

# dir_out='/tis/acps/scratch/joiner/NEE_bias/'
dir_out='/tis/acps/scratch/mbandel/studies/tempest/diurnal/model'

site_data = sio.readsav('onefluxsites.sav')

# Search for files and get the count
# Define the directory and file pattern
dir='./onefluxnet/'
file_pattern = dir + '*_HH.sav'

files = glob.glob(file_pattern)
files = sorted(files)
ncode = len(files)

minpts=3650

for icode in range(len(files)):
    fn1b = files[icode]

    # Split the file path
    strf = fn1b.split('/')

    # Extract the substring (assuming index 5 from IDL example)
    c1 = strf[2][:6]  # Equivalent to STRMID(strf(5),0,6) in IDL

    # Construct file names
    fn1 = os.path.join(dir, c1 + '_HH.sav')
    fn2 = os.path.join(dir, c1 + '_HH_mcd43.sav')

    print('restoring', fn1)
    ec_all = sio.readsav(fn1)
    rec_all = ec_all['rec_all']

    print('restoring', fn2)
    mcd_data = sio.readsav(fn2)

#     input('Press Enter to continue...')

    # Get number of records
    nrec = len(rec_all['day'])  # Assuming rec_all is iterable (array, list, etc.)
    nrec_mcd = len(mcd_data['mcd43'][:,0])  # Replace 'mcd43' with the actual key in mcd_data

    # Check if the number of records is not equal
    if nrec != nrec_mcd:
      print(f'PROBLEM!!! nrec != nrec_mcd: {nrec}, {nrec_mcd}')
      input('Press Enter to continue...')
    else:

      print(f'found {nrec} records')
      # Find the matching record
      j = np.where(site_data['recs']['name'] == c1)[0]  # Find indices where the name matches c1
      if len(j) > 0:
        lat = site_data['recs'][j[0]]['lat']
        lon = site_data['recs'][j[0]]['lon']
        print(f'Latitude: {lat}, Longitude: {lon}')
      else:
        print(f'No match in site data found for {c1}')

      # Quality control (qc) filtering
      nee_qc = rec_all['nee_vut_ref_qc']

      # Extract other variables from rec_all
      nee_uc = rec_all['nee_vut_ref_randunc']  # Uncertainty in NEE
      sw_in = rec_all['SW_IN_F']  # Shortwave incoming radiation

      """
      # Find and print the number of measurements with qc == 0
      meas_qc0 = np.where(nee_qc == 0)[0]
      nm_qc0 = len(meas_qc0)
      print(f"{nm_qc0} qc=0 out of {len(nee_qc)}")

      # Find and print the number of measurements with qc == 1
      meas_qc1 = np.where(nee_qc == 1)[0]
      nm_qc1 = len(meas_qc1)
      print(f"{nm_qc1} qc=1 out of {len(nee_qc)}")

      # Find and print the number of measurements with qc == 2
      meas_qc2 = np.where(nee_qc == 2)[0]
      nm_qc2 = len(meas_qc2)
      print(f"{nm_qc2} qc=2 out of {len(nee_qc)}")

      swp = rec_all['SW_IN_POT']  # Shortwave incoming potential
      ta_f = rec_all['TA_F']  # Air temperature
      vpd_f = rec_all['VPD_F']  # Vapor pressure deficit
      ws_f = rec_all['WS_F']  # Wind speed
      nee_vut_ref = rec_all['nee_vut_ref']  # Reference NEE

      # Compute the time and date information
      year = rec_all['year']
      month = rec_all['month']
      day = rec_all['day']
      hour = rec_all['hour']
      minute = rec_all['minute']
      time = hour + minute / 60.0  # Time in hours + fraction of hours
      ymd = year * 10000 + month * 100 + day  # Year-Month-Day as a single number
      hms = (hour * 60 + minute) / (60 * 24.0)  # Time in fraction of a day

      # Convert years and months to strings with formatting
      years = np.array([f'{yr:04d}' for yr in year])  # Equivalent to 'string(year,format="(i4)")' in IDL
      months = np.array([f'{mo:02d}' for mo in month])  # Equivalent to 'string(month,format="(i2)")' in IDL

      # Find months < 10 and prepend '0' where necessary
      k_month = np.where(month < 10)[0]
      if len(k_month) > 0:
        months[k_month] = np.array([f'0{m}' for m in month[k_month]])

      # Convert day to string with two-digit formatting
      days = np.array([f'{d:02d}' for d in day])

      # Find days < 10 and prepend '0' where necessary
      k_day = np.where(day < 10)[0]
      if len(k_day) > 0:
        days[k_day] = np.array([f'0{d}' for d in day[k_day]])

      # Combine year, month, and day into a datetime string
      date_str = years + '-' + months + '-' + days + ' 00:00:00.00'

      # Initialize day of year (doy)
      doy = day.copy()

      # Convert the date string to datetime objects and extract day of year (doy)
      for i in range(len(date_str)):
        date_obj = datetime.strptime(date_str[i], '%Y-%m-%d %H:%M:%S.%f')
        doy[i] = date_obj.timetuple().tm_yday  # Extract day of year

      # Calculate the final date as year + doy/365 + hms (fraction of day)
      date = year + doy / 365.0 + hms

      # Create date1 as year * 1000 + doy
      date1 = year * 1000 + doy

      # Replicate lat and lon arrays for each record
      lats = np.full(len(rec_all), lat)
      lons = np.full(len(rec_all), lon)

      min_date=2000060

      # Extract GPP (Gross Primary Production) values
      gpp_nt = rec_all['gpp_nt_vut_ref']
      """
      gpp = rec_all['gpp_dt_vut_ref']


      # Create the condition array using logical operations for each condition
      condition = (
        (nee_qc <= 2) &  # QC <= 2
        (nee_uc < 3) &  # Uncertainty less than 3
        (gpp > -999) &  # GPP > -999 (valid values)
        (sw_in > -999) &  # SW_IN valid
        (mcd_data['mcd43'][:,0] > -999) &  # MCD43 data valid (for each index 0-6)
        (mcd_data['mcd43'][:,1] > -999) &
        (mcd_data['mcd43'][:,2] > -999) &
        (mcd_data['mcd43'][:,3] > -999) &
        (mcd_data['mcd43'][:,4] > -999) &
        (mcd_data['mcd43'][:,5] > -999) &
        (mcd_data['mcd43'][:,6] > -999)
      )
      """
        (gpp_nt > -999) &  # GPP_nt > -999
        (date1 > min_date) &  # Date1 greater than minimum date
        (ta_f > -999) &  # Air temperature valid
        (vpd_f > -999) &  # VPD valid
        (swp > 0) &  # Shortwave potential greater than 0
      """

      # Find indices where all the conditions are met
      t = np.where(condition)[0]

      # 'nt' is the count of the filtered data points
      nt = len(t)

      # Output the number of valid data points
      print(f"Number of valid data points: {nt}")

      #train up to predict hourly GPP
      #==============================
      g_fit = np.arange(nt) # index to use all points in training

      if nt > minpts:

        #gpp_avg = (gpp[t] + gpp_nt[t]) / 2.0  # Average of GPP and GPP_NT
        gpp_avg = gpp[t]                      # Override with GPP
        sw = sw_in[t]              # Shortwave incoming radiation at valid indices
        mcd43d = mcd_data['mcd43'][t,:].T  # Subset MCD43 data for valid indices across all bands

        # name for save files for ML in Python
        name = 'GPP_hourly_'+c1

        # Call to the ML training for GPP
        gpp_trained = nn.gpp_nn ( mcd43d, gpp_avg, name + '_gpp', g_fit, g_fit, 
               sw, dir=dir_out)

        #Print statistics of the fit
        prtgpp.print_gpp_output('GPP site hourly fit ', gpp_avg, gpp_trained)

        """
        date2 = date[t]            # Subset dates for valid indices
        diff=gpp_avg-gpp_trained
        plot,date2,gpp_avg,psym=3,title=c1,xtitle='Date',ytitle='GPP'
        oplot,date2,gpp_trained, psym=3, color=2
        """

