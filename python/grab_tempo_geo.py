from pathlib import Path
import gc 
import glob
import h5py
import numpy as np
import sys
import earthaccess

#User input of date to grab TEMPO files to make Ancillary Geo Files 
year = int(sys.argv[1])
month_st = int(sys.argv[2])
day_st = int(sys.argv[3])

geo_vars = {}

earthaccess.login(persist=True)


for month in range(month_st,month_st+1):
    for day in range(day_st,day_st+1):

        time_stamp  = str(year)+'-'+str(month).zfill(2)+'-'+str(day).zfill(2)
        #Searching for TEMPO NO2 files to grab geo information
        results = earthaccess.search_data(short_name =  'TEMPO_NO2_L2',version = 'V02',cloud_hosted=True,temporal=(time_stamp+" 00:00:00", time_stamp+" 23:59:00"),count=200)
        files = earthaccess.open(results)

        for result in files:

            #Opening TEMPO NO2 file and grabbing lat/lon 
            filename =  result.full_name
            data = h5py.File(result,'r')
            file_parts = filename.split('/')

            
            lat = data['geolocation/latitude'][:]
            lon = data['geolocation/longitude'][:]

            data.close()
            del data 
            gc.collect()

            #Storing TEMPO Lat/Lon in ancillary file
            output_file = '/user/1002/zfasnach/TEMPO/Ancillary/'+str(year)+'/'+str(month).zfill(2)+'/'+str(day).zfill(2)+'/'
            Path(output_file).mkdir(parents=True, exist_ok=True)
            f = h5py.File(output_file+file_parts[-1].replace('.nc','_Ancillary.nc').replace('_NO2_L2',''),'w')
            
            
            f.create_dataset('latitude',data=lat,dtype=np.float32, compression="gzip", compression_opts=9)
            f.create_dataset('longitude',data=lon,dtype=np.float32, compression="gzip", compression_opts=9)
            f.close()

            del f, lat, lon
            gc.collect()
