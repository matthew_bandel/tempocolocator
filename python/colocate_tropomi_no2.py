import time
from matplotlib.collections import PolyCollection
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from pylab import *
import datetime
import earthaccess
import random
import h5py
import gc
from netCDF4 import Dataset
import numpy as np 
from scipy import stats
import glob 


#Simple function for finding nearest index in array for specific value
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


#Simple function for nearest neighbor interpolation to regular grid 
def colocate_data(grid,lon,lat,hour,lon_bins,lat_bins,hourly=False):

    lon_mid = (lon_bins[0:-1]+lon_bins[1:])/2.
    lat_mid = (lat_bins[0:-1]+lat_bins[1:])/2.

    lon_inds = np.array([find_nearest(lon_mid,lon[i]) for  i in range(len(lon))])
    lat_inds = np.array([find_nearest(lat_mid,lat[i]) for  i in range(len(lat))])
    #time_inds = np.array([find_nearest(np.arange(24),hour[i]) for  i in range(len(hour))])

    if not hourly:
        return np.nanmean(grid,axis=-1)[lon_inds,lat_inds]
    else:
        return grid[lon_inds,lat_inds,time_inds]

#Function for colocating TROPOMI NO2 data to PACE FoV
#Requires the following inputs:
#pace_lon - 1d array of PACE longitude values
#pace_lat - 1d array of PACE latitude values
#start_date - string representing start time in format 'YYY-MM-DD HH:MM:SS'
#end_date - string representing end time in format 'YYY-MM-DD HH:MM:SS'
#Optional Input (that normally should be passed in...):
#file_path(optional) - location for TROPOMI data files to be downloaded...works better to download TROPOMI data rather than pulling into memory each time
def colocate_tropomi(pace_lon,pace_lat,pace_hour,start_date,end_date,file_path = '/explore/nobackup/people/zfasnach/PACE_Data/',qc_thresh=75,hourly=False,grid_res=0.025,time_smooth=1):

    year = int(start_date[0:4])
    month = int(start_date[5:7])
    day = int(start_date[8:10])
    end_day = int(end_date[8:10])
    

    #Checking if TROPOMI NO2 files have already been downloaded, if not, they are downloaded from earthdata
    #Note count = 20 assumes the colocation is being run for a single day (approx 15 orbits per day)
    #Note the connection normally drops once or twice, so might need to run again to catch all files, if run again, it only downloads files that didn't download yet
    #trop_no2_files = glob.glob(file_path+'/S5P_OFFL_L2__NO2____'+str(year)+str(month).zfill(2)+str(day).zfill(2)+'*')

    trop_no2_files = []

    if len(trop_no2_files) == 0:
        earthaccess.login(persist=True)    
        results = earthaccess.search_data(short_name = 'S5P_L2__NO2____HiR',cloud_hosted=True,temporal=(start_date,end_date),count=20,bounding_box=(-180,-90,180,90))
        earthaccess.download(results,file_path)
        for i in range(day,end_day+1):
            trop_no2_files = np.hstack((trop_no2_files,glob.glob(file_path+'/S5P_OFFL_L2__NO2____'+str(year)+str(month).zfill(2)+str(i).zfill(2)+'*')))

    #Selected TROPOMI variables to colocate to PACE FoV 
    #tropomi_keys = ['no2_vcd']
    tropomi_keys = ['no2_scd','no2_vcd','no2_strat']
    #tropomi_keys = ['no2_scd','no2_vcd','amf_strat','no2_strat','spress','amf_trop']

    data_sums = {}
    data_counts = {}
    pace_colocated = {}
    gridded_data = {}
    
    lon_bins = np.arange(-180,180.0+grid_res,grid_res)
    lat_bins = np.arange(0,90.0+grid_res,grid_res)

    for key in tropomi_keys:
        data_sums[key] = np.zeros((len(lon_bins)-1,len(lat_bins)-1,24),dtype=np.float32)
        data_counts[key] = np.zeros((len(lon_bins)-1,len(lat_bins)-1,24),dtype=np.float32)

    start = True
    trop_data = {}
    for filename in trop_no2_files:
        l2_data = {}
        print(filename)

        #Reading TROPOMI L2 NO2 files
        f = h5py.File(filename,'r')  
        
        data_group = '/PRODUCT/SUPPORT_DATA/DETAILED_RESULTS/'
        product_group = '/PRODUCT/'
        vza = f['/PRODUCT/SUPPORT_DATA/GEOLOCATIONS/viewing_zenith_angle'][0]
        sza = f['/PRODUCT/SUPPORT_DATA/GEOLOCATIONS/solar_zenith_angle'][0]
        hour = np.array([datetime.datetime.strptime(str(i,'utf-8'), '%Y-%m-%dT%H:%M:%S.%fZ') for i in f['/PRODUCT/time_utc'][0]])
        hour = np.array([i.hour + (i.minute/60) for  i in hour])
                         
        l2_data['no2_strat'] = f[data_group+'nitrogendioxide_stratospheric_column'][0]
        l2_data['no2_strat'][l2_data['no2_strat'] == 9.96921e+36] = np.nan

        no2_trop = f[product_group+'nitrogendioxide_tropospheric_column'][0]
        no2_trop[no2_trop == 9.96921e+36] = np.nan
        l2_data['no2_vcd'] = no2_trop + l2_data['no2_strat']

        no2_scd = f[data_group+'nitrogendioxide_slant_column_density'][0]
        no2_scd_stripe = f[data_group+'nitrogendioxide_slant_column_density_stripe_amplitude'][0]

        #Calculating TROPOMI SCD by applying stripe correction and dividing by Geometric AMF to remove angular dependence of SCD 
        amf = (1./np.cos(np.radians(sza))) + (1./np.cos(np.radians(vza)))
        nline, nrow = no2_scd.shape

        hour = np.repeat(hour[:,np.newaxis],nrow,axis=1)
        
        no2_scd_stripe[no2_scd_stripe == 9.96921e+36] = np.nan
        no2_scd = no2_scd -  np.repeat(no2_scd_stripe[np.newaxis,:],nline,axis=0)
            
        no2_scd[no2_scd == 9.96921e+36] = np.nan
        l2_data['no2_scd'] = no2_scd/amf
        
        trop_lat = f[product_group+'latitude'][0]
        trop_lon = f[product_group+'longitude'][0]

        
        trop_snow_ice = np.array(f['/PRODUCT/SUPPORT_DATA/INPUT_DATA/snow_ice_flag'][0],dtype=int)

        
        qc = f['/PRODUCT/qa_value'][0]

        #Screening TROPOMI data to grab only highest quality TROPOMI data


        #Binning Level 2 TROPOMI data to 0.05x0.05 degree grid which will then be used to co-locate to PACE FoV
        if start:
            nan_inds = (~np.isnan(l2_data['no2_scd'])) & (l2_data['no2_strat'] != 9.96921e+36) & (qc >= qc_thresh)  & ((trop_snow_ice == 0) | ( trop_snow_ice == 255))
            for key in tropomi_keys:
                trop_data[key]  = np.array(l2_data[key])[nan_inds]
            trop_data['lat']  = np.array(trop_lat)[nan_inds]
            trop_data['lon']  = np.array(trop_lon)[nan_inds]
            if hourly:
                trop_data['hour']  = np.array(hour)[nan_inds]
            start = False
        else:
            nan_inds = (~np.isnan(l2_data['no2_scd'])) & (l2_data['no2_strat'] != 9.96921e+36) & (qc >= qc_thresh)  & ((trop_snow_ice == 0) | ( trop_snow_ice == 255))
            for key in tropomi_keys:
                trop_data[key] = np.hstack((trop_data[key],np.array(l2_data[key])[nan_inds]))
            trop_data['lat'] = np.hstack((trop_data['lat'],np.array(trop_lat)[nan_inds]))
            trop_data['lon'] = np.hstack((trop_data['lon'],np.array(trop_lon)[nan_inds]))
            if hourly:
                trop_data['hour'] = np.hstack((trop_data['hour'],np.array(hour)[nan_inds]))
                    
        f.close()                
        del l2_data, no2_scd, qc, f, no2_scd_stripe, amf
        gc.collect()

    #Calculating gridded TROPOMI 0.05x0.05 degree gridded data after summing up each individual orbit 
    for key in tropomi_keys:

        if hourly:

            pace_colocated[key] = np.zeros((len(pace_lon))) + np.nan
            
            #for hour in np.arange(1.5,24,3):
            #for hour in np.arange(0.5,24,1):
            delta = time_smooth/2
            for hour in np.arange(delta,24,time_smooth):

                hr_inds = (hour - delta < trop_data['hour'] ) & (hour + delta > trop_data['hour'] )
                if len(trop_data['hour'][hr_inds]) == 0:
                    continue
                ret_sum, x_edge, y_edge, _ = stats.binned_statistic_2d(trop_data['lon'][hr_inds], trop_data['lat'][hr_inds], trop_data[key][hr_inds], 'sum', bins=[lon_bins,lat_bins])
                ret_count, x_edge, y_edge, _  = stats.binned_statistic_2d(trop_data['lon'][hr_inds], trop_data['lat'][hr_inds], trop_data[key][hr_inds], 'count', bins=[lon_bins,lat_bins])

                gridded_data = ret_sum/ret_count
                gridded_data[ret_count == 0] = np.nan

                #inds = pace_hour == hour
                inds = (hour - delta < pace_hour) & (hour + delta > pace_hour) 
                if len(pace_hour[inds]) == 0:
                    continue

                pace_colocated[key][inds] = colocate_data(gridded_data[:,:,np.newaxis],pace_lon[inds],pace_lat[inds],pace_hour[inds],lon_bins,lat_bins)

                del ret_sum, ret_count, gridded_data, inds, hr_inds
                gc.collect()
        else:
            ret_sum, x_edge, y_edge, _ = stats.binned_statistic_2d(trop_data['lon'], trop_data['lat'], trop_data[key], 'sum', bins=[lon_bins,lat_bins])
            ret_count, x_edge, y_edge, _  = stats.binned_statistic_2d(trop_data['lon'], trop_data['lat'], trop_data[key], 'count', bins=[lon_bins,lat_bins])
            
            gridded_data = ret_sum/ret_count
            gridded_data[ret_count == 0] = np.nan

            pace_colocated[key] = colocate_data(gridded_data[:,:,np.newaxis],pace_lon,pace_lat,pace_hour,lon_bins,lat_bins)
            del ret_sum, x_edge, y_edge, ret_count,  gridded_data
            gc.collect()

    del trop_data
    gc.collect()
    #pace_colocated['spress'] = np.exp(pace_colocated['spress'])
    return pace_colocated
