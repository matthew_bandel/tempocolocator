import glob
import numpy as np
import xarray as xr
from tempo_pipeline_tools import *
import datetime

#files = glob.glob('/user/1002/zfasnach/TEMPO_CLDO4_L2/2024/*/*/*nc')                                                                                                                                                      
#files = glob.glob('/data/1002/omi/TEMPO/metadata/TEMPO_CLDO4*json')
files = glob.glob('metadata/TEMPO_CLDO4_L2/metadata/*json')

botocore.register_initializer(add_credential_refresh_to_session)
#earthaccess.login(strategy="netrc")


'''date_1 = datetime.datetime(2024,2,27); date_2 = datetime.datetime(2024,2,28)

search_kwargs = {
    'short_name'   : 'TEMPO_CLDO4_L2',
    'version'      : 'V02',
    'temporal'     : (date_1.strftime('%Y-%m-%d'),date_2.strftime('%Y-%m-%d')),
        }
results = earthaccess.search_data(cloud_hosted=True, count=200, **search_kwargs)
s3_urls = list(map(get_s3_url, results))
metadata = create_metadata(s3_urls, parallel=parallel,folder='metadata/'+shortname)'''


for filename in files:
    #data = xr.open_dataset(filename,group='product')                                                                                                                                                                      
    print(filename)

    
    data = xr.open_dataset('reference://', **{
        'engine'         : 'zarr',
        'group'          : 'product',
        'backend_kwargs' : {
        'storage_options' : {'fo': filename},
        'consolidated'    : False,
        }})


    print(filename,np.nanmean(data['cloud_fraction'].values))
