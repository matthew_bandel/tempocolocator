#!/usr/bin/env python3

# squalls.py to validate NN predictions

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula

# import system
import sys

# import re
import re

# import datetime
import datetime

# import math
import math

# import numpy functions
import numpy
import scipy

# import shap for shapely analysis
import shap

# import netcdf4
import netCDF4

# import joblib for unpickling models
import joblib

# import sklearn functions
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.metrics import r2_score, root_mean_squared_error
from sklearn.metrics.pairwise import cosine_similarity, pairwise_distances
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsRegressor
from sklearn.cluster import KMeans
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

# # import netcdf4
# import netCDF4

# import tensorflow
import tensorflow
import tensorflow_addons
import tensorflow_probability

# import earthaccess
import earthaccess

# import boto3 for aws access
import boto3
import botocore

# import gc for garbage collection
import gc

# import pipeline tools
import tempo_pipeline_tools

# try to
try:

	# import matplotlib for plots
	import matplotlib
	from matplotlib import pyplot
	from matplotlib import style as Style
	from matplotlib import rcParams
	Style.use('fast')
	rcParams['axes.formatter.useoffset'] = False
	rcParams['figure.max_open_warning'] = False

# unless import error
except ImportError:

	# ignore matplotlib
	print('matplotlib not available!')

# try to
try:

	# import cartopy
	import cartopy
	from cartopy.mpl.geoaxes import GeoAxes

# unless import error
except ImportError:

	# ignore matplotlib
	print('cartopy not available!')


# class squall to do OMOCNUV analysis
class Squall(Hydra):
	"""Squall class to prepare synthetic tempo training data.

	Inherits from:
		Hydra
	"""

	def __init__(self, year=2024, month=7, day=1, site='US-ARb', tag=None, session=None, sink='.', limits=None):
		"""Initialize instance.

		Arguments:
			year: int, the year
			month: int, the month
			day: int, the day
			tag: tag for model training
			session: date of model training
			sink: str, file path for sink folder
			limits: dict, limit alterations
		"""

		# initialize the base Core instance
		Hydra.__init__(self, sink)

		# set fill value
		self.fill = numpy.nan

		# set sink
		self.sink = sink

		# set date attribute
		self.year = year
		self.month = month
		self.day = day
		self.date = ''
		self.stub = ''
		self._calendar()

		# set version
		self.version = 'V03'

		# set grid size
		self.mirror = 1200
		self.track = 2048

		# set model information
		self.session = session
		self.tag = tag

		# retrieve model
		self.model = None
		self.details = None
		self.folder = None
		self.features = None
		self._retrieve()

		# parse yaml file
		self.yam = {}
		self.architecture = {}
		self.training = {}
		self.validation = {}
		self._parse()

		# set limits
		self.limits = limits or {}
		self._limit()

		# set default instances
		self.instances = 50
		self._instantiate()

		# create sites catalog
		self.site = site
		self.catalog = {}
		self._site()

		# set units
		self.target = '$GPP$'
		self.unit = '$umolCO_2/m^2 s$'

		return

	def __repr__(self):
		"""Create string for on screen representation.

		Arguments:
			None

		Returns:
			str
		"""

		# create representation
		formats = (self.sink, self.site, self.date, self.tag, self.session)
		representation = ' < Squall instance at {}, {}, {}, {} ( {} ) >'.format(*formats)

		return representation

	def _calendar(self):
		"""Format the date strings.

		Arguments:
			None

		Returns:
			None
		"""

		# create formatw
		formats = (self.year, self._pad(self.month), self._pad(self.day))
		self.date = '{}m{}{}'.format(*formats)
		self.stub = '{}/{}/{}'.format(*formats)

		return None

	def _access(self, name, labels=None, count=200, window=None):
		"""Collect tempo files.

		Arguments:
			name: str, short name
			labels: list of str, the scan labels
			count: number of results to obtain
			window: number of days to expand search

		Returns:
			None
		"""

		# set formats
		formats = (self.year, self._pad(self.month), self._pad(self.day))

		# create L1B folders
		self._make('{}/{}'.format(self.sink, name))
		self._make('{}/{}/{}'.format(self.sink, name, *formats[:1]))
		self._make('{}/{}/{}/{}'.format(self.sink, name, *formats[:2]))
		self._make('{}/{}/{}'.format(self.sink, name, self.stub))

		# login to earthaccess
		earthaccess.login(strategy="netrc")

		# construct date
		dates = [datetime.datetime(self.year, self.month, self.day)]
		dates += [dates[0] + datetime.timedelta(days=1)]

		# if given a window:
		if window:

			# split in half
			half = int(window / 2)

			# construct date
			date = datetime.datetime(self.year, self.month, self.day) - datetime.timedelta(days=half)
			dateii = datetime.datetime(self.year, self.month, self.day) + datetime.timedelta(days=half)
			dates = [date, dateii]

		# convert to strings
		dates = [str(date)[:10] for date in dates]

		print('dates: {}'.format(dates))

		# get results
		#results = earthaccess.search_data(short_name=name, version=self.version, temporal=tuple(dates), count=count)
		results = earthaccess.search_data(short_name=name, temporal=tuple(dates), count=count)

		# if given scan labels:
		if labels:

			# group according to scan
			groups = self._group(labels, lambda label: re.findall('S[0-9]{3}', label)[0])

			# for each label
			for scan, group in groups.items():

				# print label
				self._print('scan {}...'.format(scan))

				# filter results by date and scan labels
				date = '_{}{}{}T'.format(*formats)
				subset = [result for result in results if any([label in result['umm']['GranuleUR'] for label in group])]
				subset = [result for result in subset if date in result['umm']['GranuleUR']]

				# if there are results
				if len(subset) > 0:

					# download
					earthaccess.download(subset, '{}/{}/{}'.format(self.sink, name, self.stub))

		# otherwise
		else:

			# download
			earthaccess.download(results, '{}/{}/{}'.format(self.sink, name, self.stub))

		return None

	def _anchor(self, target, reference):
		"""Choose the corner in which to position the plot legend.

		Arguments:
			target: tuple of floats, the point for comparison
			reference: tuple of floats, the reference point

		Returns:
			str, the legend location
		"""

		# begin location
		location = ''

		# if truth is greater than prediction
		if (target[1] > reference[1]):

			# put legend at bottom
			location += 'lower '

		# otherwise
		else:

			# put legend on bottom
			location += 'upper '

		# if truth is greater than median
		if (target[0] > reference[0]):

			# put legent at left
			location += 'left'

		# otherwise
		else:

			# put legend at right
			location += 'right'

		return location

	def _balance(self, predictors, validation, bins=10):
		"""Determine weights for balancing the dataset.

		Arguments:
			predictors: feature matrix
			validation: feature matrix for validation
			bins: int, number of bins for each feature

		Returns:
			tuple of numpy arrays, the sample weights
		"""

		# collect weights
		weights = []
		weightsii = []

		# for each column except last
		for column in range(predictors.shape[1]):

			# get bin counts
			counts, edges = numpy.histogram(predictors[:, column], bins=bins)

			# assign to bins, adjusting edges
			epsilon = 1e-4
			edges[-1] = edges[-1] + epsilon
			indices = numpy.digitize(predictors[:, column], edges, right=False)
			indices = indices - 1

			# determine weights based on counts and normalize to largest
			weight = 1 / counts
			weight = weight / weight.max()

			# get sample weights for each sample based on index
			samples = weight[indices]

			# add to weights
			weights.append(samples)

			# assign validation to bins, adjusting edges
			epsilon = 1e-4
			edges[-1] = edges[-1] + epsilon
			indicesii = numpy.digitize(validation[:, column], edges, right=False)
			indicesii = indicesii - 1
			indicesii = numpy.where(indicesii > bins - 1, bins - 1)

			# get sample weights for each sample based on index
			samplesii = weight[indicesii]

			# add to weights
			weightsii.append(samplesii)

		# create array, and multiply
		weights = numpy.array(weights).sum(axis=0)
		weights = weights / weights.max()

		# create validation array, and multiply
		weightsii = numpy.array(weightsii).sum(axis=0)
		weightsii = weightsii / weights.max()

		return weights, weightsii

	def _band(self, sites, radius=0, high=True):
		"""Collect MODIS and Merra band information.

		Arguments:
			sites: list of str, the sites of interest
			radius: float, distance on either side of site location in degrees

		Returns:
			dict
		"""

		# begin collection
		data = {site: {'albedo': [], 'wave': [], 'hour': [], 'latitude': [], 'longitude': []} for site in sites}

		# for each modis band
		for band in range(7):

			# if using high resolution modis bands
			if high:

				# create hydra
				hydra = Hydra('{}/MCD43D{}/{}'.format(self.sink, band + 62, self.stub), show=False)
				hydra.ingest()

				# grab albedo
				albedo = hydra.grab('Albedo').squeeze()
				factor = hydra.grab('scale_factor')[0][0]
				albedo = albedo * factor
				shape = albedo.shape

			# otherwise
			else:

				# create hydra
				hydra = Hydra('{}/MCD43C4/{}'.format(self.sink, self.stub), show=False)
				hydra.ingest()

				# grab albedo
				albedo = hydra.grab('Band{}/Nadir_Reflectance'.format(band + 1)).squeeze()
				factor = hydra.grab('Band{}/scale_factor'.format(band + 1))[0][0]
				albedo = albedo * factor
				shape = albedo.shape

			# construct latitudes, starting at 90N
			size = 180 / shape[0]
			half = size / 2
			latitudes = numpy.array([90 - half - (index * size) for index in range(shape[0])])

			# construct longitudes, staring at 180W
			size = 360 / shape[1]
			half = size / 2
			longitudes = numpy.array([-180 + half + (index * size) for index in range(shape[1])])

			# for each site
			for site in sites:

				# get coordinates
				latitude = self.catalog[site]['latitude']
				longitude = self.catalog[site]['longitude']

				# if non zero radius
				if radius > 0:

					# get inclusive latitude and longitude indices
					parallels = numpy.where((latitudes >= latitude - radius) & (latitudes <= latitude + radius))[0]
					meridians = numpy.where((longitudes >= longitude - radius) & (longitudes <= longitude + radius))[0]

					# construct parallels grid
					latitudesii = numpy.array([latitudes[parallels.min(): parallels.max() + 1]] * len(meridians))
					data[site]['latitude'] = latitudesii.transpose(1, 0)

					# construct meridians grid
					longitudesii = numpy.array([longitudes[meridians.min(): meridians.max() + 1]] * len(parallels))
					data[site]['longitude'] = longitudesii

					# get associated albedo
					albedos = albedo[parallels.min(): parallels.max() + 1, meridians.min(): meridians.max() + 1]
					data[site]['albedo'].append(albedos)

				# otherwise
				else:

					# get albedo at nearest coordinates
					parallel = self._pin(latitude, latitudes)[0][0]
					meridian = self._pin(longitude, longitudes)[0][0]

					# add to collection
					data[site]['albedo'].append(albedo[parallel, meridian])

		# get short wave data
		hydra = Hydra('{}/Merra_SW/{}'.format(self.sink, self.stub))
		hydra.ingest(0)

		# get coordinates
		wave = hydra.grab('SWGDN')
		latitudes = hydra.grab('lat')
		longitudes = hydra.grab('lon')
		hours = (hydra.grab('time') + 30) / 60

		# for each site
		for site in sites:

			# get site location
			latitude = self.catalog[site]['latitude']
			longitude = self.catalog[site]['longitude']

			# if nonzero radius
			if radius > 0:

				# get modis latitudes and longitudes
				latitudesii = data[site]['latitude']
				longitudesii = data[site]['longitude']

				# create waves array
				waves = numpy.ones((wave.shape[0], latitudesii.shape[0], longitudesii.shape[1]))

				# for each latitude
				for index in range(waves.shape[1]):

					# and each longitude
					for indexii in range(waves.shape[2]):

						# get closest coordinates
						parallel = self._pin(latitudesii[index, indexii], latitudes)[0][0]
						meridian = self._pin(longitudesii[index, indexii], longitudes)[0][0]

						# add to waves
						waves[:, index, indexii] = wave[:, parallel, meridian]

				# add to collection
				data[site]['wave'] = waves
				data[site]['hour'] = hours

			# otherwise
			else:

				# get closest coordinates
				parallel = self._pin(latitude, latitudes)[0][0]
				meridian = self._pin(longitude, longitudes)[0][0]

				# add to waves
				data[site]['wave'] = wave[:, parallel, meridian].tolist()
				data[site]['hour'] = hours

		# reform into arrays
		data = {site: {field: numpy.array(array) for field, array in data[site].items()} for site in sites}

		return data

	def _bend(self, tensor):
		"""Define bend identity activation function.

		Arguments:
			tensor: numpy array, incoming tensor

		Returns:
			numpy array, bent identity
		"""

		# construct identity
		identity = ((tensorflow.sqrt(tensor ** 2 + 1) - 1) / 2) + tensor

		return identity

	def _bin(self, truth, prediction, samples, bracket, bins=1000):
		"""Perform binning for regression sample plots.

		Arguments:
			truth: numpy array, the truths
			prediction: numpy array, the predictions
			samples: int, number of samples for sample cutoff
			bracket: tuple of floats, the prediction limits
			bins: int, number of 2-D bins

		Returns:
			tuple of numpy arrays, the grids and counts
		"""

		# calculate sample bins
		options = {'statistic': 'count', 'range': (bracket, bracket), 'bins': bins}
		counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(truth, prediction, None, **options)
		truths = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
		predictions = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
		mesh, meshii = numpy.meshgrid(truths, predictions, indexing='ij')

		# apply mask
		mask = counts > 0
		counts = counts[mask]
		mesh = mesh[mask]
		meshii = meshii[mask]

		# clip counters to samples limit
		counts = numpy.where(counts > samples, samples, counts)

		# order counters
		order = numpy.argsort(counts)
		counts = counts[order]
		mesh = mesh[order]
		meshii = meshii[order]

		return mesh, meshii, counts

	def _catalog(self):
		"""Get a list of site names.

		Arguments:
			None

		Returns:
			list of str, the site names
		"""

		# open site list
		paths = self._see('{}/Onefluxnet/sites'.format(self.sink))
		path = [path for path in paths if '.sav' in path][0]
		sites = self._read(path)

		# get all names
		names = [code.decode('utf-8') for code in sites['recs']['name']]
		latitudes = numpy.array(sites['recs']['lat'])
		longitudes = numpy.array(sites['recs']['lon'])

		# create catalog
		zipper = zip(names, latitudes, longitudes)
		catalog = {name: {'latitude': latitude, 'longitude': longitude} for name, latitude, longitude in zipper}

		# set catalog
		self.catalog = catalog

		return None

	def _clear(self, folder):
		"""Clear a folder.

		Arugments:
			folder: str, folder name

		Returns:
			None
		"""

		# clean folder
		self._clean('{}/{}'.format(self.folder, folder))

		return None

	def _cluster(self, matrix, number, exclusions):
		"""Perform clustering on a matrix.

		Arguments:
			matrix: numpy array
			number: int, number of clusters
			exclusions: list of ints, the columns to exclude

		Returns:
			numpy array, the cluster centroids
		"""

		# create a copy
		xerox = matrix.copy()
		mean = matrix.mean(axis=0)
		deviation = matrix.std(axis=0)

		# normalize matrix
		xerox = (xerox - mean) / deviation

		# for each excluded column ( including final truth column )
		for column in exclusions + [xerox.shape[-1] - 1]:

			# replace with median
			xerox[:, column] = numpy.percentile(xerox[:, column], 50)

		# set up clusterer
		clusterer = KMeans(n_clusters=number, random_state=42)
		clusterer.fit(xerox)

		# extract labels and centroids
		centroids = clusterer.cluster_centers_

		# unnormalize
		centroids = (centroids * deviation) + mean

		return centroids

	def _constrain(self, data, constraints):
		"""Apply constraints to a data set.

		Arguments:
			data: dict of numpy arrays
			constraints: numpy array, boolean mask

		Returns:
			dict of numpy arrays
		"""

		# begin valid dataset
		valids = {}

		# for each feature
		for name, array in data.items():

			# add to data
			valids[name] = array

			# if shapes match
			if array.shape[0] == constraints.shape[0]:

				# apply constraints
				valids[name] = array[constraints]

		return valids

	def _decode(self, details):
		"""Decode byte strings.

		Arguments:
			details: dict of str, arrays

		Returns:
			dict
		"""

		# for each field
		for field, array in details.items():

			# if a string type
			if 'S' in str(array.dtype):

				# decode
				details[field] = numpy.char.decode(array, 'utf-8')

		return details

	def _feature(self):
		"""Set feature list for model training.

		Arguments:
			None

		Returns:
			None

		Populates:
			self.features
		"""

		# begin features
		features = []

		# for each feature
		for feature, limit in self.yam['features'].items():

			# if a limit is given:
			if limit:

				# add all indices
				features += ['{}|{}'.format(feature, self._pad(number)) for number in range(self.limits[limit])]

			# otherwise
			else:

				# add singlet feature
				features += [feature]

		# set features
		self.features = features

		return None

	def _fill(self, array):
		"""Replace various fill values with self.fill.

		Arguments:
			array: numpy array

		Returns:
			numpy array
		"""

		# create mask for valid data
		mask = self._mask(array)

		# fill with fill value
		array = numpy.where(mask, array, self.fill)

		return array

	def _forge(self, data, features):
		"""Forge a matrix from a dataset and list of features.

		Arguments:
			data: dict of fields and numpy arrays
			features: list of str, the features

		Returns:
			numpy array
		"""

		# begin vectors
		vectors = []

		# set transforms
		transforms = {'cos': lambda vector: numpy.cos(numpy.radians(vector))}

		# for each feature
		for feature in features:

			# set defaults
			name = feature
			position = None
			transform = None

			# if colon
			if '|' in name:

				# strip paranthesis
				name, position = name.split('|')
				position = int(position)

			# if parenthesis
			if '(' in name:

				# strip paranthesis
				transform, name = name.split('(')

			# get vector
			vector = data[name]

			# if position
			if position is not None:

				# extract position
				vector = vector[:, position].reshape(-1, 1)

			# if transform
			if transform:

				# apply transform
				vector = transforms[transform](vector)

			# add vector
			vectors.append(vector)

		# stack
		block = numpy.hstack(vectors)

		return block

	def _graph(self, abscissa, ordinate, intensity, bounds, scale, gradient, title, unit, labels, **options):
		"""Generate graph object for plotting.

		Arguments:
			abscissa: numpy array, horizontal coordinates
			ordinate: numpy array, vertical coordinates
			intensity: numpy array, value for color
			bounds: list of ( float, float ) tuples, the x-axis and y-axis bounds
			scale: tuple of floats, the color scale boundaries
			gradient: str, the name of the matplotlib color gradient
			title: str, plot title
			units: str, plot units
			labels: list of str, the x and y axis labels
			**options: unpacked dictionary of options:
				globe: boolean, draw coastlines?
				logarithm: boolean, use logarithmic scale?
				colorbar: boolean, add colorbar?
				lines: list of (array, array, str) tuples, additional lines ( x, y, format str )
				pixel: float, the pixel size
				polygons: boolean, graph as polygons?
				corners: list of numpy arrays, the latitude and longitude corners
				selection: indices of color gradient
				categories: list of ( str, str ) tuples, the colors and categories
				reflectivity: boolean, plotting reflectivity?
				alpha: float, transparency level
				font: int, title font size
				fontii: int, marker font size
				clip: boolean, clip endpoints to scale limits?
				times: format axis in time?
				annotations: list of x,y, label tuples

		Returns:
			dictionary, graph specifics
		"""

		# set default options
		globe = options.get('globe', True)
		logarithm = options.get('logarithm', False)
		colorbar = options.get('colorbar', True)
		lines = options.get('lines', [])
		pixel = options.get('pixel', abscissa.shape[0] / ( 72 * 16))
		polygons = options.get('polygons', False)
		corners = options.get('corners', [None, None])
		ticks = options.get('ticks', None)
		ticksii = options.get('ticksii', None)
		marks = options.get('marks', None)
		marksii = options.get('marksii', None)
		marker = options.get('marker', 2)
		selection = options.get('selection', (0, 256))
		categories = options.get('categories', None)
		reflectivity = options.get('reflectivity', None)
		alpha = options.get('alpha', 0.8)
		font = options.get('font', 20)
		fontii = options.get('fontii', 15)
		clip = options.get('clip', True)
		times = options.get('times', False)
		annotations = options.get('annotations', None)

		# create graph object
		graph = {'abscissa': abscissa, 'ordinate': ordinate, 'intensity': intensity}
		graph.update({'x_bounds': bounds[0], 'y_bounds': bounds[1], 'scale': scale, 'gradient': gradient})
		graph.update({'title': title, 'units': unit, 'x_label': labels[0], 'y_label': labels[1]})
		graph.update({'globe': globe, 'logarithm': logarithm, 'colorbar': colorbar, 'lines': lines})
		graph.update({'pixel': pixel, 'polygons': polygons, 'corners': corners})
		graph.update({'x_ticks': ticks, 'y_ticks': ticksii, 'x_marks': marks, 'y_marks': marksii})
		graph.update({'marker': marker, 'selection': selection, 'categories': categories})
		graph.update({'reflectivity': reflectivity, 'alpha': alpha, 'font': font, 'fontii': fontii})
		graph.update({'clip': clip, 'times': times, 'annotations': annotations})

		return graph

	def _instantiate(self):
		"""Set number of instances for bayesian means and deviations from yaml file.

		Arguments:
			None

		Returns:
			None

		Populates:
			self.instances
		"""

		# set instances
		self.instances = self.validation['instances']

		return None

	def _interpolate(self, quantity, nodes, nodesii, target, targetii, method='nearest'):
		"""Perform two dimensional interpolation of a quantity from one grid to another.

		Arguments:
			quantity: 2-D numpy.array of gridded data
			nodes: 1-D numpy array of first nodes
			nodesii: 1-D numpy array of second nodes
			target: array of first grid coordinates
			targetii: array of second grid coordinates
			method: interpolation method ('nearest', 'linear')

		Returns:
			numpy.array
		"""

		# get final shape from target
		shape = target.shape

		# interpolate gpp onto tempo coordinates
		options = {'method': method, 'fill_value': self.fill, 'bounds_error': False}
		parameters = ((nodes, nodesii), quantity, (target.reshape(-1), targetii.reshape(-1)))
		interpolation = scipy.interpolate.interpn(*parameters, **options).reshape(shape)

		return interpolation

	def _label(self, scan, granule=None):
		"""Create a scan label.

		Arguments:
			scan: int, the scan number
			granule: int, the granule number

		returns:
			str, the scan label
		"""

		# make label
		label = '_S{}G'.format(self._pad(scan, 3))

		# if a granule number given
		if granule:

			# add granule number
			label += self._pad(granule)

		return label

	def _like(self, points, distribution):
		"""Define the bayesian likelihood loss function with a penalty for higher weights.

		Arguments:
			points: numpy array of ponts
			distribution: tensorflow distribution fucntion

		Returns:
			numpy array, the loos
		"""

		# get custom loss penalty for high wieghted samples
		penalty = self.training['penalty']

		# create weights
		weights = 1 + penalty * tensorflow.math.abs(points)

		# define loss
		loss = -tensorflow.reduce_mean(weights * distribution.log_prob(points))

		return loss

	def _limit(self):
		"""Define valid data limits.

		Arguments:
			None

		Returns:
			None
		"""

		# get limits from yaml
		limits = self.yam['limits']

		# update with given limits
		limits.update(self.limits)
		self.limits = limits

		return None

	def _localize(self, seconds, longitude, epoch='1980-01-06'):
		"""Create local time form seconds past the epoch and longitude.

		Arguments:
			seconds: numpy array, seconds past epoch per mirror step
			longitude: numpy array, longitude
			epoch: str, the reference for the epoch

		Returns:
			numpy array
		"""

		# convert epoch to datetime
		year = int(epoch[:4])
		month = int(epoch[5:7])
		day = int(epoch[8:10])
		epoch = datetime.datetime(year, month, day)

		# convert seconds to datetimes
		dates = [epoch + datetime.timedelta(seconds=second) for second in seconds]

		# extract hours
		hours = numpy.array([date.hour + (date.minute / 60) + (date.second / 60 ** 2) for date in dates])

		# expand by track length
		hours = numpy.array([hours] * longitude.shape[1]).transpose(1, 0)

		# get offsets from longitudes
		offsets = longitude / 15.0

		# add offsets to get local time
		time = hours + offsets

		return time

	def _locate(self, path):
		"""Retrive the site name from a path.

		Arguments:
			path

		Returns:
			string, the site name
		"""

		# get site name from path
		site = re.findall('[A-Z]{2}-[A-Z, a-z, 0-9]{3}', path)[0]

		return site

	def _lose(self, truth, prediction):
		"""Define the custom loss function with a penalty for higher weights.

		Arguments:
			points: numpy array of ponts
			distribution: tensorflow distribution fucntion
			truth: numpy array of truth values
			prediction: numpy array of prediction values

		Returns:
			numpy array, the loos
		"""

		# get custom loss penalty for high wieghted samples
		penalty = self.training['penalty']

		# create weights
		weights = 1 + penalty * (tensorflow.math.abs(truth) ** 2)

		# # define loss
		# loss = -tensorflow.reduce_mean(weights * distribution.log_prob(points))
		loss = tensorflow.reduce_mean(weights * tensorflow.square(truth - prediction))

		return loss

	def _mask(self, array):
		"""Determine valid data in an array.

		Arguments:
			array: numpy array

		Returns:
			numpy boolean array
		"""

		# create mask
		mask = (numpy.isfinite(array)) & (abs(array) < 1e20) & (array > -998)

		return mask

	def _paint(self, destination, graphs, size=(16, 16), space=None, gap=0.065):
		"""Create a plot with subplots.

		Arguments:
			destination: str, the file path
			graphs: list of dicts, the graph objects
			size: tuple of floats, the total plot size
			space: dict, subplot spacing paramaeters
			gap: gap between grqph and colorbars

		Returns:
			None
		"""

		# print status
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase previous copy
		self._clean(destination, force=True)

		# define default spacing
		space = space or {}

		# get number of subplots
		number = len(graphs)

		# set up dictionary of subplot values based on number
		layout = {'rows': {1: 1, 2: 1, 3: 1, 4: 2}, 'columns': {1: 1, 2: 2, 3: 3, 4: 2}}

		# calculate total size
		rows = layout['rows'][number]
		columns = layout['columns'][number]

		# set up figure
		pyplot.clf()
		figure = pyplot.figure(figsize=size)
		figure.patch.set_facecolor('white')
		# words = {'subplot_kw': {'projection': cartopy.crs.PlateCarree()}, 'figsize': size}
		# figure, axes = pyplot.subplots(rows, columns, figsize=size)

		# begin axes
		axes = []

		# for each graph
		for index, graph in enumerate(graphs):

			# if drawing globe:
			if graph['globe']:

				# activate cartopy
				code = int('{}{}{}'.format(rows, columns, index + 1))
				axis = pyplot.subplot(code, projection=cartopy.crs.PlateCarree())
				axis.set_facecolor('white')
				axes.append(axis)

			# otherwise:
			else:

				# activate cartopy
				code = int('{}{}{}'.format(rows, columns, index + 1))
				axis = pyplot.subplot(code)
				axis.set_facecolor('white')
				axes.append(axis)

		# adjust spacing
		margins = {'bottom': 0.12, 'top': 0.94, 'left': 0.07, 'right': 0.93, 'wspace': 0.2, 'hspace': 0.5}
		margins.update(space)
		pyplot.subplots_adjust(**margins)

		# for each graph
		for axis, graph in zip(axes, graphs):

			# unpack arrays
			abscissa = graph['abscissa']
			ordinate = graph['ordinate']
			intensity = graph['intensity']

			# unpack bounds
			bounds = graph['x_bounds']
			boundsii = graph['y_bounds']

			# unpack color scale
			scale = graph['scale']
			gradient = graph['gradient']
			selection = graph['selection']
			categories = graph['categories']

			# if clipping:
			if graph['clip']:

				# clip intensity to scale boundaries
				epsilon = 1e-10
				intensity = numpy.where(intensity > scale[0] + epsilon, intensity, scale[0] + epsilon)
				intensity = numpy.where(intensity < scale[1] - epsilon, intensity, scale[1] - epsilon)

			# unpack texts
			title = graph['title']
			units = graph['units']
			label = graph['x_label']
			labelii = graph['y_label']
			font = graph['font']
			fontii = graph['fontii']

			# if not reflectivity
			if not graph['reflectivity']:

				# create mask for finite data
				mask = numpy.isfinite(intensity)

				# apply mask
				intensity = intensity[mask]
				abscissa = abscissa[mask]
				ordinate = ordinate[mask]

				# get scale bounds
				minimum = scale[0]
				maximum = scale[1]

				# clip intensity
				intensity = numpy.where(intensity < minimum, minimum, intensity)
				intensity = numpy.where(intensity > maximum, maximum, intensity)

			# if drawing globe:
			if graph['globe']:

				# set cartopy axis with coastlines
				style = {'edgecolor': 'black', 'linewidth': 1.0}
				# axis.coastlines(linewidth=1, color='black')
				# axis.add_feature(cartopy.feature.OCEAN)
				axis.add_feature(cartopy.feature.COASTLINE, **style)
				axis.add_feature(cartopy.feature.STATES, **style)
				axis.add_feature(cartopy.feature.LAKES, facecolor='white')
				# axis.add_feature(cartopy.feature.OCEAN, facecolor='blue')
				options = {'edgecolor': 'face', 'facecolor': 'white'}
				axis.add_feature(cartopy.feature.NaturalEarthFeature('physical', 'ocean', '50m', **options))

				# set title
				axis.set_aspect('auto')
				axis.set_title(title, fontsize=font)

				# if xticks
				if graph['x_ticks'] and graph['y_ticks']:

					# set x ticks
					axis.set_xticks(graph['x_ticks'])
					axis.set_xticklabels(graph['x_marks'])
					# axis.tick_params(axis='x', rotation=45)

					# set yticks
					axis.set_yticks(graph['y_ticks'])
					axis.set_yticklabels(graph['y_marks'])
					# axis.tick_params(axis='y', rotation=45)

					# set font size
					axis.tick_params(axis='both', which='major', labelsize=fontii)	# Major ticks
					axis.tick_params(axis='both', which='minor', labelsize=fontii)

				# otherwies
				else:

					# set grid marks
					axis.grid(True)
					axis.set_global()
					_ = axis.gridlines(draw_labels=True)

					# set font size
					axis.tick_params(axis='both', which='major', labelsize=fontii)
					axis.tick_params(axis='both', which='minor', labelsize=fontii)

			# otherwise:
			else:

				# set title
				axis.set_title(title, fontsize=font)

				# add labels and ticks
				axis.set_title(title, fontsize=font)
				axis.set_xlabel(label, fontsize=fontii)
				axis.set_ylabel(labelii, fontsize=fontii)

				# set font size
				axis.tick_params(axis='both', which='major', labelsize=fontii)
				axis.tick_params(axis='both', which='minor', labelsize=fontii)

				# if xticks
				if graph['x_ticks']:

					# set x ticks
					axis.set_xticks(graph['x_ticks'])
					axis.set_xticklabels(graph['x_marks'])
					axis.tick_params(axis='x', rotation=45)

				# if yticks
				if graph['y_ticks']:

					# set yticks
					axis.set_yticks(graph['y_ticks'])
					axis.set_yticklabels(graph['y_marks'])
					axis.tick_params(axis='y', rotation=45)

				# if formatting in times:
				if graph['times']:

					# format xaxis using time
					axis.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%Y-%m'))
					axis.tick_params(axis='x', rotation=45)
					# pyplot.gcf().autofmt_xdate()
					# pyplot.gcf().autofmt_xdate()

			# set up colors and logarithmic scale
			colors = matplotlib.cm.get_cmap(gradient)
			selection = numpy.array(list(range(*selection)))
			colors = matplotlib.colors.ListedColormap(colors(selection))
			normal = matplotlib.colors.Normalize(minimum, maximum)

			# if colorbar categores
			if categories:

				# split names and hues
				hues = [category[0] for category in categories]
				texts = [category[1] for category in categories]
				colors = matplotlib.colors.ListedColormap(hues)
				normal = matplotlib.colors.BoundaryNorm(boundaries=numpy.arange(len(hues) + 1) - 0.5, ncolors=len(hues))

			# if logarithmic
			if graph['logarithm']:

				# make normal logarithmic
				normal = matplotlib.colors.LogNorm(minimum, maximum)

			# if grpahing polygons
			if graph['polygons']:

				# begin patches
				patches = []

				# for each polygon
				for vertices, verticesii in zip(graph['corners'][1].reshape(-1, 4), graph['corners'][0].reshape(-1, 4)):

					# set up patch
					polygon = numpy.array([[first, second] for first, second in zip(vertices, verticesii)])
					patch = matplotlib.patches.Polygon(xy=polygon, linewidth=0, edgecolor=None, alpha=graph['alpha'])
					patches.append(patch)

				# if reflectivity plot
				if graph['reflectivity']:

					# plot polygons with colormap
					options = {'facecolor': intensity, 'alpha': graph['alpha']}
					collection = matplotlib.collections.PatchCollection(patches, **options)
					collection.set_edgecolor("face")
					# collection.set_clim(scale)
					# collection.set_array(intensity)
					polygons = axis.add_collection(collection)
					# axis.add_feature(cartopy.feature.OCEANS, facecolor='white')

				# otherwise
				else:

					# plot polygons with colormap
					collection = matplotlib.collections.PatchCollection(patches, cmap=colors, alpha=graph['alpha'])
					collection.set_edgecolor("face")
					collection.set_clim(scale)
					collection.set_array(intensity)
					polygons = axis.add_collection(collection)

			# otherwise
			else:

				# if reflectivity plot
				if graph['reflectivity']:

					# no need for colors
					axis.scatter(abscissa, ordinate, c=intensity, s=0.75, lw=0)

				# otheriwse
				else:

					# plot data
					words = {'c': intensity, 'cmap': colors, 'norm': normal}
					axis.scatter(abscissa, ordinate, marker='.', s=graph['pixel'], **words)

			# plot additional lines
			for line in graph['lines']:

				# plot line
				options = {'markersize': graph['marker']}
				axis.plot(*line, **options)

			# set axis limits
			axis.set_xlim(*bounds)
			axis.set_ylim(*boundsii)

			# if there are annotations:
			if graph['annotations']:

				# for eacn annotation
				for annotation in graph['annotations']:

					# annotate
					axis.annotate(annotation[0], (annotation[1], annotation[2]))

			# if using colorbar
			if graph['colorbar']:

				# add colorbar
				position = axis.get_position()
				bar = pyplot.gcf().add_axes([position.x0, position.y0 - gap, position.width, 0.02])
				scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
				# colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', label=units)

				# if cateogories
				if categories:

					# add categorical colorbar
					ticksii = numpy.array(range(len(hues)))
					colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', ticks=ticksii)
					colorbar.ax.set_xticklabels(texts)	# Set category names
					colorbar.ax.tick_params(axis='x', rotation=45, labelsize=fontii)
					# colorbar.set_label(units, labelpad=1)
					# formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
					# colorbar.ax.yaxis.set_major_formatter(formatter)
					# # Create the colorbar with category labels
					# # barii = bar.imshow(numpy.array(range(len(hues))), cmap=colors, norm=normal)
					# colorbar = pyplot.gcf().colorbar(scalar, ticks=numpy.arange(len(hues)))
					# colorbar.ax.set_yticklabels(texts)  # Set category names

				# otherwise
				else:

					# add regular colorbar
					colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal')
					colorbar.set_label(units, labelpad=1, fontsize=fontii)
					formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
					colorbar.ax.yaxis.set_major_formatter(formatter)
					colorbar.ax.tick_params(labelsize=fontii)  # For tick labels

		# save to destination
		pyplot.savefig(destination)
		pyplot.clf()

		# end status
		self._stamp('plotted.')

		return None

	def _parse(self):
		"""Parse the yaml file.

		Arguments:
			None

		Returns:
			None

		Populates:
			self.yam
		"""

		# get the yaml file
		yam = self._acquire('{}/Onefluxnet_Yams/{}.yaml'.format(self.sink, self.tag))

		# set yam
		self.yam = yam

		# populate subsections
		self.training = yam['training']
		self.architecture = yam['architecture']
		self.validation = yam['validation']

		# load training site list
		sites = '{}/Onefluxnet_Sites/{}'.format(self.sink, self.training['sites'])
		self.training['sites'] = self._load(sites)['sites']

		# load valiation site list
		sites = '{}/Onefluxnet_Sites/{}'.format(self.sink, self.validation['sites'])
		self.validation['sites'] = self._load(sites)['validation']

		return None

	def _patch(self, reflectance, latitude, longitude, cloud):
		"""Patch bad reflectance data with nearest neighbors good data.

		Arguments:
			reflectance: numpy array, reflectqnce pcas
			latitude: numpy array, latitude
			longitude: numpy array, longitude
			cloud: numpy array, cloud fraction

		Returns:
			numpy array: patched data
		"""

		# squeeze latitude and longitude
		latitude = latitude.squeeze()
		longitude = longitude.squeeze()
		cloud = cloud.squeeze()

		# create mask for good and bad data
		summation = reflectance.sum(axis=1)
		good = (numpy.isfinite(summation) & (abs(summation) < 1e20))
		bad = numpy.logical_not(good)

		print('good: {}'.format(good.sum()))
		print('bad: {}'.format(bad.sum()))

		# create a KDTree from the good lat/lon
		tree = scipy.spatial.cKDTree(numpy.column_stack((latitude[good], longitude[good])))

		# query the tree for bad data points
		distances, indices = tree.query(numpy.column_stack((latitude[bad], longitude[bad])), k=1)

		# replace bad data with the nearest good data
		reflectance[bad] = reflectance[good][indices.flatten()]

		return reflectance

	def _post(self, kernel, bias=0, datatype=None):
		"""Define the posterior distribution function.

		Arguments:
			kernal: int, the kernal size
			bias: int, the bias size
			datatype: datatype object, the datatype

		Returns:
			tensor flow model layer
		"""

		# define abbreviations
		probability = tensorflow_probability.layers
		independent = tensorflow_probability.distributions.Independent
		normal = tensorflow_probability.distributions.Normal

		# get total size
		size = kernel + bias

		# set scaling functions
		epsilon = 1e-5
		scale = 0.02
		constant = numpy.log(numpy.expm1(1.0))
		def locating(tensor): return tensor[..., :size]
		def scaling(tensor): return epsilon + scale * tensorflow.nn.tanh(constant + tensor[..., size:])

		# define independent normal function
		options = {'reinterpreted_batch_ndims': 1}
		def normalizing(tensor): return independent(normal(loc=locating(tensor), scale=scaling(tensor)), **options)

		# create layer sequence
		layers = [probability.VariableLayer(2 * size, dtype=datatype)]
		layers += [probability.DistributionLambda(normalizing)]
		sequence = tensorflow.keras.Sequential(layers)

		return sequence

	def _prioritize(self, kernel, bias=0, datatype=None):
		"""Define the prior distribution function.

		Arguments:
			kernal: int, the kernal size
			bias: int, the bias size
			datatype: datatype object, the datatype

		Returns:
			tensor flow model layer
		"""

		# define abbreviations
		probability = tensorflow_probability.layers
		independent = tensorflow_probability.distributions.Independent
		normal = tensorflow_probability.distributions.Normal

		# get total size
		size = kernel + bias

		# define independent normal function
		def normalizing(tensor): return independent(normal(loc=tensor, scale=1), reinterpreted_batch_ndims=1)

		# create layer sequence
		layers = [probability.VariableLayer(size, dtype=datatype)]
		layers += [probability.DistributionLambda(normalizing)]
		sequence = tensorflow.keras.Sequential(layers)

		return sequence

	def _project(self, horizontal, vertical, projection):
		"""Project the ABI angular grid onto a latitude longitude grid.

		Arguments:
			horizontal: numpy array, x coordinate values
			vertical: numpy array, y coordinate values
			projection: dict, projection information

		Returns:
			(numpy array, numpy array) tuple, the geocoordinates
		"""

		# get projection information
		origin = projection['longitude_of_projection_origin']
		equatorial = projection['semi_major_axis']
		polar = projection['semi_minor_axis']
		altitude = projection['perspective_point_height']
		height = altitude + equatorial

		# create mesh grid
		mesh, meshii = numpy.meshgrid(horizontal, vertical)

		# set up equations
		zero = (origin * numpy.pi) / 180.0
		sine = numpy.sin(mesh)
		sineii = numpy.sin(meshii)
		cosine = numpy.cos(mesh)
		cosineii = numpy.cos(meshii)
		variable = sine ** 2 + (cosine ** 2 * (cosineii ** 2 + (equatorial ** 2 / polar ** 2) * sineii ** 2))
		variableii = -2 * height * cosine * cosineii
		variableiii = height ** 2 - equatorial ** 2
		radius = (-variableii - numpy.sqrt(variableii ** 2 - (4 * variable * variableiii))) / (2 * variable)
		abscissa = radius * cosine * cosineii
		ordinate = -radius * sine
		applicate = radius * cosine * sineii

		# ignore errors
		errors = numpy.seterr(all='ignore')

		# calculate latitude and longitude
		degrees = (180.0 / numpy.pi)
		ratio = equatorial ** 2 / polar ** 2
		root = numpy.sqrt(((height-abscissa) * (height-abscissa)) + (ordinate ** 2))
		latitude = degrees * (numpy.arctan(ratio * ((applicate / root))))
		longitude = (zero - numpy.arctan(ordinate / (height-abscissa))) * degrees

		# reset numpy errors
		numpy.seterr(**errors)

		return latitude, longitude

	def _read(self, path):
		"""Read an IDL sav file.

		Arguments:
			path: str, path to file

		Returns:
			dict of file contents
		"""

		# read file
		contents = scipy.io.readsav(path)

		return contents

	def _recite(self):
		"""List all models.

		Arguments:
			None

		Returns:
			None
		"""

		# show all paths of model directory
		self._show('{}/Onefluxnet_Models'.format(self.sink))

		return None

	def _retrieve(self, keras=False):
		"""Retrieve a tensor flow model and scaling details.

		Arguments:
			keras: boolean, use Keras style?

		Returns:
			None

		Populates
			self.model
			self.details
			self.folder
		"""

		# set models folder
		models = 'Onefluxnet_Models'

		# get all model folders
		folders = self._see('{}/{}'.format(self.sink, models))

		# if given a date:
		if self.session:

			# restrict to date
			folders = [folder for folder in folders if self.session in folder]

		# if given a tag:
		if self.tag:

			# restrict to tag
			folders = [folder for folder in folders if self.tag == folder.split('_')[-1]]

		# try to
		try:

			# sort and get latest date
			folders.sort()
			folder = folders[-1]

			# construct model path
			path = '{}/Onefluxnet_Model'.format(folder)

			# if keras
			if keras:

				# load model
				model = tensorflow.keras.models.load_model(path, compile=False)

			# otherwise
			else:

				# load model
				model = tensorflow.saved_model.load(path)

			# also get model scaling details
			hydra = Hydra(folder)
			hydra.ingest('Model_Scaling.h5')
			details = hydra.extract(addresses=True)

			# decode strings in details
			details = self._decode(details)

			# set attributes
			self.model = model
			self.details = details
			self.folder = folder
			self.features = self.details['features'].tolist()

			# set session and tag
			self.session = folder.split('/')[-1].split('_')[2]
			self.tag = folder.split('/')[-1].split('_')[3]

		# unless no model found
		except (IndexError, OSError):

			# print status
			self._print('no model found, need to train.')

		return None

	def _search(self, pattern, string):
		"""Use regex to find a pattern in a string.

		Arguments:
			pattern: str, regex pattern
			string: str, the source string

		Returns:
			str, the found pattern
		"""

		# default element to ''
		element = ''

		# try to
		try:

			# find element
			element = re.findall(pattern, string)[0]

		# unless not found
		except IndexError:

			# in which case, pass
			pass

		return element

	def _site(self):
		"""Determine site name.

		Arguments:
			site: str, approximate site name

		Returns:
			str, site name
		"""

		# create catalog
		self._catalog()

		# get all site names
		names = list(self.catalog.keys())
		names.sort()

		# default position to first
		position = 0
		for name in names:

			# check for alphabetical distance
			if name < self.site:

				# reset position
				position += 1

		# set site at position
		self.site = names[position]

		return None

	def _smooth(self, y_true, y_pred):
		# Base loss (e.g., MSE)
		base_loss = tensorflow.keras.losses.MeanSquaredError()(y_true, y_pred)

		model = self.model
		lambda_smooth = self.training['smoothing']

		# Compute the smoothing penalty based on gradients of predictions
		with tensorflow.GradientTape() as tape:
			tape.watch(y_pred)
			outputs = model(y_pred)
		gradients = tape.gradient(outputs, y_pred)
		smoothness_penalty = tensorflow.reduce_mean(tensorflow.square(gradients))

		return base_loss + lambda_smooth * smoothness_penalty

	def _transform(self, matrix):
		"""Transform the model inputs in accordance with preprocessing steps.

		Arguments:
			matrix: numpy array, the raw model inputs and truth value

		Returns:
			tuple of numpy arrays, the inputs and truth
		"""

		# get scaling information
		minimum = self.details['scaling/input_min']
		maximum = self.details['scaling/input_max']
		mean = self.details['scaling/input_mean']
		deviation = self.details['scaling/input_std']

		# remove truth and apply scaling
		truth = matrix[:, -1]

		# if normalizing to mean
		if self.training['normalize']:

			# normalize inputs
			inputs = (matrix[:, :-1] - mean) / deviation

		# otherwise
		else:

			# scale inputs
			scale = maximum - minimum
			inputs = (matrix[:, :-1] - minimum) / scale

		# get pca coefficients
		coefficients = self.details['scaling/pca_coefficients']

		# if using pca decomposition
		if self.training['decompose']:

			# apply pca
			inputs = numpy.dot(inputs, coefficients).astype('float32')

		return inputs, truth

	def _translate(self, outputs):
		"""translate raw model outputs into model predictions.

		Arguments:
			outputs: numpy array, raw model outputs

		Returns:
			numpy array, model predictions
		"""

		# get output scaling
		minimum = self.details['scaling/output_min']
		maximum = self.details['scaling/output_max']
		mean = self.details['scaling/output_mean']
		deviation = self.details['scaling/output_std']

		# if normalizing
		if self.training['normalize']:

			# normalize to mean, stdev
			predictions = (outputs * deviation) + mean

		# otherwise
		else:

			# apply scaling
			scale = maximum - minimum
			predictions = (outputs * scale) + minimum

		return predictions

	def activate(self, quantity=17, sample=0):
		"""Inspect the activations triggered by a sample.

		Arguments:
			scan: int, scan number


		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/activation'.format(self.folder)
		self._make(folder)

		# get specific day matrix
		matrix, _, _ = self.materialize([self.site])

		# make predictions
		prediction, _, truth, _ = self.predict(matrix)

		# find the closest prediction, and order matrix
		distance = abs(prediction - quantity)
		order = numpy.argsort(distance)
		matrix = matrix[order]

		# pick the single sample
		singlet = matrix[sample: sample + 1]

		# establish the keras version of the model
		self._retrieve(keras=True)

		# Define the layers you want to inspect (e.g., by name or index)
		layers = [layer.name for layer in self.model.layers if 'dense' in layer.name]

		# create a submodel to output activations for the selected layers
		options = {'inputs': self.model.input}
		options.update({'outputs': [self.model.get_layer(name).output for name in layers]})
		model = tensorflow.keras.Model(**options)

		# transform the inputs
		inputs, truth = self._transform(singlet)
		activations = model.predict(inputs)

		# restore tensorflow model
		self._retrieve()

		# for each layer
		for layer, activation in zip(layers, activations):

			# Create a histogram
			pyplot.clf()
			pyplot.figure(figsize=(8, 8))
			pyplot.hist(activation.flatten(), bins=50, edgecolor='black')

			# Add titles and labels
			title = 'histogram of {} ( {} hiddens )'.format(layer, activation.shape[1])
			pyplot.title(title)
			pyplot.xlabel('value')
			pyplot.ylabel('counts')

			# save the plot
			formats = (folder, self.site, layer, activation.shape[1])
			destination = '{}/activations_{}_{}_{}.png'.format(*formats)
			pyplot.savefig(destination)
			pyplot.clf()

			# print status
			self._print('plotted {}'.format(destination))

		return None

	def baseline(self, path, latitude, longitude):
		"""Colocate ABI baseliner data.

		Arguments:
			path: radiance path
			latitude: numpy array, tempo latitude
			longitude: numpy arraym, tempo longitude

		Returns:
			numpy array, the baseliner data
		"""

		# cast tempo coordinates as target points
		shape = latitude.shape
		targets = numpy.vstack([latitude.reshape(-1), longitude.reshape(-1)]).transpose(1, 0)

		# create valid coordinates mask
		valid = self._mask(targets.sum(axis=1))

		# determine geographic extend
		south = targets[:, 0][valid].min()
		north = targets[:, 0][valid].max()
		west = targets[:, 1][valid].min()
		east = targets[:, 1][valid].max()

		# substitute minimums for invalids
		targets[:, 0] = numpy.where(valid, targets[:, 0], south)
		targets[:, 1] = numpy.where(valid, targets[:, 1], west)

# 		# get the ABI high resolution grid
# 		grid = Hydra('{}/ABI_High_Grid'.format(self.sink), show=False)
# 		grid.ingest(0)
#
# 		# set grid shapes
# 		shapes = {(1500, 2500): 'low', (3000, 5000): 'mid', (6000, 10000): 'high'}
#
# 		# set trees, indicess
# 		indices = {}
# 		masks = {}
#
# 		# for each mode:
# 		for mode in shapes.values():
#
# 			latitudeii = grid.grab('latitude_{}'.format(mode))
# 			longitudeii = grid.grab('longitude_{}'.format(mode))
#
# 			# cast as points
# 			points = numpy.vstack([latitudeii.reshape(-1), longitudeii.reshape(-1)]).transpose(1, 0)
#
# 			# create mask for valid points and restrict to boundaries
# 			mask = self._mask(points.sum(axis=1))
# 			mask = mask & (points[:, 0] > south - 1) & (points[:, 0] < north + 1)
# 			mask = mask & (points[:, 1] > west - 1) & (points[:, 1] < east + 1)
#
# 			# build a kd search tree
# 			tree = scipy.spatial.cKDTree(points[mask])
#
# 			# query the tree
# 			_, index = tree.query(targets, k=1)
# 			index = index.flatten()
#
# 			# add to dictionaries
# 			indices[mode] = index
# 			masks[mode] = mask

		# extract datetime from path
		search = '[0-9]{8}T[0-9]{6}Z'
		date = re.findall(search, path)[0]
		brackets = [(0, 4), (4, 6), (6, 8), (9, 11), (11, 13), (13, 15)]
		components = [int(date[bracket[0]: bracket[1]]) for bracket in brackets]
		time = datetime.datetime(*components)

		# begin reservoirs
		radiances = []
		wavelengths = []

		# for each wavelength
		for wave in range(16):

			# connect to abi data
			hydra = Hydra('{}/ABI_CONUS/{}/{}'.format(self.sink, self.stub, self._pad(wave + 1)), show=False)

			# for each path
			times = []
			for pathii in hydra.paths:

				# extract date
				search = 's[0-9]{14}'
				dateii = re.findall(search, pathii)[0]
				year = int(dateii[1:5])
				julian = int(dateii[5:8])
				brackets = [(8, 10), (10, 12), (12, 14)]
				components = [int(dateii[bracket[0]: bracket[1]]) for bracket in brackets]
				timeii = datetime.datetime(year, 1, 1, *components) + datetime.timedelta(days=julian - 1)

				# append to list with path
				times.append((timeii, pathii))

			# sort by closest time
			times.sort(key=lambda pair: abs(pair[0] - time))
			pathii = times[0][1]

			# ingest the path
			hydra.ingest(pathii)

			# get data
			radiance = hydra.grab('Rad')
			quality = hydra.grab('DQF')
			wavelength = hydra.grab('band_wavelength')

			# get geolocation data
			horizontal = hydra.grab('x')
			vertical = hydra.grab('y')

			# get projection information
			projection = hydra.dig('goes_imager_projection')[0].attributes

			# convert into latitude and longitude
			latitudeii, longitudeii = self._project(horizontal, vertical, projection)

			# cast as points
			points = numpy.vstack([latitudeii.reshape(-1), longitudeii.reshape(-1)]).transpose(1, 0)

# 			# remove infinities
# 			finite = numpy.isfinite(points.sum(axis=1))
# 			points = points[finite]

			# create mask for valid points and restrict to boundaries
			mask = self._mask(points.sum(axis=1))
			mask = mask & (points[:, 0] > south - 1) & (points[:, 0] < north + 1)
			mask = mask & (points[:, 1] > west - 1) & (points[:, 1] < east + 1)

			# build a kd search tree
			tree = scipy.spatial.cKDTree(points[mask])

			# query the tree
			_, indices = tree.query(targets, k=1)
			indices = indices.flatten()

# 			# add to dictionaries
# 			indices[mode] = index
# 			masks[mode] = mask

# 			# determine mode from radiance shape
# 			mode = shapes[radiance.shape]

			# flatten and apply geolocation boundary mask
			radiance = radiance.reshape(-1)[mask]
			quality = quality.reshape(-1)[mask]

			# add wavelenth
			wavelengths.append(wavelength[0])

			# apply indices from tree query
			radianceii = radiance[indices]
			qualityii = quality[indices]

			# reapply valid coordinate mask with fill data
			radianceii = numpy.where(valid, radianceii, self.fill)
			qualityii = numpy.where(valid, qualityii, self.fill)

			# reshape interpolations
			radianceii = radianceii.reshape(shape)
			qualityii = qualityii.reshape(shape)

			# remove low quality data and add to array
			radianceii = numpy.where(qualityii == 0, radianceii, self.fill)
			radiances.append(radianceii)

		# craete arrays, transposing radiances
		radiances = numpy.array(radiances).transpose(1, 2, 0)
		wavelengths = numpy.array(wavelengths)

		return radiances, wavelengths

	def chronicle(self, scales=None):
		"""Make plots of loss functions.

		Arguments:
			scales: dict, yscale for loss functions

		Returns:
			None
		"""

		# default scales to empty dictionary
		scales = scales or {}

		# make folder
		folder = '{}/chronicle'.format(self.folder)
		self._make(folder)

		# get history contents
		contents = self._load('{}/history.json'.format(self.folder))

		# get non validation losses
		losses = [loss for loss in contents.keys() if 'val' not in loss]

		# for each entry
		for loss in losses:

			# make validation key
			validation = 'val_' + loss

			# plot history
			pyplot.clf()
			pyplot.figure(figsize=(8,8))
			pyplot.plot(contents[loss], 'b-', label=loss)
			pyplot.plot(contents[validation], 'g-', label=validation)
			pyplot.title('{} and {}'.format(loss, validation))
			pyplot.xlabel('epoch')
			pyplot.ylabel(loss)
			pyplot.legend(loc='upper right')

			# if scale given
			if scales.get(loss, None):

				# set ylim
				pyplot.ylim(*scales[loss])

			# save figures
			pyplot.savefig('{}/{}.png'.format(folder, loss))
			pyplot.clf()

		# load learning rate
		learn = self._load('{}/rates.json'.format(self.folder))
		rates = numpy.array(learn['rates'])
		samples = learn['samples']

		# translate timepoints to epoches
		epochs = numpy.array(range(len(rates))) / (math.ceil(samples / self.training['batch']))

		# retain only changing rates for plot
		# first = rates[:-1]
		# second = rates[1:]
		# mask = (first != second)
		# maskii = numpy.append(mask, True)
		# rates = rates[maskii]
		# epochs = epochs[maskii]

		# also plot learning rate
		pyplot.clf()
		pyplot.figure(figsize=(8,8))
		pyplot.plot(epochs, rates)
		pyplot.title('learning rates')
		pyplot.xlabel('epoch')
		pyplot.ylabel('rate')
		pyplot.savefig('{}/rates.png'.format(folder))
		pyplot.clf()

		return None

	def collect(self, bands=(0, 7)):
		"""Collect data from earth data and aws.

		Arguments:
			bands: tuple of ints, the bands to collect

		Returns:
			None
		"""

		# get julian day
		julian = datetime.datetime(self.year, self.month, self.day).timetuple().tm_yday

		# construct list of bands, adding -1 for low resolution grid
		bands = list(range(*bands)) + [-1]

		# for each band
		for band in bands:

			# construct name
			name = 'MCD43D{}'.format(band + 62)

			# if band is 7
			if band == -1:

				# get the MCD43C data instead ( all seven bands in one file )
				name = 'MCD43C4'

			# create hydra
			hydra = Hydra('{}/{}/{}'.format(self.sink, name, self.stub))

			# check for paths already there
			if len(hydra.paths) < 1:

				# get modis band
				self._access(name, count=8)

				# remake hydra
				hydra = Hydra('{}/{}/{}'.format(self.sink, name, self.stub))

			# find the path with the julian day
			fours = [path for path in hydra.paths if path.endswith('.hdf')]

			# try to:
			try:

				# find day matching julian data
				day = [path for path in fours if '{}{}.'.format(self.year, self._pad(julian, 3)) in path][0]

				# convert to hdf5
				hydra._convert(day, day.replace('.hdf', '.h5'))

			# unless no hdf4 file is found
			except IndexError:

				# in which case, nevermind
				pass

			# for each hdf4 path
			for path in fours:

				# erase hdf4 file
				hydra._clean(path, force=True)

		return None

	def confirm(self, sites=None):
		"""Compare the time series predicted by the model to onefluxnet site data.

		Arguments:
			sites: list of strings, the site ids

		Returns:
			None
		"""

		# make folder
		folder = '{}/confirm'.format(self.folder)
		self._make(folder)

		# make data file folders
		folderii = '{}/Onefluxnet_Confirmation'.format(self.sink)
		self._make(folderii)
		self._make('{}/{}'.format(folderii, self.year))
		self._make('{}/{}/{}'.format(folderii, self.year, self._pad(self.month)))
		self._make('{}/{}'.format(folderii, self.stub))

		# set default sites
		sites = sites or self.training['sites']

		# collect band information from fine and coarse modis bands
		collection = self._band(sites)
		collectionii = self._band(sites, high=False)

		# for each site
		for site in sites:

			# get the matrix and data
			matrix, data, constraints = self.materialize([site])
			data = self._constrain(data, constraints)

			# get the r^2 score
			_, _, _, score = self.predict(matrix)

			# create mask for month
			mask = (data['month'] == self.month)
			mask = mask.squeeze()
			data = self._constrain(data, mask)
			matrix = matrix[mask]

			# begin truths and predictions
			truths = []
			predictions = []
			deviations = []
			deviationsii = []
			hours = []

			# if there are entries
			if matrix.shape[0] > 3:

				# begin masks for years in july
				masks = []
				for year in range(data['year'].min(), data['year'].max() + 1):

					# get all july measurements
					mask = (data['year'] == year)
					masks.append((year, mask.squeeze()))

				# sort by number of samples, and get top
				masks.sort(key=lambda pair: pair[1].sum(), reverse=True)
				year, mask = masks[0]

				# set month
				month = self.month

				# apply year mask
				matrix = matrix[mask]
				data = self._constrain(data, mask)

				# make hour, minute pairs
				zipper = zip(data['hour'].squeeze(), data['minute'].squeeze())
				triplets = [(hour, minute, hour + (minute / 60)) for hour, minute in zipper]

				# sort by local time
				triplets.sort(key=lambda triplet: triplet[2])

				# for each triplet
				for triplet in triplets:

					# create mask for hour and minute
					mask = (data['hour'] == triplet[0]) & (data['minute'] == triplet[1])
					mask = mask.squeeze()

					# predict from model
					prediction, _, truth, _ = self.predict(matrix[mask])
					predictions.append(prediction.mean())
					truths.append(truth.mean())

					# append standard deviations for error bars
					deviations.append(prediction.std())
					deviationsii.append(truth.std())

					# add time to hours
					hours.append(triplet[2])

				# create arrays
				truths = numpy.array(truths)
				predictions = numpy.array(predictions)
				deviations = numpy.array(deviations)
				deviationsii = numpy.array(deviationsii)
				hours = numpy.array(hours)

				# adjust merra time to local time
				longitude = self.catalog[site]['longitude']
				offset = longitude / 15
				local = numpy.array([time + offset for time in collection[site]['hour']])
				local = numpy.where(local < 0, local + 24, local)

				# determine order for local time
				order = numpy.argsort(local)
				local = local[order]

				# get short wave data and order according to local time
				wave = collection[site]['wave'][order]

				# begin data
				# construct file data
				dataii = {'fluxnet_hours': hours, 'fluxnet_truth': truths, 'fluxnet_deviation': deviationsii}
				dataii.update({'model_prediction': predictions, 'model_deviation': deviations})
				dataii.update({'synthetic_local_time': local})
				dataii.update({'fluxnet_year': numpy.array([year])})
				dataii.update({'fluxnet_month': numpy.array([month])})
				dataii.update({'site_name': numpy.array([site])})
				dataii.update({'site_latitude': numpy.array([self.catalog[site]['latitude']])})
				dataii.update({'site_longitude': numpy.array([self.catalog[site]['longitude']])})

				# for each mode
				modes = ('high', 'low')
				reservoirs = (collection, collectionii)
				for mode, reservoir in zip(modes, reservoirs):

					# assemble matrix
					albedo = numpy.array([reservoir[site]['albedo']] * wave.shape[0])
					faux = numpy.array([1 for _ in range(wave.shape[0])])
					matrixii = numpy.hstack([albedo, wave.reshape(-1, 1), faux.reshape(-1, 1)])

					# make predictions
					synthetic, _, _, _ = self.predict(matrixii)

					# add to data
					dataii.update({'synthetic_matrix_{}'.format(mode): matrixii})
					dataii.update({'synthetic_prediction_{}'.format(mode): synthetic})

					# begin plot
					pyplot.clf()
					pyplot.title('{} diurnal GPP ( $R^2$ = {:.3f} ), {} ( {} )'.format(site, score, self.date, mode))
					pyplot.xlabel('local hour')
					pyplot.ylabel('{} ( {} )'.format(self.target, self.unit))

					# set limits
					pyplot.xlim(5, 21)
					minimum = min([(truths - deviationsii).min(), (predictions - deviations).min(), synthetic.min()])
					maximum = max([(truths + deviationsii).max(), (predictions + deviations).max(), synthetic.max()])
					pyplot.ylim(minimum - 1, maximum + 2)

					# plot the truth with markers
					label = 'onefluxnet ( {}m{} )'.format(year, self._pad(month))
					options = {'fmt': 'k^', 'linewidth': 2, 'capsize': 2, 'ms': 5, 'label': label}
					pyplot.errorbar(hours, truths, deviationsii, **options)

					# plot the prediction with markers
					label = 'prediction  ( {}m{} )'.format(year, self._pad(month))
					options = {'fmt': 'rs', 'linewidth': 2, 'capsize': 2, 'ms': 5, 'label': label}
					pyplot.errorbar(hours, predictions, deviations, **options)

					# plot the current trend
					label = 'synthetic   ( {} )'.format(self.date)
					pyplot.plot(local, synthetic, 'gx-', linewidth=4, label=label)

					# add legend
					pyplot.legend(loc='upper left')

					# save figure
					formats = (folder, site, mode, self.date, self.session, self.tag)
					destination = '{}/Confirmation_{}_{}_{}_{}_{}.png'.format(*formats)
					pyplot.savefig(destination)
					self._print('plotted {}.'.format(destination))
					pyplot.clf()

				# stash file
				destination = '{}/{}/Onefluxnet_confirmation_{}.h5'.format(folderii, self.stub, site)
				self.spawn(destination, dataii)

		return None

	def correlate(self, scan=10):
		"""Compute the correlation amongst all feature pairs.

		Arguments:
			scan: int, scan number

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make reports folder
		self._make('{}/reports'.format(folder))

		# get training matrix for particular scan
		features = details['features']
		parameters = ([(self.site, self.site)],)
		matrix, features, _, data, constraints = self.materialize(*parameters, features=features)

		# get scaling information
		minimum = details['input_min']
		maximum = details['input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		features = features[:-1]
		truth = matrix[:, -1]
		inputs = (matrix[:, :-1] - minimum) / scale

		# begin report and scores collection
		report = ['correlation report for {}\n'.format(folder)]
		scores = []

		# compute correlation
		correlation = numpy.corrcoef(inputs, rowvar=False)

		# for each row
		for row in range(correlation.shape[0]):

			# and each column
			for column in range(row + 1, correlation.shape[1]):

				# add to scores
				triplet = (correlation[row, column], features[row], features[column])
				scores.append(triplet)

		# sort scores
		scores.sort(key=lambda triplet: triplet[0], reverse=True)

		# compute feature string lengths
		lengths = [len(feature) for feature in features]
		lengths.sort(reverse=True)
		longest = lengths[0]

		# add to report
		for score, feature, featureii in scores:

			# compute spacing
			space = ' ' * (longest - len(feature))

			# add entry
			report.append('{:3f}: {}{} x   {}'.format(score, feature, space, featureii))

		# jot report
		self._jot(report, '{}/reports/correlation.txt'.format(folder))

		return None

	def cross(self, bounds=('2004m0101', '2004m0202'), secondary='short_wave', position=0):
		"""Make a cross section plot of model results.

		Arguments:
			bounds: tuple of str, the time bounds
			secondary: str, secondary variable
			position: int: index of secondary variable

		Returns:
			None
		"""

		# make folder
		folder = '{}/cross'.format(self.folder)
		self._make(folder)

		# get training matrix for particular site
		matrix, data, constraints = self.materialize([self.site])

		# apply constraints to data
		data = self._constrain(data, constraints)

		# deconstruct bounda
		years = [int(bound[:4]) for bound in bounds]
		months = [int(bound[5:7]) for bound in bounds]
		days = [int(bound[7:9]) for bound in bounds]

		# reduce matrixii to bounding box
		mask = (data['year'] >= years[0]) & (data['year'] <= years[1])
		mask = mask & (data['month'] >= months[0]) & (data['month'] <= months[1])
		mask = mask & (data['day'] >= days[0]) & (data['day'] <= days[1])
		mask = mask.squeeze()
		matrix = matrix[mask]
		data = self._constrain(data, mask)

		# if there is at least two samples
		if len(matrix) > 1:

			# try to:
			try:

				# make predictions
				prediction, deviation, truth, score = self.predict(matrix)

				# construct times
				years = data['year'].squeeze()
				months = data['month'].squeeze()
				days = data['day'].squeeze()
				hours = data['hour'].squeeze()
				minutes = data['minute'].squeeze()
				zipper = zip(years, months, days, hours, minutes)
				times = [datetime.datetime(year, month, day, hour, minute) for year, month, day, hour, minute in zipper]

				# if secondary variable
				if secondary:

					# get the data
					tracer = data[secondary][:, position]

				# begin plot
				pyplot.clf()
				pyplot.figure(figsize=(8, 8))

				# add text
				# formats = (self.features[-1], self._orient(latitude), track, self.date)
				pyplot.title('Model vs Flux tower for {} ( $R^2$ = {:.2f} )'.format(self.site, score))
				pyplot.xlabel('')
				pyplot.ylabel('{} ( {} )'.format(self.target, self.unit))

				# set bounds
				# pyplot.xlim(*bounds)
				pyplot.ylim(*self.limits['scale'])

				# format xaxis using time
				pyplot.gca().xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%Y-%m-%d'))
				pyplot.gca().tick_params(axis='x', rotation=45)

				# plot truth
				pyplot.plot(times, truth, 'b-', linewidth=1, label='truth')

				# plot prediction
				pyplot.plot(times, prediction, 'g-', linewidth=1, label='prediction')

				# add legend
				pyplot.legend(loc='upper left')

				# if secondary axis
				label = '_'
				if secondary:

					# create secondary axis
					axisii = pyplot.gca().twinx()

					# plot row anomaly on secondary
					unit = '$W/m^2$'
					label = '{} ( {} )'.format(secondary, unit)
					axisii.plot(times, tracer, 'y-', label=secondary)
					axisii.set_ylabel(label)
					# axisii.set_xlim(*bounds)

					# create legend
					axisii.legend(loc='upper right')

				# plot deviations
				# 		pyplot.plot(longitude, mean + deviation, 'g--')
				# 		pyplot.plot(longitude, mean - deviation, 'g--')

				# save
				formats = (folder, self.site, bounds[0], self.session, self.tag, secondary)
				pyplot.savefig('{}/CrossSection_{}_{}_{}_{}_{}.png'.format(*formats))
				pyplot.clf()

			# unless not enough samples
			except TypeError:

				# print
				self._print('no samples for {}!'.format(self.site))

		return None

	def diagnose(self, truth, prediction, data, folder):
		"""Create plots of percent error vs parameter.

		Arguments:
			truth: numpy array, gpp truth
			prediction: numpy array, model prediction
			data: dict, associated data

		Returns:
			None
		"""

		# make folder
		folderii = '{}/diagnosis'.format(folder)
		self._make(folderii)

		# calculate gpp difference
		difference = prediction - truth

		# set fields
		fields = ['latitude', 'longitude', 'snow_fraction', 'cloud_fraction', 'gpp_fluxsat']
		fields += ['solar_zenith_angle', 'viewing_zenith_angle', 'relative_azimuth_angle']
		fields += ['land_type', 'mirror', 'track', 'clearsky_par']
		fields += ['max_irr_quality_high', 'max_irr_quality_low']
		fields += ['max_rad_quality_high', 'max_rad_quality_low']

		# for each variable
		for field in fields:

			# crete plot destination
			destination = '{}/Diagnosis_{}.png'.format(folderii, field)

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8,8))

			# construct date
			date = str(data['date'][0][0])
			date = '{}m{}{}'.format(date[:4], date[4:6], date[6:8])
			time = str(data['time'][0][0])
			time = '{}:{} CST'.format(int(time[:2]) - 6, time[2:4])
			scan = 'S{}'.format(self._pad(data['scan'][0][0], 3))

			# create title
			title = 'GPP {} {} {}\n Model - Fluxsat vs {}'.format(date, time, scan, field)
			pyplot.title(title)

			# add labels
			pyplot.xlabel(field)
			pyplot.ylabel('GPP difference')

			# plot data
			pyplot.plot(data[field], difference, 'g.', markersize=0.2)

			# save plot
			pyplot.savefig(destination)
			pyplot.clf()

		return None

	def distribute(self, bins=50, all=True):
		"""Generate comparative histograms of truth and prediction distributions:

		Arguments:
			bins: int, number of bins
			all: boolean, use all training samples

		Returns:
			None
		"""

		# set sites
		sites = [self.site]
		stub = self.site

		# if using all sites
		if all:

			# set sites to all training
			sites = self.training['sites']
			stub = 'ALL'

		# make folder
		folder = '{}/histograms'.format(self.folder)
		self._make(folder)

		# get training matrix and make predictions
		matrix, _, _ = self.materialize(sites)
		prediction, _, truth, _ = self.predict(matrix)

		# Create a histogram
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))
		options = {'bins': bins, 'edgecolor': 'black', 'label': ['truth', 'prediction'], 'color': ['blue', 'green']}
		options.update({'linewidth': 0.5})
		pyplot.hist([truth, prediction], **options)

		# add legend
		pyplot.legend(loc='upper right')

		# Add titles and labels
		title = 'histogram of truth vs prediction {}'.format(stub)
		pyplot.title(title)
		pyplot.xlabel('value')
		pyplot.ylabel('counts')

		# save the plot
		destination = '{}/histogram_truth_prediction_{}.png'.format(folder, stub)
		pyplot.savefig(destination)
		pyplot.clf()

		# Create a scatter plot
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# Add titles and labels
		title = 'scatter plot of truth vs prediction {}'.format(stub)
		pyplot.title(title)
		pyplot.xlabel('prediction')
		pyplot.ylabel('truth')

		# plot
		pyplot.plot(truth, prediction, 'b.', ms=1)

		# save the plot
		destination = '{}/scatter_truth_prediction_{}.png'.format(folder, stub)
		pyplot.savefig(destination)
		pyplot.clf()

		# print status
		self._print('plotted {}'.format(destination))

		return

	def err(self):
		"""Run a random forest against the model error.

		Arguments:
			scan: int, scan number

		Returns:
			None
		"""

		# make folders
		folder = '{}/reports'.format(self.folder)
		self._make(folder)

		# get training matrix for particular site
		matrix, data, constraints = self.materialize([self.site])

		# construct valid data
		valids = self._constrain(data, constraints)

		# make predictions
		prediction, deviation, truth, _ = self.predict(matrix)

		# add difference to valids
		target = self.features[-1]
		valids.update({'error': (prediction - valids[target].squeeze()).reshape(-1, 1)})

		# remove all 1-d arrays
		valids = {field: array for field, array in valids.items() if array.shape[0] == prediction.shape[0]}

		# view data
		self._view(valids)

		# get secondary axis sizes
		sizes = {field: array.shape[1] for field, array in valids.items()}

		# begin training matrix
		train = {}

		# for each field
		for field, array in valids.items():

			# add to training
			train.update({'{}|{}'.format(field, self._pad(index)): array[:, index] for index in range(sizes[field])})

		# create report destination and plant tree
		self.plant(train, 'error|00', '{}/{}_error_random_forest.txt'.format(folder, target))

		return None

	def explain(self, scan=10, instances=1, samples=500):
		"""Perform shapley study.

		Arguments:
			instances: int, number of instances for model predictions
			samples: int, number of samples to use

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make reports folder
		self._make('{}/explanation'.format(folder))

		# get training matrix for particular scan
		features = details['features']
		parameters = ([(self.site, self.site)],)
		matrix, features, _, data, constraints = self.materialize(*parameters, features=features)

		# get scaling information
		minimum = details['input_min']
		maximum = details['input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		truth = matrix[:, -1]
		inputs = (matrix[:, :-1] - minimum) / scale

		# get pca coefficients
		coefficients = details['pca_coefficients']

		# pare down sample number
		numpy.random.seed(42)
		xerox = inputs.copy()
		indices = numpy.random.choice(xerox.shape[0], size=samples, replace=False)
		xerox = xerox[indices, :]

		# define modeler
		def modeling(matrix): return model(numpy.dot(matrix, coefficients).astype('float32'))

		# create explainer
		explainer = shap.Explainer(modeling, xerox)
		importance = explainer(xerox)
		importance.feature_names = features[:-1]

		# clear figure
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# for ten samples
		for sample in range(10):

			# visualize decision plot
			shap.plots.decision(importance.base_values[sample], importance.data, feature_names=importance.feature_names)
			pyplot.savefig('{}/explanation/decision_{}.png'.format(folder, sample), bbox_inches='tight')
			pyplot.clf()

			# visualize waterfall plot
			shap.plots.waterfall(importance[sample], max_display=20)
			pyplot.savefig('{}/explanation/waterfall_{}.png'.format(folder, sample), bbox_inches='tight')
			pyplot.clf()

# 		# for each Feature
# 		for name in features[:-1]:
#
		# make scatter plot
		shap.plots.scatter(importance[:, 0], color=importance)
		pyplot.savefig('{}/explanation/scatter.png'.format(folder), bbox_inches='tight')
		pyplot.clf()

		# visualize beeswarm plot
		shap.plots.beeswarm(importance, max_display=20)
		pyplot.savefig('{}/explanation/beeswarm.png'.format(folder), bbox_inches='tight')
		pyplot.clf()

		# visualize bar plot
		shap.plots.bar(importance, max_display=20)
		pyplot.savefig('{}/explanation/bar.png'.format(folder), bbox_inches='tight')
		pyplot.clf()

		return explainer, importance

	def examine(self, points=None, margin=1, apply=True, tag=''):
		"""Inspect the model across a specific variable at a specific point, and compare with regression.

		Arguments:
			points: numpy array, list of specific points for all variables
			margin: float, margin in stdevs
			apply: apply specific trained indices?
			tag: str, tag for title

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/examine'.format(self.folder)
		self._make(folder)

		# collect training sites
		sites = self.training['sites']

		# get training matrix
		matrix, _, _ = self.materialize(sites)

		# if limiting to selected indices
		if apply:

			# apply selected indices
			matrix = matrix[self.details['samples/selected_indices']]

		# normalize matrix for regressions
		inputs = matrix[:, :-1]
		outputs = matrix[:, -1]
		average = inputs.mean(axis=0)
		spread = inputs.std(axis=0)
		inputs = (inputs - average) / spread

		# create linear regressor and fit
		regressor = LinearRegression()
		regressor.fit(inputs, outputs.reshape(-1, 1))

		# create polynomial fit
		polymerizer = PolynomialFeatures(degree=3, include_bias=True)
		polynomial = polymerizer.fit_transform(inputs)
		regressorii = LinearRegression()
		regressorii.fit(polynomial, outputs.reshape(-1, 1))

		# if no points given
		if points is None:

			# get points from clustering if not given
			points = points or self._cluster(matrix, 5, [7])

		# set variable names
		names = ['MCD43 Red', 'MCD43 NIR', 'MCD43 Blue', 'MCD43 Green']
		names += ['MCD43 SWIR1', 'MCD43 SWIR2', 'MCD43 SWIR 3', 'SW IN']

		# for each variable
		for variable, name in zip(self.features[:-1], names):

			# set units
			units = '-'
			if variable == 'short_wave':

				# set short wave units
				units = '$W/m^2$'

			# get column position for variable of interest
			column = self.features.index(variable)

			# create percentiles
			percentiles = numpy.array([numpy.percentile(matrix[:, column], percentile) for percentile in range(101)])

			# for each point
			for index, point in enumerate(points):

				# create probe matrix from medians for all features but variable in question
				point = point.astype('float32')
				probe = numpy.array([point.copy()] * 101)
				probe[:, column] = percentiles

				# predict values from the model
				prediction, deviation, _, _ = self.predict(probe)
				predictionii, _, _, _, = self.predict(point.reshape(1, -1))

				# predict at point
				ends = numpy.array([point.copy()] * 2)
				ends[0][column] = percentiles[0]
				ends[1][column] = percentiles[-1]
				regression = regressor.predict((ends[:, :-1] - average) / spread)

				# predict polynomial regression at point
				series = numpy.array([point.copy()] * len(percentiles))
				series[:, column] = percentiles
				polynomial = polymerizer.transform((series[:, :-1] - average) / spread)
				regressionii = regressorii.predict(polynomial)

				# begin plot
				pyplot.clf()
				pyplot.figure(figsize=(8, 8))

				# create title
				tagii = (tag or 'point') + ' {}'.format(self._pad(index))

				vector = '[ ' + ' '.join(['{:.3f}'.format(quantity) for quantity in point[:-1]]) + ' ]'
				formats = (name, tagii, vector)
				title = 'Onefluxnet model, {}, {}\n{}'.format(*formats)
				pyplot.title(title)

				# add labels
				pyplot.xlabel('{} ( {} )'.format(name, units))
				pyplot.ylabel('{} ( {} )'.format(self.target, self.unit))

				# set limits
				pyplot.ylim(self.limits['scale'])

				# Sample data
				abscissa = percentiles
				ordinate = prediction

				# plot regression line
				pyplot.plot(ends[:, column], regression, 'g--', label='linear fit', linewidth=4)

				# plot polynomial line
				pyplot.plot(abscissa, regressionii, 'b--', label='polynomial fit', linewidth=4)

				# create line plots
				pyplot.plot(abscissa, ordinate, 'co-', label='model')
				pyplot.plot(abscissa, ordinate - deviation, 'c--')
				pyplot.plot(abscissa, ordinate + deviation, 'c--')

				# plot prediction and truth
				# pyplot.plot([row[index]], [truth], 'gX', markersize=15, label='truth')
				pyplot.plot([point[column]], [predictionii], 'rX', markersize=15, label='prediction')

				# copy matrix
				xerox = matrix.copy()
				mean = matrix.mean(axis=0)
				deviation = matrix.std(axis=0)

				# # get percents
				# lower = numpy.percentile(xerox, 50 - margin, axis=0)
				# upper = numpy.percentile(xerox, 50 + margin, axis=0)

				# print final sahape
				self._print('{} samples: {}'.format(variable, xerox.shape))

				# calculate differences from median for color scale
				difference = (((xerox - mean) / deviation) - ((point - mean) / deviation)) ** 2
				difference[:, column] = 0
				difference = difference.sum(axis=1)

				# add training points
				# label = 'samples +/- {} std'.format(str(margin))
				label = 'training'
				pyplot.scatter(xerox[:, column], xerox[:, -1], c=difference, cmap='gray', s=10, marker='.', label=label)

				# add legend
				# middle = (percentiles[0] + percentiles[100]) / 2
				# location = self._anchor((point[column], predictionii), (middle, prediction))
				pyplot.legend(loc='upper right')

				# save plot
				tagii = tagii.replace(' ', '_').replace(',', '').replace(':', '_')
				formats = (folder, variable, self._pad(index), tagii, self.session, self.tag)
				destination = '{}/examine_{}_{}_{}_{}_{}.png'.format(*formats)
				pyplot.savefig(destination)
				pyplot.clf()

		return None

	def histolyze(self, variable='fluxnet_gpp', position=0, bins=50, limits=None, constrain=False):
		"""Generate a histogram of a variable:

		Arguments:
			scan: int, the scan number
			variable: str, variable name
			position: int, the index of a multi-column variable
			bins: int, number of bins
			limits: tuple of floats, the limits of the data
			constrain: apply constraints?

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/histograms'.format(self.folder)
		self._make(folder)

		# get training matrix for particular scan
		_, data, constraints = self.materialize(self.training['sites'])

		self._view(data)

		# if constraining
		if constrain:

			# apply constraints
			data = self._constrain(data, constraints)

		# get data
		datum = data[variable][:, position]

		# remove nans
		mask = numpy.isfinite(datum)
		datum = datum[mask]

		# if given a limit
		if limits:

			# apply limit
			mask = (datum >= limits[0]) & (datum <= limits[1])
			datum = datum[mask]

		# print bounds
		self._print(datum.min(), datum.max())
		self._print(datum.shape)

		# Create a histogram
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))
		pyplot.hist(datum, bins=bins, edgecolor='black')

		# Add titles and labels
		title = 'histogram of {}:{}'.format(variable, position)
		pyplot.title(title)
		pyplot.xlabel('value')
		pyplot.ylabel('counts')

		# save the plot
		formats = (folder, variable, self._pad(position))
		destination = '{}/histogram_{}_{}.png'.format(*formats)
		pyplot.savefig(destination)
		pyplot.clf()

		# print status
		self._print('plotted {}'.format(destination))

		return None

	def inspect(self, bounds=('2010m0101', '2010m0401'), point=None, variables=None, margin=1.0):
		"""Inspect the model around a validation point with high error.

		Arguments:
			bounds: tuple of strings, the bounding dates
			point: tuple of time information, year, month, day, minute, hour
			variables: list of str, the variables to check
			margin: int, percent margin

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/inspection'.format(self.folder)
		self._make(folder)

		# collect training sites
		sites = self.training['sites']

		# get training matrix
		matrix, data, constraints = self.materialize(sites)

		# set default variables
		variables = variables or self.features[:-1]

		# get specific site matrix
		matrixii, dataii, constraintsii = self.materialize([self.site])
		dataii = self._constrain(dataii, constraintsii)

		# add hours and minutes to bounds
		bounds = (bounds[0] + 'T00:00', bounds[1] + 'T23:59')

		# if a point is given
		if point:

			# reconstruct bounds based on particular point
			year, month, day, hour, minute = point
			string = '{}m{}{}T{}:{}'.format(year, self._pad(month), self._pad(day), self._pad(hour), self._pad(minute))
			bounds = (string, string)

		# deconstruct bounda
		years = [int(bound[:4]) for bound in bounds]
		months = [int(bound[5:7]) for bound in bounds]
		days = [int(bound[7:9]) for bound in bounds]
		hours = [int(bound[10:12]) for bound in bounds]
		minutes = [int(bound[13:15]) for bound in bounds]

		# reduce matrixii to bounding box
		mask = (dataii['year'] >= years[0]) & (dataii['year'] <= years[1])
		mask = mask & (dataii['month'] >= months[0]) & (dataii['month'] <= months[1])
		mask = mask & (dataii['day'] >= days[0]) & (dataii['day'] <= days[1])
		mask = mask & (dataii['hour'] >= hours[0]) & (dataii['hour'] <= hours[1])
		mask = mask & (dataii['minute'] >= minutes[0]) & (dataii['minute'] <= minutes[1])
		mask = mask.squeeze()
		matrixii = matrixii[mask]

		# predict values from the model
		predictions, _, truths, _ = self.predict(matrixii)

		# get error between prediction and truth
		error = predictions - truths

		# get the sample with the highest absolute error, and corresponding matrix row
		sample = numpy.argmax(abs(error))
		row = matrixii[sample]
		prediction = predictions[sample]
		truth = truths[sample]
		year = dataii['year'].squeeze()[mask][sample]
		month = dataii['month'].squeeze()[mask][sample]
		day = dataii['day'].squeeze()[mask][sample]
		hour = dataii['hour'].squeeze()[mask][sample]
		minute = dataii['minute'].squeeze()[mask][sample]
		formats = (year, self._pad(month), self._pad(day), self._pad(hour), self._pad(minute))
		time = '{}m{}{} {}:{}'.format(*formats)
		self._print('date: {}'.format(time))

		# for each variable
		for variable in variables:

			# get the index of the feature
			index = self.features.index(variable)

			# create percentiles
			percentiles = numpy.array([numpy.percentile(matrix[:, index], percentile) for percentile in range(101)])

			# create probe matrix from medians for all features but variable in question
			probe = numpy.array([row.copy()] * 101)
			probe[:, index] = percentiles

			# predict values from the model
			predictionii, deviationii, _, _ = self.predict(probe)

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8, 8))

			# create title
			formats = (self.site, time, variable)
			title = '{} at {}, {}'.format(*formats)
			pyplot.title(title)

			# add labels
			pyplot.xlabel(variable)
			pyplot.ylabel(self.features[-1])

			# set limits
			pyplot.ylim(self.limits['scale'][0], truth + 1)

			# Sample data
			abscissa = percentiles
			ordinate = predictionii

			# create line plots
			pyplot.plot(abscissa, ordinate, 'co-', label='model')
			pyplot.plot(abscissa, ordinate - deviationii, 'c--')
			pyplot.plot(abscissa, ordinate + deviationii, 'c--')

			# plot prediction and truth
			pyplot.plot([row[index]], [truth], 'gX', markersize=15, label='truth')
			pyplot.plot([row[index]], [prediction], 'rX', markersize=15, label='prediction')

			# copy matrix
			xerox = matrix.copy()

			# # get percents
			# lower = numpy.percentile(xerox, 50 - margin, axis=0)
			# upper = numpy.percentile(xerox, 50 + margin, axis=0)

			# get standard deviation
			deviation = xerox.std(axis=0)

			# for each feature
			for position, feature in enumerate(self.features[:-1]):

				# if feature not varible of interest
				if feature != variable:

					# subset matrix
					buffer = deviation[position] * margin
					mask = (xerox[:, position] >= row[position] - buffer)
					mask = mask & (xerox[:, position] <= row[position] + buffer)
					xerox = xerox[mask]

			# print final sahape
			self._print('{} samples: {}'.format(variable, xerox.shape))

			# calculate differences from median for color scale
			difference = abs(xerox - row)
			difference[:, index] = 0
			difference = difference.sum(axis=1)

			# add truth points within median margins
			label = 'samples +/- {} std'.format(str(margin))
			pyplot.scatter(xerox[:, index], xerox[:, -1], c=difference, cmap='gray', s=25, marker='x', label=label)

			# add legend
			middle = (percentiles[0] + percentiles[100]) / 2
			location = self._anchor((row[index], truth), (middle, prediction))
			pyplot.legend(loc=location)

			# save plot
			destination = '{}/inspection_{}_{}_{}.png'.format(folder, self.site, time[:9], variable)
			pyplot.savefig(destination)
			pyplot.clf()

		return None

	def inventory(self, products=None):
		"""Create inventory of training day holdings.

		Arguments:
			products: list of str, the directories

		Returns:
			None
		"""

		# make inventory folder
		self._make('{}/Inventory'.format(self.sink))

		# set default folders
		products = ['GPP_Inputs', 'TEMPO_RAD_L1', 'TEMPO_CLDO4_L2', 'TEMPO_NO2_L2', 'Nitro_Inputs']
		products += ['Synthetic_GPP', 'Mosaic_GPP']

		# maximum length of products
		lengths = [len(product) for product in products]
		maximum = max(lengths)

		# begin inventory
		inventory = []

		# for each product folder
		for product in products:

			# get list of year subfolders
			years = self._see('{}/{}'.format(self.sink, product))
			years = [self._file(folder) for folder in years]

			# for each year subfolder
			for year in years:

				# get month folders
				months =  self._see('{}/{}/{}'.format(self.sink, product, year))
				months = [self._file(folder) for folder in months]

				# for each month
				for month in months:

					# get day folders
					days = self._see('{}/{}/{}/{}'.format(self.sink, product, year, month))
					days = [self._file(folder) for folder in days]

					# for each day
					for day in days:

						# make hydra
						formats = (self.sink, product, year, self._pad(month), self._pad(day))
						hydra = Hydra('{}/{}/{}/{}/{}'.format(*formats), show=False)

						# get all granules
						granules = [self._search('S[0-9]{3}G[0-9]{2}', path) for path in hydra.paths]

						# group by scan
						scans = self._group(granules, lambda granule: granule[:4])

						# for each scan
						for scan, members in scans.items():

							# reduce members and sort
							members = [member[-2:] for member in members]
							members.sort()
							members = ' '.join(members)

							# add element to inventory
							date = '{}-{}-{}:  '.format(year, month, day)
							tab = ' ' * (maximum - len(product))
							element = [date, '{}:  '.format(scan), '{}:  {}'.format(product, tab), members]
							inventory.append(element)

		# for each field except members
		for index in (2, 1, 0):

			# sort inventory
			inventory.sort(key=lambda element: element[index])

		# begin secondary inventory
		inventoryii = [[entry for entry in inventory[0]]]

		# for each element
		for index, element in enumerate(inventory):

			# if not the first
			if index > 0:

				# copy element
				elementii = [entry for entry in element]

				# if date is the same as above
				if element[0] == inventory[index - 1][0]:

					# make it blank instead
					elementii[0] = ' ' * len(element[0])

				# if scan is the same as above
				if element[1] == inventory[index - 1][1]:

					# make it blank instead
					elementii[1] = ' ' * len(element[1])

				# add to secondar inventory
				inventoryii.append(elementii)

		# begin report
		report = ['Training Data Inventory']

		# or each element in the inventory
		for elementii in inventoryii:

			# append to report
			line = '{}{}{}{}'.format(*elementii)
			report.append(line)

			# print to screen
			self._print(line)

		# jot report
		self._jot(report, '{}/Inventory/training_inventory.txt'.format(self.sink))

		return None

	def materialize(self, sites, constrain=True, exclude=False, reservoir=None):
		"""Form training matrix from a set of input files.

		Arguments:
			sites: list str, the sites of interest
			constrain: boolean, apply constraints to matrix?
			exclude: boolean, exclude truths from limits?
			reservoir: str, name of folder from which to grab data

		Returns:
			numpy array, dict of numpy arrays
		"""

		# set features
		features = self.features

		# set reservoir
		reservoir = reservoir or 'Onefluxnet_Inputs'

		# map fields to yaml limits
		limits = {'quality': 'quality', 'uncertainty': 'uncertainty', 'year': 'year', 'short_wave': 'wave'}
		limits.update({'short_wave_quality': 'qualifier', 'fluxnet_gpp': 'productivity'})
		limits.update({'day_night_difference': 'difference', 'night_gpp': 'night'})

		# begin reservoirs
		matrix = []
		constraints = []
		data = {}

		# construct hydra
		hydra = Hydra('{}/{}'.format(self.sink, reservoir))

		# for each day
		for site in sites:

			# ingest site
			hydra.ingest(site)

			# extract data
			extract = hydra.extract()

			# separate extract based on dimensionality
			ones = {field: array for field, array in extract.items() if len(array.shape) == 1}
			twos = {field: array for field, array in extract.items() if len(array.shape) == 2}

			# remove 1-d arrays
			extract = {field: array for field, array in extract.items() if field not in ones.keys()}

			# reshape all 2 and 3-d arrays
			extract.update({field: array.reshape(-1, array.shape[1]) for field, array in twos.items()})

			# get inputs
			block = self._forge(extract, features)

			# begin constraint
			constraint = numpy.ones(extract['latitude'].shape).astype(bool)

			# for each limit
			for field, limit in limits.items():

				# add lower and upper limit constraints
				constraint = constraint & (extract[field] >= self.limits[limit][0])
				constraint = constraint & (extract[field] <= self.limits[limit][1])

			# if excluding truth from limits:
			if exclude:

				# check for nans in truth
				constraint = constraint & (self._mask(block[:, :-1].sum(axis=1)).reshape(-1, 1))
				constraint = constraint & (self._mask(block[:, :-1].min(axis=1)).reshape(-1, 1))
				constraint = constraint & (self._mask(block[:, :-1].max(axis=1)).reshape(-1, 1))
				constraint = constraint.squeeze()

			# otherwise
			else:

				# check for nans in truth
				constraint = constraint & (self._mask(block.sum(axis=1)).reshape(-1, 1))
				constraint = constraint & (self._mask(block.min(axis=1)).reshape(-1, 1))
				constraint = constraint & (self._mask(block.max(axis=1)).reshape(-1, 1))
				constraint = constraint.squeeze()

			# if apply constraints
			if constrain:

				# apply constraints
				block = block[constraint]

			# add 1-d arrays back
			extract.update({field: array.reshape(1, -1) for field, array in ones.items()})

			# add to reservoirs
			[data.setdefault(field, []).append(array) for field, array in extract.items()]
			matrix.append(block)
			constraints.append(constraint)

		# stack into matrix
		matrix = numpy.vstack(matrix)
		constraints = numpy.hstack(constraints)
		data = {field: numpy.vstack(arrays) for field, arrays in data.items()}

		return matrix, data, constraints

	def mosaic(self, scan=10, radius=0.04, limit=None, sites=None):
		"""Produce a synthetic fluxnet file from several onefluxnet sites.

		Arguments:
			scan: int, the scan number
			radius: float, the radius in degrees around each site
			limit: float, the r2 score limit for sites to include
			sites: list of str, the list of sites to use

		Returns:
			None
		"""

		# default limit
		limit = limit or self.limits['score'][0]

		# set formats
		formats = (self.year, self._pad(self.month), self._pad(self.day))
		folder = 'Mosaic_GPP'

		# create folders
		self._make('{}/{}'.format(self.sink, folder))
		self._make('{}/{}/{}'.format(self.sink, folder, *formats[:1]))
		self._make('{}/{}/{}/{}'.format(self.sink, folder, *formats[:2]))
		self._make('{}/{}/{}'.format(self.sink, folder, self.stub))

		# create label from scan
		label = self._label(scan)

		# create hydra for cloud files
		clouds = Hydra('{}/TEMPO_CLDO4_L2/{}'.format(self.sink, self.stub), show=False)
		paths = [path for path in clouds.paths if label in path]

		# get onefluxnet models
		models = self._see('{}/Onefluxnet/models'.format(self.sink))

		# get onefluxnet input files
		hydraii = Hydra('Onefluxnet_Inputs')

		# begin scores
		scores = {}

		# for each path
		for path in hydraii.paths:

			# ingest and get score
			hydraii.ingest(path)
			score = hydraii.grab('r2_score')[0][0]
			site = self._locate(path)

			# add to collection
			scores[site] = score

		# get list of sites and sort by score, highest last
		sites = sites or self._know('Onefluxnet/sites/study_sites.txt')
		sites = [site for site in sites if scores[site] > limit]
		sites.sort(key=lambda site: scores[site])

		# for each path
		for path in paths:

			# ingest and get latitude, longitude
			clouds.ingest(path)
			latitude = clouds.grab('latitude')
			longitude = clouds.grab('longitude')
			shape = latitude.shape

			# begin data
			data = {'latitude': latitude, 'longitude': longitude}

			# extract the time
			time = self._search('T[0-9]{6}Z', path)

			# create hydra for shortwave files
			shorts = Hydra('{}/Merra_SW/{}'.format(self.sink, self.stub), show=False)
			shorts.ingest()

			# grab data
			minutes = shorts.grab('time')
			waves = shorts.grab('SWGDN')
			latitudes = shorts.grab('lat')
			longitudes = shorts.grab('lon')

			# convert the time to minutes
			elements = [int(time[pair[0]: pair[1]]) for pair in [(1, 3), (3, 5), (5, 7)]]
			date = datetime.datetime(self.year, self.month, self.day, *elements)
			midnight = datetime.datetime(self.year, self.month, self.day)
			interval = (date - midnight).seconds / 60

			# determine closest index and subset waves
			intervals = [(interval - minute) ** 2 for minute in minutes]
			index = numpy.argsort(numpy.array(intervals))[0]
			wave = waves[index]

			# interpolate onto tempo grid, capping short wave at 1e5
			interpolation = self._interpolate(wave, latitudes, longitudes, latitude, longitude, method='linear')
			interpolation = numpy.where(interpolation <= 1e5, interpolation, -1e30)
			interpolation = numpy.where(interpolation >= 0, interpolation, -1e30)
			data['short_wave'] = interpolation

			# begin bands
			bands = []

			# for each modis band
			for band in range(7):

				# create hydra
				albedos = Hydra('{}/MCD43D{}/{}'.format(self.sink, band + 62, self.stub), show=False)
				albedos.ingest()

				# grab albedo
				albedo = albedos.grab('Albedo').squeeze()
				factor = albedos.grab('scale_factor')[0][0]
				albedo = albedo * factor
				shapeii = albedo.shape

				# construct latitudes, starting at 90N
				size = 180 / shapeii[0]
				half = int(size / 2)
				latitudes = [90 - half - (index * size) for index in range(shapeii[0])]

				# construct longitudes, staring at 180W
				size = 360 / shapeii[1]
				half = int(size / 2)
				longitudes = [-180 + half + (index * size) for index in range(shapeii[1])]

				# interpolate onto tempo grid
				parameters = (albedo, latitudes, longitudes, latitude, longitude)
				interpolation = self._interpolate(*parameters, method='linear')
				interpolation = numpy.where(interpolation <= 10, interpolation, -1e30)
				interpolation = numpy.where(interpolation >= 0, interpolation, -1e30)
				bands.append(interpolation)

			# add to data
			data['mcd43_bands'] = numpy.array(bands).transpose(1, 2, 0)

			# construct matrix
			matrix = numpy.hstack([data['mcd43_bands'].reshape(-1, 7), data['short_wave'].reshape(-1, 1)])

			# set up mosaic grid with fill
			mosaic = numpy.ones(shape).reshape(-1) * self.fill

			# reshape tempo latitude and longitude
			latitude = latitude.reshape(-1)
			longitude = longitude.reshape(-1)

			# for each site
			for site in sites:

				# print current site
				self._print(site, scores[site])

				# get center point
				point = (self.catalog[site]['latitude'], self.catalog[site]['longitude'])

				# get model and scalers
				pathsii = [path for path in models if site in path]
				pathsii.sort()
				model = joblib.load(pathsii[0])
				scaler = joblib.load(pathsii[1])
				scalerii = joblib.load(pathsii[2])

				# create mask for points within radius
				mask = ((latitude - point[0]) ** 2 + (longitude - point[1]) ** 2) <= radius ** 2

				# if there are any points within radius
				if mask.sum() > 0:

					# print status:
					self._print(scan)
					self._print(site)
					self._print(mask.sum())

					# subset inputs and apply scaler
					inputs = matrix[mask]
					inputs = scaler.transform(inputs)

					# get predictions
					prediction = model.predict(inputs)
					prediction = scalerii.inverse_transform(prediction.reshape(-1, 1)).squeeze()

					# add prediction to mosaic
					mosaic[mask] = prediction

			# remove values above 30 and below 0
			mosaic = numpy.where(mosaic < 0, self.fill, mosaic)
			mosaic = numpy.where(mosaic > 30, self.fill, mosaic)

			# reshapa and add to data
			mosaic = mosaic.reshape(*shape)
			data['gpp_mosaic'] = mosaic

			# check data
			self._view(data)

			# construct destination
			destination = path.replace('TEMPO_CLDO4_L2', folder).replace('.nc', '.h5')
			self.spawn(destination, data)

		return None

	def neighbor(self, instances=50, samples=1000, fraction=0.5, metric='euclidean', number=None):
		"""Run a KNN regressor to approximate irreducible error.

		Arguments:
			instances: int, number of instances for model predictions
			samples: int, number of samples to use
			fraction: float, fraction for training, test split
			metric: str, similarity metric
			number: int, number of nearest neighbors

		Returns:
			None
		"""

		# default number to sqrt of samples
		number = number or int(numpy.sqrt(samples * fraction))

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folders
		self._make('{}/reports'.format(folder))

		# begin report
		report = [self._print('KNN Nearest Neighbors regressor...')]

		# get training matrix for particular scan
		features = details['features']
		parameters = ([(self.site, self.site)],)
		matrix, features, _, _, _ = self.materialize(*parameters, features=features)

		# get scaling information
		minimum = details['input_min']
		maximum = details['input_max']
		scale = maximum - minimum

		# print size
		report.append(self._print('matrix shape: {}'.format(matrix.shape)))

		# calculate total variables
		variance = numpy.var(matrix[:, -1])
		report.append(self._print('total variance: {}'.format(variance)))

		# shuffle the matrix
		numpy.random.shuffle(matrix)

		# get the splitting index
		split = int(samples * fraction)

		# remove truth and apply scaling
		truth = matrix[:split, -1]
		inputs = (matrix[:split, :-1] - minimum) / scale

		# remove test set and apply scaling
		truthii = matrix[split:samples, -1]
		inputsii = (matrix[split:samples, :-1] - minimum) / scale

		# print train and test sizes
		report.append(self._print('training matrix shape: {}'.format(inputs.shape)))
		report.append(self._print('testing matrix shape: {}'.format(inputsii.shape)))

		# create the knn regressor and fit
		report.append(self._print('training knn with {} neighbors, {} metric...'.format(number, metric)))
		regressor = KNeighborsRegressor(n_neighbors=number, metric=metric)
		regressor.fit(inputs, truth)

		# predict on test data and calculate residuals
		prediction = regressor.predict(inputsii)
		residuals = truthii - prediction

		# estimate irreducible error from variance of residuatlw
		error = numpy.var(residuals)
		percent = 100 * (error / variance)
		report.append(self._print('irreducible error: {}'.format(error)))
		report.append(self._print('irreducible error: {} %'.format(percent)))

		# jot report
		formats = (folder, self.site, metric, samples, number)
		destination = '{}/reports/knn_neighbors_estimate_{}_{}_{}_{}.txt'.format(*formats)
		self._jot(report, destination)

		return None

	def point(self, date, hour=12, minute=0):
		"""Get a matrix point from the dataset.

		Arguments:
			date: str, date in YYYYmMMDD format
			hour: int, the hour
			minute: int, the minutes

		Returns:
			numpy array, str tuple, the points and a descriptive tag
		"""

		# get the matrix and data
		matrix, data, constraints = self.materialize([self.site])
		data = self._constrain(data, constraints)

		# parse date
		year = int(date[:4])
		month = int(date[5:7])
		day = int(date[7:9])

		# construct mask
		mask = (data['year'] == year)
		mask = mask & (data['month'] == month)
		mask = mask & (data['day'] == day)
		mask = mask & (data['hour'] == hour)
		mask = mask & (data['minute'] == minute)
		mask = mask.squeeze()

		# get points
		points = matrix[mask]

		# create description
		formats = (self.site, date, self._pad(hour), self._pad(minute))
		tag = '{}, {}, {}:{}'.format(*formats)

		return points, tag

	def predict(self, matrix, instances=None):
		"""Use the model to make predictions on a matrix.

		Arguments:
			model: tensorflow model instance
			details: dict of model information

		Returns:
			tuple of numpy arrays, the truth, the averaged predictions, and deviation
		"""

		# set default instances
		instances = instances or self.instances

		# transform the input data
		inputs, truth = self._transform(matrix)

		# get model outputs
		outputs = []
		for _ in range(instances):

			# get outputs
			output = self.model(inputs)
			output = output.numpy().squeeze()
			outputs.append(output)

		# create array
		outputs = numpy.array(outputs)

		# if outputs are 1-D
		if len(outputs.shape) < 2:

			# reshape for second dimension
			outputs = outputs.reshape(-1, 1)

		# translate the predictions from the raw outputs
		predictions = self._translate(outputs)

		# take mean and stdev of predictions
		average = numpy.array(predictions).mean(axis=0)
		spread = numpy.array(predictions).std(axis=0)

		# make sure they are finite
		mask = (numpy.isfinite(truth) & numpy.isfinite(average))

		# default score to 0
		score = 0

		# if there are samples available
		if mask.sum() > 1:

			# compute r^2 score on mean of predictions
			score = r2_score(truth[mask], average[mask])

		return average, spread, truth, score

	def prepare(self, bracket=('AA-A', 'ZZ-Z')):
		"""Prepare training data files for GPP simulator.

		Arguments:
			bracket: tuple of str, the alphabetical site brackets

		Returns:
			None
		"""

		# create destination folders
		folder = '{}/Onefluxnet_Inputs'.format(self.sink)
		self._make(folder)

		# get paths for ingestion
		records = self._see('{}/Onefluxnet/records'.format(self.sink))
		reflectivities = self._see('{}/Onefluxnet/modis'.format(self.sink))
		models = self._see('{}/Onefluxnet/models'.format(self.sink))

		# get all name names
		names = [self._locate(path) for path in records]
		names.sort()

		# restrict names
		names = [name for name in names if name >= bracket[0] and name <= bracket[1]]

		# for each model path
		for name in names:

			# try to
			try:

				# print status
				self._print('extracting site {}...'.format(name))

				# extract record data
				path = [path for path in records if name in path][0]
				record = self._read(path)['rec_all']

				# extract modis data
				path = [path for path in reflectivities if name in path][0]
				reflectivity = self._read(path)['mcd43']

				# default model to None
				model = None
				score = 0.0

				# try to
				try:

					# extract model and scalers
					paths = [path for path in models if name in path]
					paths.sort()
					model = joblib.load(paths[0])
					scaler = joblib.load(paths[1])
					scalerii = joblib.load(paths[2])

				# unless not found
				except IndexError:

					# in which case, print warning
					self._print('no model for {}'.format(name))

				# begin record collection
				year = []
				month = []
				day = []
				hour = []
				minute = []
				productivity = []
				night = []
				wave = []
				quality = []
				assessment = []
				uncertainty = []
				bands = []
				difference = []
				temperature = []
				vapor = []
				water = []
				sites = []

				# for each member in record
				for datum, datumii in zip(record, reflectivity):

					# append data records
					year.append(datum['year'])
					month.append(datum['month'])
					day.append(datum['day'])
					hour.append(datum['hour'])
					minute.append(datum['minute'])
					productivity.append(datum['gpp_dt_vut_ref'])
					night.append(datum['gpp_nt_vut_ref'])
					wave.append(datum['sw_in_f'])
					quality.append(datum['nee_vut_ref_qc'])
					assessment.append(datum['sw_in_f_qc'])
					uncertainty.append(datum['nee_vut_ref_randunc'])
					difference.append(abs(productivity[-1] - night[-1]))
					temperature.append(datum['ta_f'])
					vapor.append(datum['vpd_f'])
					water.append(datum['ws_f'])
					bands.append(datumii)

				# create data
				data = {'year': year, 'month': month, 'day': day, 'hour': hour, 'minute': minute}
				data.update({'fluxnet_gpp': productivity, 'short_wave': wave, 'quality': quality})
				data.update({'uncertainty': uncertainty, 'mcd43_bands': bands, 'short_wave_quality': assessment})
				data.update({'night_gpp': night, 'day_night_difference': difference})
				data.update({'temperature': temperature, 'vapor': vapor, 'water': water})

				# create numpy arrays
				data = {field: numpy.array(array) for field, array in data.items()}

				# reshape
				data.update({field: array.reshape(-1, 1) for field, array in data.items() if len(array.shape) == 1})

				# add geocoordinates to data
				latitude = [self.catalog[name]['latitude'] for _ in range(data['year'].shape[0])]
				longitude = [self.catalog[name]['longitude'] for _ in range(data['year'].shape[0])]
				sites = [name for _ in range(data['year'].shape[0])]
				data.update({'latitude': numpy.array(latitude).reshape(-1, 1)})
				data.update({'longitude': numpy.array(longitude).reshape(-1, 1)})
				data.update({'site': numpy.array(sites).reshape(-1, 1)})

				# if model exists
				if model:

					# generate matrix
					matrix = numpy.hstack([data['mcd43_bands'], data['short_wave']])

					# filter for quality
					mask = (numpy.isfinite(matrix.sum(axis=1))) & (matrix.sum(axis=1) > 0)
					mask = mask & (data['quality'].squeeze() <= self.limits['quality'][1])
					mask = mask & (data['uncertainty'].squeeze() <= self.limits['uncertainty'][1])
					mask = mask & (data['fluxnet_gpp'].squeeze() > 0)
					mask = mask & (data['short_wave'].squeeze() >= 0)
					mask = mask & (data['mcd43_bands'].sum(axis=1) >= 0)

					# apply mask and scale
					matrix = matrix[mask]
					matrix = scaler.transform(matrix)

					# get predictions
					prediction = model.predict(matrix)
					prediction = scalerii.inverse_transform(prediction.reshape(-1, 1)).squeeze()

					# calculate r^2 score
					truth = data['fluxnet_gpp'].squeeze()[mask]
					score = r2_score(truth, prediction)

				# add score to data
				scores = [score for _ in range(data['year'].shape[0])]
				data.update({'r2_score': numpy.array(scores).reshape(-1, 1)})

				# view data
				self._view(data)

				# stash data
				destination = '{}/Onefluxnet_Inputs/Onefluxnet_inputs_{}.h5'.format(self.sink, name)
				self.spawn(destination, data)

			# unless a problem
			except IndexError:

				# print message
				self._print('{} skipped!'.format(name))

		return None

	def present(self, destination, truth, prediction, deviation, valids, data, instances, chart=True):
		"""Make presentation plots.

		Arguments:
			destination: str, destination filepath
			truth: array of gpp fluxsat data
			prediction: NN model prediction
			deviation: NN model stdev
			valids: dict of valid data
			data: dict of all data
			instances: int, number of prediction instances
			chart: boolean, plot maps?

		Returns:
			None
		"""

		# begin status
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase preevious copy
		self._clean(destination, force=True)

		# load statistics json
		statistics = self._load('{}/statistics/statistics.json'.format(self.folder))

		# retrieve statistics
		score = statistics[self.site]['score']
		error = statistics[self.site]['error']
		bias = statistics[self.site]['bias']

		# construct times
		years = valids['year'].squeeze()
		months = valids['month'].squeeze()
		days = valids['day'].squeeze()
		hours = valids['hour'].squeeze()
		minutes = valids['minute'].squeeze()
		zipper = zip(years, months, days, hours, minutes)
		times = [datetime.datetime(year, month, day, hour, minute) for year, month, day, hour, minute in zipper]

		# begin masks for years in july
		masks = []
		for year in range(years.min(), years.max() + 1):

			# get all july measurements
			mask = (months == 7) & (years == year)
			masks.append((year, mask))

		# sort by number of samples, and get top
		masks.sort(key=lambda pair: pair[1].sum(), reverse=True)
		year, mask = masks[0]

		# for each hour
		means = []
		meansii = []
		deviations = []
		deviationsii = []
		diurnal = [hour for hour in range(3, 24)]
		for hour in range(3, 24):

			# get all samples in hour range
			maskii = mask & (hours == hour)

			# calculates means and stdeves
			means.append(truth[maskii].mean())
			deviations.append(truth[maskii].std())
			meansii.append(prediction[maskii].mean())
			deviationsii.append(prediction[maskii].std())

		# create new figure
		figure, axes = pyplot.subplots(nrows=3, ncols=1, figsize=(21, 15), constrained_layout=True)
		figure.subplots_adjust(left=0.1, right=0.95, top=0.95, bottom=0.05)
		font = 20

		# plot truth and predictions
		axis = axes[0]
		axis.tick_params(axis='both', which='major', labelsize=font)
		axis.set_title(self.site, fontsize=font)
		axis.set_xlabel('date', fontsize=font)
		axis.set_ylabel('{} ( {} )'.format(self.target, self.unit), fontsize=font)
		# axis.set_xlim(datetime.datetime(min(times).year - 1, 1, 1), datetime.datetime(max(times).year + 2, 1, 1))
		axis.set_ylim(0, max(truth))
		axis.plot(times, truth, 'k.', markersize=3)
		axis.plot(times, prediction, 'r.', markersize=3)

		# plot scatter plot
		axis = axes[1]
		title = '$R^2$={:.3f} bias={:.3f} $RMSE$={:.3f}'.format(score, bias, error)
		axis.tick_params(axis='both', which='major', labelsize=font)
		axis.set_title(title, fontsize=font)
		axis.plot(truth, prediction, 'k.', markersize=3)
		axis.plot([min(truth), max(truth)], [min(truth), max(truth)], 'r-', linewidth=2)

		# plot error plot
		axis = axes[2]
		title = '{} {} July'.format(self.site, year)
		axis.tick_params(axis='both', which='major', labelsize=font)
		axis.set_title(title, fontsize=font)
		axis.set_xlabel('hour', fontsize=font)
		axis.set_ylabel('{} ( {} )'.format(self.target, self.unit), fontsize=font)
		axis.errorbar(diurnal, means, deviations, fmt='k^', linewidth=2, capsize=1, ms=20)
		axis.errorbar(diurnal, meansii, deviationsii, fmt='rs', linewidth=2, capsize=1, ms=20)

		# save the plot
		self._print('saving {}'.format(destination))
		pyplot.savefig(destination)

		return None

	def probe(self, variables=None, margin=25):
		"""Probe model as a function of independent variable, with median values at all others.

		Arguments:
			variable: str, dependent variable
			margin: int, perentile margin

		Returns:
			None
		"""

		# make shapely folder
		folder = '{}/probe'.format(self.folder)
		self._make(folder)

		# set default variable
		variables = variables or self.features[:-1]

		# get training matrix for particular scan
		matrix, data, constraints = self.materialize(self.training['sites'])

		# get the upper and lower margins based on the median
		lower = numpy.percentile(matrix, 50 - margin, axis=0)
		upper = numpy.percentile(matrix, 50 + margin, axis=0)

		# also calculate each tenth percentile
		# tens = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
		tens = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
		tens += [55, 60, 65, 70, 75, 80, 85, 90, 95, 100]
		tens = [index * 2 for index in range(51)]
		tens.reverse()
		tenths = {ten: numpy.percentile(matrix, ten, axis=0) for ten in tens}

		# get a color map
		colors = matplotlib.cm.get_cmap('turbo_r')
		# colors = matplotlib.cm.get_cmap('gist_earth')

		# for each variable
		for variable in variables:

			# get the index of the feature
			index = self.features.index(variable)

			# create percentiles
			percentiles = numpy.array([numpy.percentile(matrix[:, index], percentile) for percentile in range(101)])

			# begin predictions and deviations
			predictions = {}
			deviations = {}

			# for each tenth
			for ten in tens:

				# create probe matrix from medians for all features but variable in question
				probe = numpy.array([tenths[ten]] * 101)
				probe[:, index] = percentiles

				# predict values from the model
				prediction, deviation, _, _ = self.predict(probe)
				predictions[ten] = prediction
				deviations[ten] = deviation

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8, 8))

			# create title
			title = '{} at medians, +\- {}th percentile'.format(variable, margin)
			pyplot.title(title)

			# add labels
			pyplot.xlabel(variable)
			pyplot.ylabel(self.features[-1])

			# set limits
			minimum = matrix[:, -1].min(axis=0)
			maximum = matrix[:, -1].max(axis=0)
			buffer = (maximum - minimum) * 0.05
			pyplot.xlim(percentiles[0], percentiles[90])
			pyplot.ylim(self.limits['scale'][0] - 5, self.limits['scale'][1] - 10)

			# for each prediction
			for ten in tens:

				# set alpha
				alpha = 0.1 + 0.00036 * (50 - abs(ten - 50)) ** 2

				# Sample data
				abscissa = percentiles
				ordinate = predictions[ten]
				deviation = deviations[ten]

				# create line plots
				pyplot.plot(abscissa, ordinate, marker='.', linestyle='solid', alpha=alpha, color=colors(ten / 100))
				# pyplot.plot(abscissa, ordinate - deviation, 'c--', alpha=alpha)
				# pyplot.plot(abscissa, ordinate + deviation, 'c--', alpha=alpha)

			# copy matrix
			xerox = matrix.copy()

			# for each feature
			for position, feature in enumerate(self.features[:-1]):

				# if feature not varible of interest
				if feature != variable:

					# subset matrix
					mask = (xerox[:, position] >= lower[position]) & (xerox[:, position] <= upper[position])
					xerox = xerox[mask]

			# calculate differences from median for color scale
			difference = abs(xerox - tenths[50])
			difference[:, index] = 0
			difference = difference.sum(axis=1)

			# add truth points within median margins
			pyplot.scatter(xerox[:, index], xerox[:, -1], c=difference, cmap='gray', s=10, marker='.')

			# save plot
			destination = '{}/probe_{}_{}_{}.png'.format(folder, self.session, self.tag, variable)
			pyplot.savefig(destination)
			pyplot.clf()

		return None

	def produce(self, scan=10, instances=20):
		"""Produce a synthetic fluxnet file from the onefluxnet model.

		Arguments:
			scan: the TEMPO scan number
			instances: int, number of predition instances

		Returns:
			None
		"""

		# set formats
		formats = (self.year, self._pad(self.month), self._pad(self.day))
		folder = 'Synthetic_GPP'

		# create folders
		self._make('{}/{}'.format(self.sink, folder))
		self._make('{}/{}/{}'.format(self.sink, folder, *formats[:1]))
		self._make('{}/{}/{}/{}'.format(self.sink, folder, *formats[:2]))
		self._make('{}/{}/{}'.format(self.sink, folder, self.stub))

		# create label from scan
		label = self._label(scan)

		# create hydra for cloud files
		clouds = Hydra('{}/TEMPO_CLDO4_L2/{}'.format(self.sink, self.stub), show=False)
		paths = [path for path in clouds.paths if label in path]

		# for each path
		for path in paths:

			# ingest and get latitude, longitude
			clouds.ingest(path)
			latitude = clouds.grab('latitude')
			longitude = clouds.grab('longitude')
			seconds = clouds.grab('time')
			shape = latitude.shape

			# begin data
			data = {'latitude': latitude, 'longitude': longitude}

			# convert times in seconds since 1980/1/6 to minutes
			dates = [datetime.datetime(1980, 1, 6) + datetime.timedelta(seconds=second) for second in seconds]
			minutes = numpy.array([(date.hour * 60) + date.minute + (date.second / 60) for date in dates])

			# create hydra for shortwave files
			shorts = Hydra('{}/Merra_SW/{}'.format(self.sink, self.stub), show=False)
			shorts.ingest()

			# create hydra for shortwave files
			shortsii = Hydra('{}/Merra/{}'.format(self.sink, self.stub), show=False)
			shortsii.ingest()

			# grab data, time is minutes since 00:30 ( so add 30 )
			minutesii = shorts.grab('time') + 30
			waves = shorts.grab('SWGDN')
			latitudes = shorts.grab('lat')
			longitudes = shorts.grab('lon')
			fractions = shortsii.grab('CLDTOT')

			# augment short wave and cloud fractions and minutes data by wrapping endpoints around
			waves = numpy.vstack([waves[waves.shape[0] - 1: waves.shape[0]], waves, waves[0:1]])
			fractions = numpy.vstack([fractions[fractions.shape[0] - 1: fractions.shape[0]], fractions, fractions[0:1]])
			minutesii = numpy.hstack([minutesii[-1] - 1440, minutesii, minutesii[0] + 1440])

			# determine time indices from merra short wave files
			first = [minute <= minutes.min() for minute in minutesii].index(False) - 1
			last = [minute > minutes.max() for minute in minutesii].index(True) + 1

			# begin latitude, longitude interpolations
			interpolations = []
			interpolationsii = []

			# for each index
			for index in range(first, last):

				# interpolate onto tempo grid
				parameters = (waves[index], latitudes, longitudes, latitude, longitude)
				interpolation = self._interpolate(*parameters, method='linear')
				interpolation = numpy.where(interpolation <= 1e5, interpolation, -1e30)
				interpolation = numpy.where(interpolation >= 0, interpolation, -1e30)
				interpolations.append(interpolation)

				# interpolate cloud fractions onto tempo grid
				parameters = (fractions[index], latitudes, longitudes, latitude, longitude)
				interpolationii = self._interpolate(*parameters, method='linear')
				interpolationii = numpy.where(interpolationii <= 1e5, interpolationii, -1e30)
				interpolationii = numpy.where(interpolationii >= 0, interpolationii, -1e30)
				interpolationsii.append(interpolationii)

			# create arrays
			interpolations = numpy.array(interpolations)
			interpolationsii = numpy.array(interpolationsii)
			bracket = minutesii[first:last]

			# get grid shape
			shape = interpolations[0].shape

			# expand minutes to include track position
			minutes = numpy.array([minutes] * shape[1]).transpose(1, 0)

			# set up interpolator
			axes = (bracket, numpy.arange(shape[0]), numpy.arange(shape[1]))
			options = {'method': 'linear', 'bounds_error': False, 'fill_value': None}
			interpolator = scipy.interpolate.RegularGridInterpolator(axes, interpolations, **options)

			# get results
			mesh, meshii = numpy.meshgrid(axes[1], axes[2], indexing='ij')
			points = numpy.stack([minutes.ravel(), mesh.ravel(), meshii.ravel()], axis=-1)
			interpolation = interpolator(points).reshape(shape)

			# set data
			data['short_wave'] = interpolation

			# set up interpolator
			axes = (bracket, numpy.arange(shape[0]), numpy.arange(shape[1]))
			options = {'method': 'linear', 'bounds_error': False, 'fill_value': None}
			interpolator = scipy.interpolate.RegularGridInterpolator(axes, interpolationsii, **options)

			# get results
			mesh, meshii = numpy.meshgrid(axes[1], axes[2], indexing='ij')
			points = numpy.stack([minutes.ravel(), mesh.ravel(), meshii.ravel()], axis=-1)
			interpolation = interpolator(points).reshape(shape)

			# set data
			data['cloud_fraction_merra'] = interpolation

			# begin bands
			bands = []

			# for each modis band
			for band in range(7):

				# create hydra
				albedos = Hydra('{}/MCD43D{}/{}'.format(self.sink, band + 62, self.stub), show=False)
				albedos.ingest()

				# grab albedo
				albedo = albedos.grab('Albedo').squeeze()
				factor = albedos.grab('scale_factor')[0][0]
				albedo = albedo * factor
				shapeii = albedo.shape

				# construct latitudes, starting at 90N
				size = 180 / shapeii[0]
				half = size / 2
				latitudes = [90 - half - (index * size) for index in range(shapeii[0])]

				# construct longitudes, staring at 180W
				size = 360 / shapeii[1]
				half = size / 2
				longitudes = [-180 + half + (index * size) for index in range(shapeii[1])]

				# interpolate onto tempo grid, imposing limits from 0 to 10
				parameters = (albedo, latitudes, longitudes, latitude, longitude)
				interpolation = self._interpolate(*parameters, method='nearest')
				interpolation = numpy.where(interpolation <= 10, interpolation, -1e30)
				interpolation = numpy.where(interpolation >= 0, interpolation, -1e30)
				bands.append(interpolation)

			# add to data
			data['mcd43d_bands'] = numpy.array(bands).transpose(1, 2, 0)

			# construct matrix
			faux = numpy.ones(data['short_wave'].shape).reshape(-1, 1)
			matrix = numpy.hstack([data['mcd43d_bands'].reshape(-1, 7), data['short_wave'].reshape(-1, 1), faux])

			# get predictions
			mean, deviation, _, _ = self.predict(matrix, instances=instances)

			# create mask for matrix in case of fills or nans in bands
			mask = self._mask(matrix).prod(axis=1).astype(bool)
			mean = numpy.where(mask, mean, self.fill)
			deviation = numpy.where(mask, deviation, self.fill)

			# remove values above synthetic data limits and below 0
			mask = (mean >= self.limits['synthetic'][0]) & (mean <= self.limits['synthetic'][1])
			mean = numpy.where(mask, mean, self.fill)
			deviation = numpy.where(mask, deviation, self.fill)

			# reshape and add to data
			data['gpp_onefluxnet_high'] = mean.reshape(*shape)
			data['onefluxnet_high_deviation'] = deviation.reshape(*shape)

			# begin low resolution bands
			bandsii = []

			# create hydra
			albedosii = Hydra('{}/MCD43C4/{}'.format(self.sink, self.stub), show=False)
			albedosii.ingest()

			# for each modis band
			for band in range(7):

				# grab albedo
				albedo = albedosii.grab('Band{}/Nadir_Reflectance'.format(band + 1)).squeeze()
				factor = albedosii.grab('Band{}/scale_factor'.format(band + 1))[0][0]
				albedo = albedo * factor
				shapeii = albedo.shape

				# construct latitudes, starting at 90N
				size = 180 / shapeii[0]
				half = size / 2
				latitudes = [90 - half - (index * size) for index in range(shapeii[0])]

				# construct longitudes, staring at 180W
				size = 360 / shapeii[1]
				half = size / 2
				longitudes = [-180 + half + (index * size) for index in range(shapeii[1])]

				# interpolate onto tempo grid, imposing limits from 0 to 10
				parameters = (albedo, latitudes, longitudes, latitude, longitude)
				interpolation = self._interpolate(*parameters, method='nearest')
				interpolation = numpy.where(interpolation <= 10, interpolation, -1e30)
				interpolation = numpy.where(interpolation >= 0, interpolation, -1e30)
				bandsii.append(interpolation)

			# add to data
			data['mcd43c_bands'] = numpy.array(bandsii).transpose(1, 2, 0)

			# construct matrix
			faux = numpy.ones(data['short_wave'].shape).reshape(-1, 1)
			matrix = numpy.hstack([data['mcd43c_bands'].reshape(-1, 7), data['short_wave'].reshape(-1, 1), faux])

			# get predictions
			mean, deviation, _, _ = self.predict(matrix, instances=instances)

			# create mask for matrix in case of fills or nans in bands
			mask = self._mask(matrix).prod(axis=1).astype(bool)
			mean = numpy.where(mask, mean, self.fill)
			deviation = numpy.where(mask, deviation, self.fill)

			# remove values above synthetic data limits and below 0
			mask = (mean >= self.limits['synthetic'][0]) & (mean <= self.limits['synthetic'][1])
			mean = numpy.where(mask, mean, self.fill)
			deviation = numpy.where(mask, deviation, self.fill)

			# reshape and add to data
			data['gpp_onefluxnet_low'] = mean.reshape(*shape)
			data['onefluxnet_low_deviation'] = deviation.reshape(*shape)

			# add model name
			data['model'] = numpy.array(['{}_{}'.format(self.session, self.tag)])

			# inspect data
			self._view(data)

			# construct destination
			destination = path.replace('TEMPO_CLDO4_L2', folder).replace('.nc', '.h5')
			self.spawn(destination, data)

		return None

	def quiver(self):
		"""Perform mulitple linear regression for all sites and compare using cosine similarity.

		Arguments:
			None

		Returns:
			None
		"""

		# create reports folder
		folder = '{}/reports'.format(self.folder)
		self._make(folder)

		# begin report
		report = ['mulitple linear regression for {} {}'.format(self.tag, self.session)]
		report += ['cosine similarity report']
		report += ['']

		# create training matrix and normalize
		matrix, _, _ = self.materialize(self.training['sites'])
		mean = matrix.mean(axis=0)
		deviation = matrix.std(axis=0)
		matrix = (matrix - mean) / deviation

		# create regressor and fit, getting coefficients
		regressor = LinearRegression()
		regressor.fit(matrix[:, :-1], matrix[:, -1].reshape(-1, 1))
		vector = regressor.coef_

		# begin cosine similaritiy scores
		cosines = []

		# for each site
		for site in self.training['sites']:

			# create training matrix and normalize
			matrixii, _, _ = self.materialize([site])
			matrixii = (matrixii - mean) / deviation

			# if there are smaples
			if matrixii.shape[0] > 2:

				# create regressor and fit, getting coefficients
				regressorii = LinearRegression()
				regressorii.fit(matrixii[:, :-1], matrixii[:, -1].reshape(-1, 1))
				vectorii = regressorii.coef_

				# get cosine similarity measurement
				cosine = cosine_similarity(vector.reshape(1, -1), vectorii.reshape(1, -1))
				cosines.append((site, cosine[0][0]))

		# sort scores
		cosines.sort(key=lambda pair: pair[1])

		# for each one
		for site, cosine in cosines:

			# print score
			report.append(self._print('{}: {}'.format(site, cosine)))

		# dump report
		self._jot(report, '{}/cosine_similarity_site_report'.format(folder))

		return None

	def reflect(self, point=None):
		"""Calculate modis reflectances at a particular location.

		Arguments:
			point: tuple of floats, the latitude and longitude

		Returns:
			None
		"""

		# get model folder and make reports folder
		_, _, folder = self._retrieve()
		folderii = '{}/reports'.format(folder)
		self._make(folderii)

		# default point
		point = point or (self.catalog[self.site]['latitude'], self.catalog[self.site]['longitude'])

		# begin report
		formats = (self.site, self._orient(point[0]), self._orient(point[1], east=True))
		report = ['Albedo comparison for {}, {}, {}'.format(*formats)]

		# for each band
		for band in range(7):

			# create hydra for the data
			hydra = Hydra('{}/MCD43D{}/{}'.format(self.sink, band + 62, self.stub), show=False, extensions=('.h5',))

			# ingest first file
			hydra.ingest(0)

			# grab albedo data
			albedos = hydra.grab('Albedo').squeeze()
			factor = hydra.grab('scale_factor')[0][0]
			albedos = albedos * factor
			shape = albedos.shape

			# construct latitudes, starting at 90N
			size = 180 / shape[0]
			half = int(size / 2)
			latitudes = [90 - half - (index * size) for index in range(shape[0])]

			# construct longitudes, staring at 180W
			size = 360 / shape[1]
			half = int(size / 2)
			longitudes = [-180 + half + (index * size) for index in range(shape[1])]

			# pinpoint the pixel
			index = self._pin(point[0], latitudes)[0][0]
			indexii = self._pin(point[1], longitudes)[0][0]

			# get hydra for inputs
			hydraii = Hydra('Onefluxnet_Inputs')
			hydraii.ingest(self.site)

			# get albedo data
			albedosii = hydraii.grab('mcd43_bands')

			# create mean after filtering for negatives
			mask = (albedosii.sum(axis=1) > 0)
			mean = albedosii[mask].mean(axis=0)

			# print albedo
			albedo = albedos[index, indexii]
			albedoii = mean[band]
			formats = [band + 1, self._orient(point[0]), self._orient(point[1], east=True), self.site]
			formats += [albedo, albedoii]
			report.append(self._print('albedo, band {} at {}, {} vs {}: {}, {}'.format(*formats)))

		# for each line
		for line in report:

			# print line
			self._print(line)

		# jot reports
		formats = (folderii, self.site, self._orient(point[0]), self._orient(point[1], east=True))
		destination = '{}/albedo_report_{}_{}_{}.txt'.format(*formats)
		self._jot(report, destination)

		return None

	def repair(self, scan=None, baseline=False):
		"""Repair input files by adding new fields.

		Arguments:
			baseline: boolean, redo baseline data?

		Returns:
			None
		"""

		# create smoothed data folders
		for folder in ('TEMPO_RAD_Smooth',):

			# create destination folders
			self._make('{}/{}'.format(self.sink, folder))
			self._make('{}/{}/{}'.format(self.sink, folder, self.year))
			self._make('{}/{}/{}/{}'.format(self.sink, folder, self.year, self._pad(self.month)))
			self._make('{}/{}/{}'.format(self.sink, folder, self.stub))

		# get all tempo l1b files
		hydraii = Hydra('{}/TEMPO_RAD_L1/{}'.format(self.sink, self.stub))

		# get all inputs files
		hydra = Hydra('{}/GPP_Inputs/{}'.format(self.sink, self.stub))
		paths = hydra.paths

		# if scan number given
		if scan:

			# create labels and subset paths
			label = self._label(scan)
			paths = [path for path in paths if label in path]

		# for each path
		for path in paths:

			# ingest the path
			hydra.ingest(path)

			# extract data
			data = hydra.extract()

			# get date from path
			search = '[0-9]{8}T[0-9]{6}Z'
			date = re.findall(search, path)[0]

			# get granule from path
			search = 'S[0-9]{3}G[0-9]{2}'
			granule = re.findall(search, path)[0]

			# begin data with time and granule information
			data.update({'date': numpy.array([int(date[0:8])]), 'time': numpy.array([int(date[9:15])])})
			data.update({'scan': numpy.array([int(granule[1:4])]), 'granule': numpy.array([int(granule[5:7])])})

			# get shape
			shape = data['latitude'].shape

			# get irradiance qualifiers
			qualifier = data['max_irr_quality_high']
			qualifierii = data['max_irr_quality_low']

			# if qualifiers are 1-d
			if len(qualifier.shape) == 1:

				# tile qualifiers to match shape
				qualifier = numpy.tile(qualifier, (shape[0], 1))
				qualifierii = numpy.tile(qualifierii, (shape[0], 1))

				# update data
				data.update({'max_irr_quality_high': qualifier, 'max_irr_quality_low': qualifierii})

			# try to
			try:

				# access l1b data
				hydraii.ingest(date)

				# get the seconds since 1-6-1980
				seconds = hydraii.grab('time')

				# get the longitude
				longitude = data['longitude']

				# create local time, and add to data
				time = self._localize(seconds, longitude)
				data.update({'local_time': time})

				# for each band
				for band in (1, 2):

					# for each cardinal
					for cardinal in ('east', 'west'):

						# add abi data
						search = 'ABI_Band_{}_{}'.format(band, cardinal.capitalize())
						data.update({'abi_band{}_{}'.format(band, cardinal): hydraii.grab(search)})

			# unless TEMPOL1B not available
			except IndexError:

				# print status
				self._print('TEMPO_L1_RAD not available')

			# if relative azimuth in keys
			if 'relative_azimuth' in data.keys():

				# replace relative azimuth with angle name
				data['relative_azimuth_angle'] = data['relative_azimuth']
				del data['relative_azimuth']

			# if redoing baseline
			if baseline:

				# redo baseline data
				latitude = hydra.grab('latitude')
				longitude = hydra.grab('longitude')
				baselines, wavelengths = self.baseline(path, latitude, longitude)
				baselines = numpy.where(baselines >= 0, baselines, self.fill)
				data.update({'abi_radiance': baselines, 'abi_wavelengths': wavelengths})

			# add synthetic data
			synthetics = Hydra('{}/Synthetic_GPP/{}'.format(self.sink, self.stub))
			synthetics.ingest(date)
			fields = ['short_wave', 'mcd43d_bands', 'gpp_onefluxnet_high', 'onefluxnet_high_deviation']
			fields += ['mcd43c_bands', 'gpp_onefluxnet_low', 'onefluxnet_low_deviation', 'cloud_fraction_merra']
			data.update({field: synthetics.grab(field) for field in fields})

			# remove old band information
			exclusions = ['mcd43_bands', 'gpp_onefluxnet', 'onefluxnet_deviation']
			data = {field: array for field, array in data.items() if field not in exclusions}

			# add modis bands
			pairs = [(620, 670), (841, 876), (459, 479), (545, 565)]
			pairs += [(1230, 1250), (1628, 1652), (2105, 2155)]
			data.update({'mcd43_wavelengths': numpy.array(pairs)})

			# add mosaic data
			mosaics = Hydra('{}/Mosaic_GPP/{}'.format(self.sink, self.stub))
			mosaics.ingest(date)
			data.update({'gpp_mosaic': mosaics.grab('gpp_mosaic')})

			# fill values
			data = {name: self._fill(array) for name, array in data.items()}

			# try to
			try:

				# get smoothed reflectances
				fields = ('smoothed_reflectances', 'reflectance_wavelengths')
				dataii = {field: data[field] for field in fields}

				# for each field
				for field in fields:

					# excise from data
					del data[field]

				# stash
				self.spawn(path.replace('GPP_Inputs', 'TEMPO_RAD_Smooth'), dataii)

			# unless not found
			except KeyError:

				# pass
				self._print('smoothed reflectances not found')

			# restash file
			self.spawn(path, data)

		return None

	def regress(self):
		"""Perform mulitple linear regression on training set.

		Arguments:
			None

		Returns:
			None
		"""

		# create reports folder
		folder = '{}/reports'.format(self.folder)
		self._make(folder)

		# create training matrix and validation nmatix
		matrix, _, _ = self.materialize(self.training['sites'])
		matrixii, _, _ = self.materialize(self.validation['sites'])

		# create regressor and fit
		regressor = LinearRegression()
		regressor.fit(matrix[:, :-1], matrix[:, -1].reshape(-1, 1))

		# predict on training
		prediction = regressor.predict(matrix[:, :-1])
		score = r2_score(matrix[:, -1].reshape(-1, 1), prediction)
		self._print('training score: {}'.format(score))

		# predict on validation
		predictionii = regressor.predict(matrixii[:, :-1])
		score = r2_score(matrixii[:, -1].reshape(-1, 1), predictionii)
		self._print('validation score: {}'.format(score))

		# print intercept
		self._print('intercept: {}'.format(regressor.intercept_))

		# for each feature
		for feature, slope in zip(self.features[:-1], regressor.coef_):

			# print slope
			self._print('{}: {}'.format(feature, slope))

		return None

	def run(self, predictors, targets, validation):
		"""Define and run the Bayesian model.

		Arguments:
			predictors: numpy array, inputs matrix
			targets: numpy array, outputs vector
			validation: list of numpy array, the validation set
			epochs: number of training epochs

		Returns:
			None
		"""

		# define abbreviations
		optimizers = tensorflow_addons.optimizers
		layers = tensorflow_probability.layers
		backend = tensorflow.keras.backend

		# unpack shape
		samples, features = predictors.shape

		# default sample and validation weights to 1
		weights = numpy.ones(samples)
		weightsii = numpy.ones(validation[0].shape[0])

		# if balancing sample weights
		if self.training['balance']:

			# get weights for balancing
			weights, weightsii = self._balance(predictors, validation[0])

		# # add weighs to validation
		# validation += [weightsii]

		# define batch size and kernel weight
		batch = self.training['batch']
		weight = batch / samples

		# if not using weights
		if not self.architecture['weight']:

			# remove weight
			weight = 0

		# set up bayesian layers
		prior = self._prioritize
		posterior = self._post

		# set activation functions if not plain strings
		activations = {'leaky': tensorflow.keras.layers.LeakyReLU(alpha=0.3)}
		activations.update({'softsign': tensorflow.keras.activations.softsign})
		activations.update({'bent': self._bend})

		# set up beginning input layer
		input = tensorflow.keras.Input(shape=(features))
		output = input

		# set regularizer
		regularization = tensorflow.keras.regularizers.L2(float(self.architecture['regularization']))

		# for each hidden layer
		for hiddens, activation in zip(self.architecture['hiddens'], self.architecture['activations']):

			# set activation
			activation = activations.get(activation, activation)

			# if using variational
			if self.architecture['bayesian']:

				# add variational layer
				options = {'kl_weight': weight, 'activation': activation}
				output = layers.DenseVariational(hiddens, posterior, prior, **options)(output)

			# otherwise
			else:

				# add normal dense layer
				options = {'activation': activation, 'kernel_regularizer': regularization}
				output = tensorflow.keras.layers.Dense(hiddens, **options)(output)

			# if dropping out
			if self.architecture['dropout'] > 0:

				# add dropout layer
				output = tensorflow.keras.layers.Dropout(rate=self.architecture['dropout'])(output)

		# if bayesian
		if self.architecture['bayesian']:

			# set activation
			activation = activations.get(self.architecture['final'], self.architecture['final'])

			# add final mean, deviation layers
			output = tensorflow.keras.layers.Dense(units=2, activation=activation)(output)
			output = layers.IndependentNormal(1)(output)

		# otherwise
		else:

			# set activation
			activation = activations.get(self.architecture['final'], self.architecture['final'])

			# add normal output
			output = tensorflow.keras.layers.Dense(units=1, activation=activation)(output)

		# if using cyclic learning rate
		if self.training['cyclic']:

			# define cyclical learning rate
			def scaling(timepoint): return 1 / (2 ** (timepoint - 1))
			learning = self.training['learning']
			options = {'initial_learning_rate': float(learning[0])}
			options.update({'maximal_learning_rate': float(learning[1])})
			options.update({'scale_fn': scaling, 'step_size': 5 * math.ceil(samples / batch)})
			learning = optimizers.CyclicalLearningRate(**options)

		# otherwise
		else:

			# use constant learning rate from initial
			learning = float(self.training['learning'][0])

		# set loss functions
		losses = {'custom': self._lose, 'bayes': self._like}
		loss = losses.get(self.training['loss'], self.training['loss'])

		# if smoothing pentaly
		if self.training['smoothing'] > 0:

			# set loss to smooth
			loss = self._smooth

		# begin learning rate accumulation
		rates = []

		# crate callback to log and store learning rates
		monitoring = lambda batch, logs: rates.append(backend.get_value(model.optimizer.learning_rate))
		monitor = tensorflow.keras.callbacks.LambdaCallback(on_batch_end=monitoring)

		# build model
		epochs = self.training['epochs']
		metrics = ['mae', 'mse', 'acc', 'mape']
		model = tensorflow.keras.Model(input, output, name="GPP")
		self.model = model
		model.compile(loss=loss, optimizer=tensorflow.optimizers.Adam(learning), metrics=metrics)

		# train model
		options = {'epochs': epochs, 'batch_size': batch, 'validation_data': validation}
		options.update({'callbacks': [monitor], 'shuffle': True, 'sample_weight': weights})
		history = model.fit(predictors, targets, **options)

		# save model
		model.save('{}/Onefluxnet_Model'.format(self.folder))

		# clear session
		tensorflow.keras.backend.clear_session()

		# store history
		self._dump(history.history, '{}/history.json'.format(self.folder))

		# store learning rates
		rates = numpy.array(rates).astype('float32').tolist()
		learn = {'rates': rates, 'samples': samples}
		self._dump(learn, '{}/rates.json'.format(self.folder))

		# analyze history
		self.chronicle()

		return None

	def score(self):
		"""Produce list of r2 scores for all sites.

		Arguments:
			None

		Returns:
			None
		"""

		# get all input files
		hydra = Hydra('{}/Onefluxnet_Inputs'.format(self.sink))

		# begin reservoirs
		names = []
		scores = []
		latitudes = []
		longitudes = []

		# for each path
		for path in hydra.paths:

			# ingest and get data
			hydra.ingest(path)
			names.append(self._locate(path))
			scores.append(hydra.grab('r2_score')[0][0])
			latitudes.append(hydra.grab('latitude')[0][0])
			longitudes.append(hydra.grab('longitude')[0][0])

		# create list
		report = ['Onefluxnet Site Scores']

		# for each set
		for name, score, latitude, longitude in zip(names, scores, latitudes, longitudes):

			# create formats
			formats = (name, self._orient(latitude), self._orient(longitude, east=True), self._round(score, 3))
			line = '{} ( {}, {} ): {}'.format(*formats)
			report.append(line)

		# create report
		destination = '{}/Onefluxnet/scores/Onefluxnet_site_scores.txt'.format(self.sink)
		self._jot(report, destination)

		return None

	def sense(self, instances=50, isolate=False):
		"""Perform sensitivity study, by removal of each feature.

		Arguments:
			instances: int, number of instances for model predictions
			isolate: perfrom isolation study?

		Returns:
			None
		"""

		# set mode
		mode = 'sensitivity'

		# if isolating
		if isolate:

			# set mode to isolation
			mode = 'isolation'

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make reports folder
		self._make('{}/reports'.format(folder))

		# get training matrix for particular scan
		features = details['features']
		parameters = ([(self.site, self.site)],)
		matrix, features, _, data, constraints = self.materialize(*parameters, features=features)

		# get scaling information
		minimum = details['input_min']
		maximum = details['input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		truth = matrix[:, -1]
		inputs = (matrix[:, :-1] - minimum) / scale

		# begin report and scores collection
		report = ['{} report for {}\n'.format(mode.capitalize(), folder)]
		scores = []

		# rename last feature from target to all predictors
		features[-1] = 'all predictors'

		# for each feature
		for feature in range(inputs.shape[1] + 1):

			# copy inputs
			xerox = inputs.copy()

			# if feature is valid
			if feature < inputs.shape[1] - 1:

				# if mode is isolation
				if isolate:

					# replace all features but index with median
					xerox[:, :] = numpy.array([numpy.percentile(inputs, 50, axis=0)] * inputs.shape[0])
					xerox[:, feature] = inputs[:, feature]

				# otherwise
				else:

					# replace feature index with median
					xerox[:, feature] = numpy.percentile(inputs[:, feature], 50)

			# apply pca coefficients
			coefficients = details['pca_coefficients']
			xerox = numpy.dot(xerox, coefficients).astype('float32')

			# get predictions for each instance
			predictions = []
			for instance in range(instances):

				# get predictions
				prediction = model(xerox)
				prediction = prediction.numpy().squeeze()
				predictions.append(prediction)

			# create array
			predictions = numpy.array(predictions)

			# apply scaling to predictions
			minimumii = details['output_min']
			maximumii = details['output_max']
			scaleii = maximumii - minimumii
			predictions = (predictions * scaleii) + minimumii

			# take mean and stdev of predictions
			mean = numpy.array(predictions).mean(axis=0)

			# print r^2 score
			score = r2_score(mean, truth)
			pair = (features[feature], score)
			self._print('{}: {}'.format(*pair))

			# add scores
			scores.append(pair)

		# sort scores, reversing for isolation
		scores.sort(key=lambda pair: pair[1], reverse=isolate)

		# for each score
		for pair in scores:

			# add to report
			report.append('{}: {}'.format(*pair))

		# jot to reports folder
		self._jot(report, '{}/reports/{}.txt'.format(folder, mode))

		return None

	def sow(self, days=None, scans=(10, 11), granules=(1, 2)):
		"""Gather up training data into a matrix, and run a random forest.

		Arguments:
			days: list of (int, int) tuples, the month and day of training.
			scans: tuple of ints, the scans to use
			granules: tuple 0f ints, the granules bracket

		Returns:
			None
		"""

		# get current date
		today = datetime.datetime.today().date()
		date = '{}m{}{}'.format(today.year, self._pad(today.month), self._pad(today.day))

		# create folder
		folder = '{}/GPP_Models/GPP_NN_{}_{}'.format(self.sink, date, self.tag)
		self._make(folder)
		self._make('{}/reportw'.format(folder))

		# construct training days
		days = days or [(5, day) for day in (19, 20, 21, 27, 28)] + [(6, day) for day in (16, 21, 22, 24, 25)]

		# construct matrix
		matrix, _ = self.materialize(days, scans, granules)

		# set fields
		fields = ['reflectance_pca_{}'.format(self._pad(number)) for number in range(30)]
		fields += ['abi_wavelength_{}'.format(self._pad(number + 1)) for number in range(16)]
		fields += ['par', 'zenith', 'cloud', 'gpp']

		# create data
		data = {field: matrix[:, index] for index, field in enumerate(fields)}

		# create report destination and plant tree
		self.plant(data, 'gpp', '{}/reports/gpp_random_forest.txt'.format(folder))

		return None

	def spot(self):
		"""Create map of onefluxnet sample spots.

		Arguments:
			None

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folder
		folderii = '{}/spots'.format(folder)
		self._make(folderii)

		# get list of scores
		paths = self._see('Onefluxnet/scores')
		text = self._know(paths[0])

		# get scores and points
		sites = [line.split(':')[0].split()[0].strip() for line in text[1:]]
		scores = numpy.array([float(line.split(':')[-1]) for line in text[1:]])
		pairs = [(site, score) for site, score in zip(sites, scores)]

		# get study sites
		study = self._know('Onefluxnet/sites/study_sites.txt')

		# prune sites
		pairs = [pair for pair in pairs if pair[0] in study]
		sites = [pair[0] for pair in pairs]
		scores = numpy.array([pair[1] for pair in pairs])

		# get latitudes and longitudes
		latitude = numpy.array([self.catalog[site]['latitude'] for site in sites])
		longitude = numpy.array([self.catalog[site]['longitude'] for site in sites])

		# begin status
		destination = '{}/Site_map.png'.format(folderii)
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase preevious copy
		self._clean(destination, force=True)

		# begin graphs
		graphs = []

		# make blank
		blank = numpy.array([0])

		# construct Onefluxnet site map
		title = 'Onefluxnet sites\n'
		unit = '$R^2$'
		labels = ['', '']

		# construct annotations
		zipper = zip(sites, longitude, latitude)
		annotations = [(site, meridian, parallel) for site, meridian, parallel in zipper]

		# construct difference graph
		bounds = [(-125, -70), (25, 60)]
		scale = (0.5, 1.0)
		line = (longitude, latitude, 'bx')
		lines = [line]
		parameters = (longitude, latitude, scores, bounds, scale, 'plasma', title, unit, labels)
		options = {'globe': True, 'polygons': False, 'colorbar': True, 'times': True, 'marker': 5, 'pixel': 80}
		options.update({'annotations': annotations})
		graph = self._graph(*parameters, **options)
		graphs.append(graph)

		# create plot
		space = {'top': 0.92, 'bottom': 0.18, 'left': 0.08, 'right': 0.92}
		self._paint(destination, graphs, size=(8, 8), gap=0.08, space=space)

		# remove annotations
		destination = destination.replace('.png', '_unannotated.png')
		graphs[0]['annotations'] = None
		self._paint(destination, graphs, size=(8, 8), gap=0.08, space=space)

		return None

	def statisticize(self):
		"""Compare site model performace statistics with previous analysis.

		Arguments:
			None

		Returns:
			None
		"""

		# set folder
		folder = '{}/statistics'.format(self.folder)
		self._make(folder)

		# grab statistics data
		statistics = self._load('{}/statistics.json'.format(folder))
		statisticsii = self._load('Onefluxnet/sites/statistics.json')

		# get sites in both
		sites = list(statistics.keys())
		sitesii = list(statisticsii.keys())
		common = list(set(sites) & set(sitesii))

		# set variables
		variables = ['error', 'bias', 'ratio', 'score', 'absolute']
		stubs = {'samples': 'samples', 'error': '$RMSE$', 'bias': 'bias', 'ratio': '$RMSE/abs(bias)$'}
		stubs.update({'score': '$R^2$', 'absolute': 'abs(bias)'})

		# for each variable
		for variable in variables:

			# get results
			results = numpy.array([statistics[site][variable] for site in common])
			resultsii = numpy.array([statisticsii[site][variable] for site in common])

			# calculate score, slope and intercepts
			score = r2_score(resultsii, results)
			slope, intercept, _, _, _ = scipy.stats.linregress(resultsii, results)

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8,8))

			# label plot
			title = 'Onefluxnet model site statistics\nTensorflow vs IDL {}'.format(stubs[variable])
			pyplot.title(title)
			pyplot.xlabel('IDL {}'.format(stubs[variable]))
			pyplot.ylabel('tensorflow {}'.format(stubs[variable]))

			# get minimums and maximums
			minimum = min([results.min(), resultsii.min()])
			maximum = max([results.max(), resultsii.max()])

			# add buffer
			minimum = minimum - 0.1 * abs(minimum)
			maximum = maximum + 0.1 * abs(maximum)

			# set scales
			pyplot.xlim(minimum, maximum)
			pyplot.ylim(minimum, maximum)

			# plot 1:1 line
			pyplot.plot([minimum, maximum], [minimum, maximum], 'k-')

			# plot regression line
			points = [intercept + slope * point for point in [minimum, maximum]]
			pyplot.plot([minimum, maximum], points, 'k--')

			# plot scatter plot
			pyplot.plot(resultsii, results, 'bx')

			# save
			destination = '{}/scatter_{}.png'.format(folder, variable)
			pyplot.savefig(destination)
			pyplot.clf()

			# print status
			self._print('plotted {}'.format(destination))

			# get results
			results = numpy.array([statistics[site][variable] for site in sites])
			resultsii = numpy.array([statisticsii[site][variable] for site in sitesii])

			# create a histogram
			pyplot.clf()
			pyplot.figure(figsize=(8, 8))
			bins = 50
			options = {'bins': bins, 'edgecolor': 'black', 'label': ['previous', 'new'], 'color': ['blue', 'green']}
			pyplot.hist([resultsii, results], **options)

			# add legend
			pyplot.legend(loc='upper right')

			# Add titles and labels
			title = 'histogram of {}, IDL vs tensorflow model'.format(stubs[variable])
			pyplot.title(title)
			pyplot.xlabel('value')
			pyplot.ylabel('counts')

			# save the plot
			destination = '{}/histogram_{}.png'.format(folder, variable)
			pyplot.savefig(destination)
			pyplot.clf()

			# print status
			self._print('plotted {}'.format(destination))

		return None

	def streak(self, scans=(8, 9), bins=200, number=15):
		"""Check for streaks in histogram of a synthetic file.

		Arguments:
			scans: tuple of int, scan number bracket
			bins: number of histogram bins
			number: number of bins to plot

		Returns:
			None
		"""

		# make folder
		folder = '{}/streaks'.format(self.folder)
		self._make(folder)

		# get the synthetic files
		hydra = Hydra('{}/Synthetic_GPP/{}'.format(self.sink, self.stub))

		# get scan label
		labels = [self._label(scan) for scan in range(*scans)]
		paths = [path for path in hydra.paths if any([label in path for label in labels])]

		# begin synthetic data collection
		synthetics = []
		latitudes = []
		longitudes = []

		# for each path
		for path in paths:

			# get the synthetic data
			hydra.ingest(path)
			synthetic = hydra.grab('gpp_onefluxnet')
			synthetics.append(synthetic)

			# add the coordinates
			latitude = hydra.grab('latitude')
			latitudes.append(latitude)
			longitude = hydra.grab('longitude')
			longitudes.append(longitude)

		# create arrays
		synthetics = numpy.vstack(synthetics)
		latitudes = numpy.vstack(latitudes)
		longitudes = numpy.vstack(longitudes)

		# filter out invalids
		mask = self._mask(synthetics) & self._mask(latitudes) & self._mask(longitudes)
		synthetics = synthetics[mask]
		latitudes = latitudes[mask]
		longitudes = longitudes[mask]

		# Create a histogram
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))
		options = {'bins': bins, 'edgecolor': 'black', 'linewidth': 0.5}
		counts, edges, _ = pyplot.hist(synthetics, **options)

		# Add titles and labels
		title = 'histogram of synethetic data {}, {}'.format(self.date, scans)
		pyplot.title(title)
		pyplot.xlabel('value')
		pyplot.ylabel('counts')

		# save the plot
		destination = '{}/histogram_streaks_{}.png'.format(folder, self.date)
		pyplot.savefig(destination)
		pyplot.clf()

		# for each number
		for index in range(1, number + 1):

			# get the highest count
			highest = numpy.argsort(counts)[-index]

			# get the coordinates of the highest count
			mask = (synthetics >= edges[highest]) & (synthetics <= edges[highest + 1])
			latitudesii = latitudes[mask]
			longitudesii = longitudes[mask]

			# Create a plot
			pyplot.clf()
			pyplot.figure(figsize=(8, 8))

			# Add titles and labels
			formats = (self.date, scans, counts[highest], edges[highest], edges[highest + 1])
			title = 'locations of highest counts, {}, {}\n{}, {:.2f}, {:.2f}'.format(*formats)
			pyplot.title(title)
			pyplot.xlabel('longitude')
			pyplot.ylabel('latitude')

			# plot
			pyplot.plot(longitudesii, latitudesii, 'b.', ms=0.5)

			# save the plot
			destination = '{}/map_streaks_{}_{}.png'.format(folder, self.date, self._pad(index))
			pyplot.savefig(destination)
			pyplot.clf()

		return None

	def survey(self, sites=None, variables=['fluxnet_gpp']):
		"""Survey the modis band types using PCA clustering

		Arguments:
			sites: list of str, the site names
			variable: str, variable name for color scale

		Returns:
			None
		"""

		# make folder
		folder = '{}/survey'.format(self.folder)
		self._make(folder)

		# set default sites
		sites = sites or [self.site]

		# collect band information
		collection = self._band(sites)

		# grab the training matrix
		matrix, data, constraints = self.materialize(self.training['sites'])
		data = self._constrain(data, constraints)

		# keep only the 7 modis bands
		matrix = matrix[:, :7]

		# normalize
		mean = matrix.mean(axis=0)
		deviation = matrix.std(axis=0)
		matrix = (matrix - mean) / deviation

		# perform 2-d PCA
		decomposer = PCA(n_components=2)
		flat = decomposer.fit_transform(matrix)
		vectors = decomposer.components_
		ratio = decomposer.explained_variance_ratio_

		# for each site
		for site in sites:

			# and each variable
			for variable in variables:

				# get intensity for color map
				intensity = data[variable].squeeze()
				gradient = 'plasma'

				# order scatter by intensity
				order = numpy.argsort(intensity)
				intensity = intensity[order]
				scatter = flat[order]

				# mask all data with particular site
				mask = (data['site'] == site.encode('utf-8')).squeeze()
				trail = flat[mask]

				# begin masks for years in particular month
				masks = []
				for year in range(data['year'][mask].min(), data['year'][mask].max() + 1):

					# get all july measurements
					maskii = (data['year'][mask] == year) & (data['month'][mask] == self.month)
					masks.append((year, maskii.squeeze()))

				# sort by number of samples, and get top
				masks.sort(key=lambda pair: pair[1].sum(), reverse=True)
				year, maskii = masks[0]

				# get trail subset of year, moth with most data
				trailii = trail[maskii]

				# convert modern modis
				modern =  collection[site]['albedo']
				modern = (modern - mean) / deviation
				point = decomposer.transform(modern)

				# add point to trail
				trail = numpy.vstack([trail, point])

				# begin plot
				pyplot.clf()
				pyplot.figure(figsize=(8, 8))

				# set labels
				pyplot.title('{} {} MODIS 7 band PCA ( {} )'.format(site, self.date, ratio))
				pyplot.xlabel(' '.join(['{:.2f}'.format(band) for band in vectors[0]]))
				pyplot.ylabel(' '.join(['{:.2f}'.format(band) for band in vectors[1]]))

				# plot data
				pyplot.scatter(scatter[:, 0], scatter[:, 1], c=intensity, cmap=gradient, s=0.2, alpha=0.5)

				# plot the trail
				pyplot.plot(trail[:, 0], trail[:, 1], 'go-', markersize=3)
				pyplot.plot(trailii[:, 0], trailii[:, 1], 'co-', markersize=3)
				pyplot.plot(trail[-1:, 0], trail[-1:, 1], 'rX', markersize=20)

				# add colorbar
				pyplot.colorbar(label=variable)

				# save figure
				formats = (folder, site, variable, self.date, self.session, self.tag)
				destination = '{}/survey_{}_{}_{}_{}_{}.png'.format(*formats)
				pyplot.savefig(destination)
				pyplot.clf()

				# print status
				self._print('plotted {}'.format(destination))

		return None

	def train(self):
		"""Gather up training data into a matrix, and run neural network.

		Arguments:
			None

		Returns:
			None
		"""

		# clear models
		self.model = None
		self.details = None
		self.folder = None
		self.session = None

		# read in features from yaml
		self._feature()

		# get current date
		today = datetime.datetime.today().date()
		date = '{}m{}{}'.format(today.year, self._pad(today.month), self._pad(today.day))

		# create folder
		folder = '{}/Onefluxnet_Models/Onefluxnet_NN_{}_{}'.format(self.sink, date, self.tag)
		self._make(folder)
		self.folder = folder

		# begin data details
		data = {'training/{}'.format(field): numpy.array(datum) for field, datum in self.training.items()}
		data.update({'validation/{}'.format(field): numpy.array(datum) for field, datum in self.validation.items()})
		data.update({'architecture/{}'.format(field): numpy.array(datum) for field, datum in self.architecture.items()})
		data.update({'limits/{}'.format(field): numpy.array(datum) for field, datum in self.limits.items()})

		# construct matrix
		sites = self.training['sites']
		matrix, _, _ = self.materialize(sites)

		# get the minimums and maximums
		minimum = matrix.min(axis=0)
		maximum = matrix.max(axis=0)
		mean = matrix.mean(axis=0)
		deviation = matrix.std(axis=0)

		# add data for scaling factors
		data.update({'scaling/input_min': minimum[:-1], 'scaling/input_max': maximum[:-1]})
		data.update({'scaling/output_min': minimum[-1], 'scaling/output_max': maximum[-1]})
		data.update({'scaling/input_mean': mean[:-1], 'scaling/input_std': deviation[:-1]})
		data.update({'scaling/output_mean': mean[-1], 'scaling/output_std': deviation[-1]})

		# add features list
		data.update({'features': numpy.array(self.features)})

		# if normalizing
		if self.training['normalize']:

			# apply normalization to traiing matrix
			matrix = (matrix - mean) / deviation

		# otherwise
		else:

			# apply scaling to training matrix
			scale = maximum - minimum
			matrix = (matrix - minimum) / scale

		# fit PCA on inputs and get coefficients, and transpose them
		decomposer = PCA(n_components=matrix.shape[1] - 1)
		decomposer.fit(matrix[:, :-1])
		coefficients = decomposer.components_.transpose(1, 0)

		# add to data
		data.update({'scaling/pca_coefficients': coefficients})

		# separate into predictors and targets
		targets = matrix[:, -1]
		predictors = matrix[:, :-1]

		# if using pca decomposition
		if self.training['decompose']:

			# apply coefficients
			predictors = numpy.dot(predictors, coefficients)

		# if randomly sampling
		if self.training['random']:

			# randomly sample indices
			size = int(self.training['fraction'] * predictors.shape[0])
			indices = numpy.random.randint(0, predictors.shape[0], size=(size,))

		# otherwise
		else:

			# take samples evenly spread
			increment = int(1 / self.training['fraction'])
			indices = [position * increment for position in range(predictors.shape[0])]
			indices = numpy.array([index for index in indices if index < predictors.shape[0]])

		# subset based on indices
		targets = targets[indices]
		predictors = predictors[indices]

		# add indices to data
		data.update({'samples/selected_indices': indices})

		# construct validation matrix
		sitesii = self.validation['sites']
		matrixii, _, _ = self.materialize(sitesii)

		# if normalizing
		if self.training['normalize']:

			# apply normalization to validation matrix
			matrixii = (matrixii - mean) / deviation

		# otherwise
		else:

			# apply scaling to training matrix
			scale = maximum - minimum
			matrixii = (matrixii - minimum) / scale

		# separate into predictors and targets
		targetsii = matrixii[:, -1]
		predictorsii = matrixii[:, :-1]

		# if using pca decomposition
		if self.training['decompose']:

			# apply coefficients
			predictorsii = numpy.dot(predictorsii, coefficients)

		# if randomly sampling
		if self.validation['random']:

			# randomly sample indices
			size = int(self.validation['fraction'] * predictorsii.shape[0])
			indicesii = numpy.random.randint(0, predictorsii.shape[0], size=(size,))

		# otherwise
		else:

			# take samples evenly spread
			increment = int(1 / self.validation['fraction'])
			indicesii = [position * increment + 1 for position in range(predictorsii.shape[0])]
			indicesii = numpy.array([index for index in indicesii if index < predictorsii.shape[0]])

		# subset based on indices
		targetsii = targetsii[indicesii]
		predictorsii = predictorsii[indicesii]
		validation = [predictorsii, targetsii]

		# stash
		destination = '{}/Model_Scaling.h5'.format(folder)
		self.spawn(destination, data)

		# run the model
		self._print('matrix: {}'.format(predictors.shape))
		self.run(predictors, targets, validation)

		# reload the mode
		self._retrieve()

		return None

	def validate(self, instances=50, apply=False, verify=True, present=False, platform=None, large=False):
		"""Produce plots of one scan to validate the NN model.

		Arguments:
			scan: int, scan number
			instances: int, number of instances for model predictions
			apply: boolean, apply selected indices?
			verify: boolean, make verifiation plots?
			present: boolean, make presentation plots?
			large: boolean, make large scale plots?

		Returns:
			None
		"""

		# set default platform
		platform = platform or 'Onefluxnet'

		# make folders
		folder = '{}/validation'.format(self.folder)
		folderii = '{}/presentation'.format(self.folder)
		self._make(folder)
		self._make(folderii)

		# get training matrix for particular site
		matrix, data, constraints = self.materialize([self.site])

		# construct valid data
		valids = self._constrain(data, constraints)

		# if applying sampled indices:
		if apply:

			# get selected indices
			indices = self.details['samples/selected_indices']

			# get fields with same shape as truth and apply indices
			fields = [field for field, array in valids.items() if array.shape[0] == matrix[:, -1].shape[0]]
			valids.update({field: valids[field][indices] for field in fields})

			# apply indices to inputs and truth
			matrix = matrix[indices]

		# make predictions
		prediction, deviation, truth, score = self.predict(matrix)

		# print r^2 score
		self._print('score: {}'.format(score))

		# create statistics json
		folderii = '{}/statistics'.format(self.folder)
		self._make(folderii)
		destinationii = '{}/statistics.json'.format(folderii)
		statistics = self._load(destinationii)

		# check for prediction length
		if prediction.shape[0] > 0:

			# calculate regression line
			slope, intercept, _, _, _ = scipy.stats.linregress(truth, prediction)

			# calculate bias and mean error
			error = root_mean_squared_error(truth, prediction)
			bias = (prediction - truth).mean()
			ratio = error / abs(bias)

			# add to statistics file
			information = {'score': score, 'error': error, 'bias': bias, 'ratio': ratio, 'samples': truth.shape[0]}
			information.update({'slope': slope, 'intercept': intercept, 'absolute': abs(bias)})
			information = {field: float(quantity) for field, quantity in information.items()}
			statistics[self.site] = information
			self._dump(statistics, destinationii)

			# if making verification plot
			if verify:

				# create plot destination
				formats = (folder, self.site, self.session, self.tag, instances)
				destination = '{}/Onefluxnet_verify_{}_{}_{}_{}.png'.format(*formats)

				# if site in validation set
				if self.site in self.validation['sites']:

					# replace destination with validation
					destination = destination.replace('verify', 'validate')

				# verify with plot
				parameters = (destination, truth, prediction, deviation, valids)
				options = {'platform': platform, 'large': large}
				self.verify(*parameters, **options)

			# if making presentation plots
			if present:

				# create plot destination
				formats = (self.folder, self.site, self.instances)
				destination = '{}/presentation/{}_GPP_hourly_July_{}.png'.format(*formats)

				# verify with plot
				parameters = (destination, truth, prediction, deviation, valids, data, self.instances)
				self.present(*parameters, chart=True)

		return None

	def verify(self, destination, truth, prediction, deviation, data, margin=1, platform=None, large=False):
		"""Plot a validation sample.

		Arguments:
			destination: str, destination filepath
			truth: ground truth observations
			prediction: NN predictions
			deviation: NN deviation
			data: dict of numpy arrays
			margin: int, percentile margin
			platform: str, the platform name
			large: boolean, make large scale plots?

		Returns:
			None
		"""

		# set default platform
		platform = platform or 'Onefluxnet'

		# begin status
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase preevious copy
		self._clean(destination, force=True)

		# load statistics json
		statistics = self._load('{}/statistics/statistics.json'.format(self.folder))

		# retrieve statistics
		score = statistics[self.site]['score']
		error = statistics[self.site]['error']
		bias = statistics[self.site]['bias']
		slope = statistics[self.site]['slope']
		intercept = statistics[self.site]['intercept']

		# calculate differences
		difference = prediction - truth

		# perform binning
		bracket = self.limits['scale']
		samples = 100
		mesh, meshii, counts = self._bin(truth, prediction, samples, bracket)

		# get minimum and maximum tracer values
		minimum = min([truth.min(), prediction.min()])
		maximum = max([truth.max(), prediction.max()])

		# set percent bracket
		absolute = max([abs(numpy.percentile(difference, margin)), abs(numpy.percentile(difference, 100 - margin))])

		# set bounds
		bounds = [(-125, -60), (15, 55)]
		scale = (minimum, maximum)
		scaleii = (-absolute, absolute)

		# construct times
		years = data['year'].squeeze()
		months = data['month'].squeeze()
		days = data['day'].squeeze()
		hours = data['hour'].squeeze()
		minutes = data['minute'].squeeze()
		zipper = zip(years, months, days, hours, minutes)
		times = [datetime.datetime(year, month, day, hour, minute) for year, month, day, hour, minute in zipper]
		# times = [time.timestamp() for time in times]

		# begin graphs
		graphs = []

		# make blank
		blank = numpy.array([0])

		# construct GPP truth plot
		formats = [self.target, platform, self.site, self._orient(self.catalog[self.site]['latitude'])]
		formats += [self._orient(self.catalog[self.site]['longitude'], east=True)]
		title = '{} {}\n {} ( {} {} )'.format(*formats)
		unit = self.unit
		labels = ['time', '{} {}'.format(self.target, self.unit)]

		# construct difference graph
		boundsii = [(times[0], times[-1]), self.limits['scale']]
		scaleiii = (1, 100)
		line = (times, truth, 'bx')
		lines = [line]
		options = {'globe': False, 'lines': lines, 'polygons': False, 'colorbar': False, 'times': True, 'marker': 5}
		graph = self._graph(blank, blank, blank, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
		graphs.append(graph)

		# construct GPP prediction plot
		formats = [self.target, self.site, self._orient(self.catalog[self.site]['latitude'])]
		formats += [self._orient(self.catalog[self.site]['longitude'], east=True)]
		title = '{} Prediction\n {} ( {} {} )'.format(*formats)
		unit = self.unit
		labels = ['time', '{} {}'.format(self.target, self.unit)]

		# construct difference graph
		boundsii = [(times[0], times[-1]), self.limits['scale']]
		scaleiii = (1, 100)
		lineii = (times, prediction, 'gx')
		lineiii = (times, prediction + deviation, 'g--')
		lineiv = (times, prediction - deviation, 'g--')
		# lines = [line, lineii, lineiii, lineiv]
		# lines = [line, lineii]
		lines = [lineii]
		options = {'globe': False, 'lines': lines, 'polygons': False, 'colorbar': False, 'times': True, 'marker': 5}
		graph = self._graph(blank, blank, blank, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
		graphs.append(graph)

		# construct samples title depending on instances
		formats = (platform, score, error, bias)
		title = 'Prediction vs {} ( 1 instance )\n( $R^2$ = {:.3f}, $RMSE$ = {:.3f}, bias = {:.3f} )'.format(*formats)
		if self.instances > 1:

			# add instances
			title = 'Prediction vs {} ( avg of {} instances )\n'.format(platform, self.instances)
			title += '( $R^2$ = {:.3f}, $RMSE$ = {:.3f}, bias = {:.3f} )'.format(score, error, bias)

		# construct samples graph
		unit = 'samples'
		labels = ['{} truth'.format(self.target), '{} prediction'.format(self.target)]
		boundsii = [self.limits['scale'], self.limits['scale']]
		scaleiii = (1, 10)
		points = self.limits['scale']
		regression = [intercept + point * slope for point in points]
		line = (points, points, 'k-')
		lineii = (points, regression, 'k--')
		lines = [line, lineii]
		# lines = [line]
		options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 20, 'polygons': False}
		graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
		graphs.append(graph)

		# construct GPP truth plot
		formats = [self.target, platform, self.site, self._orient(self.catalog[self.site]['latitude'])]
		formats += [self._orient(self.catalog[self.site]['longitude'], east=True)]
		title = '{} Prediction - {}\n {} ( {} {} )'.format(*formats)
		unit = self.unit
		labels = ['time', '{} {}'.format(self.target, self.unit)]

		# construct difference graph
		boundsii = [(times[0], times[-1]), self.limits['scale']]
		boundsii = [(times[0], times[-1]), (-absolute, absolute)]
		scaleiii = (1, 100)
		line = (times, truth, 'bx')
		line = (times, difference, 'cx')
		lineii = (times, prediction, 'gx')
		lineiii = (times, prediction + deviation, 'g--')
		lineiv = (times, prediction - deviation, 'g--')
		lines = [line, lineii, lineiii, lineiv]
		lines = [line, lineii]
		lines = [line]
		options = {'globe': False, 'lines': lines, 'polygons': False, 'colorbar': False, 'times': True, 'marker': 5}
		graph = self._graph(blank, blank, blank, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
		graphs.append(graph)

		# create plot
		space = {'top': 0.92, 'bottom': 0.12, 'wspace': 0.36, 'right': 0.96}
		self._paint(destination, graphs, space=space, gap=0.08)

		# if large plots:
		if large:

			# for each graph
			for index, graph in enumerate(graphs):

				# plot each one
				self._paint(destination.replace('.png', '_{}.png'.format(index)), [graph], size=(16, 16))

		return None

	def visualize(self, scan=10):
		"""Visualize the dataset with PCA.

		Arguments:
			scan: int, scan number

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make shapely folder
		self._make('{}/visualization'.format(folder))

		# get training matrix for particular scan
		features = details['features']
		matrix, features, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1), features=features)

		# apply constraints
		data = self._constrain(data, constraints)

		# get scaling information
		minimum = details['input_min']
		maximum = details['input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		truth = matrix[:, -1]
		inputs = (matrix[:, :-1] - minimum) / scale

		# fit PCA on inputs and get coefficients
		decomposer = PCA(n_components=2)
		decomposer.fit(inputs)
		coefficients = decomposer.components_.transpose(1, 0)

		# get representation
		representation = numpy.dot(inputs, coefficients)

		# set heatmap variables
		variables = ['gpp_fluxsat', 'solar_zenith_angle', 'cloud_fraction']
		variables += ['land_type', 'latitude', 'longitude', 'clearsky_par']
		variables += ['viewing_zenith_angle']

		# for each variable
		for variable in variables:

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8,8))

			# Sample data
			abscissa = representation[:, 0]
			ordinate = representation[:, 1]
			colors = data[variable]

			# create scatter plot
			pyplot.scatter(abscissa, ordinate, c=colors, cmap='viridis', s=0.05)  # Use 'c' for colors and 'cmap' for colormap
			pyplot.colorbar(label=variable)  # Add a colorbar to indicate the scale
			pyplot.xlabel('PCA 0')
			pyplot.ylabel('PCA 1')
			pyplot.title('PCA components vs {}'.format(variable))

			# save
			pyplot.savefig('{}/visualization/pca_{}.png'.format(folder, variable))
			pyplot.clf()

		return None

	def window(self, sites=None, radius=0.1):
		"""Examine overlap of modis and merra data with tempo pixels.

		Arguments:
			sites: list of str, site names
			radius: float, bounds of window in degrees

		Returns:
			None
		"""

		# make data file folders
		folderii = '{}/Onefluxnet_Window'.format(self.sink)
		self._make(folderii)
		self._make('{}/{}'.format(folderii, self.year))
		self._make('{}/{}/{}'.format(folderii, self.year, self._pad(self.month)))
		self._make('{}/{}'.format(folderii, self.stub))

		# set default sites
		sites = sites or self.training['sites']

		# expand radius for extra coverage
		radius = radius * 1.5

		# collect band information
		collection = self._band(sites, radius=radius)
		collectionii = self._band(sites, radius=radius, high=False)

		# for each site
		for site in sites:

			# get the geocoordinates
			latitude = self.catalog[site]['latitude']
			longitude = self.catalog[site]['longitude']

			# get the albedo and short wave info
			wave = collection[site]['wave']
			waveii = collectionii[site]['wave']
			albedo = collection[site]['albedo']
			albedoii = collectionii[site]['albedo']

			# adjust merra time to local time
			offset = longitude / 15
			local = numpy.array([time + offset for time in collection[site]['hour']])
			local = numpy.where(local < 0, local + 24, local)

			# determine order for local time
			order = numpy.argsort(local)
			local = local[order]

			# order short wave data and order according to local time
			wave = wave[order]
			waveii = waveii[order]

			# begin data
			data = {'site_latitude': numpy.array([latitude])}
			data.update({'site_longitude': numpy.array([longitude])})
			data.update({'latitude_high': collection[site]['latitude']})
			data.update({'longitude_high': collection[site]['longitude']})
			data.update({'latitude_low': collectionii[site]['latitude']})
			data.update({'longitude_low': collectionii[site]['longitude']})
			data.update({'albedo_high': collection[site]['albedo']})
			data.update({'albedo_low': collectionii[site]['albedo']})
			data.update({'wave_high': collection[site]['wave']})
			data.update({'wave_low': collectionii[site]['wave']})
			data.update({'hour': local})

			# for each albedo mode
			arrays = (albedo, albedoii)
			arraysii = (wave, waveii)
			modes = ('high', 'low')
			for array, arrayii, mode in zip(arrays, arraysii, modes):

				# construct input matrix for high resolution modis data
				albedos = numpy.array([array] * arrayii.shape[0])
				waves = arrayii.reshape(arrayii.shape[0], 1, arrayii.shape[1], arrayii.shape[2])
				faux = numpy.ones(waves.shape)
				matrix = numpy.hstack([albedos, waves, faux])

				# transpose for predictions
				matrix = matrix.transpose(0, 2, 3, 1)
				shape = matrix.shape[:3]
				matrix = matrix.reshape(-1, matrix.shape[3])

				# get model predictions and reshape
				prediction, _, _, _ = self.predict(matrix)
				prediction = prediction.reshape(shape)

				# add to data
				data.update({'prediction_{}'.format(mode): prediction})

			# stash
			destination = '{}/{}/Onefluxnet_window_{}_{}.h5'.format(folderii, self.stub, site, self.date)
			self.spawn(destination, data)

		return None

# if arguments are given
arguments = sys.argv[1:]
if len(arguments) > 0:

	# separate options and arguments
	options = [argument for argument in arguments if argument.startswith('-')]
	arguments = [argument for argument in arguments if not argument.startswith('-')]

	# check for prepare
	if '--produce' in options:

		# unpack arguments
		sink, year, month, day, scan, scanii, tag = arguments

		# set up instance
		squall = Squall(int(year), int(month), int(day), '', sink=sink, tag=tag)

		# for each scan
		for scan in range(int(scan), int(scanii)):

			# prepare data
			squall.produce(scan)

	# check for prepare
	if '--repair' in options:

		# unpack arguments
		sink, year, month, day, scan, scanii, tag = arguments

		# set up instance
		squall = Squall(int(year), int(month), int(day), '', sink=sink, tag=tag)

		# for each scan
		for scan in range(int(scan), int(scanii)):

			# prepare data
			squall.repair(scan)

	# check for prepare
	if '--mosaic' in options:

		# unpack arguments
		sink, year, month, day, scan, scanii, tag = arguments

		# set up instance
		squall = Squall(int(year), int(month), int(day), '', sink=sink, tag=tag)

		# for each scan
		for scan in range(int(scan), int(scanii)):

			# prepare data
			squall.mosaic(scan)

	# check for collect
	if '--collect' in options:

		# unpack arguments
		sink, year, month, day = arguments

		# set up instance
		squall = Squall(int(year), int(month), int(day), sink=sink)

		# prepare data
		squall.collect()