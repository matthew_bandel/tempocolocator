from tempo_pipeline_tools import *
import h5py
import gc
import scipy
from scipy.interpolate import interpn
from scipy import interpolate
from pylab import *
import math
import botocore, aiobotocore, fsspec


#Defining the day to calculate TEMPO PC reduced reflectances
year = 2024; month = 6; day = 29
formats = (str(year), str(month).zfill(2), str(day).zfill(2))
date = '{}{}{}T'.format(*formats)
stub = '{}/{}/{}'.format(*formats)
bucket = 'gems-oceans/TEMPO/GPP_Project'
scans = (8,)
labels = ['_S{}G'.format(str(scan).zfill(3)) for scan in scans]

from tempo_class import *

print(year, month, day)
#Grabbing TEMPO irradiance metatada. The TEMPO irradiance files aren't always on a regular pattern, particularly earlier in the record
#As a result, code is set up to check current day for irradiance file then move further away until it finds a match
for delta in range(15):

    print(delta)

    # try to
    try:

        # find irr files
        irr_metadata = grab_tempo_data('TEMPO_IRR_L1',datetime.datetime(year,month,day)-datetime.timedelta(days=delta),datetime.datetime(year,month,day)+datetime.timedelta(days=1+delta))
        if len(irr_metadata) > 0:
            print(irr_metadata)

    # unless none found
    except IndexError:

        # pass
        print('none found')
        pass
