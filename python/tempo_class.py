import xarray as xr
import numpy as np
import gc, h5py, scipy
from scipy import interpolate

class tempo_data():
    
    def __init__ (self,metadata):
        self.metadata = metadata
        
    def rd_data(self,group,field_list):
        data = xr.open_dataset('reference://', **{
        'engine': 'zarr','group': group,'backend_kwargs' : {'storage_options' : {'fo': self.metadata},'consolidated': False,}})
        self.data = {}
        
        for key in data.dims.keys():
            self.data[key.replace(group.replace('/',''),'').replace('/','')] = data.dims[key]
            
        for key in field_list:
            self.data[key] = np.array(np.ma.filled(data[key].values,float('nan')),dtype=np.float32)
            
        self.data['steps'] = np.repeat(np.arange(self.data['mirror_step'])[:,np.newaxis],self.data['xtrack'],axis=-1)
        self.data['lines'] = np.repeat(np.arange(self.data['xtrack'])[np.newaxis,:],self.data['mirror_step'],axis=0)
        
        data.close()
        del data 
        gc.collect()
        
def smooth_l1b(smooth_waves,rad_data,irr_data,tempo_water_mask,land,fwhm=1,fine_spacing=0.2):
    
    nstep = rad_data['mirror_step']
    xtrack = rad_data['xtrack']    
    
    interp_rad = np.array(np.zeros((nstep,xtrack,len(smooth_waves))) + np.nan,dtype=np.float32)

    for i in range(xtrack):

        inds = ~np.isnan(irr_data['irradiance'][0,i,:])
        if len(irr_data['irradiance'][0,i,:][inds]) == 0:
            continue

        fn = interpolate.interp1d(irr_data['nominal_wavelength'][i,:][inds],irr_data['irradiance'][0,i,:][inds],kind='linear',bounds_error=False,fill_value=np.nan)
        irr_interp = fn(rad_data['nominal_wavelength'][i,:])

                                          
        if land:
            water_inds = (tempo_water_mask[:,i] > -1)
        elif not land:
            water_inds = (tempo_water_mask[:,i] != 1)
            
        fine_waves = np.arange(smooth_waves[0]-1,smooth_waves[-1]+1,fine_spacing,dtype=np.float32).round(1)

        wave_inds = np.array([np.where(fine_waves == smooth_waves[i])[0] for i in range(len(smooth_waves))]).flatten()
    
        
        common_grid = np.zeros((nstep,len(fine_waves))) + np.nan
        fn = interpolate.interp1d(rad_data['nominal_wavelength'][i,:],np.array(rad_data['radiance'][:,i,:]/np.repeat(irr_interp[np.newaxis,:],nstep,axis=0))[water_inds,:],axis=1,kind='linear')
        common_grid[water_inds,:] = fn(fine_waves)
       

        inds, = np.where(np.isnan(np.mean(common_grid,axis=1)))
        for irow in inds:
            if tempo_water_mask[irow,i]  == 1:
                continue
            tmp_rad = np.array(rad_data['radiance'][:,i,:]/np.repeat(irr_interp[np.newaxis,:],nstep,axis=0))[irow,:]
            if len(tmp_rad[np.isnan(tmp_rad)]) > 0:
                inds = np.isnan(common_grid[irow,:])
                if len(tmp_rad[~np.isnan(tmp_rad)]) < 1000:
                    continue
                common_grid[irow,inds] = np.interp(fine_waves[inds], rad_data['nominal_wavelength'][i,:][~np.isnan(tmp_rad)],tmp_rad[~np.isnan(tmp_rad)])

        boxcar_width = fwhm*2  # in nm                                                                                                                                                          

        nsamples = len(common_grid[:,0])
        boxcar = np.ones(int(boxcar_width / fine_spacing)) / int(boxcar_width / fine_spacing)

        for samp in range(nstep):
            interp_rad[samp,i,:] = scipy.signal.convolve(common_grid[samp,:], boxcar, mode='same')[wave_inds]
    del common_grid, fine_waves
    
    return interp_rad

def pca_reduction(refl,pc_dir,fnam_prefix,wave_ind,n_pc=50):
                                                                                                                    

    f = h5py.File(pc_dir+'/'+fnam_prefix+'.h5','r')
    pca_coeff = f['PCA_Coeff'][:]
    f.close()

    pca_proj = np.array(np.swapaxes(np.dot(pca_coeff.T,refl[:,wave_ind].T),0,1)[:,0:n_pc])
    pca_proj[np.isnan(pca_proj)] = -2**100
    
    return pca_proj 

def write_pc_reduced(fname,data,water_inds):
    f = h5py.File(fname,'w')
    
    geo_keys = ['mirror_step','xtrack','longitude','latitude','solar_azimuth_angle','solar_zenith_angle','viewing_azimuth_angle','viewing_zenith_angle','steps','lines']
    
    for key in geo_keys:
        if (key == 'mirror_step') | (key == 'xtrack'):
            f.create_dataset(key,data=data[key],dtype=int)
        elif (key == 'steps') | (key == 'lines'):
            f.create_dataset(key,data=data[key].flatten()[water_inds],dtype=int, compression="gzip", compression_opts=9)
        else:
            f.create_dataset(key,data=data[key].flatten()[water_inds],dtype=np.float32, compression="gzip", compression_opts=9)
        
    f.create_dataset('smoothed_radiance',data=data['smoothed_radiance'][:,water_inds],dtype=np.float32, compression="gzip", compression_opts=9)
    f.create_dataset('water_mask',data=data['water_mask'].flatten()[water_inds],dtype=np.float32, compression="gzip", compression_opts=9)
    f.close()