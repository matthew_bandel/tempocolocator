# import datetime
import datetime

# import re
import re

# import os
import os

# import earthaccess
import earthaccess

# import namespace
from types import SimpleNamespace, MethodType

# create dummpy self object
self = SimpleNamespace()


def _print(self, *messages):
    """Print the message, localizes print selfments.

    Arguments:
        *messagea: unpacked list of str, etc

    Returns:
        None
    """

    # construct  message
    message = ', '.join([str(message) for message in messages])

    # print
    print(message)

    # # go through each meassage
    # for message in messages:
    #
    #     # print
    #     print(message)

    return message

def _make(self, folder):
    """Make a folder in the directory if not yet made.

    Arguments:
        folder: str, directory path

    Returns:
        None
    """

    # try to
    try:

        # create the directory
        os.mkdir(folder)
        self._print('made {}'.format(folder))

    # or if part of the directory is not found
    except FileNotFoundError:

        # in which case, print error
        self._print('error making {}, file not found error!'.format(folder))

    # or if there are permissions issues
    except PermissionError:

        # in which case, print error
        self._print('error making {}, permissions error!'.format(folder))

    # unless directory already exists
    except FileExistsError:

        # in which case, nevermind
        pass

    return None

def _pad(self, number, length=2):
        """Pad a number by converting to string and zfilling.

        Arguments:
            number: int
            length: length of final number

        Returns:
            str
        """

        # convert to str
        pad = str(number)

        # zfill
        pad = pad.zfill(length)

        return pad

def _skim(self, members, maintain=False):
        """Skim off the unique members from a list.

        Arguments:
            members: list
            maintain: boolean: maintain order?

        Returns:
            list
        """

        # if maintaining order
        if maintain:

            # for each member
            uniques = []
            for member in members:

                # if not already in uniques
                if member not in uniques:

                    # add it
                    uniques.append(member)

        # otherwise
        else:

            # trim duplicates and sort
            uniques = list(set(members))
            uniques.sort()

        return uniques

def _group(self, members, function):
    """Group a set of members by the result of a function of the member.

    Arguments:
        members: list of dicts
        function: str

    Return dict
    """

    # get all fields
    fields = self._skim([function(member) for member in members])

    # create groups
    groups = {field: [] for field in fields}
    [groups[function(member)].append(member) for member in members]

    return groups

# set up self methods
self._print = MethodType(_print, self)
self._make = MethodType(_make, self)
self._pad = MethodType(_pad, self)
self._skim = MethodType(_skim, self)
self._group = MethodType(_group, self)

# set self attributes
self.year = 2024
self.month = 7
self.day = 1
self.sink = '.'
self.version = 'V03'
self.stub = '{}/{}/{}'.format(self.year, self._pad(self.month), self._pad(self.day))

# access
def _access(name, labels=None, count=200, window=None):
    """Collect tempo files.

    Arguments:
        name: str, short name
        labels: list of str, the scan labels
        count: number of results to obtain
        window: number of days to expand search

    Returns:
        None
    """

    # set formats
    formats = (self.year, self._pad(self.month), self._pad(self.day))

    # create L1B folders
    self._make('{}/{}'.format(self.sink, name))
    self._make('{}/{}/{}'.format(self.sink, name, *formats[:1]))
    self._make('{}/{}/{}/{}'.format(self.sink, name, *formats[:2]))
    self._make('{}/{}/{}'.format(self.sink, name, self.stub))

    # login to earthaccess
    earthaccess.login(strategy="netrc", persist=True)

    # construct date
    dates = [datetime.datetime(self.year, self.month, self.day)]
    dates += [dates[0] + datetime.timedelta(days=1)]

    # if given a window:
    if window:

        # split in half
        half = int(window / 2)

        # construct date
        date = datetime.datetime(self.year, self.month, self.day) - datetime.timedelta(days=half)
        dateii = datetime.datetime(self.year, self.month, self.day) + datetime.timedelta(days=half)
        dates = [date, dateii]

    # convert to strings
    dates = [str(date)[:10] for date in dates]

    # get results
    results = earthaccess.search_data(short_name=name, version=self.version, temporal=tuple(dates), count=count)

    # if given scan labels:
    if labels:

        # group according to scan
        groups = self._group(labels, lambda label: re.findall('S[0-9]{3}', label)[0])

        # for each label
        for scan, group in groups.items():

            # print label
            self._print('scan {}...'.format(scan))

            # filter results by date and scan labels
            date = '_{}{}{}T'.format(*formats)
            subset = [result for result in results if any([label in result['umm']['GranuleUR'] for label in group])]
            subset = [result for result in subset if date in result['umm']['GranuleUR']]

            # if there are results
            if len(subset) > 0:

                # download
                earthaccess.download(subset, '{}/{}/{}'.format(self.sink, name, self.stub))

    # otherwise
    else:

        # download
        earthaccess.download(results, '{}/{}/{}'.format(self.sink, name, self.stub))

    return None