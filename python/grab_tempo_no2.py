import xarray as xr
from tempo_pipeline_tools import *
import sys

year = 2024
month = 2
day = 27


l1b_metadata = grab_tempo_data('TEMPO_CLDO4_L2',datetime.datetime(year,month,day),datetime.datetime(year,month,day)+datetime.timedelta(days=1),parallel=6,version=2)

for metadata in l1b_metadata:
    print(metadata)

    group = 'product'
    data = xr.open_dataset('reference://', **{
        'engine'         : 'zarr',
        'group'          : group,
        'backend_kwargs' : {
            'storage_options' : {'fo': metadata},
            'consolidated'    : False,
        }})

    print(np.nanmean(data['cloud_fraction']))
    stop
    #lat = data['latitude'].values
    #lon = data['longitude'].values

