#!/usr/bin/env python3

# nightshades.py to validate NN predictions

# import local classes
from cores import Core
from hydras import Hydra
from features import Feature
from formulas import Formula

# import system
import sys

# import re
import re

# import datetime
import datetime

# import math
import math

# import numpy functions
import numpy
import scipy

# import shap for shapely analysis
import shap

# import netcdf4
import netCDF4

# import sklearn functions
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.metrics import r2_score
from sklearn.metrics.pairwise import cosine_similarity, pairwise_distances
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsRegressor

# # import netcdf4
# import netCDF4

# import tensorflow
import tensorflow
import tensorflow_addons
import tensorflow_probability

# import earthaccess
import earthaccess

# import boto3 for aws access
import boto3
import botocore

# import gc for garbage collection
import gc

# import pipeline tools
import tempo_pipeline_tools

# try to
try:

	# import matplotlib for plots
	import matplotlib
	from matplotlib import pyplot
	from matplotlib import style as Style
	from matplotlib import rcParams
	Style.use('fast')
	rcParams['axes.formatter.useoffset'] = False
	rcParams['figure.max_open_warning'] = False

# unless import error
except ImportError:

	# ignore matplotlib
	print('matplotlib not available!')

# try to
try:

	# import cartopy
	import cartopy
	from cartopy.mpl.geoaxes import GeoAxes

# unless import error
except ImportError:

	# ignore matplotlib
	print('cartopy not available!')


# class Nightshade to do OMOCNUV analysis
class Nightshade(Hydra):
	"""Nightshade class to prepare tempo training data.

	Inherits from:
		Hydra
	"""

	def __init__(self, year=2024, month=6, day=25, tag='nitrogen', session=None, sink='.', limits=None):
		"""Initialize instance.

		Arguments:
			year: int, the year
			month: int, the month
			day: int, the day
			tag: tag for model training
			session: date of model training
			sink: str, file path for sink folder
			limits: dict, limit alterations
		"""

		# initialize the base Core instance
		Hydra.__init__(self, sink)

		# set fill value
		self.fill = numpy.nan

		# set sink
		self.sink = sink

		# set date attribute
		self.year = year
		self.month = month
		self.day = day
		self.date = ''
		self.stub = ''
		self._calendar()

		# set version
		self.version = 'V03'

		# set grid size
		self.mirror = 1200
		self.track = 2048

		# set model information
		self.session = session
		self.tag = tag

		# parse yaml file
		self.yam = {}
		self.architecture = {}
		self.training = {}
		self.validation = {}
		# self.features = {}
		self._parse()

		# set limits
		self.limits = limits or {}
		self._limit()

		# set default instances
		self.instances = 50
		self._instantiate()

		# set units
		self.target = '$NO_2$'
		self.unit = '$mol N/m^2$'

		# retrieve model
		self.model = None
		self.details = None
		self.folder = None
		self.variables = None
		self._retrieve()

		return

	def __repr__(self):
		"""Create string for on screen representation.

		Arguments:
			None

		Returns:
			str
		"""

		# create representation
		formats = (self.sink, self.date, self.tag, self.session)
		representation = ' < Nightshade instance at {}, {}, {} ( {} ) >'.format(*formats)

		return representation

	def _access(self, name, count=30, pattern=None, labels=None, window=None):
		"""Collect tempo files.

		Arguments:
			name: str, short name
			pattern: str, regex pattern
			labels: list of str, the scan labels
			count: number of results to obtain
			window: number of days to expand search

		Returns:
			None
		"""

		# set formats
		formats = (self.year, self._pad(self.month), self._pad(self.day))

		# create L1B folders
		self._make('{}/{}'.format(self.sink, name))
		self._make('{}/{}/{}'.format(self.sink, name, *formats[:1]))
		self._make('{}/{}/{}/{}'.format(self.sink, name, *formats[:2]))
		self._make('{}/{}/{}'.format(self.sink, name, self.stub))

		# login to earthaccess
		earthaccess.login(strategy="netrc", persist=True)

		# construct date
		dates = [datetime.datetime(self.year, self.month, self.day)]
		dates += [dates[0] + datetime.timedelta(days=1)]

		# if given a window:
		if window:

			# split in half
			half = int(window / 2)

			# construct date
			date = datetime.datetime(self.year, self.month, self.day) - datetime.timedelta(days=half)
			dateii = datetime.datetime(self.year, self.month, self.day) + datetime.timedelta(days=half)
			dates = [date, dateii]

		# convert to strings
		dates = [str(date)[:10] for date in dates]

		# get results
		results = earthaccess.search_data(short_name=name, temporal=tuple(dates), count=count)

		# if given scan labels:
		if labels:

			# group according to scan
			groups = self._group(labels, lambda label: re.findall('S[0-9]{3}', label)[0])

			# for each label
			for scan, group in groups.items():

				# filter results by date and scan labels
				date = '_{}{}{}T'.format(*formats)
				subset = [result for result in results if any([label in result['umm']['GranuleUR'] for label in group])]
				subset = [result for result in subset if date in result['umm']['GranuleUR']]

				# if there are results
				if len(subset) > 0:

					# download
					earthaccess.download(subset, '{}/{}/{}'.format(self.sink, name, self.stub))

		# otherwise
		else:

			# set subset to results
			subset = results

			# if given a pattern
			if pattern:

				# subset based on regex pattern
				subset = [result for result in results if self._search(pattern, result['umm']['GranuleUR'])]

			# if there are results
			if len(subset) > 0:

				# download
				earthaccess.download(subset, '{}/{}/{}'.format(self.sink, name, self.stub))

		return None

	def _average(self, array, indices, distances, radius):
		"""Create a weighted average from kdTree results.

		Arguments:
			array: numpy array, the original data
			indices: numpy array, block of indices
			distances: numpy array, block of distances
			radius: float, the largest distance to count.

		Returns:
			numpy array
		"""

		# get shape
		shape = indices.shape

		# create columns for each block of indices
		columns = numpy.array([array[indices[:, column].flatten()] for column in range(shape[1])])
		columns = columns.transpose(1, 0)

		# get distances less than radius
		mask = (distances ** 2 <= radius ** 2)

		# create weights from inverse square distances, and zero out mask
		weights = 1 / (distances ** 2)
		weights = numpy.where(mask, weights, 0)

		# zero out array for values outside mask
		columns = numpy.where(mask, columns, 0)

		# perform weighted average
		average = (columns * weights).sum(axis=1) / weights.sum(axis=1)

		return average

	def _block(self, data, size):
		"""Reshape a collection of data based on a size.

		Arguments:
			data: dict of names and arrays
			size: int, size of middle dimension

		Returns:
			dict
		"""

		# begin block
		block = {}

		# for each field
		for field, array in data.items():

			# try to:
			try:

				# reshpae array
				block[field] = array.reshape(-1, size, array.shape[1])

			# unless unable
			except ValueError:

				# in which case, pass
				pass

		return block

	def _calendar(self):
		"""Format the date strings.

		Arguments:
			None

		Returns:
			None
		"""

		# create formatw
		formats = (self.year, self._pad(self.month), self._pad(self.day))
		self.date = '{}m{}{}'.format(*formats)
		self.stub = '{}/{}/{}'.format(*formats)

		return None

	def _constrain(self, data, constraints):
		"""Apply constraints to a data set.

		Arguments:
			data: dict of numpy arrays
			constraints: numpy array, boolean mask

		Returns:
			dict of numpy arrays
		"""

		# begin valid dataset
		valids = {}

		# for each feature
		for name, array in data.items():

			# add to data
			valids[name] = array

			# if shapes match
			if array.shape[0] == constraints.shape[0]:

				# apply constraints
				valids[name] = array[constraints]

		return valids

	def _daze(self, validation=True):
		"""Get the list of days involved.

		Arguments:
			validation: boolean, get validation days first?

		Returns:
			list of (int, int) tuples, the months and days
		"""

		# get days
		days = self.training['days'] + self.validation['days']

		# if putting validation first
		if validation:

			# get days
			days = self.validation['days'] + self.training['days']

		return days

	def _decode(self, details):
		"""Decode byte strings.

		Arguments:
			details: dict of str, arrays

		Returns:
			dict
		"""

		# for each field
		for field, array in details.items():

			# if a string type
			if 'S' in str(array.dtype):

				# decode
				details[field] = numpy.char.decode(array, 'utf-8')

		return details

	def _feature(self):
		"""Set feature list for model training.

		Arguments:
			None

		Returns:
			list of str
		"""

		# begin features
		features = []

		# for each feature
		for feature, limit in self.features.items():

			# if a limit is given:
			if limit:

				# add all indices
				features += ['{}|{}'.format(feature, self._pad(number)) for number in range(self.limits[limit])]

			# otherwise
			else:

				# add singlet feature
				features += [feature]

		return features

	def _fill(self, array):
		"""Replace various fill values with self.fill.

		Arguments:
			array: numpy array

		Returns:
			numpy array
		"""

		# create mask for valid data
		mask = self._mask(array)

		# fill with fill value
		array = numpy.where(mask, array, self.fill)

		return array

	def _forge(self, data, features):
		"""Forge a matrix from a dataset and list of features.

		Arguments:
			data: dict of fields and numpy arrays
			features: list of str, the features

		Returns:
			numpy array
		"""

		# begin vectors
		vectors = []

		# set transforms
		transforms = {'cos': lambda vector: numpy.cos(numpy.radians(vector))}
		transforms.update({'log': lambda vector: numpy.log(vector)})

		# for each feature
		for feature in features:

# 			# try to
# 			try:
#
# 				# decode byte string
# 				name = feature.decode('utf-8')
#
# 			# unless it is already a string
# 			except AttributeError:
#
# 				# in which case, pass
# 				name = feature

			# set defaults
			name = feature
			position = None
			transform = None

			# if colon
			if '|' in name:

				# strip paranthesis
				name, position = name.split('|')
				position = int(position)

			# if parenthesis
			if '(' in name:

				# strip paranthesis
				transform, name = name.split('(')

			# get vector
			vector = data[name]

			# if position
			if position is not None:

				# extract position
				vector = vector[:, position].reshape(-1, 1)

			# if transform
			if transform:

				# apply transform
				vector = transforms[transform](vector)

			# add vector
			vectors.append(vector)

		# stack
		block = numpy.hstack(vectors)

		return block

	def _graph(self, abscissa, ordinate, intensity, bounds, scale, gradient, title, unit, labels, **options):
		"""Generate graph object for plotting.

		Arguments:
			abscissa: numpy array, horizontal coordinates
			ordinate: numpy array, vertical coordinates
			intensity: numpy array, value for color
			bounds: list of ( float, float ) tuples, the x-axis and y-axis bounds
			scale: tuple of floats, the color scale boundaries
			gradient: str, the name of the matplotlib color gradient
			title: str, plot title
			units: str, plot units
			labels: list of str, the x and y axis labels
			**options: unpacked dictionary of options:
				globe: boolean, draw coastlines?
				logarithm: boolean, use logarithmic scale?
				colorbar: boolean, add colorbar?
				lines: list of (array, array, str) tuples, additional lines ( x, y, format str )
				pixel: float, the pixel size
				polygons: boolean, graph as polygons?
				corners: list of numpy arrays, the latitude and longitude corners
				selection: indices of color gradient
				categories: list of ( str, str ) tuples, the colors and categories
				reflectivity: boolean, plotting reflectivity?
				alpha: float, transparency level
				font: int, title font size
				fontii: int, marker font size
				clip: boolean, clip endpoints to scale limits?

		Returns:
			dictionary, graph specifics
		"""

		# set default options
		globe = options.get('globe', True)
		logarithm = options.get('logarithm', False)
		colorbar = options.get('colorbar', True)
		lines = options.get('lines', [])
		pixel = options.get('pixel', abscissa.shape[0] / ( 72 * 16))
		polygons = options.get('polygons', False)
		corners = options.get('corners', [None, None])
		ticks = options.get('ticks', None)
		ticksii = options.get('ticksii', None)
		marks = options.get('marks', None)
		marksii = options.get('marksii', None)
		marker = options.get('marker', 2)
		selection = options.get('selection', (0, 256))
		categories = options.get('categories', None)
		reflectivity = options.get('reflectivity', None)
		alpha = options.get('alpha', 0.8)
		font = options.get('font', 20)
		fontii = options.get('fontii', 15)
		clip = options.get('clip', True)

		# create graph object
		graph = {'abscissa': abscissa, 'ordinate': ordinate, 'intensity': intensity}
		graph.update({'x_bounds': bounds[0], 'y_bounds': bounds[1], 'scale': scale, 'gradient': gradient})
		graph.update({'title': title, 'units': unit, 'x_label': labels[0], 'y_label': labels[1]})
		graph.update({'globe': globe, 'logarithm': logarithm, 'colorbar': colorbar, 'lines': lines})
		graph.update({'pixel': pixel, 'polygons': polygons, 'corners': corners})
		graph.update({'x_ticks': ticks, 'y_ticks': ticksii, 'x_marks': marks, 'y_marks': marksii})
		graph.update({'marker': marker, 'selection': selection, 'categories': categories})
		graph.update({'reflectivity': reflectivity, 'alpha': alpha, 'font': font, 'fontii': fontii})
		graph.update({'clip': clip})

		return graph

	def _instantiate(self):
		"""Set number of instances for bayesian means and deviations from yaml file.

		Arguments:
			None

		Returns:
			None

		Populates:
			self.instances
		"""

		# set instances
		self.instances = self.validation['instances']

		return None

	def _interpolate(self, quantity, nodes, nodesii, target, targetii, method='nearest'):
		"""Perform two dimensional interpolation of a quantity from one grid to another.

		Arguments:
			quantity: 2-D numpy.array of gridded data
			nodes: 1-D numpy array of first nodes
			nodesii: 1-D numpy array of second nodes
			target: array of first grid coordinates
			targetii: array of second grid coordinates
			method: interpolation method ('nearest', 'linear')

		Returns:
			numpy.array
		"""

		# get final shape from target
		shape = target.shape

		# interpolate gpp onto tempo coordinates
		options = {'method': method, 'fill_value': self.fill, 'bounds_error': False}
		parameters = ((nodes, nodesii), quantity, (target.reshape(-1), targetii.reshape(-1)))
		interpolation = scipy.interpolate.interpn(*parameters, **options).reshape(shape)

		return interpolation

	def _label(self, scan, granule=None):
		"""Create a scan label.

		Arguments:
			scan: int, the scan number
			granule: int, the granule number

		returns:
			str, the scan label
		"""

		# make label
		label = '_S{}G'.format(self._pad(scan, 3))

		# if a granule number given
		if granule:

			# add granule number
			label += self._pad(granule)

		return label

	def _limit(self):
		"""Define valid data limits.

		Arguments:
			None

		Returns:
			None
		"""

		# get limits from yaml
		limits = self.yam['limits']

		# update with given limits
		limits.update(self.limits)
		self.limits = limits

		return None

	def _lose(self, truth, prediction):
		"""Define the custom loss function with a penalty for higher weights.

		Arguments:
			points: numpy array of ponts
			distribution: tensorflow distribution fucntion
			truth: numpy array of truth values
			prediction: numpy array of prediction values

		Returns:
			numpy array, the loos
		"""

		# get custom loss penalty for high wieghted samples
		penalty = self.training['penalty']

		# create weights
		weights = 1 + penalty * (tensorflow.math.abs(truth) ** 2)

		# # define loss
		# loss = -tensorflow.reduce_mean(weights * distribution.log_prob(points))
		loss = tensorflow.reduce_mean(weights * tensorflow.square(truth - prediction))

		return loss

	def _mask(self, array):
		"""Determine valid data in an array.

		Arguments:
			array: numpy array

		Returns:
			numpy boolean array
		"""

		# create mask
		mask = (numpy.isfinite(array)) & (abs(array) < 1e20) & (array > -9998)

		return mask

	def _paint(self, destination, graphs, size=(16, 16), space=None, gap=0.065):
		"""Create a plot with subplots.

		Arguments:
			destination: str, the file path
			graphs: list of dicts, the graph objects
			size: tuple of floats, the total plot size
			space: dict, subplot spacing paramaeters
			gap: gap between grqph and colorbars

		Returns:
			None
		"""

		# print status
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase previous copy
		self._clean(destination, force=True)

		# define default spacing
		space = space or {}

		# get number of subplots
		number = len(graphs)

		# set up dictionary of subplot values based on number
		layout = {'rows': {1: 1, 2: 1, 3: 1, 4: 2}, 'columns': {1: 1, 2: 2, 3: 3, 4: 2}}

		# calculate total size
		rows = layout['rows'][number]
		columns = layout['columns'][number]

		# set up figure
		pyplot.clf()
		figure = pyplot.figure(figsize=size)
		figure.patch.set_facecolor('white')
		# words = {'subplot_kw': {'projection': cartopy.crs.PlateCarree()}, 'figsize': size}
		# figure, axes = pyplot.subplots(rows, columns, figsize=size)

		# begin axes
		axes = []

		# for each graph
		for index, graph in enumerate(graphs):

			# if drawing globe:
			if graph['globe']:

				# activate cartopy
				code = int('{}{}{}'.format(rows, columns, index + 1))
				axis = pyplot.subplot(code, projection=cartopy.crs.PlateCarree())
				axis.set_facecolor('white')
				axes.append(axis)

			# otherwise:
			else:

				# activate cartopy
				code = int('{}{}{}'.format(rows, columns, index + 1))
				axis = pyplot.subplot(code)
				axis.set_facecolor('white')
				axes.append(axis)

		# adjust spacing
		margins = {'bottom': 0.12, 'top': 0.94, 'left': 0.07, 'right': 0.93, 'wspace': 0.2, 'hspace': 0.5}
		margins.update(space)
		pyplot.subplots_adjust(**margins)

		# for each graph
		for axis, graph in zip(axes, graphs):

			# unpack arrays
			abscissa = graph['abscissa']
			ordinate = graph['ordinate']
			intensity = graph['intensity']

			# unpack bounds
			bounds = graph['x_bounds']
			boundsii = graph['y_bounds']

			# unpack color scale
			scale = graph['scale']
			gradient = graph['gradient']
			selection = graph['selection']
			categories = graph['categories']

			# if clipping:
			if graph['clip']:

				# clip intensity to scale boundaries
				epsilon = 1e-10
				intensity = numpy.where(intensity > scale[0] + epsilon, intensity, scale[0] + epsilon)
				intensity = numpy.where(intensity < scale[1] - epsilon, intensity, scale[1] - epsilon)

			# unpack texts
			title = graph['title']
			units = graph['units']
			label = graph['x_label']
			labelii = graph['y_label']
			font = graph['font']
			fontii = graph['fontii']

			# if not reflectivity
			if not graph['reflectivity']:

				# create mask for finite data
				mask = numpy.isfinite(intensity)

				# apply mask
				intensity = intensity[mask]
				abscissa = abscissa[mask]
				ordinate = ordinate[mask]

				# get scale bounds
				minimum = scale[0]
				maximum = scale[1]

				# clip intensity
				intensity = numpy.where(intensity < minimum, minimum, intensity)
				intensity = numpy.where(intensity > maximum, maximum, intensity)

			# if drawing globe:
			if graph['globe']:

				# set cartopy axis with coastlines
				axis.coastlines()
				# axis.add_feature(cartopy.feature.OCEAN)
				# axis.add_feature(cartopy.feature.COASTLINE, edgecolor='black', linewidth=1)
				axis.add_feature(cartopy.feature.STATES)
				axis.add_feature(cartopy.feature.LAKES, facecolor='white')
				# axis.add_feature(cartopy.feature.OCEAN, facecolor='blue')
				options = {'edgecolor': 'face', 'facecolor': 'white'}
				axis.add_feature(cartopy.feature.NaturalEarthFeature('physical', 'ocean', '50m', **options))

				# set title
				axis.set_aspect('auto')
				axis.set_title(title, fontsize=font)

				# if xticks
				if graph['x_ticks'] and graph['y_ticks']:

					# set x ticks
					axis.set_xticks(graph['x_ticks'])
					axis.set_xticklabels(graph['x_marks'])
					# axis.tick_params(axis='x', rotation=45)

					# set yticks
					axis.set_yticks(graph['y_ticks'])
					axis.set_yticklabels(graph['y_marks'])
					# axis.tick_params(axis='y', rotation=45)

					# set font size
					axis.tick_params(axis='both', which='major', labelsize=fontii)	# Major ticks
					axis.tick_params(axis='both', which='minor', labelsize=fontii)

				# otherwies
				else:

					# set grid marks
					axis.grid(True)
					axis.set_global()
					_ = axis.gridlines(draw_labels=True)

					# set font size
					axis.tick_params(axis='both', which='major', labelsize=fontii)
					axis.tick_params(axis='both', which='minor', labelsize=fontii)

			# otherwise:
			else:

				# set title
				axis.set_title(title, fontsize=font)

				# add labels and ticks
				axis.set_title(title, fontsize=font)
				axis.set_xlabel(label, fontsize=fontii)
				axis.set_ylabel(labelii, fontsize=fontii)

				# set font size
				axis.tick_params(axis='both', which='major', labelsize=fontii)
				axis.tick_params(axis='both', which='minor', labelsize=fontii)

				# if xticks
				if graph['x_ticks']:

					# set x ticks
					axis.set_xticks(graph['x_ticks'])
					axis.set_xticklabels(graph['x_marks'])
					axis.tick_params(axis='x', rotation=45)

				# if yticks
				if graph['y_ticks']:

					# set yticks
					axis.set_yticks(graph['y_ticks'])
					axis.set_yticklabels(graph['y_marks'])
					axis.tick_params(axis='y', rotation=45)

			# set up colors and logarithmic scale
			colors = matplotlib.cm.get_cmap(gradient)
			selection = numpy.array(list(range(*selection)))
			colors = matplotlib.colors.ListedColormap(colors(selection))
			normal = matplotlib.colors.Normalize(minimum, maximum)

			# if colorbar categores
			if categories:

				# split names and hues
				hues = [category[0] for category in categories]
				texts = [category[1] for category in categories]
				colors = matplotlib.colors.ListedColormap(hues)
				normal = matplotlib.colors.BoundaryNorm(boundaries=numpy.arange(len(hues) + 1) - 0.5, ncolors=len(hues))

			# if logarithmic
			if graph['logarithm']:

				# make normal logarithmic
				normal = matplotlib.colors.LogNorm(minimum, maximum)

			# if grpahing polygons
			if graph['polygons']:

				# begin patches
				patches = []

				# for each polygon
				for vertices, verticesii in zip(graph['corners'][1].reshape(-1, 4), graph['corners'][0].reshape(-1, 4)):

					# set up patch
					polygon = numpy.array([[first, second] for first, second in zip(vertices, verticesii)])
					patch = matplotlib.patches.Polygon(xy=polygon, linewidth=0, edgecolor=None, alpha=graph['alpha'])
					patches.append(patch)

				# if reflectivity plot
				if graph['reflectivity']:

					# plot polygons with colormap
					options = {'facecolor': intensity, 'alpha': graph['alpha']}
					collection = matplotlib.collections.PatchCollection(patches, **options)
					collection.set_edgecolor("face")
					# collection.set_clim(scale)
					# collection.set_array(intensity)
					polygons = axis.add_collection(collection)
					# axis.add_feature(cartopy.feature.OCEANS, facecolor='white')

				# otherwise
				else:

					# plot polygons with colormap
					collection = matplotlib.collections.PatchCollection(patches, cmap=colors, alpha=graph['alpha'])
					collection.set_edgecolor("face")
					collection.set_clim(scale)
					collection.set_array(intensity)
					polygons = axis.add_collection(collection)

			# otherwise
			else:

				# if reflectivity plot
				if graph['reflectivity']:

					# no need for colors
					axis.scatter(abscissa, ordinate, c=intensity, s=0.75, lw=0)

				# otheriwse
				else:

					# plot data
					words = {'c': intensity, 'cmap': colors, 'norm': normal}
					axis.scatter(abscissa, ordinate, marker='.', s=graph['pixel'], **words)

			# plot additional lines
			for line in graph['lines']:

				# plot line
				options = {'markersize': graph['marker']}
				axis.plot(*line, **options)

			# set axis limits
			axis.set_xlim(*bounds)
			axis.set_ylim(*boundsii)

			# if using colorbar
			if graph['colorbar']:

				# add colorbar
				position = axis.get_position()
				bar = pyplot.gcf().add_axes([position.x0, position.y0 - gap, position.width, 0.02])
				scalar = matplotlib.cm.ScalarMappable(norm=normal, cmap=colors)
				# colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', label=units)

				# if cateogories
				if categories:

					# add categorical colorbar
					ticksii = numpy.array(range(len(hues)))
					colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal', ticks=ticksii)
					colorbar.ax.set_xticklabels(texts)	# Set category names
					colorbar.ax.tick_params(axis='x', rotation=45, labelsize=fontii)
					# colorbar.set_label(units, labelpad=1)
					# formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
					# colorbar.ax.yaxis.set_major_formatter(formatter)
					# # Create the colorbar with category labels
					# # barii = bar.imshow(numpy.array(range(len(hues))), cmap=colors, norm=normal)
					# colorbar = pyplot.gcf().colorbar(scalar, ticks=numpy.arange(len(hues)))
					# colorbar.ax.set_yticklabels(texts)  # Set category names

				# otherwise
				else:

					# add regular colorbar
					colorbar = pyplot.gcf().colorbar(scalar, cax=bar, orientation='horizontal')
					colorbar.set_label(units, labelpad=1, fontsize=fontii)
					formatter = matplotlib.ticker.StrMethodFormatter('{x: .3f}')
					colorbar.ax.yaxis.set_major_formatter(formatter)
					colorbar.ax.tick_params(labelsize=fontii)  # For tick labels

		# save to destination
		pyplot.savefig(destination)
		pyplot.clf()

		# end status
		self._stamp('plotted.')

		return None

	def _parse(self):
		"""Parse the yaml file.

		Arguments:
			None

		Returns:
			None

		Populates:
			self.yam
		"""

		# get the yaml file
		yam = self._acquire('{}/Nitro_Yams/{}.yaml'.format(self.sink, self.tag))

		# set yam
		self.yam = yam

		# populate subsections
		self.training = yam['training']
		self.architecture = yam['architecture']
		self.features = yam['features']
		self.validation = yam['validation']

		return None

	def _patch(self, reflectance, latitude, longitude, cloud):
		"""Patch bad reflectance data with nearest neighbors good data.

		Arguments:
			reflectance: numpy array, reflectqnce pcas
			latitude: numpy array, latitude
			longitude: numpy array, longitude
			cloud: numpy array, cloud fraction

		Returns:
			numpy array: patched data
		"""

		# squeeze latitude and longitude
		latitude = latitude.squeeze()
		longitude = longitude.squeeze()
		cloud = cloud.squeeze()

		# create mask for good and bad data
		summation = reflectance.sum(axis=1)
		good = (numpy.isfinite(summation) & (abs(summation) < 1e20))
		bad = numpy.logical_not(good)

		print('good: {}'.format(good.sum()))
		print('bad: {}'.format(bad.sum()))

		# create a KDTree from the good lat/lon
		tree = scipy.spatial.cKDTree(numpy.column_stack((latitude[good], longitude[good])))

		# query the tree for bad data points
		distances, indices = tree.query(numpy.column_stack((latitude[bad], longitude[bad])), k=1)

		# replace bad data with the nearest good data
		reflectance[bad] = reflectance[good][indices.flatten()]

		return reflectance

	def _post(self, kernel, bias=0, datatype=None):
		"""Define the posterior distribution function.

		Arguments:
			kernal: int, the kernal size
			bias: int, the bias size
			datatype: datatype object, the datatype

		Returns:
			tensor flow model layer
		"""

		# define abbreviations
		probability = tensorflow_probability.layers
		independent = tensorflow_probability.distributions.Independent
		normal = tensorflow_probability.distributions.Normal

		# get total size
		size = kernel + bias

		# set scaling functions
		epsilon = 1e-5
		scale = 0.02
		constant = numpy.log(numpy.expm1(1.0))
		def locating(tensor): return tensor[..., :size]
		def scaling(tensor): return epsilon + scale * tensorflow.nn.tanh(constant + tensor[..., size:])

		# define independent normal function
		options = {'reinterpreted_batch_ndims': 1}
		def normalizing(tensor): return independent(normal(loc=locating(tensor), scale=scaling(tensor)), **options)

		# create layer sequence
		layers = [probability.VariableLayer(2 * size, dtype=datatype)]
		layers += [probability.DistributionLambda(normalizing)]
		sequence = tensorflow.keras.Sequential(layers)

		return sequence

	def _prioritize(self, kernel, bias=0, datatype=None):
		"""Define the prior distribution function.

		Arguments:
			kernal: int, the kernal size
			bias: int, the bias size
			datatype: datatype object, the datatype

		Returns:
			tensor flow model layer
		"""

		# define abbreviations
		probability = tensorflow_probability.layers
		independent = tensorflow_probability.distributions.Independent
		normal = tensorflow_probability.distributions.Normal

		# get total size
		size = kernel + bias

		# define independent normal function
		def normalizing(tensor): return independent(normal(loc=tensor, scale=1), reinterpreted_batch_ndims=1)

		# create layer sequence
		layers = [probability.VariableLayer(size, dtype=datatype)]
		layers += [probability.DistributionLambda(normalizing)]
		sequence = tensorflow.keras.Sequential(layers)

		return sequence

	def _project(self, horizontal, vertical, projection):
		"""Project the ABI angular grid onto a latitude longitude grid.

		Arguments:
			horizontal: numpy array, x coordinate values
			vertical: numpy array, y coordinate values
			projection: dict, projection information

		Returns:
			(numpy array, numpy array) tuple, the geocoordinates
		"""

		# get projection information
		origin = projection['longitude_of_projection_origin']
		equatorial = projection['semi_major_axis']
		polar = projection['semi_minor_axis']
		altitude = projection['perspective_point_height']
		height = altitude + equatorial

		# create mesh grid
		mesh, meshii = numpy.meshgrid(horizontal, vertical)

		# set up equations
		zero = (origin * numpy.pi) / 180.0
		sine = numpy.sin(mesh)
		sineii = numpy.sin(meshii)
		cosine = numpy.cos(mesh)
		cosineii = numpy.cos(meshii)
		variable = sine ** 2 + (cosine ** 2 * (cosineii ** 2 + (equatorial ** 2 / polar ** 2) * sineii ** 2))
		variableii = -2 * height * cosine * cosineii
		variableiii = height ** 2 - equatorial ** 2
		radius = (-variableii - numpy.sqrt(variableii ** 2 - (4 * variable * variableiii))) / (2 * variable)
		abscissa = radius * cosine * cosineii
		ordinate = -radius * sine
		applicate = radius * cosine * sineii

		# ignore errors
		errors = numpy.seterr(all='ignore')

		# calculate latitude and longitude
		degrees = (180.0 / numpy.pi)
		ratio = equatorial ** 2 / polar ** 2
		root = numpy.sqrt(((height-abscissa) * (height-abscissa)) + (ordinate ** 2))
		latitude = degrees * (numpy.arctan(ratio * ((applicate / root))))
		longitude = (zero - numpy.arctan(ordinate / (height-abscissa))) * degrees

		# reset numpy errors
		numpy.seterr(**errors)

		return latitude, longitude

	def _represent(self, coefficients, features, number=3):
		"""Determine the representation of the pca coefficients.

		Arugments:
			coefficients: numpy array of transposed pca components
			features: list of features
			number: int, number of top entries

		Returns:
			list of str, the pca componeetn labesl
		"""

		# begin labels
		labels = []

		# transpose to get components on rows
		components = coefficients.transpose(1, 0)

		# for each component
		for component in components:

			# zip values with features
			pairs = list(zip(features, component))

			# sort by highest and keep top
			pairs.sort(key=lambda pair: abs(pair[1]), reverse=True)
			top = pairs[:number]

			# create label
			label = ['{}: {:.2f}'.format(*pair) for pair in top]
			label =' '.join(label)
			labels.append(label)

		return labels

	def _retrieve(self, keras=False):
		"""Retrieve a tensor flow model and scaling details.

		Arguments:
			keras: boolean, use Keras style?

		Returns:
			tensorflow model object, dict, str tuple, the model, scaling details, and model folder
		"""

		# set models folder
		models = 'Nitro_Models'

		# get all model folders
		folders = self._see('{}/{}'.format(self.sink, models))

		# if given a date:
		if self.session:

			# restrict to date
			folders = [folder for folder in folders if self.session in folder]

		# if given a tag:
		if self.tag:

			# restrict to tag
			folders = [folder for folder in folders if self.tag == folder.split('_')[-1]]

		# sort and get latest date
		folders.sort()
		folder = folders[-1]

		# construct model path
		path = '{}/TEMPO_Nitro_Model'.format(folder)

		# if keras
		if keras:

			# load model
			model = tensorflow.keras.models.load_model(path, compile=False)

		# otherwise
		else:

			# load model
			model = tensorflow.saved_model.load(path)

		# also get model scaling details
		hydra = Hydra(folder)
		hydra.ingest('Model_Scaling.h5')
		details = hydra.extract(addresses=True)

		# decode strings in details
		details = self._decode(details)

		return model, details, folder

	def _search(self, pattern, string):
		"""Use regex to find a pattern in a string.

		Arguments:
			pattern: str, regex pattern
			string: str, the source string

		Returns:
			str, the found pattern
		"""

		# default element to ''
		element = ''

		# try to
		try:

			# find element
			element = re.findall(pattern, string)[0]

		# unless not found
		except IndexError:

			# in which case, pass
			pass

		return element

	def baseline(self, path, latitude, longitude):
		"""Colocate ABI baseliner data.

		Arguments:
			path: radiance path
			latitude: numpy array, tempo latitude
			longitude: numpy arraym, tempo longitude

		Returns:
			numpy array, the baseliner data
		"""

		# cast tempo coordinates as target points
		shape = latitude.shape
		targets = numpy.vstack([latitude.reshape(-1), longitude.reshape(-1)]).transpose(1, 0)

		# create valid coordinates mask
		valid = self._mask(targets.sum(axis=1))

		# determine geographic extend
		south = targets[:, 0][valid].min()
		north = targets[:, 0][valid].max()
		west = targets[:, 1][valid].min()
		east = targets[:, 1][valid].max()

		# substitute minimums for invalids
		targets[:, 0] = numpy.where(valid, targets[:, 0], south)
		targets[:, 1] = numpy.where(valid, targets[:, 1], west)

# 		# get the ABI high resolution grid
# 		grid = Hydra('{}/ABI_High_Grid'.format(self.sink), show=False)
# 		grid.ingest(0)
#
# 		# set grid shapes
# 		shapes = {(1500, 2500): 'low', (3000, 5000): 'mid', (6000, 10000): 'high'}
#
# 		# set trees, indicess
# 		indices = {}
# 		masks = {}
#
# 		# for each mode:
# 		for mode in shapes.values():
#
# 			latitudeii = grid.grab('latitude_{}'.format(mode))
# 			longitudeii = grid.grab('longitude_{}'.format(mode))
#
# 			# cast as points
# 			points = numpy.vstack([latitudeii.reshape(-1), longitudeii.reshape(-1)]).transpose(1, 0)
#
# 			# create mask for valid points and restrict to boundaries
# 			mask = self._mask(points.sum(axis=1))
# 			mask = mask & (points[:, 0] > south - 1) & (points[:, 0] < north + 1)
# 			mask = mask & (points[:, 1] > west - 1) & (points[:, 1] < east + 1)
#
# 			# build a kd search tree
# 			tree = scipy.spatial.cKDTree(points[mask])
#
# 			# query the tree
# 			_, index = tree.query(targets, k=1)
# 			index = index.flatten()
#
# 			# add to dictionaries
# 			indices[mode] = index
# 			masks[mode] = mask

		# extract datetime from path
		search = '[0-9]{8}T[0-9]{6}Z'
		date = re.findall(search, path)[0]
		brackets = [(0, 4), (4, 6), (6, 8), (9, 11), (11, 13), (13, 15)]
		components = [int(date[bracket[0]: bracket[1]]) for bracket in brackets]
		time = datetime.datetime(*components)

		# begin reservoirs
		radiances = []
		wavelengths = []

		# for each wavelength
		for wave in range(16):

			# connect to abi data
			hydra = Hydra('{}/ABI_CONUS/{}/{}'.format(self.sink, self.stub, self._pad(wave + 1)), show=False)

			# for each path
			times = []
			for pathii in hydra.paths:

				# extract date
				search = 's[0-9]{14}'
				dateii = re.findall(search, pathii)[0]
				year = int(dateii[1:5])
				julian = int(dateii[5:8])
				brackets = [(8, 10), (10, 12), (12, 14)]
				components = [int(dateii[bracket[0]: bracket[1]]) for bracket in brackets]
				timeii = datetime.datetime(year, 1, 1, *components) + datetime.timedelta(days=julian - 1)

				# append to list with path
				times.append((timeii, pathii))

			# sort by closest time
			times.sort(key=lambda pair: abs(pair[0] - time))
			pathii = times[0][1]

			# ingest the path
			hydra.ingest(pathii)

			# get data
			radiance = hydra.grab('Rad')
			quality = hydra.grab('DQF')
			wavelength = hydra.grab('band_wavelength')

			# get geolocation data
			horizontal = hydra.grab('x')
			vertical = hydra.grab('y')

			# get projection information
			projection = hydra.dig('goes_imager_projection')[0].attributes

			# convert into latitude and longitude
			latitudeii, longitudeii = self._project(horizontal, vertical, projection)

			# cast as points
			points = numpy.vstack([latitudeii.reshape(-1), longitudeii.reshape(-1)]).transpose(1, 0)

# 			# remove infinities
# 			finite = numpy.isfinite(points.sum(axis=1))
# 			points = points[finite]

			# create mask for valid points and restrict to boundaries
			mask = self._mask(points.sum(axis=1))
			mask = mask & (points[:, 0] > south - 1) & (points[:, 0] < north + 1)
			mask = mask & (points[:, 1] > west - 1) & (points[:, 1] < east + 1)

			# build a kd search tree
			tree = scipy.spatial.cKDTree(points[mask])

			# query the tree
			_, indices = tree.query(targets, k=1)
			indices = indices.flatten()

# 			# add to dictionaries
# 			indices[mode] = index
# 			masks[mode] = mask

# 			# determine mode from radiance shape
# 			mode = shapes[radiance.shape]

			# flatten and apply geolocation boundary mask
			radiance = radiance.reshape(-1)[mask]
			quality = quality.reshape(-1)[mask]

			# add wavelenth
			wavelengths.append(wavelength[0])

			# apply indices from tree query
			radianceii = radiance[indices]
			qualityii = quality[indices]

			# reapply valid coordinate mask with fill data
			radianceii = numpy.where(valid, radianceii, self.fill)
			qualityii = numpy.where(valid, qualityii, self.fill)

			# reshape interpolations
			radianceii = radianceii.reshape(shape)
			qualityii = qualityii.reshape(shape)

			# remove low quality data and add to array
			radianceii = numpy.where(qualityii == 0, radianceii, self.fill)
			radiances.append(radianceii)

		# craete arrays, transposing radiances
		radiances = numpy.array(radiances).transpose(1, 2, 0)
		wavelengths = numpy.array(wavelengths)

		return radiances, wavelengths

	def chart(self, scan=99, margin=2, variables=None, constrain=False):
		"""Make maps of various variables.

		Arguments:
			scan: int, scan number
			margin: float, percentile margin
			variables: list of str, the variables to plot
			constrain: boolean, apply constraints?

		Returns:
			None
		"""

		# if variables not given
		if not variables:

			# set variables
			variables = ['tropomi_vertical_column_no2', 'tempo_vertical_column_no2']
			variables += ['tropomi_quality_assurance']
			variables += ['reflectance_pcas|1', 'reflectance_pcas|2', 'reflectance_pcas|3']
			variables += ['reflectance_pcas|4', 'reflectance_pcas|5', 'reflectance_pcas|6']
			variables += ['cloud_fraction', 'solar_zenith_angle']
			variables += ['viewing_zenith_angle', 'relative_azimuth_angle']

		# retrieve model and scaling details
		_, _, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make troubleshooting folder
		folderii = '{}/chart'.format(folder)
		self._make(folderii)

		# get data set
		_, _, data, constraints = self.materialize([(self.month, self.day)], (scan, scan + 1))

		# if constraining:
		if constrain:

			# apply constraints
			data = self._constrain(data, constraints)

		# get geo coordinates
		latitude = data['latitude']
		longitude = data['longitude']
		corners = data['latitude_bounds']
		cornersii = data['longitude_bounds']

		# set scales
		bracket = self.limits['scale']
		scales = {'gpp_onefluxnet': (0, 35), 'gpp_fluxsat': (0, 35), 'short_wave': (0, 1000)}
		scales.update({'onefluxnet_deviation': (0, 10), 'gpp_mosaic': (0, 35)})
		scales.update({'tempo_vertical_column_no2': bracket})
		scales.update({'tropomi_vertical_column_no2': bracket})
		scales.update({'tropomi_quality_assurance': (0, 1)})
		scales.update({'tempo_vertical_column_stratosphere': bracket})
		scales.update({'tempo_vertical_column_troposphere': bracket})
		scales.update({'tempo_vertical_uncertainty': bracket})

		# set units
		units = {'assurance': '-'}

		# for each variable
		for variable in variables:

			# default name to variable
			name = variable
			position = None

			# if a position is given
			if '|' in variable:

				# unpack
				name, position = variable.split('|')
				position = int(position)

			# get array
			array = data[name]

			# if a position
			if position is not None:

				# get the subset
				array = array[:, position]

			# begin graphs
			graphs = []

			# create destination
			destination = '{}/{}_{}_{}.png'.format(folderii, self.date, self._pad(scan), variable.replace('|', '_'))

			# construct percent difference plot
			title = '{} {} scan {}\n'.format(variable.replace('|', '_'), self.date, self._pad(scan))
			unit = units.get(variable, self.unit)
			bounds = [(-125, -60), (20, 50)]

			# create valid mask
			mask = self._mask(array).squeeze()

			# set gradient
			gradient = 'viridis'

			# construct scale
			minimum = numpy.percentile(array[mask], margin)
			maximum = numpy.percentile(array[mask], 100 - margin)
			scale = (minimum, maximum)

			# set categories to None
			categories = None

			# if land type
			if variable == 'land_type':

				# adjust scale and gradient
				scale = (0, 16)
				gradient = 'gist_ncar'

				# construct categories
				categories = [('lightblue', 'water'), ('darkgreen', 'egn ndle'), ('green', 'egn brdlf')]
				categories += [('orange', 'dec ndle'), ('salmon', 'dec brdlf'), ('lightgreen', 'mxd frst')]
				categories += [('brown', 'c shrbld'), ('tan', 'o shrbld'), ('khaki', 'wdy svna')]
				categories += [('beige', 'svna'), ('lime', 'grsslnd'), ('aquamarine', 'wetlnd')]
				categories += [('yellow', 'croplnd'), ('gray', 'urban'), ('orchid', 'mosaic')]
				categories += [('ivory', 'snow'), ('lavender', 'barren')]

			# get scale
			scale = scales.get(variable, scale)

			# make plot
			labels = ['', '']
			options = {'polygons': True, 'corners': [corners[mask], cornersii[mask]], 'bar': True, 'categories': categories}
			parameters = (longitude[mask], latitude[mask], array[mask], bounds, scale, gradient, title, unit, labels)
			graph = self._graph(*parameters, **options)
			graphs.append(graph)

			# create plot
			self._paint(destination, graphs)

		return None

	def chronicle(self, history, rates, samples, folder):
		"""Make plots of loss functions.

		Arguments:
			history: model history
			rates: learning rate record
			samples: int, number of training samples
			folder: str, model folder

		Returns:
			None
		"""

		# make folder
		folderii = '{}/chronicle'.format(folder)
		self._make(folderii)

		# get history contents
		contents = history.history

		# for each entry
		for loss, trace in contents.items():

			# plot history
			pyplot.clf()
			pyplot.figure(figsize=(8,8))
			pyplot.plot(trace)
			pyplot.title(loss)
			pyplot.savefig('{}/{}.png'.format(folderii, loss))
			pyplot.clf()

		# translate timepoints to epoches
		epochs = numpy.array(range(len(rates))) * (self.training['batch'] / samples)

		# retain only changing rates for plot
		first = rates[:-1]
		second = rates[1:]
		mask = (first != second)
		maskii = numpy.append(mask, True)
		rates = rates[maskii]
		epochs = epochs[maskii]

		# also plot learning rate
		pyplot.clf()
		pyplot.figure(figsize=(8,8))
		pyplot.plot(epochs, rates)
		pyplot.title('learning rates')
		pyplot.savefig('{}/rates.png'.format(folderii, loss))
		pyplot.clf()

		return None

	def collect(self, scans=(8, 15), granules=(1, 10)):
		"""Collect TROPOMI data from earth data.

		Arguments:
			scans: tuple of ints, the scan bracket
			granules: tuple of ints, the granule bracket

		Returns:
			None
		"""

		# get tropomi nitro data
		self._print('tropomi no2...')
		formats = (self.year, self._pad(self.month), self._pad(self.day))
		stub = '{}{}{}T'.format(*formats)
		pattern = stub + '[0-9]{6}_' + stub + '[0-9]{6}_'
		self._access('S5P_L2__NO2____HiR', pattern=pattern)

		# get tempo nitro data
		self._print('tempo no2...')
		labels = [self._label(scan, granule) for scan in range(*scans) for granule in range(*granules)]
		self._access('TEMPO_NO2_L2', labels=labels, count=300)

		return None

	def compare(self, scan=99, large=False):
		"""Compare the TROPOMI model predictions with TEMPO truth.

		Arguments:
			scan: int, the scan number
			large: boolean, make large version of graphs?

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folder
		folderii = '{}/comparison'.format(folder)
		self._make(folderii)

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, features, data, constraints = self.materialize(*parameters, features=features, constrain=True)

		# mask out infinities
		mask = self._mask(matrix[:, :-1].sum(axis=1))
		matrix = matrix[mask]
		data = self._constrain(data, constraints)
		data = self._constrain(data, mask)

		# make predictions
		prediction, deviation, _, _ = self.predict(model, matrix, details)

		# using tempo data as truth
		truth = data['tempo_vertical_column_no2'].squeeze()

		# remove nans
		mask = self._mask(prediction) & self._mask(truth)
		truth = truth[mask]
		prediction = prediction[mask]

		# constrain data
		data = self._constrain(data, mask)

		# print r^2 score
		score = r2_score(truth, prediction)
		self._print('score: {}'.format(score))

		# begin status
		formats = (folderii, self.date, self._pad(scan))
		destination = '{}/tropomi_model_tempo_no2_column_comparison_{}_{}.png'.format(*formats)
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase preevious copy
		self._clean(destination, force=True)

		# calculate r2 score
		score = r2_score(truth, prediction)

		# calculate regression line
		slope, intercept, _, _, _ = scipy.stats.linregress(truth, prediction)

		# create titles
		date = str(data['date'][0][0])
		time = str(data['time'][0][0])
		date = '{}m{}{}'.format(date[:4], date[4:6], date[6:8])
		time = '{}:{} CST'.format(int(time[:2]) - 6, time[2:4])
		# titles = ['{} TEMPO {}\n'.format(self.target, date)]
		# titles += ['{} Prediction {} {}\n'.format(self.target, date, time)]
		# titles += ['Prediction vs TEMPO\n( R^2 = {} )'.format(self._round(score, 2))]
		# titles += ['Percent Difference\n']

		# calculate differences
		difference = prediction - truth

		# set bracket
		bracket = self.limits['scale']

		# calculate sample bins
		bins = 1000
		options = {'statistic': 'count', 'range': (bracket, bracket), 'bins': bins}
		counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(truth, prediction, None, **options)
		truths = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
		predictions = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
		mesh, meshii = numpy.meshgrid(truths, predictions, indexing='ij')

		# apply mask
		mask = counts > 0
		counts = counts[mask]
		mesh = mesh[mask]
		meshii = meshii[mask]

		# clip counters to 100
		samples = 100
		counts = numpy.where(counts > samples, samples, counts)

		# order counters
		order = numpy.argsort(counts)
		counts = counts[order]
		mesh = mesh[order]
		meshii = meshii[order]

		# get minimum and maximum tracer values
		minimum = min([truth.min(), prediction.min()])
		maximum = max([truth.max(), prediction.max()])

		# set percent bracket
		absolute = max([abs(numpy.percentile(difference, 2)), abs(numpy.percentile(difference, 98))])

		# set bounds
		bounds = [(-125, -60), (15, 55)]
		scale = (minimum, maximum)
		scale = self.limits['scale']
		scaleii = (-absolute, absolute)

		# begin graphs
		graphs = []

		# get data
		latitude = data['latitude']
		longitude = data['longitude']
		corners = data['latitude_bounds']
		cornersii = data['longitude_bounds']

		# construct GPP truth plot
		title = '{} TEMPO {} {}\n'.format(self.target, date, time)
		unit = self.unit
		labels = ['', '']
		options = {'polygons': True, 'corners': [corners, cornersii]}
		graph = self._graph(longitude, latitude, truth, bounds, scale, 'viridis', title, unit, labels, **options)
		graphs.append(graph)

		# construct GPP prediction plot
		title = '{} Prediction {} {}\n'.format(self.target, date, time)
		unit = self.unit
		labels = ['', '']
		options = {'polygons': True, 'corners': [corners, cornersii]}
		graph = self._graph(longitude, latitude, prediction, bounds, scale, 'viridis', title, unit, labels, **options)
		graphs.append(graph)

		# # construct rgb plot
		# colors = data['rgb']
		# colors = numpy.where(colors > 1, 1, colors)
		# colors = numpy.where(colors < 0, 0, colors)
		# title = 'RGB Map {} {}\n'.format(date, time)
		# unit = self.unit
		# labels = ['', '']
		# options = {'polygons': True, 'corners': [corners, cornersii], 'globe': True, 'colorbar': False}
		# options.update({'reflectivity': True, 'alpha': 1.0})
		# # options.update({'reflectivity': True, 'ticks': tick, 'alpha': alpha})
		# # options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
		# parameters = (longitude, latitude, colors, bounds, scaleii, 'plasma', title, unit, labels)
		# graph = self._graph(*parameters, **options)
		# graphs.append(graph)

		# construct samples title depending on instances
		title = 'Prediction vs TEMPO ( 1 instance )\n( R^2 = {} )'.format(self._round(score, 2))
		if self.instances > 1:

			# add instances
			formats = (self.instances, self._round(score, 2))
			title = 'Prediction vs TEMPO ( avg of {} instances )\n( R^2 = {} )'.format(*formats)

		# construct samples graph
		unit = 'samples'
		labels = ['{} truth'.format(self.target), '{} prediction'.format(self.target)]
		boundsii = [self.limits['scale'],  self.limits['scale']]
		scaleiii = (1, 100)
		points = [0, truth.max()]
		regression = [intercept + point * slope for point in points]
		line = (points, points, 'k-')
		lineii = (points, regression, 'k--')
		lines = [line, lineii]
		# lines = [line]
		options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.01, 'polygons': False}
		graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
		graphs.append(graph)

		# construct percent difference plot
		title = 'Difference between Model and TEMPO\n'
		unit = self.unit
		labels = ['', '']
		options = {'polygons': True, 'corners': [corners, cornersii]}
		graph = self._graph(longitude, latitude, difference, bounds, scaleii, 'coolwarm', title, unit, labels, **options)
		graphs.append(graph)

		# create plot
		self._paint(destination, graphs)

		# if large plots:
		if large:

			# for each graph
			for index, graph in enumerate(graphs):

				# plot each one
				self._paint(destination.replace('.png', '_{}.png'.format(index)), [graph], size=(16, 16))

		return None

	def correlate(self, scan=11):
		"""Compute the correlation amongst all feature pairs.

		Arguments:
			scan: int, scan number

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make reports folder
		self._make('{}/reports'.format(folder))

		# get training matrix for particular scan
		features = details['features'].tolist()
		matrix, features, _, _ = self.materialize([(self.month, self.day)], (scan, scan + 1), features=features)

		# get scaling information
		minimum = details['input_min']
		maximum = details['input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		features = features[:-1]
		truth = matrix[:, -1]
		inputs = (matrix[:, :-1] - minimum) / scale

		# begin report and scores collection
		report = ['correlation report for {}\n'.format(folder)]
		scores = []

		# compute correlation
		correlation = numpy.corrcoef(inputs, rowvar=False)

		# for each row
		for row in range(correlation.shape[0]):

			# and each column
			for column in range(row + 1, correlation.shape[1]):

				# add to scores
				triplet = (correlation[row, column], features[row], features[column])
				scores.append(triplet)

		# sort scores
		scores.sort(key=lambda triplet: triplet[0], reverse=True)

		# compute feature string lengths
		lengths = [len(feature) for feature in features]
		lengths.sort(reverse=True)
		longest = lengths[0]

		# add to report
		for score, feature, featureii in scores:

			# compute spacing
			space = ' ' * (longest - len(feature))

			# add entry
			report.append('{:.3f}: {}{}x   {}'.format(score, feature, space, featureii))

		# jot report
		self._jot(report, '{}/reports/correlation.txt'.format(folder))

		return None

	def cosine(self, scan=99, fraction=0.5):
		"""compute the cosine similarity score amonst all matrices.

		Arguments:
			scan: int, scan number
			fraction: float, fraction of data to use

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make shapely folder
		folderii = '{}/cosine'.format(folder)
		self._make(folderii)

		# collect training days
		days = self._daze(validation=False)
		# days.sort()

		# begin matrices and scores
		matrices = []
		correlations = []

		# for each day
		for month, day in days:

			# get training matrix for particular scan
			features = details['features'].tolist()
			parameters = [(month, day)], (scan, scan + 1)
			matrix, features, data, constraints = self.materialize(*parameters, features=features)

			# get r squared score
			_, _, _, correlation = self.predict(model, matrix, details)
			correlations.append(correlation)

			# get scaling information
			minimum = numpy.hstack([details['input_min'], numpy.array([details['output_min']])])
			maximum = numpy.hstack([details['input_max'], numpy.array([details['output_max']])])
			scale = maximum - minimum

			# scale matrix
			matrix = (matrix - minimum) / scale

			# add to matrices
			matrices.append(matrix)

		# determine sample size from smallest matrix
		lengths = [len(matrix) for matrix in matrices]
		rows = int(min(lengths) * fraction)

		# begin samples
		samples = []

		# for each matrix
		for matrix in matrices:

			# Randomly select 50 unique row indices from 0 to 99 without replacement
			indices = numpy.random.choice(matrix.shape[0], rows, replace=False)

			# Get the random sampling of 50 unique rows
			sample = matrix[indices]
			samples.append(sample)

		# begin cosine similarity scores and euclidian scores
		scores = numpy.ones((len(days), len(days)))
		scoresii = numpy.ones((len(days), len(days)))

		# for each matrix
		for index in range(len(days)):

			# and each second matrix
			for indexii in range(len(days)):

				# flatten samples
				flat = samples[index].reshape(1, -1)
				flatii = samples[indexii].reshape(1, -1)

				# compute cosine similarity and euclidean distance
				score = cosine_similarity(flat, flatii)
				scoreii = pairwise_distances(flat, flatii, metric='euclidean')

				# add to matrices
				scores[index][indexii] = score
				scoresii[index][indexii] = scoreii

		# clip cosine similarity to highest non diagonal score
		xerox = scores.copy()
		numpy.fill_diagonal(xerox, 0)
		highest = xerox.max()
		scores = numpy.where(scores >= highest, highest, scores)

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# plot the matrix
		pyplot.imshow(scores, cmap='viridis')

		# add color bar
		pyplot.colorbar(label='Cosine Similarity')

		# add labels
		labels = ['{} ({:.2f})'.format(day, correlation) for day, correlation in zip(days, correlations)]
		pyplot.xticks(ticks=numpy.arange(len(labels)), labels=labels)
		pyplot.yticks(ticks=numpy.arange(len(labels)), labels=labels)

		# add title
		pyplot.title('Cosine similarity')

		# save the plot
		pyplot.savefig('{}/cosine_similarity.png'.format(folderii))
		pyplot.clf()

		# clip euclidean  to highest non diagonal score
		xeroxii = scoresii.copy()
		numpy.fill_diagonal(xeroxii, 1e9)
		lowest = xeroxii.min()
		scoresii = numpy.where(scoresii <= lowest, lowest, scoresii)

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# plot the matrix
		pyplot.imshow(scoresii, cmap='viridis_r')

		# add color bar
		pyplot.colorbar(label='Euclidean Distance')

		# add labels
		labels = ['{} ({:.2f})'.format(day, correlation) for day, correlation in zip(days, correlations)]
		pyplot.xticks(ticks=numpy.arange(len(labels)), labels=labels)
		pyplot.yticks(ticks=numpy.arange(len(labels)), labels=labels)

		# add title
		pyplot.title('Euclidean Distance')

		# save the plot
		pyplot.savefig('{}/euclidean_distance.png'.format(folderii))
		pyplot.clf()

		# get all deviations
		deviations = numpy.array([matrix.std(axis=0) for matrix in matrices])

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# plot the matrix
		pyplot.imshow(deviations, cmap='viridis', aspect=5)

		# add color bar
		pyplot.colorbar(label='Standard Deviation')

		# add labels
		labels = ['{} ({:.2f})'.format(day, correlation) for day, correlation in zip(days, correlations)]
		pyplot.xlabel('features')
		pyplot.yticks(ticks=numpy.arange(len(labels)), labels=labels)

		# add title
		pyplot.title('Feature standard deviations')

		# save the plot
		pyplot.savefig('{}/deviations.png'.format(folderii))
		pyplot.clf()

		return None

	def cross(self, scan=99, latitude=31, longitude=-105, secondary=None, position=0, bounds=None):
		"""Make a longitudinal cross section plot of model results.

		Arguments:
			scan: int, scan number
			latitude: float, the closest latitude
			longitude: float, the closest longitude
			bounds: longitude bounds

		Returns:
			None
		"""

		# set default bounds
		bounds = bounds or (-120, -80)

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folders
		self._make('{}/cross'.format(folder))

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, features, data, constraints = self.materialize(*parameters, features=features)

		# apply constraints to data
		data = self._constrain(data, constraints)

		# get scanline closest to latitude
		index = self._pin([latitude, longitude], [data['latitude'].squeeze(), data['longitude'].squeeze()])[0][0]
		track = int(data['track'][index][0])

		# get all data at scanline
		line = (data['track'].squeeze() == track)
		matrix = matrix[line]

		# organize by longitude
		longitude = data['longitude'].squeeze()[line]
		order = numpy.argsort(longitude)
		longitude = longitude[order]
		matrix = matrix[order]

		# make predictions
		prediction, deviation, truth, _ = self.predict(model, matrix, details)

		# if secondary variable
		if secondary:

			# get the data
			tracer = data[secondary][:, position][line][order]

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# add text
		formats = (features[-1], self._orient(latitude), track, self.date)
		pyplot.title('Model vs {} for latitude {}, track {}, {}'.format(*formats))
		pyplot.xlabel('longitude')
		pyplot.ylabel('{} ({})'.format(self.target, self.unit))

		# set bounds
		pyplot.xlim(*bounds)
		pyplot.ylim(*self.limits['scale'])

		# plot truth
		pyplot.plot(longitude, truth, 'b-', linewidth=1, label='truth')

		# plot prediction
		pyplot.plot(longitude, prediction, 'g-', linewidth=1, label='prediction')

		# add legend
		pyplot.legend(loc='upper left')

		# if secondary axis
		label = '_'
		if secondary:

			# create secondary axis
			axisii = pyplot.gca().twinx()

			# plot row anomaly on secondary
			label = '{}_{}'.format(secondary, self._pad(position))
			axisii.plot(longitude, tracer, 'r-', label=label)
			axisii.set_ylabel(label)
			axisii.set_xlim(*bounds)

			# create legend
			axisii.legend(loc='upper right')

		# plot deviations
# 		pyplot.plot(longitude, mean + deviation, 'g--')
# 		pyplot.plot(longitude, mean - deviation, 'g--')

		# save
		formats = (folder, *formats, label)
		pyplot.savefig('{}/cross/CrossSection_{}_{}_{}_{}_{}.png'.format(*formats))
		pyplot.clf()

		return None

	def decompose(self, scans=(9, 12)):
		"""Generate pca coefficients from smoothed radiances.

		Arguments:
			scans: tuple int, the scan range bracket

		Returns:
			None
		"""

		# create folder
		folder = '{}/Nitro_PCs'.format(self.sink)
		self._make(folder)

		# get all days referenced
		days = self._daze()

		# retrieve model and scaling details
		_, details, _ = self._retrieve()

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = (days, scans)
		options = {'granules': (3, 7), 'features': features, 'reservoir': 'GPP_Inputs'}
		_, _, data, constraints = self.materialize(*parameters, **options)

		# reduce data to needed fiels
		data = {field: data[field] for field in ('smoothed_reflectances', 'reflectance_wavelengths')}

		# apply constraints
		data = self._constrain(data, constraints)

		# get data
		reflectance = data['smoothed_reflectances']
		wavelength = data['reflectance_wavelengths'][0]

		print(reflectance.shape)
		print(wavelength.shape)

		# get mask for wavelength range, only using high frequency waves
		waves = (wavelength >= self.limits['high'][0]) & (wavelength <= self.limits['high'][1])

		print(waves.shape)

		# subset reflectances
		reflectance = reflectance[:, waves]

		print(reflectance.shape)

		# fit PCA on inputs and get coefficients
		decomposer = PCA(n_components=waves.sum())
		decomposer.fit(reflectance)
		coefficients = decomposer.components_.transpose(1, 0)

		# create PCs file
		destination = '{}/Nitro_PCs.h5'.format(folder)
		dataii = {'no2_pca_coefficients': coefficients, 'no2_pca_wavelengths': wavelength[waves]}
		self.spawn(destination, dataii)

		return None

	def diagnose(self, truth, prediction, data, folder):
		"""Create plots of percent error vs parameter.

		Arguments:
			truth: numpy array, gpp truth
			prediction: numpy array, model prediction
			data: dict, associated data

		Returns:
			None
		"""

		# make folder
		folderii = '{}/diagnosis'.format(folder)
		self._make(folderii)

		# calculate gpp difference
		difference = prediction - truth

		# set fields
		fields = ['latitude', 'longitude', 'snow_fraction', 'cloud_fraction']
		fields += ['solar_zenith_angle', 'viewing_zenith_angle', 'relative_azimuth_angle']
		fields += ['land_type', 'mirror', 'track']
		fields += ['max_irr_quality_high', 'max_irr_quality_low']
		fields += ['max_rad_quality_high', 'max_rad_quality_low']
		fields += ['tropomi_vertical_column_no2', 'tempo_vertical_column_no2']
		fields += ['tropomi_quality_assurance']

		# for each variable
		for field in fields:

			# crete plot destination
			destination = '{}/Diagnosis_{}_{}.png'.format(folderii, self.date, field)

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8,8))

			# construct date
			date = str(data['date'][0][0])
			date = '{}m{}{}'.format(date[:4], date[4:6], date[6:8])
			time = str(data['time'][0][0])
			time = '{}:{} CST'.format(int(time[:2]) - 6, time[2:4])
			scan = 'S{}'.format(self._pad(data['scan'][0][0], 3))

			# create title
			title = '{} {} {} {}\n Model - TROPOMI vs {}'.format(self.target, date, time, scan, field)
			pyplot.title(title)

			# add labels
			pyplot.xlabel(field)
			pyplot.ylabel('{} difference'.format(self.target))

			# plot data
			pyplot.plot(data[field], difference, 'g.', markersize=0.2)

			# save plot
			pyplot.savefig(destination)
			pyplot.clf()

		return None

	def err(self, scan=99):
		"""Run a random forest against the model error.

		Arguments:
			scan: int, scan number

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folders
		self._make('{}/reports'.format(folder))

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, features, data, constraints = self.materialize(*parameters, features=features)

		# construct valid data
		valids = self._constrain(data, constraints)

		# make predictions
		prediction, deviation, truth, _ = self.predict(model, matrix, details)

		# add difference to valids
		valids.update({'error': (prediction - valids['tropomi_vertical_column_no2'].squeeze()).reshape(-1, 1)})

		# remove all 1-d arrays
		valids = {field: array for field, array in valids.items() if array.shape[0] == prediction.shape[0]}

		# view data
		self._view(valids)

		# get secondary axis sizes
		sizes = {field: array.shape[1] for field, array in valids.items()}

		# begin training matrix
		train = {}

		# for each field
		for field, array in valids.items():

			# add to training
			train.update({'{}|{}'.format(field, self._pad(index)): array[:, index] for index in range(sizes[field])})

		# create report destination and plant tree
		self.plant(train, 'error|00', '{}/reports/no2_error_random_forest.txt'.format(folder))

		return None

	def exhibit(self, scan=99, dependent=None, independent=None):
		"""Visualize all datasets using PCA and different colors for each.

		Arguments:
			scan: int, scan number
			variables: list of str, dependent and independent variables for scatters

		Returns:
			None
		"""

		# set default variables
		dependent = dependent or 'tropomi_vertical_column_no2'
		independent = independent or 'reflectance_pcas|05'

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make shapely folder
		folderii = '{}/exhibit'.format(folder)
		self._make(folderii)

		# collect training days
		days = self._daze(validation=False)

		# begin matrices and scores
		matrices = []
		scores = []

		# set colors
		colors = ['magenta', 'red', 'orange', 'lightgreen']
		colors += ['green', 'lightblue', 'blue', 'violet']
		colors += ['magenta', 'red', 'orange', 'lightgreen']
		colors += ['indigo', 'black', 'indigo']

		# for each day
		for month, day in days:

			# get training matrix for particular scan
			features = details['features'].tolist()
			parameters = [(month, day)], (scan, scan + 1)
			matrix, features, data, constraints = self.materialize(*parameters, features=features)

			# add to matrices
			matrices.append(matrix)

			# get score
			_, _, _, score = self.predict(model, matrix, details)
			scores.append(score)

		# get scaling information
		minimum = details['input_min']
		maximum = details['input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		inputs = [(matrix[:, :-1] - minimum) / scale for matrix in matrices]

		# create block of all inputs
		block = numpy.vstack(inputs)

		print(block.shape)

		# fit PCA on inputs and get coefficients
		decomposer = PCA(n_components=2)
		decomposer.fit(block)
		coefficients = decomposer.components_.transpose(1, 0)

		# get representation
		representations = [numpy.dot(matrix, coefficients) for matrix in inputs]

		# make labels
		labels = self._represent(coefficients, features)

		# get minimum and maximums
		margin = 0.2
		minimum = min([representation[:, 0].min() for representation in representations]) - margin
		minimumii = min([representation[:, 1].min() for representation in representations]) - margin
		maximum = max([representation[:, 0].max() for representation in representations]) + margin
		maximumii = max([representation[:, 1].max() for representation in representations]) + margin

		# store components
		data = {'2d_pca': coefficients}
		data.update({'minimum': minimum, 'maximum': maximum, 'minimumii': minimumii, 'maximumii': maximumii})
		self._make('{}/pca'.format(folder))
		self.spawn('{}/pca/2d_pca_components.h5'.format(folder), data)

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# for each dataset
		for index, representation in enumerate(representations):

			# Sample data
			abscissa = representation[:, 0]
			ordinate = representation[:, 1]

			# create scatter plot
			pyplot.scatter(abscissa, ordinate, c=colors[index], cmap='viridis', s=0.1, label=days[index])

		# make labels
		pyplot.xlabel('PCA 0 ( {} )'.format(labels[0]))
		pyplot.ylabel('PCA 1 ( {} )'.format(labels[1]))
		pyplot.title('PCA components vs day, scan {}'.format(scan))
		pyplot.legend(markerscale=20)

		# save
		pyplot.savefig('{}/pca_exhibit.png'.format(folderii))
		pyplot.clf()

		# begin median plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# for each dataset
		for index, representation in enumerate(representations):

			# Sample data
			abscissa = numpy.percentile(representation[:, 0], 50).reshape(-1)
			ordinate = numpy.percentile(representation[:, 1], 50).reshape(-1)

			# create scatter plot
			pyplot.scatter(abscissa, ordinate, c=colors[index], cmap='viridis', s=20, label=days[index])

			# annotate
			text = '{} ( {:.2f} )'.format(days[index], scores[index])
			pyplot.annotate(xy=(abscissa,  ordinate), text=text)

		# make labels
		pyplot.xlabel('PCA 0 ( {} )'.format(labels[0]))
		pyplot.ylabel('PCA 1 ( {} )'.format(labels[1]))
		pyplot.title('Median PCA components vs day, scan {}'.format(scan))
		# pyplot.legend(markerscale=1)

		# save
		pyplot.savefig('{}/pca_exhibit_median.png'.format(folderii))
		pyplot.clf()

		# begin scatter plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# for each dataset
		for index, matrix in enumerate(matrices):

			# Sample data
			abscissa = matrix[:, features.index(independent)]
			ordinate = matrix[:, features.index(dependent)]

			# create scatter plot
			pyplot.scatter(abscissa, ordinate, c=colors[index], cmap='viridis', s=1, label=days[index])

			# calculate regression line
			slope, intercept, _, _, _ = scipy.stats.linregress(abscissa, ordinate)

			# plot line
			horizontal = [abscissa.min(), abscissa.max()]
			vertical = [intercept + slope * point for point in horizontal]
			pyplot.plot(horizontal, vertical, linestyle='dashed', color=colors[index])

		# make labels
		pyplot.xlabel(independent)
		pyplot.ylabel(dependent)
		pyplot.title('{} vs {}, scan {}'.format(dependent, independent, scan))
		pyplot.legend(markerscale=10)

		# save
		pyplot.savefig('{}/pca_exhibit_scatters_{}.png'.format(folderii, independent))
		pyplot.clf()

		return None

	def explain(self, scan=99, samples=500):
		"""Perform shapley study.

		Arguments:
			scan: int, scan number
			samples: int, number of samples to use

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make shapely folder
		self._make('{}/explanation'.format(folder))

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, features, _, _ = self.materialize(*parameters, features=features)

		# get scaling information
		minimum = details['input_min']
		maximum = details['input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		truth = matrix[:, -1]
		inputs = (matrix[:, :-1] - minimum) / scale

		# get pca coefficients
		coefficients = details['pca_coefficients']

		# pare down sample number
		numpy.random.seed(42)
		xerox = inputs.copy()
		indices = numpy.random.choice(xerox.shape[0], size=samples, replace=False)
		xerox = xerox[indices, :]

		# define modeler
		def modeling(matrix): return model(numpy.dot(matrix, coefficients).astype('float32'))

		# create explainer
		explainer = shap.Explainer(modeling, xerox)
		importance = explainer(xerox)
		importance.feature_names = features[:-1]

		# set plot size
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# for ten samples
		for sample in range(10):

			# visualize decision plot
			shap.plots.decision(importance.base_values[sample], importance.data, feature_names=importance.feature_names)
			pyplot.savefig('{}/explanation/decision_{}.png'.format(folder, sample), bbox_inches='tight')
			pyplot.clf()

			# visualize waterfall plot
			shap.plots.waterfall(importance[sample], max_display=20)
			pyplot.savefig('{}/explanation/waterfall_{}.png'.format(folder, sample), bbox_inches='tight')
			pyplot.clf()

# 		# for each Feature
# 		for name in features[:-1]:
#
		# make scatter plot
		shap.plots.scatter(importance[:, 0], color=importance)
		pyplot.savefig('{}/explanation/scatter.png'.format(folder), bbox_inches='tight')
		pyplot.clf()

		# visualize beeswarm plot
		shap.plots.beeswarm(importance, max_display=20)
		pyplot.savefig('{}/explanation/beeswarm.png'.format(folder), bbox_inches='tight')
		pyplot.clf()

		# visualize bar plot
		shap.plots.bar(importance, max_display=20)
		pyplot.savefig('{}/explanation/bar.png'.format(folder), bbox_inches='tight')
		pyplot.clf()

		return explainer, importance

	def grid(self, neighbors=True, number=5, radius=0.2):
		"""Create the high resolution nitrogen grid from tropomi files.

		Arguments:
			neighbors: boolean, use kdTree with nearest neighbors?
			number: int, number of nearest neighbors ( k )
			radius: float, radius of distances to use

		Returns:
			tuple of arrays, the latitude nodes, longitude nodes, column grid, and slant grid
		"""

		# get data
		data, mask = self.nitrify()

		# apply mask
		column = data['vertical_column'][mask]
		slant = data['slant_column'][mask]
		latitude = data['latitude'][mask]
		longitude = data['longitude'][mask]
		assurance = data['assurance'][mask]
		time = data['time'][mask]

		# if using kdttree:
		if neighbors:

			# construct latitude and longitude nodes at 0.05 degree spacing
			size = 0.05
			half = size / 2
			latitudes = numpy.array([-90 + half + size * index for index in range(int(180 / size))])
			longitudes = numpy.array([-180 + half + size * index for index in range(int(360 / size))])
			shape = (len(latitudes), len(longitudes))

			# create mesh grid
			mesh, meshii = numpy.meshgrid(latitudes, longitudes, indexing='ij')

			# create a KDTree from good latitude and longitude data
			tree = scipy.spatial.cKDTree(numpy.column_stack([latitude, longitude]))

			# query the tree for grid
			distances, indices = tree.query(numpy.column_stack([mesh.flatten(), meshii.flatten()]), k=number)

			# # create regular grid from nearest neighbors
			# columns = column[indices.flatten()].reshape(shape)
			# slants = slant[indices.flatten()].reshape(shape)
			# assurances = assurance[indices.flatten()].reshape(shape)

			# create regular grid from average of nearest neighbors
			columns = self._average(column, indices, distances, radius).reshape(shape)
			slants = self._average(slant, indices, distances, radius).reshape(shape)
			assurances = self._average(assurance, indices, distances, radius).reshape(shape)
			times = self._average(time, indices, distances, radius).reshape(shape)

		# otherwise:
		else:

			# construct latitude and longitude nodes at 0.05 degree spacing
			size = 0.1
			half = size / 2
			latitudes = numpy.array([-90 + half + size * index for index in range(int(180 / size))])
			longitudes = numpy.array([-180 + half + size * index for index in range(int(360 / size))])
			shape = (len(latitudes), len(longitudes))

			# define geographic range
			brackets = [(-90, 90), (-180, 180)]
			options = {'range': brackets, 'bins': list(shape)}

			# compute binned statistics for column
			sums, _, _, _ = scipy.stats.binned_statistic_2d(latitude, longitude, column, 'sum', **options)
			counts, _, _, _ = scipy.stats.binned_statistic_2d(latitude, longitude, column, 'count', **options)
			columns = sums / counts

			# compute binned statistics for slant column
			sums, _, _, _ = scipy.stats.binned_statistic_2d(latitude, longitude, slant, 'sum', **options)
			counts, _, _, _ = scipy.stats.binned_statistic_2d(latitude, longitude, slant, 'count', **options)
			slants = sums / counts

			# compute binned statistics for quality assurance
			sums, _, _, _ = scipy.stats.binned_statistic_2d(latitude, longitude, assurance, 'sum', **options)
			counts, _, _, _ = scipy.stats.binned_statistic_2d(latitude, longitude, assurance, 'count', **options)
			assurances = sums / counts

			# compute binned statistics for times
			sums, _, _, _ = scipy.stats.binned_statistic_2d(latitude, longitude, time, 'sum', **options)
			counts, _, _, _ = scipy.stats.binned_statistic_2d(latitude, longitude, time, 'count', **options)
			times = sums / counts

		return latitudes, longitudes, columns, slants, assurances, times

	def histolyze(self, scan=99, variable='tropomi_vertical_column_no2', position=0, bins=50, limits=None, constrain=False):
		"""Generate a histogram of a variable:

		Arguments:
			scan: int, the scan number
			variable: str, variable name
			position: int, the index of a multi-column variable
			bins: int, number of bins
			limits: tuple of floats, the limits of the data
			constrain: apply constraints?

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make shapely folder
		folderii = '{}/histograms'.format(folder)
		self._make(folderii)

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = [(self.month, self.day)], (scan, scan + 1)
		_, _, data, constraints = self.materialize(*parameters, features=features)

		# if constraining
		if constrain:

			# apply constraints
			data = self._constraint(data, constraints)

		# get data
		datum = data[variable][:, position]

		# remove nans
		mask = numpy.isfinite(datum)
		datum = datum[mask]

		# if given a limit
		if limits:

			# apply limit
			mask = (datum >= limits[0]) & (datum <= limits[1])
			datum = datum[mask]

		self._print(datum.min(), datum.max())
		self._print(datum.shape)

		# Create a histogram
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))
		pyplot.hist(datum, bins=bins, edgecolor='black')

		# Add titles and labels
		title = 'histogram of {}:{} for {}, scan {}'.format(variable, position, self.date, scan)
		pyplot.title(title)
		pyplot.xlabel('value')
		pyplot.ylabel('frequency')

		# save the plot
		formats = (folderii, variable, self.date, self._pad(scan), self._pad(position))
		destination = '{}/histogram_{}_{}_{}_{}.png'.format(*formats)
		pyplot.savefig(destination)
		pyplot.clf()

		# print status
		self._print('plotted {}'.format(destination))

		return None

	def inspect(self, scan=99, bounds=[(31, 34), (-106, -100)], variables=None, margin=1.5):
		"""Inspect the model around a validation point with high error.

		Arguments:
			scan: int, scan number
			bounds: list of float tuples, the geographic bounds
			variables: list of str, the variables to check
			margin: int, percent margin

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make shapely folder
		folderii = '{}/inspection'.format(folder)
		self._make(folderii)

		# collect training days
		days = self.training['days']

		# get training matrix
		features = details['features'].tolist()
		parameters = (days, (scan, scan + 1))
		matrix, features, data, constraints = self.materialize(*parameters, features=features)

		# set default variables
		variables = variables or features[:-1]

		# get specific day matrix
		features = details['features'].tolist()
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrixii, features, dataii, constraintsii = self.materialize(*parameters, features=features)
		dataii = self._constrain(dataii, constraintsii)

		# reduce matrixii to bounding box
		mask = (dataii['latitude'] >= bounds[0][0]) & (dataii['latitude'] <= bounds[0][1])
		mask = mask & (dataii['longitude'] >= bounds[1][0]) & (dataii['longitude'] <= bounds[1][1])
		mask = mask.squeeze()
		matrixii = matrixii[mask]

		# predict values from the model
		predictions, _, truths, _ = self.predict(model, matrixii, details)

		# get error between prediction and truth
		error = predictions - truths

		# get the sample with highest absolute error, and corresponding matrix row
		sample = numpy.argmax(abs(error))
		row = matrixii[sample]
		prediction = predictions[sample]
		truth = truths[sample]
		latitude = dataii['latitude'].squeeze()[mask][sample]
		longitude = dataii['longitude'].squeeze()[mask][sample]
		self._print('latitude, longitude: {}, {}'.format(latitude, longitude))

		# for each variable
		for variable in variables:

			# get the index of the feature
			index = features.index(variable)

			# create percentiles
			percentiles = numpy.array([numpy.percentile(matrix[:, index], percentile) for percentile in range(101)])

			# create probe matrix from medians for all features but variable in question
			probe = numpy.array([row.copy()] * 101)
			probe[:, index] = percentiles

			# predict values from the model
			predictionii, deviationii, _, _ = self.predict(model, probe, details)

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8, 8))

			# create title
			formats = (self.date, self._orient(latitude), self._orient(longitude, east=True), variable)
			title = '{} at {}, {}, {}'.format(*formats)
			pyplot.title(title)

			# add labels
			pyplot.xlabel(variable)
			pyplot.ylabel(features[-1])

			# set limits
			pyplot.ylim(self.limits['scale'][0], truth + 0.0001)

			# Sample data
			abscissa = percentiles
			ordinate = predictionii

			# create line plots
			pyplot.plot(abscissa, ordinate, 'co-', label='model')
			pyplot.plot(abscissa, ordinate - deviationii, 'c--')
			pyplot.plot(abscissa, ordinate + deviationii, 'c--')

			# plot prediction and truth
			pyplot.plot([row[index]], [truth], 'gX', markersize=15, label='truth')
			pyplot.plot([row[index]], [prediction], 'rX', markersize=15, label='prediction')

			# copy matrix
			xerox = matrix.copy()

			# # get percents
			# lower = numpy.percentile(xerox, 50 - margin, axis=0)
			# upper = numpy.percentile(xerox, 50 + margin, axis=0)

			# get standard deviation
			deviation = xerox.std(axis=0)

			# for each feature
			for position, feature in enumerate(features[:-1]):

				# if feature not varible of interest
				if feature != variable:

					# subset matrix
					buffer = deviation[position] * margin
					mask = (xerox[:, position] >= row[position] - buffer)
					mask = mask & (xerox[:, position] <= row[position] + buffer)
					xerox = xerox[mask]

			# print final sahape
			self._print('{} samples: {}'.format(variable, xerox.shape))

			# calculate differences from median for color scale
			difference = abs(xerox - row)
			difference[:, index] = 0
			difference = difference.sum(axis=1)

			# add truth points within median margins
			label = 'samples +/- {} std'.format(str(margin))
			pyplot.scatter(xerox[:, index], xerox[:, -1], c=difference, cmap='gray', s=25, marker='x', label=label)

			# add legend
			pyplot.legend(loc='lower left')

			# save plot
			destination = '{}/inspection_{}_{}_{}_{}.png'.format(folderii, *formats)
			pyplot.savefig(destination)
			pyplot.clf()

		return None

	def inventory(self, products=None):
		"""Create inventory of training day holdings.

		Arguments:
			products: list of str, the directories

		Returns:
			None
		"""

		# make inventory folder
		self._make('{}/Inventory'.format(self.sink))

		# set default folders
		products = ['GPP_Inputs', 'TEMPO_RAD_L1', 'TEMPO_CLDO4_L2', 'TEMPO_NO2_L2', 'Nitro_Inputs']
		products += ['Synthetic_GPP', 'Mosaic_GPP']

		# maximum length of products
		lengths = [len(product) for product in products]
		maximum = max(lengths)

		# begin inventory
		inventory = []

		# for each product folder
		for product in products:

			# get list of year subfolders
			years = self._see('{}/{}'.format(self.sink, product))
			years = [self._file(folder) for folder in years]

			# for each year subfolder
			for year in years:

				# get month folders
				months =  self._see('{}/{}/{}'.format(self.sink, product, year))
				months = [self._file(folder) for folder in months]

				# for each month
				for month in months:

					# get day folders
					days = self._see('{}/{}/{}/{}'.format(self.sink, product, year, month))
					days = [self._file(folder) for folder in days]

					# for each day
					for day in days:

						# make hydra
						formats = (self.sink, product, year, self._pad(month), self._pad(day))
						hydra = Hydra('{}/{}/{}/{}/{}'.format(*formats), show=False)

						# get all granules
						granules = [self._search('S[0-9]{3}G[0-9]{2}', path) for path in hydra.paths]

						# group by scan
						scans = self._group(granules, lambda granule: granule[:4])

						# for each scan
						for scan, members in scans.items():

							# reduce members and sort
							members = [member[-2:] for member in members]
							members.sort()
							members = ' '.join(members)

							# add element to inventory
							date = '{}-{}-{}:  '.format(year, month, day)
							tab = ' ' * (maximum - len(product))
							element = [date, '{}:  '.format(scan), '{}:  {}'.format(product, tab), members]
							inventory.append(element)

		# for each field except members
		for index in (2, 1, 0):

			# sort inventory
			inventory.sort(key=lambda element: element[index])

		# begin secondary inventory
		inventoryii = [[entry for entry in inventory[0]]]

		# for each element
		for index, element in enumerate(inventory):

			# if not the first
			if index > 0:

				# copy element
				elementii = [entry for entry in element]

				# if date is the same as above
				if element[0] == inventory[index - 1][0]:

					# make it blank instead
					elementii[0] = ' ' * len(element[0])

				# if scan is the same as above
				if element[1] == inventory[index - 1][1]:

					# make it blank instead
					elementii[1] = ' ' * len(element[1])

				# add to secondar inventory
				inventoryii.append(elementii)

		# begin report
		report = ['Training Data Inventory']

		# or each element in the inventory
		for elementii in inventoryii:

			# append to report
			line = '{}{}{}{}'.format(*elementii)
			report.append(line)

			# print to screen
			self._print(line)

		# jot report
		self._jot(report, '{}/Inventory/training_inventory.txt'.format(self.sink))

		return None

	def land(self, scan=10):
		"""Create land type based scatter plots.

		Arguments:
			scan: int, scan number

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folders
		folderii = '{}/land'.format(folder)
		self._make(folderii)

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, features, data, constraints = self.materialize(*parameters, features=features)

		# apply constraints to data
		data = self._constrain(data, constraints)

		# make predictions
		prediction, deviation, truth, score = self.predict(model, matrix, details)

		# get land type
		land = data['land_type']

		# get landforms dictionary
		names = self._acquire('Land_Types/land.yaml')

		# begin graphs
		graphs = []

		# for each land type, excluding water
		for index in range(1, 17):

			# apply screen
			mask = (land == index).squeeze()
			truthii = truth[mask]
			predictionii = prediction[mask]

			# calculate r2 score
			score = r2_score(truthii, predictionii)

			# calculate regression line
			slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(truthii, predictionii)

			# calculate sample bins
			bins = 1000
			options = {'statistic': 'count', 'range': ((0, self.limits['scale'][1]), (0, self.limits['scale'][1]))}
			options.update({'bins': bins})
			counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(truthii, predictionii, None, **options)
			truths = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
			predictions = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
			mesh, meshii = numpy.meshgrid(truths, predictions, indexing='ij')

			# apply mask
			mask = counts > 0
			counts = counts[mask]
			mesh = mesh[mask]
			meshii = meshii[mask]

			# clip counters to 100
			samples = 100
			counts = numpy.where(counts > samples, samples, counts)

			# order counters
			order = numpy.argsort(counts)
			counts = counts[order]
			mesh = mesh[order]
			meshii = meshii[order]

			# construct samples title depending on instances
			name = names[index]
			title = 'Prediction vs FluxSat {}\n{} ( {} ), $R^2$ = {:.2f}'.format(self.date, name, index, score)

			# construct samples graph
			unit = 'samples'
			labels = ['{} TROPOMI'.format(self.target), '{} prediction'.format(self.target)]
			boundsii = [(0, self.limits['scale'][1]), (0, self.limits['scale'][1])]
			scaleiii = (1, 100)
			points = [0, self.limits['scale'][1]]
			regression = [intercept + point * slope for point in points]
			line = (points, points, 'k-')
			lineii = (points, regression, 'k--')
			lines = [line, lineii]
			options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.5, 'polygons': False}
			graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
			graphs.append(graph)

		# for each block
		for first, last in ((0, 4), (4, 8), (8, 12), (12, 16)):

			# plot land type scatters
			space = {'hspace': 0.6}
			formats = (folderii, self.date, self._pad(first), self._pad(last))
			destination = '{}/land_type_scatters_{}_{}_{}.png'.format(*formats)
			self._paint(destination, graphs[first:last], space=space)

		return None

	def materialize(self, days, scans=(10, 11), granules=(1, 10), features=None, constrain=True, reservoir=None):
		"""Form training matrix from a set of input files.

		Arguments:
			days: list of (int, int) tuples, the month and day
			scans: tuple of ints, the scan bracket
			granules: tuple of ints, the granule bracket
			features: list of feature names
			constrain: boolean, apply constraints to matrix?
			reservoir: str, name of folder from which to grab data

		Returns:
			numpy array, dict of numpy arrays, list of str
		"""

		# set features to default not present
		features = features or self._feature()

		# set reservoir
		reservoir = reservoir or 'Nitro_Inputs'

		# map fields to yaml limits
		limits = {'latitude': 'latitude', 'longitude': 'longitude'}
		limits.update({'solar_zenith_angle': 'zenith', 'viewing_zenith_angle': 'view'})
		limits.update({'cloud_fraction': 'cloud', 'snow_fraction': 'snow'})
		limits.update({'water_mask': 'water'})
		limits.update({'max_rad_quality_high': 'quality', 'max_rad_quality_low': 'quality'})
		limits.update({'max_irr_quality_high': 'quality', 'max_rad_quality_low': 'quality'})

		# begin reservoirs
		matrix = []
		constraints = []
		data = {}

		# for each day
		for month, day in days:

			# create hydra
			formats = (self.sink, reservoir, self.year, self._pad(month), self._pad(day))
			hydra = Hydra('{}/{}/{}/{}/{}'.format(*formats), show=False)

			# create labels for scans
			labels = [self._label(scan, granule) for scan in range(*scans) for granule in range(*granules)]

			# subset paths
			paths = [path for path in hydra.paths if any([label in path for label in labels])]

			# for each path
			for path in paths:

				# ingest path
				hydra.ingest(path)

				# extract data
				extract = hydra.extract()

				# separate extract based on dimensionality
				ones = {field: array for field, array in extract.items() if len(array.shape) == 1}
				twos = {field: array for field, array in extract.items() if len(array.shape) == 2}
				threes = {field: array for field, array in extract.items() if len(array.shape) == 3}

				# remove 1-d arrays
				extract = {field: array for field, array in extract.items() if field not in ones.keys()}

				# reshape all 2 and 3-d arrays
				extract.update({field: array.reshape(-1, 1) for field, array in twos.items()})
				extract.update({field: array.reshape(-1, array.shape[2]) for field, array in threes.items()})

				# get inputs
				block = self._forge(extract, features)

				# begin constraint
				constraint = numpy.ones(extract['latitude'].shape).astype(bool)

				# for each limit
				for field, limit in limits.items():

					# add lower and upper limit constraints
					constraint = constraint & (extract[field] >= self.limits[limit][0])
					constraint = constraint & (extract[field] <= self.limits[limit][1])

				# add nan, fill limit and squeeze
				constraint = constraint & (self._mask(block.sum(axis=1)).reshape(-1, 1))
				constraint = constraint.squeeze()

				# if apply constraints
				if constrain:

					# apply constraints
					block = block[constraint]

				# add 1-d arrays back
				extract.update({field: array.reshape(1, -1) for field, array in ones.items()})

				# add to reservoirs
				[data.setdefault(field, []).append(array) for field, array in extract.items()]
				matrix.append(block)
				constraints.append(constraint)

		# stack into matrix
		matrix = numpy.vstack(matrix)
		constraints = numpy.hstack(constraints)
		data = {field: numpy.vstack(arrays) for field, arrays in data.items()}

		return matrix, features, data, constraints

	def neighbor(self, scan=99, samples=1000, fraction=0.5, metric='euclidean', number=None):
		"""Run a KNN regressor to approximate irreducible error.

		Arguments:
			scan: int, scan number
			samples: int, number of samples to use
			fraction: float, fraction for training, test split
			metric: str, similarity metric
			number: int, number of nearest neighbors

		Returns:
			None
		"""

		# default number to sqrt of samples
		number = number or int(numpy.sqrt(samples * fraction))

		# create a set of ten numbers, beginning with 5
		numbers = numpy.linspace(5, number, 10).astype(int)

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folders
		self._make('{}/reports'.format(folder))
		self._make('{}/neighbors'.format(folder))

		# begin error percents
		percents = []

		# for each number
		for number in numbers:

			# begin report
			report = [self._print('KNN Nearest Neighbors regressor...')]

			# get training matrix for particular scan
			features = details['features'].tolist()
			parameters = ([(self.month, self.day)], (scan, scan + 1))
			matrix, features, _, _ = self.materialize(*parameters, features=features)

			# get scaling information
			minimum = details['input_min']
			maximum = details['input_max']
			scale = maximum - minimum

			# print size
			report.append(self._print('matrix shape: {}'.format(matrix.shape)))

			# calculate total variables
			variance = numpy.var(matrix[:, -1])
			report.append(self._print('total variance: {}'.format(variance)))

			# shuffle the matrix
			numpy.random.shuffle(matrix)

			# get the splitting index
			split = int(samples * fraction)

			# remove truth and apply scaling
			truth = matrix[:split, -1]
			inputs = (matrix[:split, :-1] - minimum) / scale

			# remove test set and apply scaling
			truthii = matrix[split:samples, -1]
			inputsii = (matrix[split:samples, :-1] - minimum) / scale

			# print train and test sizes
			report.append(self._print('training matrix shape: {}'.format(inputs.shape)))
			report.append(self._print('testing matrix shape: {}'.format(inputsii.shape)))

			# create the knn regressor and fit
			report.append(self._print('training knn with {} neighbors, {} metric...'.format(number, metric)))
			regressor = KNeighborsRegressor(n_neighbors=number, metric=metric)
			regressor.fit(inputs, truth)

			# predict on test data and calculate residuals
			prediction = regressor.predict(inputsii)
			residuals = truthii - prediction

			# estimate irreducible error from variance of residuatlw
			error = numpy.var(residuals)
			percent = 100 * (error / variance)
			report.append(self._print('irreducible error: {}'.format(error)))
			report.append(self._print('irreducible error: {} %'.format(percent)))

			# append percent
			percents.append(percent)

			# jot report
			formats = (folder, self.date, metric, samples, number)
			destination = '{}/reports/knn_neighbors_estimate_{}_{}_{}_{}.txt'.format(*formats)
			self._jot(report, destination)

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8,8))

		# plot percents versus number
		pyplot.title('KNN nearest neighbors for scan {}, {}'.format(scan, self.date))

		# label
		pyplot.xlabel('neighbors ( k )')
		pyplot.ylabel('irreducible error ( % )')

		# plot
		pyplot.plot(numbers, percents, 'b-')

		# save figure
		pyplot.savefig('{}/neighbors/nearest_neighbors_{}_{}.png'.format(folder, self.date, scan))
		pyplot.clf()

		return None

	def nitrify(self):
		"""Gather up the TROPOMI nitrggen dioxide data.

		Arguments:
			None

		Returns:
			dict, array tuple, the data and constraining mask
		"""

		# get nitrogen dioxide files from tropomi
		hydra = Hydra('{}/S5P_L2__NO2____HiR/{}'.format(self.sink, self.stub), show=False)

		# if empty
		if len(hydra.paths) < 1:

			# get nitrogen dioxide files from tropomi level 3
			hydra = Hydra('{}/S5P_L3__NO2____HiR/{}'.format(self.sink, self.stub), show=False)

		# begin nitrogen collection
		data = {}

		# set fields
		fields = {'latitude': 'latitude', 'longitude': 'longitude'}
		fields.update({'zenith': 'solar_zenith_angle', 'view': 'viewing_zenith_angle'})
		fields.update({'stratosphere': 'nitrogendioxide_stratospheric_column'})
		fields.update({'troposphere': 'nitrogendioxide_tropospheric_column'})
		fields.update({'slant': 'nitrogendioxide_slant_column_density'})
		fields.update({'stripe': 'nitrogendioxide_slant_column_density_stripe_amplitude'})
		fields.update({'snow': 'snow_ice_flag', 'assurance': 'qa_value'})
		fields.update({'latitude_bounds': 'latitude_bounds', 'longitude_bounds': 'longitude_bounds'})
		fields.update({'time': 'time_utc'})

		# for each nitrogen path
		for path in hydra.paths:

			# ingest
			hydra.ingest(path)

			# for each field
			for field, search in fields.items():

				# append to data
				data.setdefault(field, []).append(hydra.grab(search).squeeze())

			# get shape
			shape = data['latitude'][-1].shape

			# get times
			times = data['time'][-1]

			# convert to timestamps
			format = '%Y-%m-%dT%H:%M:%S.%fZ'
			objects = [datetime.datetime.strptime(time.decode('utf-8'), format) for time in times]
			stamps = [object.timestamp() for object in objects]

			# expand and add to data
			data['time'][-1] = numpy.array([stamps] * shape[1]).transpose(1, 0)

		# create arrays
		data = {field: numpy.vstack(arrays) for field, arrays in data.items()}

		# get total column from tropospheric and stratospheric portions
		column = data['stratosphere'] + data['troposphere']

		# substract striping from slant column
		slant = data['slant']
		stripe = data['stripe'].mean(axis=0)
		stripe = numpy.array([stripe] * slant.shape[0])
		slant = slant - stripe

		# correct slant column by air mass factor
		secant = 1 / numpy.cos(numpy.radians(data['zenith']))
		secantii = 1 / numpy.cos(numpy.radians(data['view']))
		air = secant + secantii
		slant = slant / air

		# construct mask
		mask = self._mask(column) & self._mask(slant)
		mask = mask & self._mask(data['latitude'])
		mask = mask & self._mask(data['longitude'])
		mask = mask & ((data['snow'] == 0) | (data['snow'] == 255))
		mask = mask & (data['assurance'] >= self.limits['assurance'])

		# create data
		data.update({'vertical_column': column, 'slant_column': slant})

		return data, mask

	def parallel(self, scan=99, latitude=33, longitude=-100, constrain=False):
		"""Make a longitudinal cross section plot of data.

		Arguments:
			scan: int, scan number
			latitude: float, the closest latitude
			constrain: boolean, constrain data?

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folders
		folderii = '{}/parallel'.format(folder)
		self._make(folderii)

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, features, data, constraints = self.materialize(*parameters, features=features)

		# if constraining data
		if constrain:

			# apply constraints to data
			data = self._constrain(data, constraints)

		# get scanline closest to latitude and longitude
		index = self._pin([latitude, longitude], [data['latitude'].squeeze(), data['longitude'].squeeze()])[0][0]
		track = int(data['track'][index][0])

		# get all data at scanline
		line = (data['track'].squeeze() == track)

		# organize by longitude
		longitude = data['longitude'].squeeze()[line]
		order = numpy.argsort(longitude)
		longitude = longitude[order]

		# get dioxide data
		nitrogen = data['tempo_vertical_column_no2'].squeeze()[line][order]
		nitrogenii = data['tropomi_vertical_column_no2'].squeeze()[line][order]

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# add text
		formats = (self._orient(latitude), track)
		pyplot.title('TROPOMI vs TEMPO for latitude {}, track {}'.format(*formats))
		pyplot.xlabel('longitude')
		pyplot.ylabel('{} ({})'.format(self.target, self.unit))

		# plot truth
		pyplot.plot(longitude, nitrogen, 'b-', linewidth=1, label='tempo')

		# plot prediction
		pyplot.plot(longitude, nitrogenii, 'g-', linewidth=1, label='tropomi')

		# add legend
		pyplot.legend()

		# plot deviations
		# 		pyplot.plot(longitude, mean + deviation, 'g--')
		# 		pyplot.plot(longitude, mean - deviation, 'g--')

		# save
		pyplot.savefig('{}/parallel/Parallel_{}_{}_{}.png'.format(folder, *formats, self.date))
		pyplot.clf()

		return None

	def predict(self, model, matrix, details, instances=None):
		"""Use the model to make predictions on a matrix.

		Arguments:
			model: tensorflow model instance
			matrix: numpy array
			details: dict of model information
			instances: int, number of instances to average

		Returns:
			tuple of numpy arrays, the truth, the averaged predictions, and deviation
		"""

		# set default instances
		instances = instances or self.instances

		# get scaling information
		minimum = details['scaling/input_min']
		maximum = details['scaling/input_max']
		mean = details['scaling/input_mean']
		deviation = details['scaling/input_std']

		# remove truth and apply scaling
		truth = matrix[:, -1]

		# if normalizing to mean
		if self.training['normalize']:

			# normalize inputs
			inputs = (matrix[:, :-1] - mean) / deviation

		# otherwise
		else:

			# scale inputs
			scale = maximum - minimum
			inputs = (matrix[:, :-1] - minimum) / scale

		# apply pca coefficients
		coefficients = details['scaling/pca_coefficients']
		inputs = numpy.dot(inputs, coefficients).astype('float32')

		# get predictions for each instance
		predictions = []
		for _ in range(instances):

			# get predictions
			prediction = model(inputs)
			prediction = prediction.numpy().squeeze()
			predictions.append(prediction)

		# create array
		predictions = numpy.array(predictions)

		# apply scaling to predictions
		minimumii = details['scaling/output_min']
		maximumii = details['scaling/output_max']
		meanii = details['scaling/output_mean']
		deviationii = details['scaling/output_std']

		# if normalizing
		if self.training['normalize']:

			# normalize to mean, stdev
			predictions = (predictions * deviationii) + meanii

		# otherwise
		else:

			# apply scaling
			scaleii = maximumii - minimumii
			predictions = (predictions * scaleii) + minimumii

		# take mean and stdev of predictions
		average = numpy.array(predictions).mean(axis=0)
		spread = numpy.array(predictions).std(axis=0)

		# make sure they are finite
		mask = (numpy.isfinite(truth) & numpy.isfinite(average))

		# compute r^2 score on mean of predictions
		score = r2_score(truth[mask], average[mask])

		return average, spread, truth, score

	def prepare(self, scans=(9, 14), granules=(1, 10), neighbors=False):
		"""Prepare training data files for TEMPO.

		Arguments:
			scans: tuple of ints, the scan bracket
			granules: tuple of ints, the granule bracket
			neighbors: boolean, use nearest neighbors for gridding?

		Returns:
			None
		"""
		# construct scan labels
		labels = [self._label(scan, granule) for scan in range(*scans) for granule in range(*granules)]

		# create destination folders
		self._make('{}/Nitro_Inputs'.format(self.sink))
		self._make('{}/Nitro_Inputs/{}'.format(self.sink, self.year))
		self._make('{}/Nitro_Inputs/{}/{}'.format(self.sink, self.year, self._pad(self.month)))
		self._make('{}/Nitro_Inputs/{}'.format(self.sink, self.stub))

		# create grid from tropomi files
		latitudes, longitudes, column, slant, assurance, time = self.grid(neighbors=neighbors)

		# construct hydras for tempo data ingestion
		radiances = Hydra('{}/GPP_Inputs/{}'.format(self.sink, self.stub), show=False)
		nitrogens = Hydra('{}/TEMPO_NO2_L2/{}'.format(self.sink, self.stub), show=False)
		clouds = Hydra('{}/TEMPO_CLDO4_L2/{}'.format(self.sink, self.stub), show=False)
		coefficients = Hydra('{}/Nitro_PCs'.format(self.sink))

		# filter radiance paths
		paths = [path for path in radiances.paths if any([label in path for label in labels])]
		for path in paths:

			# prints status
			self._stamp('ingesting {} and related paths...'.format(path), initial=True)

			# ingest the path
			radiances.ingest(path)

			# get date from path
			pattern = '[0-9]{8}T[0-9]{6}Z'
			date = self._search(pattern, path)

			# get granule from path
			pattern = 'S[0-9]{3}G[0-9]{2}'
			granule = self._search(pattern, path)

			# grab all data
			data = radiances.extract()

			# get smmoothed reflectances
			reflectance = data['smoothed_reflectances']
			wavelength = data['reflectance_wavelengths']
			shape = reflectance.shape

			# add first 120 to data
			data.update({'smoothed_reflectances': reflectance[:, :, :120]})

			# reshape
			reflectance = reflectance.reshape(-1, shape[2])

			# get mask for wavelength range, only using high frequency waves
			waves = (wavelength >= self.limits['high'][0]) & (wavelength <= self.limits['high'][1])
			reflectance = reflectance[:, waves]

			# grab PCA coefficients and reduce and reshape
			coefficients.ingest()
			coefficient = coefficients.grab('no2_pca_coefficients')
			reduction = numpy.dot(reflectance, coefficient)
			reduction = reduction.reshape(*shape[:2], -1)

			# add to file
			data.update({'no2_reflectance_pcas': reduction})

			# remove unnecessary fields
			words = ['gpp', 'fluxsat', 'onefluxnet', 'abi', 'par', 'mcd43', 'short_wave', 'smoothed']
			data = {field: array for field, array in data.items() if not any([word in field for word in words])}

			# get latitude and longitude
			latitude = data['latitude']
			longitude = data['longitude']

			# interpolate vertical column grid
			interpolation = self._interpolate(column, latitudes, longitudes, latitude, longitude, 'linear')
			data.update({'tropomi_vertical_column_no2': interpolation})

			# interpolate slant column grid
			interpolation = self._interpolate(slant, latitudes, longitudes, latitude, longitude, 'linear')
			data.update({'tropomi_slant_column_no2': interpolation})

			# interpolate tropomi quality assurance
			interpolation = self._interpolate(assurance, latitudes, longitudes, latitude, longitude, 'linear')
			data.update({'tropomi_quality_assurance': interpolation})

			# interpolate tropomi time
			interpolation = self._interpolate(time, latitudes, longitudes, latitude, longitude, 'linear')
			data.update({'tropomi_time': interpolation})

			# get tempo no2 data
			nitrogens.ingest(date)

			# calculate conversion from molecules / cm^2 to moles / m^2
			avagadro = 6.022e23
			factor = 100 * 100 / avagadro

			# calculate the air mass factor
			secant = 1 / numpy.cos(numpy.radians(data['solar_zenith_angle']))
			secantii = 1 / numpy.cos(numpy.radians(data['viewing_zenith_angle']))
			air = secant + secantii

			# add tempo data
			data.update({'tempo_vertical_column_no2': nitrogens.grab('vertical_column_total') * factor})
			data.update({'tempo_slant_column_no2': nitrogens.grab('fitted_slant_column') * factor / air})
			data.update({'tempo_vertical_column_stratosphere': nitrogens.grab('vertical_column_stratosphere') * factor})
			data.update({'tempo_vertical_column_troposphere': nitrogens.grab('vertical_column_troposphere') * factor})
			data.update({'tempo_vertical_uncertainty': nitrogens.grab('vertical_column_total_uncertainty') * factor})
			data.update({'albedo': nitrogens.grab('albedo')})
			data.update({'gas_profile': nitrogens.grab('gas_profile')})

			# grab time
			times = nitrogens.grab('time')

			# convert to time stamps, based on 1980-01-06 reference
			objects = [datetime.datetime(1980, 1, 6) + datetime.timedelta(seconds=time) for time in times]
			stamps = [object.timestamp() for object in objects]

			# expand
			stamps = numpy.array([stamps] * data['latitude'].shape[1]).transpose(1, 0)
			data.update({'tempo_time': stamps})

			# grab gler data
			clouds.ingest(date)
			data.update({'surface_440': clouds.grab('GLER440')})
			data.update({'surface_466': clouds.grab('GLER466')})

			# fill values
			data = {name: self._fill(array) for name, array in data.items()}

			# check out data
			self._view(data)

			# stash data
			destination = '{}/Nitro_Inputs/{}/Nitro_Inputs_{}_{}.h5'.format(self.sink, self.stub, date, granule)
			self.spawn(destination, data)

			# print status
			self._stamp('prepared')

		return None

	def present(self, destination, truth, prediction, deviation, valids, data, chart=True):
		"""Make presentation plots.

		Arguments:
			destination: str, destination filepath
			truth: array of gpp fluxsat data
			prediction: NN model prediction
			deviation: NN model stdev
			valids: dict of valid data
			data: dict of all data
			chart: boolean, plot maps?

		Returns:
			None
		"""

		# begin status
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase preevious copy
		self._clean(destination, force=True)

		# extract date and time
		date = str(data['date'][0][0])
		time = str(data['time'][0][0])
		date = '{}m{}{}'.format(date[:4], date[4:6], date[6:8])
		time = '{}:{} CST'.format(int(time[:2]) - 6, time[2:4])

		# grab data
		latitude = valids['latitude']
		longitude = valids['longitude']
		cloud = valids['cloud_fraction']
		uncertainty = valids['fluxsat_uncertainty']
		corners = valids['latitude_bounds']
		cornersii = valids['longitude_bounds']

		# caluclate differencd
		difference = prediction - truth

		# calculate r2 score
		score = r2_score(truth, prediction)

		# calculate regression line
		slope, intercept, _, _, _ = scipy.stats.linregress(truth, prediction)

		# create titles
		titles = ['{} Truth {} {}\n'.format(self.target, date, time)]
		titles += ['{} Prediction {} {}\n'.format(self.target, date, time)]
		titles += ['Prediction vs Truth\n( R^2 = {} )'.format(self._round(score, 2))]
		titles += ['Percent Difference\n']

		# calculate differences
		difference = prediction - truth
		difference = numpy.where(numpy.isfinite(difference), difference, 0)

		# calculate sample bins
		bins = 1000
		options = {'statistic': 'count', 'range': ((0, self.limits['scale'][1]), (0, self.limits['scale'][1]))}
		options.update({'bins': bins})
		counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(truth, prediction, None, **options)
		truths = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
		predictions = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
		mesh, meshii = numpy.meshgrid(truths, predictions)

		# apply mask
		mask = counts > 0
		counts = counts[mask]
		mesh = mesh[mask]
		meshii = meshii[mask]

		# clip counters to 100
		samples = 100
		counts = numpy.where(counts > samples, samples, counts)

		# order counters
		order = numpy.argsort(counts)
		counts = counts[order]
		mesh = mesh[order]
		meshii = meshii[order]

		# get minimum and maximum tracer values
		minimum = min([truth.min(), prediction.min()])
		maximum = max([truth.max(), prediction.max()])

		# set percent bracket
		absolute = max([abs(numpy.percentile(difference, 5)), abs(numpy.percentile(difference, 95))])

		# if plotting maps:
		if chart:

			# set boundaries
			boundaries = [[(-125, -70), (25, 50)]]
			boundaries += [[(-100, -80), (28, 40)]]
			boundaries += [[(-95, -85), (29, 36)]]

			# set alphas
			alphas = [1.0, 1.0, 1.0]

			# set longitude ticks
			ticks = [(-120, -110, -100, -90, -80, -70)]
			ticks += [(-100, -95, -90, -85, -80)]
			ticks += [(-95, -90, -85)]

			# set longitude tickmarks
			marks = [[self._orient(entry, east=True).strip('0') for entry in tick] for tick in ticks]

			# set latitude ticks
			ticksii = [(30, 35, 40, 45, 50)]
			ticksii += [(30, 35, 40)]
			ticksii += [(30, 32, 34)]

			# set latitude tickmarks
			marksii = [[self._orient(entry).strip('0') for entry in tick] for tick in ticksii]

			# set boundaries
			zipper = zip(boundaries, alphas, ticks, marks, ticksii, marksii)
			for bounds, alpha, tick, mark, tickii, markii in zipper:

				# construct geo tag
				formats = [self._orient(bounds[0][0], east=True), self._orient(bounds[0][1], east=True)]
				formats += [self._orient(bounds[1][0]), self._orient(bounds[1][1])]
				tag = '{}{}{}{}'.format(*formats)

				# replace destination
				destinationii = destination.replace('.png', '_{}.png'.format(tag))
				destinationiii = destination.replace('.png', '_uncertainty_{}.png'.format(tag))

				# set bounds
				scale = (minimum, maximum)
				scaleii = (-absolute, absolute)

				# begin graphs
				graphs = []

				# construct GPP truth plot
				title = '{} TROPOMI {} {}'.format(self.target, date, time)
				unit = self.unit
				labels = ['', '']
				options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				graph = self._graph(longitude, latitude, truth, bounds, scale, 'plasma', title, unit, labels, **options)
				graphs.append(graph)

				# construct GPP prediction plot
				title = '{} Prediction {} {}'.format(self.target, date, time)
				unit = self.unit
				labels = ['', '']
				options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				parameters = (longitude, latitude, prediction, bounds, scale, 'plasma', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# unpack reflectivity
				latitudeii = data['latitude']
				longitudeii = data['longitude']
				norths = data['latitude_bounds']
				easts = data['longitude_bounds']
				colors = data['rgb']
				land = data['land_type']

				# clip boundaries
				colors = numpy.where(colors > 1, 1, colors)
				colors = numpy.where(colors < 0, 0, colors)
				mask = (numpy.isfinite(latitudeii)) & (numpy.isfinite(longitudeii)) & (land != 0)
				mask = mask & (latitudeii > (-longitudeii - 95))
				mask = mask.squeeze()
				colors = colors[mask]
				latitudeii = latitudeii[mask]
				longitudeii = longitudeii[mask]
				norths = norths[mask]
				easts = easts[mask]

				# construct rgb plot
				title = 'RGB Map {} {}'.format(date, time)
				unit = self.unit
				labels = ['', '']
				options = {'polygons': True, 'corners': [norths, easts], 'globe': True, 'colorbar': False}
				options.update({'reflectivity': True, 'ticks': tick, 'alpha': alpha})
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				parameters = (longitudeii, latitudeii, colors, bounds, scaleii, 'plasma', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# construct percent difference plot
				title = 'Difference ( Prediction - FluxSat )'
				unit = self.unit
				labels = ['', '']
				options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				parameters = (longitude, latitude, difference, bounds, scaleii, 'coolwarm', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# construct percent difference plot
				title = 'FluxSat Uncertainty {}'.format(date)
				unit = self.unit
				labels = ['', '']
				scaleiii = (0, 3)
				options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				parameters = (longitude, latitude, uncertainty, bounds, scaleiii, 'plasma', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# construct percent difference plot
				title = 'Prediction Stdev {} {}'.format(date, time)
				unit = self.unit
				labels = ['', '']
				scaleiii = (0, 3)
				options = {'polygons': True, 'corners': [corners, cornersii], 'alpha': alpha}
				options.update({'ticks': tick, 'marks': mark, 'ticksii': tickii, 'marksii': markii})
				parameters = (longitude, latitude, deviation, bounds, scaleiii, 'plasma', title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# create plots
				self._paint(destinationii, graphs[:4])
				space = {'top': 0.88, 'bottom': 0.28, 'wspace': 0.35, 'right': 0.95}
				self._paint(destinationiii, graphs[4:6], size=(16, 8), space=space, gap=0.11)

		# begin scatter plots
		scatters = []

		# set cloud sceens
		fractions = [(0, 1.0), (0, 0.25), (0.25, 0.5), (0.5, 0.75), (0.75, 1.0)]
		screens = [(one * self.limits['cloud'], two * self.limits['cloud']) for one, two in fractions]

		# for each screen
		for screen in screens:

			# create less than or equal symbol
			symbol = '\u2264'

			# unpack screen
			low, high = screen

			# apply screen
			mask = (cloud >= low) & (cloud <= high)
			mask = mask.squeeze()
			truthii = truth[mask]
			predictionii = prediction[mask]
			cloudii = cloud[mask].squeeze()
			differenceii = difference[mask].squeeze()

			# calculate r2 score
			score = r2_score(truthii, predictionii)

			# calculate regression line
			slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(truthii, predictionii)

			# calculate sample bins
			bins = 1000
			options = {'statistic': 'count', 'range': ((0, self.limits['scale'][1]), (0, self.limits['scale'][1]))}
			options.update({'bins': bins})
			counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(truthii, predictionii, None, **options)
			truths = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
			predictions = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
			mesh, meshii = numpy.meshgrid(truths, predictions, indexing='ij')

			# apply mask
			mask = counts > 0
			counts = counts[mask]
			mesh = mesh[mask]
			meshii = meshii[mask]

			# clip counters to 100
			samples = 100
			counts = numpy.where(counts > samples, samples, counts)

			# order counters
			order = numpy.argsort(counts)
			counts = counts[order]
			mesh = mesh[order]
			meshii = meshii[order]

			# construct samples title depending on instances
			formats = (low, symbol, symbol, high, date, time, self._round(score, 2))
			title = 'Prediction vs FluxSat ( {:.2f} {} CRF {} {:.2f} )\n{} {} CST ( $R^2$ = {:.2f} )'.format(*formats)

			# construct samples graph
			unit = 'samples'
			labels = ['{} TROPOMI'.format(self.target), '{} prediction'.format(self.target)]
			boundsii = [(0, self.limits['scale'][1]), (0, self.limits['scale'][1])]
			scaleiii = (1, 100)
			points = [0, truthii.max()]
			regression = [intercept + point * slope for point in points]
			line = (points, points, 'k-')
			lineii = (points, regression, 'k--')
			lines = [line, lineii]
			options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.5, 'polygons': False}
			graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
			scatters.append(graph)

			# if full cloud screen
			if screen == screens[0]:

				zipper = zip(truthii, predictionii, differenceii, cloudii)
				for i, j, k, l in list(zipper)[:100]:
					print(i, j, k, l)

				# calculate r2 score
				score = r2_score(cloudii, differenceii)

				# calculate regression line
				slope, intercept, rvalue, pvalue, stderr = scipy.stats.linregress(cloudii, differenceii)

				# remove outliers
				lower = numpy.percentile(differenceii, 0)
				upper = numpy.percentile(differenceii, 100)

				# calculate sample bins
				bins = 1000
				options = {'statistic': 'count', 'range': ((0, self.limits['cloud']), (lower, upper))}
				options.update({'bins': bins})
				counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(cloudii, differenceii, None, **options)
				clouds = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
				differences = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
				mesh, meshii = numpy.meshgrid(clouds, differences, indexing='ij')

				# apply mask
				mask = counts > 0
				counts = counts[mask]
				mesh = mesh[mask]
				meshii = meshii[mask]

				# clip counters to 100
				samples = 100
				counts = numpy.where(counts > samples, samples, counts)

				# order counters
				order = numpy.argsort(counts)
				counts = counts[order]
				mesh = mesh[order]
				meshii = meshii[order]

				# construct samples title depending on instances
				title = 'Prediction - FluxSat vs CRF\n{} {}'.format(date, time)

				# set limits for scatter
				minimum = lower
				maximum = upper

				# construct samples graph
				unit = 'samples'
				labels = ['CRF', 'Prediction - FluxSat']
				boundsii = [(0, self.limits['cloud']), (minimum, maximum)]
				scaleiii = (1, 100)
				points = [0, self.limits['cloud']]
				pointsii = [0, 0]
				regression = [intercept + point * slope for point in points]
				line = (points, pointsii, 'k-')
				lineii = (points, regression, 'k--')
				lines = [line, lineii]
				options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.5, 'polygons': False}
				options.update({'marker': 0.1})
				graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
				scatters.append(graph)

		# plot scatters
		space = {'top': 0.88, 'bottom': 0.24, 'wspace': 0.36, 'right': 0.96}
		destinationii = destination.replace('.png', '_crf_{}.png'.format(int(self.limits['cloud'] * 10)))
		self._paint(destinationii, scatters[0:2], size=(16, 8), space=space, gap=0.12)

		# plot crf scatters
		space = {'hspace': 0.6}
		destinationiii = destination.replace('.png', '_scatters_{}.png'.format(int(self.limits['cloud'] * 10)))
		self._paint(destinationiii, scatters[2:6], space=space)

		return None

	def probe(self, scan=99, variables=None, margin=25):
		"""Probe model as a function of independent variable, with median values at all others.

		Arguments:
			scan: int, scan number
			variable: str, dependent variable
			margin: int, perentile margin

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make shapely folder
		folderii = '{}/probe'.format(folder)
		self._make(folderii)

		# collect training days
		days = self._daze(validation=False)

		# begin matrices and scores
		matrices = []

		# for each day
		for month, day in days:
			# get training matrix for particular scan
			features = details['features'].tolist()
			parameters = [(month, day)], (scan, scan + 1)
			matrix, features, data, constraints = self.materialize(*parameters, features=features)

			# add to matrices
			matrices.append(matrix)

		# set default variable
		variables = variables or features[:-1]

		# create block of all inputs
		block = numpy.vstack(matrices)

		# get the median of the matrix, as well as the lower and upper bounds
		median = numpy.median(block, axis=0)
		lower = numpy.percentile(block, 50 - margin, axis=0)
		upper = numpy.percentile(block, 50 + margin, axis=0)

		# for each variable
		for variable in variables:

			# get the index of the feature
			index = features.index(variable)

			# create percentiles
			percentiles = numpy.array([numpy.percentile(block[:, index], percentile) for percentile in range(101)])

			# create probe matrix from medians for all features but variable in question
			probe = numpy.array([median] * 101)
			probe[:, index] = percentiles

			# predict values from the model
			prediction, deviation, _, _ = self.predict(model, probe, details)

			# for each dataset
			for day, matrix in zip(days, matrices):

				# begin plot
				pyplot.clf()
				pyplot.figure(figsize=(8, 8))

				# create title
				date = '{}m{}{}'.format(self.year, self._pad(day[0]), self._pad(day[1]))
				title = '{} {} at medians, +\- {}th percentile'.format(date, variable, margin)
				pyplot.title(title)

				# add labels
				pyplot.xlabel(variable)
				pyplot.ylabel(features[-1])

				# set limits
				minimum = block[:, -1].min(axis=0)
				maximum = block[:, -1].max(axis=0)
				buffer = (maximum - minimum) * 0.05
				pyplot.ylim(*self.limits['scale'])

				# Sample data
				abscissa = percentiles
				ordinate = prediction

				# create line plots
				pyplot.plot(abscissa, ordinate, 'bo-')
				pyplot.plot(abscissa, ordinate - deviation, 'b--')
				pyplot.plot(abscissa, ordinate + deviation, 'b--')

				# copy matrix
				xerox = matrix.copy()

				# for each feature
				for position, feature in enumerate(features[:-1]):

					# if feature not varible of interest
					if feature != variable:

						# subset matrix
						mask = (xerox[:, position] >= lower[position]) & (xerox[:, position] <= upper[position])
						xerox = xerox[mask]

				# calculate differences from median for color scale
				difference = abs(xerox - median)
				difference[:, index] = 0
				difference = difference.sum(axis=1)

				# add truth points within median margins
				pyplot.scatter(xerox[:, index], xerox[:, -1], c=difference, cmap='gray', s=25, marker='x')

				# save plot
				destination = '{}/probe_{}_{}.png'.format(folderii, date, variable)
				pyplot.savefig(destination)
				pyplot.clf()

		return None

	def repair(self, baseline=False):
		"""Repair input files by adding new fields.

		Arguments:
			baseline: boolean, redo baseline data?

		Returns:
			None
		"""

		# get all tempo l1b files
		hydraii = Hydra('{}/TEMPO_RAD_L1/{}'.format(self.sink, self.stub))

		# get all inputs files
		hydra = Hydra('{}/GPP_Inputs/{}'.format(self.sink, self.stub))
		for path in hydra.paths:

			# ingest the path
			hydra.ingest(path)

			# extract data
			data = hydra.extract()

			# get date from path
			search = '[0-9]{8}T[0-9]{6}Z'
			date = re.findall(search, path)[0]

			# get granule from path
			search = 'S[0-9]{3}G[0-9]{2}'
			granule = re.findall(search, path)[0]

			# begin data with time and granule information
			data.update({'date': numpy.array([int(date[0:8])]), 'time': numpy.array([int(date[9:15])])})
			data.update({'scan': numpy.array([int(granule[1:4])]), 'granule': numpy.array([int(granule[5:7])])})

			# get shape
			shape = data['latitude'].shape

			# get irradiance qualifiers
			qualifier = data['max_irr_quality_high']
			qualifierii = data['max_irr_quality_low']

			# if qualifiers are 1-d
			if len(qualifier.shape) == 1:

				# tile qualifiers to match shape
				qualifier = numpy.tile(qualifier, (shape[0], 1))
				qualifierii = numpy.tile(qualifierii, (shape[0], 1))

				# update data
				data.update({'max_irr_quality_high': qualifier, 'max_irr_quality_low': qualifierii})

			# access l1b data
			hydraii.ingest(date)

			# for each band
			for band in (1, 2):

				# for each cardinal
				for cardinal in ('east', 'west'):

					# add abi data
					search = 'ABI_Band_{}_{}'.format(band, cardinal.capitalize())
					data.update({'abi_band{}_{}'.format(band, cardinal): hydraii.grab(search)})

			# if relative azimuth in keys
			if 'relative_azimuth' in data.keys():

				# replace relative azimuth with angle name
				data['relative_azimuth_angle'] = data['relative_azimuth']
				del data['relative_azimuth']

			# if redoing baseline
			if baseline:

				# redo baseline data
				latitude = hydra.grab('latitude')
				longitude = hydra.grab('longitude')
				baselines, wavelengths = self.baseline(path, latitude, longitude)
				baselines = numpy.where(baselines >= 0, baselines, self.fill)
				data.update({'abi_radiance': baselines, 'abi_wavelengths': wavelengths})

			# add synthetic data
			synthetics = Hydra('{}/Synthetic_GPP/{}'.format(self.sink, self.stub))
			synthetics.ingest(date)
			fields = ['short_wave', 'mcd43_bands', 'gpp_onefluxnet', 'onefluxnet_deviation']
			data.update({field: synthetics.grab(field) for field in fields})

			# add modis bands
			pairs = [(620, 670), (841, 876), (459, 479), (545, 565)]
			pairs += [(1230, 1250), (1628, 1652), (2105, 2155)]
			data.update({'mcd43_wavelengths': numpy.array(pairs)})

			# add mosaic data
			mosaics = Hydra('{}/Mosaic_GPP/{}'.format(self.sink, self.stub))
			mosaics.ingest(date)
			data.update({'gpp_mosaic': mosaics.grab('gpp_mosaic')})

			# fill values
			data = {name: self._fill(array) for name, array in data.items()}

			# restash file
			self.spawn(path, data)

		return None

	def run(self, predictors, targets, validation, folder):
		"""Define and run the Bayesian model.

		Arguments:
			predictors: numpy array, inputs matrix
			targets: numpy array, outputs vector
			validation: list of numpy array, the validation set
			folder: str, folder for storing model
			epochs: number of training epochs

		Returns:
			None
		"""

		# define abbreviations
		optimizers = tensorflow_addons.optimizers
		layers = tensorflow_probability.layers
		backend = tensorflow.keras.backend

		# unpack shape
		samples, features = predictors.shape

		# define batch size and kernel weight
		batch = self.training['batch']
		weight = batch / samples

		# if not using weights
		if not self.architecture['weight']:

			# remove weight
			weight = 0

		# set up bayesian layers
		prior = self._prioritize
		posterior = self._post

		# set activation functions if not plain strings
		activations = {'leaky': tensorflow.keras.layers.LeakyReLU(alpha=0.3)}

		# set up beginning input layer
		input = tensorflow.keras.Input(shape=(features))
		output = input

		# for each hidden layer
		for hiddens in self.architecture['hiddens']:

			# set activation
			activation = activations.get(self.architecture['activation'], self.architecture['activation'])

			# if using variational
			if self.architecture['bayesian']:

				# add variational layer
				options = {'kl_weight': weight, 'activation': activation}
				output = layers.DenseVariational(hiddens, posterior, prior, **options)(output)

			# otherwise
			else:

				# add normal dense layer
				options = {'activation': activation}
				output = tensorflow.keras.layers.Dense(hiddens, **options)(output)

			# add dropout layer
			output = tensorflow.keras.layers.Dropout(rate=self.architecture['dropout'])(output)

		# if bayesian
		if self.architecture['bayesian']:

			# add final mean, deviation layers
			output = tensorflow.keras.layers.Dense(units=2)(output)
			output = layers.IndependentNormal(1)(output)

		# otherwise
		else:

			# add normal output
			output = tensorflow.keras.layers.Dense(units=1)(output)

		# if using cyclic learning rate
		if self.training['cyclic']:

			# define cyclical learning rate
			def scaling(timepoint): return 1 / (2 ** (timepoint - 1))
			learning = self.training['learning']
			options = {'initial_learning_rate': float(learning[0])}
			options.update({'maximal_learning_rate': float(learning[1])})
			options.update({'scale_fn': scaling, 'step_size': math.ceil(samples / batch)})
			learning = optimizers.CyclicalLearningRate(**options)

		# otherwise
		else:

			# use constant learning rate from initial
			learning = float(self.training['learning'][0])

		# set loss functions
		losses = {'custom': self._lose}
		loss = losses.get(self.training['loss'], self.training['loss'])

		# begin learning rate accumulation
		rates = []

		# crate callback to log and store learning rates
		monitoring = lambda batch, logs: rates.append(backend.get_value(model.optimizer.learning_rate))
		monitor = tensorflow.keras.callbacks.LambdaCallback(on_batch_end=monitoring)

		# build model
		epochs = self.training['epochs']
		metrics = ['mae', 'mse', 'acc', 'mape']
		model = tensorflow.keras.Model(input, output, name="Bayesian")
		model.compile(loss=loss, optimizer=tensorflow.optimizers.Adam(learning), metrics=metrics)
		options = {'epochs': epochs, 'batch_size': batch, 'validation_data': validation}
		options.update({'callbacks': [monitor]})
		history = model.fit(predictors, targets, **options)
		model.save('{}/TEMPO_GPP_Model'.format(folder))

		# clear session
		tensorflow.keras.backend.clear_session()

		# write chronicle
		rates = numpy.array(rates)
		self.chronicle(history, rates, samples, folder)

		return None

	def scatter(self, dependent=None, independent=None, scan=99, constrain=False):
		"""Make a scatter plot.

		Arguments:
			dependent: str, name of dependent variable
			independent: str, name of independent variable
			scan: int, scan number
			constrain: boolean, use constraints for variables?

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folders
		folderii = '{}/scatter'.format(folder)
		self._make(folderii)

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, features, data, constraints = self.materialize(*parameters, features=features)

		# if constraining
		if constrain:

			# apply constraints to data
			data = self._constrain(data, constraints)

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# add text
		formats = (self.date, scan, dependent, independent)
		pyplot.title('{}, scan {}, {} vs {}'.format(*formats))
		pyplot.xlabel(independent)
		pyplot.ylabel(dependent)

		# plot truth
		pyplot.plot(data[independent], data[dependent], 'bx')

		# save
		formats = (self.date, self._pad(scan), dependent, independent)
		pyplot.savefig('{}/Scatter_{}_{}_{}.png'.format(folderii, *formats, self.date))
		pyplot.clf()

		return None

	def sense(self, scan=99, isolate=False):
		"""Perform sensitivity study, by removal of each feature.

		Arguments:
			scan: int, scan number
			isolate: perfrom isolation study?

		Returns:
			None
		"""

		# set mode
		mode = 'sensitivity'

		# if isolating
		if isolate:

			# set mode to isolation
			mode = 'isolation'

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make reports folder
		self._make('{}/reports'.format(folder))

		# get training matrix for particular scan
		features = details['features'].tolist()
		matrix, features, _, _ = self.materialize([(self.month, self.day)], (scan, scan + 1), features=features)

		# begin report and scores collection
		report = ['{} report for {}\n'.format(mode.capitalize(), folder)]
		scores = []

		# rename last feature from target to all predictors
		features[-1] = 'all predictors'

		# for each feature
		for feature in range(matrix.shape[1]):

			# copy inputs
			xerox = matrix.copy()

			# if feature is valid
			if feature < matrix.shape[1] - 2:

				# if mode is isolation
				if isolate:

					# replace all features but index with median
					xerox[:, :-1] = numpy.array([numpy.percentile(matrix[:, :-1], 50, axis=0)] * matrix.shape[0])
					xerox[:, feature] = matrix[:, feature]

				# otherwise
				else:

					# replace feature index with median
					xerox[:, feature] = numpy.percentile(matrix[:, feature], 50)

			# make predictions
			prediction, deviation, truth, score = self.predict(model, xerox, details)

			# print r^2 score
			pair = (features[feature], score)
			self._print('{}: {}'.format(*pair))

			# add scores
			scores.append(pair)

		# sort scores, reversing for isolation
		scores.sort(key=lambda pair: pair[1], reverse=isolate)

		# for each score
		for pair in scores:

			# add to report
			report.append('{}: {}'.format(*pair))

		# jot to reports folder
		self._jot(report, '{}/reports/{}.txt'.format(folder, mode))

		return None

	def sow(self, scans=(11, 12), granules=(1, 10)):
		"""Gather up training data into a matrix, and run a random forest.

		Arguments:
			scans: tuple of ints, the scans to use
			granules: tuple 0f ints, the granules bracket

		Returns:
			None
		"""

		# retrieve model and scaling details
		_, _, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# create folder
		self._make('{}/reports'.format(folder))

		# construct training days
		days = self.training['days']

		# construct matrix
		matrix, features, _, _ = self.materialize(days, scans, granules)

		# create data
		data = {feature: matrix[:, index] for index, feature in enumerate(features)}

		# create report destination and plant tree
		self.plant(data, features[-1], '{}/reports/no2_random_forest.txt'.format(folder))

		return None

	def swath(self, variables=None, margin=5):
		"""Make maps of TROPOMI swath data.

		Arguments:
			variables: list of str, the variables
			margin: int, margin for percentiles

		Returns:
			None
		"""

		# retrieve model and scaling details
		_, _, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folder
		folderii = '{}/swaths'.format(folder)
		self._make(folderii)

		# get data
		data, mask = self.nitrify()
		valids = self._constrain(data, mask)
		reservoirs = {'data': data, 'valids': valids}

		# set scales
		scales = {'vertical_column': (0, self.limits['scale'][1])}
		scales.update({'slant_column': (0, self.limits['scale'][1])})
		scales.update({'troposphere': (0, self.limits['scale'][1])})
		scales.update({'stratosphere': (0, self.limits['scale'][1])})
		scales.update({'snow': (0, 100), 'assurance': (0, 1.0)})

		# set units
		units = {'assurance': '-'}

		# for each reservoir
		for reservoir, collection in reservoirs.items():

			# for each variable
			variables = variables or ['vertical_column', 'slant_column']
			for variable in variables:

				# begin graphs
				graphs = []

				# get geo coordinates
				latitude = collection['latitude'].squeeze()
				longitude = collection['longitude'].squeeze()
				corners = collection['latitude_bounds'].squeeze()
				cornersii = collection['longitude_bounds'].squeeze()
				array = collection[variable].squeeze()

				# get percentiles
				valid = self._mask(array)
				minimum = numpy.percentile(array[valid], margin)
				maximum = numpy.percentile(array[valid], 100 - margin)

				# set scale and unit
				scale = scales.get(variable, (minimum, maximum))
				unit = units.get(variable, self.unit)

				# create destination
				formats = (folderii, self.date, reservoir, variable)
				destination = '{}/tropomi_{}_{}_{}.png'.format(*formats)

				# construct percent difference plot
				title = 'TROPOMI {} {} {}\n'.format(variable, self.target, self.date)
				bounds = [(-125, -60), (20, 50)]

				# create valid mask
				mask = self._mask(array)
				mask = mask & (longitude >= bounds[0][0])
				mask = mask & (longitude <= bounds[0][1])
				mask = mask & (latitude >= bounds[1][0])
				mask = mask & (latitude <= bounds[1][1])

				# apply mask
				abscissa = longitude[mask]
				ordinate = latitude[mask]
				tracer = array[mask]

				# set gradient
				gradient = 'viridis'

				# make plot
				labels = ['', '']
				options = {'polygons': True, 'corners': [corners[mask], cornersii[mask]], 'bar': True}
				parameters = (abscissa, ordinate, tracer, bounds, scale, gradient, title, unit, labels)
				graph = self._graph(*parameters, **options)
				graphs.append(graph)

				# create plot
				self._paint(destination, graphs)

		return None

	def synchronize(self, scans=(9, 14)):
		"""Create an input file from a collection of scans based on closeness to the TROPOMI time.

		Arguments:
			scans: tuple of ints, the bracket of scans.

		Returns:
			None
		"""

		# create base array
		#base = numpy.ones((scans[1] - scans[0], self.mirror, self.track)) * self.fill
		base = numpy.ones((scans[1] - scans[0], self.mirror, self.track)) * 0

		# begin data reservoir
		reservoir = []

		# for each scan
		for depth, scan in enumerate(range(*scans)):

			# get the data
			_, _, data, _ = self.materialize(days=[(self.month, self.day)], scans=(scan, scan + 1))

			# remove original reducted pcas
			del data['reflectance_pcas']

			# # remove gas_profile
			# del data['gas_profile']

			# remove last ste of pcas
			data['no2_reflectance_pcas'] = data['no2_reflectance_pcas'][:, :50]

			# grab date and time
			date = data['date']
			hour = data['time']

			# reshape data into blocks
			data = self._block(data, self.track)

			# add data
			reservoir.append(data)

			# get tempo times and reshpae
			time = data['tempo_time'].reshape(-1, self.track)

			print('time, scan {}: {}'.format(scan, time.shape))

			# insert into base array
			base[depth, :time.shape[0], :self.track] = time

		# get tropomi times
		timeii = reservoir[0]['tropomi_time'].reshape(-1, self.track)

		print('timeii shape: {}'.format(timeii.shape))

		# begin depths
		depths = numpy.ones((self.mirror)).astype(int) * -999

		# for each mirror step
		for step in range(self.mirror):

			# try to
			try:

				# take time averages along each mirror step strip
				mirror = base[:, step, :]
				average = mirror.mean(axis=1)

				# remove infinities
				average = numpy.where(numpy.isfinite(average), average, 0)

				# and repeat for tropomi
				mirrorii = timeii[step, :]
				averageii = mirrorii[numpy.isfinite(mirrorii)].mean()

				# if both are finite
				if averageii:

					# determine closest
					closest = numpy.argsort((averageii - average) ** 2)[0]
					depths[step] = int(closest)

			# unless out of bounds
			except IndexError:

				# in which case pass
				pass

		# begin combined data
		first = reservoir[0]
		blank = {field: numpy.ones((self.mirror, self.track, array.shape[2])) for field, array in first.items()}
		blank = {field: array * self.fill for field, array in blank.items()}

		# for each field
		for field in first.keys():

			# for each position
			for step, depth in enumerate(depths):

				# if depth is real
				if depth >= 0:

					# add mirror strip
					blank[field][step] = reservoir[depth][field][step]

		# squeeze arrays
		blank = {field: array.squeeze() for field, array in blank.items()}

		# record scan numbers
		numbers = [depth + scans[0] for depth in depths]
		blank['scan_numbers'] = numpy.array(numbers)

		# add 1-d fields field
		blank['date'] = date[0]
		blank['time'] = hour[0]
		blank['scan'] = numpy.array([99])

		# create file
		formats = (self.sink, self.stub, self.year, self._pad(self.month), self._pad(self.day))
		destination = '{}/Nitro_Inputs/{}/Nitro_Inputs_{}{}{}T000000Z_S099G01.h5'.format(*formats)
		self.spawn(destination, blank)

		return None

	def train(self):
		"""Gather up training data into a matrix, and run neural network.

		Arguments:
			None

		Returns:
			None
		"""

		# get current date
		today = datetime.datetime.today().date()
		date = '{}m{}{}'.format(today.year, self._pad(today.month), self._pad(today.day))

		# create folder
		folder = '{}/Nitro_Models/Nitro_NN_{}_{}'.format(self.sink, date, self.tag)
		self._make(folder)

		# begin data details
		data = {'training/{}'.format(field): numpy.array(datum) for field, datum in self.training.items()}
		data.update({'validation/{}'.format(field): numpy.array(datum) for field, datum in self.validation.items()})
		data.update({'architecture/{}'.format(field): numpy.array(datum) for field, datum in self.architecture.items()})
		data.update({'limits/{}'.format(field): numpy.array(datum) for field, datum in self.limits.items()})

		# construct matrix
		days = self.training['days']
		scans = self.training['scans']
		granules = self.training['granules']
		matrix, features, _, _ = self.materialize(days, scans, granules)

		# get the minimums and maximums
		minimum = matrix.min(axis=0)
		maximum = matrix.max(axis=0)
		mean = matrix.mean(axis=0)
		deviation = matrix.std(axis=0)

		# add data for scaling factors
		data.update({'scaling/input_min': minimum[:-1], 'scaling/input_max': maximum[:-1]})
		data.update({'scaling/output_min': minimum[-1], 'scaling/output_max': maximum[-1]})
		data.update({'scaling/input_mean': mean[:-1], 'scaling/input_std': deviation[:-1]})
		data.update({'scaling/output_mean': mean[-1], 'scaling/output_std': deviation[-1]})

		# add features list
		data.update({'features': numpy.array(features)})

		# if normalizing
		if self.training['normalize']:

			# apply normalization to traiing matrix
			matrix = (matrix - mean) / deviation

		# otherwise
		else:

			# apply scaling to training matrix
			scale = maximum - minimum
			matrix = (matrix - minimum) / scale

		# fit PCA on inputs and get coefficients, and transpose them
		decomposer = PCA(n_components=matrix.shape[1] - 1)
		decomposer.fit(matrix[:, :-1])
		coefficients = decomposer.components_.transpose(1, 0)

		# add to data
		data.update({'scaling/pca_coefficients': coefficients})

		# separate into predictors and targets
		targets = matrix[:, -1]
		predictors = numpy.dot(matrix[:, :-1], coefficients)

		# randomly sample based on fraction
		size = int(self.training['fraction'] * predictors.shape[0])
		indices = numpy.random.randint(0, predictors.shape[0], size=(size,))
		targets = targets[indices]
		predictors = predictors[indices]

		# add indices to data
		data.update({'samples/selected_indices': indices})

		# construct validation matrix
		daysii = self.validation['days']
		matrixii, _, _, _ = self.materialize(daysii, scans, granules)

		# if normalizing
		if self.training['normalize']:

			# apply normalization to validation matrix
			matrixii = (matrixii - mean) / deviation

		# otherwise
		else:

			# apply scaling to training matrix
			scale = maximum - minimum
			matrixii = (matrixii - minimum) / scale

		# separate into predictors and targets
		targetsii = matrixii[:, -1]
		predictorsii = numpy.dot(matrixii[:, :-1], coefficients)

		# randomly sample based on fraction
		size = int(self.validation['fraction'] * predictorsii.shape[0])
		indicesii = numpy.random.randint(0, predictorsii.shape[0], size=(size,))
		targetsii = targetsii[indicesii]
		predictorsii = predictorsii[indicesii]
		validation = [predictorsii, targetsii]

		# stash
		destination = '{}/Model_Scaling.h5'.format(folder)
		self.spawn(destination, data)

		# run the model
		self.run(predictors, targets, validation, folder)

		return None

	def trouble(self, scan=10, granule=6, track=1000, mirror=50, waves=(405, 460), linear=False):
		"""Troubleshoot pca deocomposition of nitrogen dioxide.

		Arguments:
			scan: int, scan number
			granule: int, granule number
			track: int, track position
			mirror: int, mirror step
			waves: tuple of floats, the wavelength range

		Returns:
			None
		"""

		# set tag
		tag = 'native_spectrum'

		# if linear correction
		if linear:

			# set tag
			tag = 'linear_fit_removed'

		# get model
		_, _, folder = self._retrieve()

		# create folder
		folder = '{}/trouble'.format(folder)
		self._make(folder)

		# get smoothed radiances
		label = self._label(scan=scan, granule=granule)
		hydra = Hydra('TEMPO_RAD_Smooth/{}'.format(self.stub))
		hydra.ingest(label)

		# get smoothed spectra
		spectra = hydra.grab('smoothed_reflectances')
		wavelengths = hydra.grab('wavelengths')

		# mask the waves
		mask = (wavelengths >= waves[0]) & (wavelengths <= waves[1])
		wavelengths = wavelengths[mask]
		nitrogen = spectra[mirror, track][mask]
		slope, intercept, _, _, _ = scipy.stats.linregress(wavelengths, nitrogen)

		# get block of spectra
		block = spectra[mirror, track + 1: track + 50]
		block = block.reshape(-1, block.shape[-1])
		block = block[:, mask]

		# if removing linear fit
		if linear:

			# for each sample
			for sample in range(block.shape[0]):

				# get linear fit
				slopeii, interceptii, _, _, _ = scipy.stats.linregress(wavelengths, block[sample])
				line = numpy.array([interceptii + slopeii * wavelength for wavelength in wavelengths])
				block[sample] = block[sample] - line

		# normalize
		mean = block.mean(axis=0)
		deviation = block.std(axis=0)
		block = (block - mean) / deviation

		# get eigenvectors and values
		covariance = numpy.cov(block, ddof=1, rowvar=False)
		eigenvalues, eigenvectors = numpy.linalg.eig(covariance)
		summation = eigenvalues.sum()

		# make plot for spectra
		pyplot.clf()
		pyplot.title('NO2 spectrum, {}, {}, [{}, {}]\n{}'.format(self.date, label, mirror, track, tag))
		pyplot.xlabel('wavelength')
		pyplot.ylabel('reflectance')
		pyplot.xlim(420, 440)
		pyplot.plot(wavelengths, nitrogen, 'bo-')

		# plot regreesion
		points = [wavelengths[0], wavelengths[-1]]
		regression = [intercept + slope * point for point in points]
		pyplot.plot(points, regression, 'r-')

		# save plot
		destination = '{}/nitrogen_spectrum_{}.png'.format(folder, tag)
		self._print('saving {}'.format(destination))
		pyplot.savefig(destination)
		pyplot.clf()

		# make plot for eigenvectors
		pyplot.clf()
		pyplot.title('NO2 first 5 weighted eigenvectors\n{}'.format(tag))
		pyplot.xlabel('wavelength')
		pyplot.ylabel('eigenvector * eigenvalue')

		# for each eigenvector
		for index in range(5):

			# plot the product
			pyplot.plot(wavelengths, eigenvalues[index] * eigenvectors[:, index], label=str(index))

		# set legend
		pyplot.legend()

		# save plot
		destination = '{}/eigenvectors_{}.png'.format(folder, tag)
		self._print('saving {}'.format(destination))
		pyplot.savefig(destination)
		pyplot.clf()

		# make plot for eigenvalues
		pyplot.clf()
		pyplot.title('NO2 eigenvalues\n{}'.format(tag))
		pyplot.xlabel('eigenvalue number')
		pyplot.ylabel('eigenvalue')
		pyplot.plot(range(eigenvalues.shape[0]), eigenvalues, 'bo-')

		# save plot
		destination = '{}/eigenvalues_{}.png'.format(folder, tag)
		self._print('saving {}'.format(destination))
		pyplot.savefig(destination)
		pyplot.clf()

		# make plot for PCAs
		pyplot.clf()
		pyplot.title('NO2 first 5 eigenvectors\n{}'.format(tag))
		pyplot.xlabel('wavelength')
		pyplot.ylabel('eigenvector')

		# for each eigenvector
		for index in range(5):

			# plot the product
			pyplot.plot(wavelengths, eigenvectors[:, index], label=str(index))

		# set legend
		pyplot.legend()

		# save plot
		destination = '{}/components_{}.png'.format(folder, tag)
		self._print('saving {}'.format(destination))
		pyplot.savefig(destination)
		pyplot.clf()

		return None

	def validate(self, scan=99, apply=False, verify=True, platform=None, large=False, give=False):
		"""Produce plots of one scan to validate the NN model.

		Arguments:
			scan: int, scan number
			apply: boolean, apply selected indices?
			verify: boolean, make verifiation plots?
			platform: str, name of satellite
			large: boolean, make single panel plots?
			give: boolean, give matrix data as output?

		Returns:
			None
		"""

		# set default platform
		platform = platform or 'TROPOMI'

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folders
		self._make('{}/validation'.format(folder))
		self._make('{}/presentation'.format(folder))

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = ([(self.month, self.day)], (scan, scan + 1))
		matrix, features, data, constraints = self.materialize(*parameters, features=features)

		# construct valid data
		valids = self._constrain(data, constraints)

		# if applying sampled indices:
		if apply:

			# get selected indices
			indices = details['samples/selected_indices']

			# get fields with same shape as truth and apply indices
			fields = [field for field, array in valids.items() if array.shape[0] == truth.shape[0]]
			valids.update({field: valids[field][indices] for field in fields})

			# apply indices to inputs and truth
			matrix = matrix[indices]

		# make predictions
		prediction, deviation, truth, score = self.predict(model, matrix, details)

		# print r^2 score
		self._print('score: {}'.format(score))

		# plot model errors
		self.diagnose(truth, prediction, valids, folder)

		# if making verification plot
		if verify:

			# create plot destination
			formats = (folder, self.date, int(data['time'][0][0]), self._pad(scan), self.instances)
			destination = '{}/validation/Nitro_verify_{}_{}_{}_{}.png'.format(*formats)

			# verify with plot
			parameters = (destination, truth, prediction, valids)
			options = {'platform': platform, 'large': large}
			self.verify(*parameters, **options)

		# # if making presentation plots
		# if present:
		#
		# 	# create plot destination
		# 	formats = (folder, self.date, data['time'][0][0], instances)
		# 	destination = '{}/presentation/Nitro_present_{}_{}_{}.png'.format(*formats)
		#
		# 	# verify with plot
		# 	parameters = (destination, truth, prediction, deviation, valids, data, instances)
		# 	self.present(*parameters, chart=True)

		# default package to None
		package = None

		# if giving output
		if give:

			# construct package
			package = {'model': model, 'details': details}
			package.update({'matrix': matrix, 'features': features})
			package.update({'data': data, 'constraints': constraints})
			package.update({'truth': truth, 'prediction': prediction, 'deviation': deviation})
			package.update({'score': score})

		return package

	def verify(self, destination, truth, prediction, data, samples=100, platform=None, large=False):
		"""Plot a validation sample.

		Arguments:
			destination: str, destination filepath
			truth: ground truth observations
			prediction: NN predictions
			data: dict of numpy arrays
			samples: int, largest number of scatterplot samples
			platform: name of satellite
			large: boolean, make large vesion of plots?

		Returns:
			None
		"""

		# set default platform
		platform = platform or 'TROPOMI'

		# begin status
		self._stamp('plotting {}...'.format(destination), initial=True)

		# erase preevious copy
		self._clean(destination, force=True)

		# calculate r2 score
		score = r2_score(truth, prediction)

		# calculate regression line
		slope, intercept, _, _, _ = scipy.stats.linregress(truth, prediction)

		# create titles
		date = str(data['date'][0][0])
		time = str(data['time'][0][0])
		date = '{}m{}{}'.format(date[:4], date[4:6], date[6:8])
		time = '{}:{} CST'.format(int(time[:2]) - 6, time[2:4])

		# calculate differences
		difference = prediction - truth

		# set scaling bracket
		bracket = self.limits['scale']

		# calculate sample bins
		bins = 1000
		options = {'statistic': 'count', 'range': (bracket, bracket), 'bins': bins}
		counts, edge, edgeii, number = scipy.stats.binned_statistic_2d(truth, prediction, None, **options)
		truths = numpy.array([(one + two) / 2 for one, two in zip(edge[:-1], edge[1:])])
		predictions = numpy.array([(one + two) / 2 for one, two in zip(edgeii[:-1], edgeii[1:])])
		mesh, meshii = numpy.meshgrid(truths, predictions, indexing='ij')

		# apply mask
		mask = counts > 0
		counts = counts[mask]
		mesh = mesh[mask]
		meshii = meshii[mask]

		# clip counters to samples
		counts = numpy.where(counts > samples, samples, counts)

		# order counters
		order = numpy.argsort(counts)
		counts = counts[order]
		mesh = mesh[order]
		meshii = meshii[order]

		# get minimum and maximum tracer values
		minimum = min([truth.min(), prediction.min()])
		maximum = max([truth.max(), prediction.max()])

		# set percent bracket
		absolute = max([abs(numpy.percentile(difference, 2)), abs(numpy.percentile(difference, 98))])

		# set bounds
		bounds = [(-125, -60), (15, 55)]
		scale = (minimum, maximum)
		scale = self.limits['scale']
		scaleii = (-absolute, absolute)

		# begin graphs
		graphs = []

		# get data
		latitude = data['latitude']
		longitude = data['longitude']
		corners = data['latitude_bounds']
		cornersii = data['longitude_bounds']

		# construct GPP truth plot
		title = '{} {} {} {}\n'.format(self.target, platform, date, time)
		unit = self.unit
		labels = ['', '']
		options = {'polygons': True, 'corners': [corners, cornersii]}
		graph = self._graph(longitude, latitude, truth, bounds, scale, 'viridis', title, unit, labels, **options)
		graphs.append(graph)

		# construct GPP prediction plot
		title = '{} Prediction {} {}\n'.format(self.target, date, time)
		unit = self.unit
		labels = ['', '']
		options = {'polygons': True, 'corners': [corners, cornersii]}
		graph = self._graph(longitude, latitude, prediction, bounds, scale, 'viridis', title, unit, labels, **options)
		graphs.append(graph)

		# construct samples title depending on instances
		title = 'Prediction vs {} ( 1 instance )\n( R^2 = {} )'.format(platform, self._round(score, 2))
		if self.instances > 1:

			# add instances
			formats = (self.instances, self._round(score, 2))
			title = 'Prediction vs {} ( avg of {} instances )\n( R^2 = {} )'.format(platform, *formats)

		# construct samples graph
		unit = 'samples'
		labels = ['{} truth'.format(self.target), '{} prediction'.format(self.target)]
		boundsii = [self.limits['scale'], self.limits['scale']]
		scaleiii = (1, samples)
		points = [0, truth.max()]
		regression = [intercept + point * slope for point in points]
		line = (points, points, 'k-')
		lineii = (points, regression, 'k--')
		lines = [line, lineii]
		# lines = [line]
		options = {'globe': False, 'lines': lines, 'logarithm': True, 'pixel': 0.01, 'polygons': False}
		graph = self._graph(mesh, meshii, counts, boundsii, scaleiii, 'plasma', title, unit, labels, **options)
		graphs.append(graph)

		# construct percent difference plot
		title = 'Difference between Model and {}\n'.format(platform)
		unit = self.unit
		labels = ['', '']
		options = {'polygons': True, 'corners': [corners, cornersii]}
		graph = self._graph(longitude, latitude, difference, bounds, scaleii, 'coolwarm', title, unit, labels, **options)
		graphs.append(graph)

		# create plot
		self._paint(destination, graphs)

		# if large plots:
		if large:

			# for each graph
			for index, graph in enumerate(graphs):

				# plot each one
				self._paint(destination.replace('.png', '_{}.png'.format(index)), [graph], size=(16, 16))

		return None

	def visualize(self, scan=99, variables=None):
		"""Visualize the dataset with PCA.

		Arguments:
			scan: int, scan number
			variables: list of str, the variables to plot

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make shapely folder
		folderii = '{}/visualization'.format(folder)
		self._make(folderii)

		# get training matrix for particular scan
		features = details['features'].tolist()
		parameters = [(self.month, self.day)], (scan, scan + 1)
		matrix, features, data, constraints = self.materialize(*parameters, features=features)

		# make predictions
		prediction, _, truth, score = self.predict(model, matrix, details)
		prediction = prediction.reshape(-1, 1)

		# apply constraints
		data = self._constrain(data, constraints)

		# get scaling information
		minimum = details['scaling/input_min']
		maximum = details['scaling/input_max']
		scale = maximum - minimum

		# remove truth and apply scaling
		truth = matrix[:, -1]
		inputs = (matrix[:, :-1] - minimum) / scale

		# default bounds
		bounds = None
		boundsii = None

		# try to
		try:

			# get pca components already stored
			hydra = Hydra('{}/pca'.format(folder))
			hydra.ingest()
			coefficients = hydra.grab('2d_pca')
			bounds = (hydra.grab('minimum'), hydra.grab('maximum'))
			boundsii = (hydra.grab('minimumii'), hydra.grab('maximumii'))

		# unless not made
		except IndexError:

			# fit PCA on inputs and get coefficients
			decomposer = PCA(n_components=2)
			decomposer.fit(inputs)
			coefficients = decomposer.components_.transpose(1, 0)

		# get representation
		representation = numpy.dot(inputs, coefficients)

		# make labels
		labels = self._represent(coefficients, features)

		# if no set variables
		if not variables:

			# set heatmap variables
			variables = ['tropomi_vertical_column_no2', 'solar_zenith_angle', 'cloud_fraction']
			variables += ['land_type', 'latitude', 'longitude', 'tropomi_quality_assurance']
			variables += ['viewing_zenith_angle', 'tempo_vertical_column_no2']

		# set color scales
		scales = {'tropomi_vertical_column_no2': self.limits['scale']}
		scales.update({'prediction': self.limits['scale']})

		# add predictions
		data['prediction'] = prediction
		variables = variables + ['prediction']

		# for each variable
		for variable in variables:

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8,8))

			# Sample data
			abscissa = representation[:, 0]
			ordinate = representation[:, 1]
			colors = data[variable]

			# normalize colors
			normal = matplotlib.colors.Normalize(vmin=colors.min(), vmax=colors.max(), clip=True)

			# check for scale
			scale = scales.get(variable, None)

			# if scale available
			if scale:

				# clip colors
				normal = matplotlib.colors.Normalize(vmin=scale[0], vmax=scale[1], clip=True)

				# clip colors
				colors = numpy.where(colors <= scale[0], scale[0], colors)
				colors = numpy.where(colors >= scale[1], scale[1], colors)

			# create scatter plot
			pyplot.scatter(abscissa, ordinate, c=colors, cmap='viridis', norm=normal, s=0.05)  # Use 'c' for colors and 'cmap' for colormap
			pyplot.colorbar(label=variable)  # Add a colorbar to indicate the scale
			pyplot.xlabel('PCA 0 ( {} )'.format(labels[0]))
			pyplot.ylabel('PCA 1 ( {} )'.format(labels[1]))
			pyplot.title('PCA components vs {}\nscan {}, {}, {:.3f}'.format(variable, scan, self.date, score))

			# if bounds are set
			if bounds:

				# set limits
				pyplot.xlim(*bounds)
				pyplot.ylim(*boundsii)

			# save
			pyplot.savefig('{}/pca_{}_{}.png'.format(folderii, self.date, variable))
			pyplot.clf()

		return None

	def wave(self, scan=10, granule=7, parallel=31, meridian=-105):
		"""Make a plot of wavelengths to verify smoothing.

		Arguments:
			scan: int, scan number
			granule int, granule number
			parallel: float, the closest latitude
			meridian: float, the closest longitude

		Returns:
			None
		"""

		# retrieve model and scaling details
		model, details, folder = self._retrieve()
		self._print('model: {}'.format(folder))

		# make folders
		folderii = '{}/wave'.format(folder)
		self._make(folderii)

		# get data
		radiances = Hydra('{}/TEMPO_RAD_L1/{}'.format(self.sink, self.stub), show=False)
		irradiances = Hydra('{}/TEMPO_IRR_L1/{}'.format(self.sink, self.stub), show=False)
		nitrogens = Hydra('{}/Nitro_Inputs/{}'.format(self.sink, self.stub), show=False)
		components = Hydra('{}/Nitro_PCs'.format(self.sink), show=False)
		productivity = Hydra('{}/GPP_Inputs/{}'.format(self.sink, self.stub), show=False)

		# make label
		label = self._label(scan, granule)

		# ingest the files
		radiances.ingest(label)
		irradiances.ingest()
		nitrogens.ingest(label)
		productivity.ingest(label)
		components.ingest()

		# get radiance data
		latitude = radiances.grab('latitude')
		longitude = radiances.grab('longitude')
		zenith = radiances.grab('solar_zenith_angle')
		view = radiances.grab('viewing_zenith_angle')
		azimuth = radiances.grab('solar_azimuth_angle')
		azimuthii = radiances.grab('viewing_azimuth_angle')
		high = radiances.grab('band_290_490/radiance')
		shorts = radiances.grab('band_290_490/nominal_wavelength')

		# get irradiance data
		highii = irradiances.grab('band_290_490/radiance').squeeze()
		shortsii = irradiances.grab('band_290_490/nominal_wavelength').squeeze()

		# get pca coefficents
		coefficients = components.grab('no2_pca_coefficients')
		wavelengths = components.grab('wavelength')

		# get scanline closest to latitude
		mirror, track = self._pin([parallel, meridian], [latitude, longitude])[0]

		# get decomposition and recompose
		smooth = productivity.grab('smoothed_reflectances')[mirror, track]
		wavelengthsii = productivity.grab('reflectance_wavelengths')
		decomposition = nitrogens.grab('no2_reflectance_pcas')[mirror, track]
		composition = numpy.dot(coefficients, decomposition.reshape(-1, 1))

		print('decomposition: {}'.format(decomposition.shape))
		print('coefficients: {}'.format(coefficients.shape))
		print('composition: {}'.format(composition.shape))

		# subset radiance and irradiance
		high = high[mirror, track]
		shorts = shorts[track]
		highii = highii[track]
		shortsii = shortsii[track]

		# set gridding attributes
		nodes = 5
		width = 2
		widthii = 1 / nodes

		# construct wavelength grid
		grid = numpy.array(range(self.limits['high'][0], self.limits['high'][1] + 1))

		# construct finely spaced grids ( 0.2 nm )
		fine = numpy.array(range((self.limits['high'][0] - 1) * nodes, self.limits['high'][1] * nodes + 1)) / nodes

		# construct boxcar
		boxcar = numpy.ones(int(width / widthii)) / int(width / widthii)

		# create masks for valid data
		mask = self._mask(highii)

		# interpolate irradiance onto radiance wavelengths
		options = {'kind': 'linear', 'fill_value': 'extrapolate', 'bounds_error': False}
		interpolator = scipy.interpolate.interp1d(shortsii[mask], highii[mask], **options)
		irradiance = interpolator(shorts)

		# divide by irradiance
		ratio = high / irradiance

		print('high')
		print(high[35:100])
		print('irr')
		print(irradiance[35:100])
		print('ratio')
		print(ratio[35:100])

		# get mask for valid data
		mask = self._mask(ratio)

		# interpolate to fine grid
		interpolator = scipy.interpolate.interp1d(shorts[mask], ratio[mask], **options)
		interpolation = interpolator(fine)

		# convolve onto course grid
		convolution = scipy.signal.convolve(interpolation, boxcar, mode='same')

		# get indices of wavelengths that also in the grid
		indices = numpy.where(numpy.isin(fine, grid))
		reflectance = convolution[indices]

		# normalize by zenith angle
		cosine = reflectance / numpy.cos(numpy.radians(zenith[mirror, track]))

		print(fine.shape, ratio.shape)
		print(reflectance.shape, grid.shape)

		# begin plot
		pyplot.clf()
		pyplot.figure(figsize=(8, 8))

		# add text
		formats = (self.date, label, mirror, track, self._orient(parallel), self._orient(meridian, east=True))
		pyplot.title('Smoothed reflectance for {}, {}, {}, {}, ( {}, {} )'.format(*formats))
		pyplot.xlabel('wavelength')
		pyplot.ylabel('reflectance'.format(self.target, self.unit))

		# # set bounds
		# pyplot.xlim(*bounds)
		# pyplot.ylim(*self.limits['scale'])

		# plot reflectance
		pyplot.plot(fine, interpolation, 'b-', linewidth=1, label='unsmoothed')

		# plot smoothed reflectance
		pyplot.plot(grid, reflectance, 'g-', linewidth=1, label='smoothed')

		# plot zenith adjusted reflectance
		pyplot.plot(grid, cosine, 'r-', linewidth=1, label='zenith adjusted')

		# # plot smoothed
		# pyplot.plot(wavelengthsii, smooth, 'm-', linewidth=1, label='no2 smoothed')
		#
		# plot recomposition
		pyplot.plot(wavelengths, composition, 'k-', linewidth=1, label='pca recomposition')

		# add legend
		pyplot.legend(loc='upper left')

		# save
		pyplot.savefig('{}/Reflectance_{}_{}_{}_{}_{}_{}.png'.format(folderii, *formats))
		pyplot.clf()

		# tranpose coefficients to get coponent vectors
		vectors = coefficients.transpose(1, 0)

		# for each vector
		for index, vector in enumerate(vectors[:10]):

			# begin plot
			pyplot.clf()
			pyplot.figure(figsize=(8, 8))

			# add text
			formats = (index, self.date, label, mirror, track, self._orient(parallel), self._orient(meridian, east=True))
			pyplot.title('PCA Vector: {} for {}, {}, {}, {}, ( {}, {} )'.format(*formats))
			pyplot.xlabel('wavelength')
			pyplot.ylabel('pca coefficient'.format(self.target, self.unit))

			# plot vector
			pyplot.plot(wavelengths, vector, 'b-', linewidth=1, label='pca vector')

			# add legend
			pyplot.legend(loc='upper left')

			# save
			pyplot.savefig('{}/Vector_{}_{}_{}_{}_{}_{}_{}.png'.format(folderii, *formats))
			pyplot.clf()

		return None


# if arguments are given
arguments = sys.argv[1:]
if len(arguments) > 0:

	# separate options and arguments
	options = [argument for argument in arguments if argument.startswith('-')]
	arguments = [argument for argument in arguments if not argument.startswith('-')]

	# check for prepare
	if '--prepare' in options:

		# unpack arguments
		sink, year, month, day, scan, scanii, granule, granuleii = arguments

		# set up instance
		nightshade = Nightshade(int(year), int(month), int(day), sink)

		# prepare data
		nightshade.prepare((int(scan), int(scanii)), (int(granule), int(granuleii)))

	# check for prepare
	if '--train' in options:

		# unpack arguments
		sink, tag = arguments

		# set up instance
		nightshade = Nightshade(sink=sink, tag=tag)

		# prepare data
		nightshade.train()