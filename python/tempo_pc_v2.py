import gc
import glob
from sklearn.preprocessing import StandardScaler
import xarray as xr
import h5py
import numpy as np
from scipy.interpolate import interpn
import math
import re
import time


files = glob.glob('TEMPO_L1b_Smooth/2024/02/*/*LandOcean*_V2.nc')
np.random.shuffle(files)


start = True
for filename in files:


    print(filename)
    #Reading TEMPO smoothed reflectances 
    try:
        f = h5py.File(filename,'r')
    except:
        time.sleep(10)
        f = h5py.File(filename,'r')

    time_stamp = re.findall('[0-9]{8}T[0-9]{4}',filename)[0]
    year = int(time_stamp[0:4])
    month = int(time_stamp[4:6])
    day = int(time_stamp[6:8])

    refl= f['smoothed_reflectance'][:,:]
    nmirror, nstep, nwaves = refl.shape 
    
    refl = refl.reshape(nmirror*nstep,nwaves)
    refl[refl ==-999] = np.nan
    
    water_mask = f['water_mask'][:].flatten()



    waves = f['smoothed_wavelength'][:]

    sza = f['solar_zenith_angle'][:].flatten()
    vza = f['viewing_zenith_angle'][:].flatten()

    f.close()
    del f
    gc.collect()

    #Reading cloud file to use for screening on CRF 
    cld_file = glob.glob('TEMPO_CLDO4_L2/'+str(year)+'/'+str(month).zfill(2)+'/'+str(day).zfill(2)+'/*'+time_stamp+'*nc')
    if len(cld_file) == 0:
        print('No cloud')
        continue

    #Lines 1780:1798 excluded due to some calibration problems 
    f = h5py.File(cld_file[0],'r')
    crf = f['/product/CloudRadianceFraction466'][:]
    crf[:,1780:1798] = np.nan
    crf = crf.flatten()
    
    crf[crf ==-2**100] = np.nan

    f.close()
    del f
    gc.collect()
    
    #Screening data to avoid high angles, for some applications, also screen on thick cloud when retrieval
    #doesn't work well for such conditions (ozone and GPP work ok for thick clouds) 
    good_refl =  (~np.isnan(np.min(refl,axis=1))) & (vza < 80) & (sza < 80) 
    refl = refl[good_refl]

    #Concatenating reflectances to store in array 
    if start:
        tot_refl = np.array(refl[0::5,:])
        start = False
    else:
        tot_refl = np.array(np.vstack((tot_refl,refl[0::5,:])),dtype=np.float32)
    del refl, crf, sza, vza,good_refl
    print(np.shape(tot_refl))
    gc.collect()




nsamples, nwaves = tot_refl.shape

#Performing PCA decomposition and storing eigenvector. Note that 371-375nm is excluded 
#due to noticable calibration issues 
all_waves = (waves  > 350) & ((waves < 371) | (waves > 375))
covmatrix = np.dot(tot_refl[:,all_waves].T ,tot_refl[:,all_waves])
_, pca_coeff = np.linalg.eigh(covmatrix)
pca_coeff = pca_coeff[:,::-1]


f = h5py.File('TEMPO_PCs/TEMPO_PCA_Coefficients_Feb2024_LandOcean_V2_AllWaves.h5','w')
f.create_dataset('PCA_Coeff',data=pca_coeff)
f.close()

stop

uv_waves = (waves > 350)  & (waves < 500) & ((waves < 371) | (waves > 375))
covmatrix = np.dot(tot_refl[:,uv_waves].T ,tot_refl[:,uv_waves])
_, pca_coeff = np.linalg.eigh(covmatrix)
pca_coeff = pca_coeff[:,::-1]


f = h5py.File('TEMPO_PCs/TEMPO_PCA_Coefficients_FebMar2024_OceanOnly_V2_UVBlue.h5','w')
f.create_dataset('PCA_Coeff',data=pca_coeff)
f.close()


green_waves = waves > 500 
covmatrix = np.dot(tot_refl[:,green_waves].T ,tot_refl[:,green_waves])
_, pca_coeff = np.linalg.eigh(covmatrix)
pca_coeff = pca_coeff[:,::-1]


f = h5py.File('TEMPO_PCs/TEMPO_PCA_Coefficients_FebMar2024_OceanOnly_V2_GreenRed.h5','w')
f.create_dataset('PCA_Coeff',data=pca_coeff)
f.close()


uv_waves = (waves > 350)  & ((waves < 371) | ((waves > 375) & (waves < 500)) | (waves > 675))
covmatrix = np.dot(tot_refl[:,uv_waves].T ,tot_refl[:,uv_waves])
_, pca_coeff = np.linalg.eigh(covmatrix)
pca_coeff = pca_coeff[:,::-1]


f = h5py.File('TEMPO_PCs/TEMPO_PCA_Coefficients_FebMar2024_OceanOnly_V2_TROPOMIWaves.h5','w')
f.create_dataset('PCA_Coeff',data=pca_coeff)
f.close()


gc.collect()

uv_waves = (waves > 400)  & (waves < 600)
covmatrix = np.dot(tot_refl[:,uv_waves].T ,tot_refl[:,uv_waves])
_, pca_coeff = np.linalg.eigh(covmatrix)
pca_coeff = pca_coeff[:,::-1]


f = h5py.File('TEMPO_PCs/TEMPO_PCA_Coefficients_FebMar2024_OceanOnly_V2_MODISWaves.h5','w')
f.create_dataset('PCA_Coeff',data=pca_coeff)
f.close()


gc.collect()


