#!/bin/bash
#SBATCH -A s2255     # project account code
#SBATCH --job-name=tempo_nitro      # Job name
#SBATCH --output=output_%j.log        # Output file (%j will be replaced with the job ID)
#SBATCH --error=error_%j.log          # Error file (%j will be replaced with the job ID)
#SBATCH --nodes=4              # Request 2 nodes
#SBATCH --ntasks=4                    # Number of tasks (typically 1 for single-node Python)
#SBATCH --cpus-per-task=8             # Number of CPU cores per task
#SBATCH --mem=128G                      # Memory per node (e.g., 4GB)
#SBATCH --time=12:00:00               # Time limit (e.g., 1 hour)

# Load any necessary modules (e.g., Python)
module load python/3.11                # Adjust the Python version to your needs

# Activate your virtual environment, if using one
source ../../eleven/bin/activate

# Run your Python script
python ../nightshades.py .. gridii --train    # Replace with the actual Python script name