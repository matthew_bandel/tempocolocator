#!/bin/bash
#SBATCH -A s2255     # project account code
#SBATCH --job-name=tempo_gpp      # Job name
#SBATCH --output=output_%j.log        # Output file (%j will be replaced with the job ID)
#SBATCH --error=error_%j.log          # Error file (%j will be replaced with the job ID)
#SBATCH --nodes=4              # Request 2 nodes
#SBATCH --ntasks=4                    # Number of tasks (typically 1 for single-node Python)
#SBATCH --cpus-per-task=8             # Number of CPU cores per task
#SBATCH --mem=32G                      # Memory per node (e.g., 4GB)
#SBATCH --time=04:00:00               # Time limit (e.g., 1 hour)

# Load any necessary modules (e.g., Python)
module load python/3.11                # Adjust the Python version to your needs

# Activate your virtual environment, if using one
source ../../eleven/bin/activate

# Run your Python script
python ../squalls.py ../ 2024 7 4 1 19 balance --mosaic    # Replace with the actual Python script name